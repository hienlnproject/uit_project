﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HmsService.ViewModels
{
    public class OrderOnlineViewModel
    {
        //Thông tin khách hàng
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public DateTime OrderDate { get; set; }
        public string Note { get; set; }
        public string Email { get; set; }

        public int DeliveryType { get; set; }
        public int DeliveryPayment { get; set; }
        public bool VATInvoice { get; set; }

        public string CheckInPerson { get; set; }
        public double Fee { get; set; }

        public string Voucher { get; set; }
        //Đơn hàng
        //public string OrderId { get; set; }
        public string DeliveryAddress { get; set; }
        public string StoreName { get; set; }
        public double DeliveryCost { get; set; }
        public string RequiredCustomer { get; set; }
        public int AreaId { get; set; }
        public List<ProductList> ProductList { get; set; }
    }
    public class ProductList
    {
        //sản phẩm yêu cầu
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double FinalAmount { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public int? TempId { get; set; }
        public Nullable<int> OrderDetailPromotionMappingId { get; set; }
        public Nullable<int> OrderPromotionMappingId { get; set; }
        public List<ProductWithExtras> ProductWithExtras { get; set; }
    }
    public class ProductWithExtras
    {
        //sản phẩm đi kèm
        public int ProductExtraId { get; set; }
        public string ProductCode { get; set; }
        public int ParentId { get; set; }
        public double Price { get; set; }
        public double FinalAmount { get; set; }
        public int Quantity { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public string Style { get; set; }
        public string ImageUrl { get; set; }
    }
}
