﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public partial class CostViewModel
    {
        public IEnumerable<Cost> costList;
        public IEnumerable<CostCategoryViewModel> Categories;

    }
    public class CostEditViewModel
    {
        public IEnumerable<CostCategoryViewModel> Categories;
        public int CatId { get; set; }
        public string CostDescription { get; set; }
        public DateTime CostDate { get; set; }
        public double Amount { get; set; }
        public string PaidPerson { get; set; }
        public int CostType { get; set; }
        public string LoggedPerson { get; set; }
    }
    public class CostOverViewModel
    {
        public double AmountReceipt { get; set; }

        public double AmountSpend { get; set; }

        public int Status { get; set; }

        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
