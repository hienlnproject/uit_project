﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class MonthReportViewModel
    {
        public int Month { get; set; }

        public string MonthName { get; set; }

        public int TotalOrder { get; set; }
        public double TotalFinalAmount { get; set; }
        public double TotalDiscount { get; set; }

        public double TakeAway { get; set; }
        public double AtStore { get; set; }
        public double Delivery { get; set; }
    }
}
