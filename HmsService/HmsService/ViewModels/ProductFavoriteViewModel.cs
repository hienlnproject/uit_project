﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class ProductFavoriteViewModel
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double DiscountPercent { get; set; }
        public string DisplayPrice { get; set; }
        public double ProductPrice { get; set; }
        public string ProductImg { get; set; }
        public int ProductID { get; set; }
        public string SeoName { get; set; }



    }
}
