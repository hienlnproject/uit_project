﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IBlogCategoryService
    {
        Task<BlogCategory> GetActiveByTitleAsync(string title);
        IQueryable<BlogCategory> GetByStoreId(int storeId);
        IQueryable<BlogCategory> GetActiveByStoreId(int storeId);
    }
    public partial class BlogCategoryService
    {
        public async Task<BlogCategory> GetActiveByTitleAsync(string title)
        {
            var blogCategory = await this.Get(m => title.Equals(m.Title))
                .FirstOrDefaultAsync();
            return blogCategory;
        }

        public IQueryable<BlogCategory> GetByStoreId(int storeId)
        {
            var blogCategories = this.Get(m => m.StoreId == storeId);
            return blogCategories;
        }

        public IQueryable<BlogCategory> GetActiveByStoreId(int storeId)
        {
            var blogCategories = this.Get(m => m.StoreId == storeId && m.IsActive == true);
            return blogCategories;
        }
    }
}
