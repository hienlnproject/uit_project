﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface ICostService
    {
        #region CostManage
        IQueryable<Cost> GetCostCategoriesByRangeTime(DateTime startDate, DateTime endDate, int storeId);
        IQueryable<Cost> GetCostCategoriesByRangeTimeAndStatus(DateTime startDate, DateTime endDate, int storeId, int status);
        IQueryable<Cost> GetCostCategoriesByRangeTimeBrandIdAndStatus(DateTime startDate, DateTime endDate, int brandId, int status);
        IQueryable<Cost> GetCostbyCostCategoryandBrand(int brandId, int categoryId);
        #endregion

        IEnumerable<Cost> GetCosts();
    }
    public partial class CostService
    {
        #region CostManage
        public IQueryable<Cost> GetCostCategoriesByRangeTime(DateTime startDate, DateTime endDate, int storeId)
        {
            return this.Get(q => q.CostDate >= startDate && q.CostDate <= endDate && q.StoreId == storeId);
        }
        public IQueryable<Cost> GetCostCategoriesByRangeTimeAndStatus(DateTime startDate, DateTime endDate, int storeId, int status)
        {
            return this.Get(q => q.CostDate >= startDate && q.CostDate <= endDate && q.StoreId == storeId && q.CostType == status);
        }
        public IQueryable<Cost> GetCostCategoriesByRangeTimeBrandIdAndStatus(DateTime startDate, DateTime endDate, int brandId, int status)
        {
            if (status != 3) { 
            return this.Get(q => q.CostDate >= startDate && q.CostDate <= endDate && q.Store.BrandId == brandId && q.CostType == status);
            }
            else
            {
                return this.Get(q => q.CostDate >= startDate && q.CostDate <= endDate && q.Store.BrandId == brandId);
            }
        }

        public IQueryable<Cost> GetCostbyCostCategoryandBrand(int brandId, int categoryId)
        {
                return this.Get(q => q.Store.BrandId == brandId && q.CostCategory.CatID == categoryId);
        }
        #endregion

        public IEnumerable<Cost> GetCosts()
        {
            var cost = this.GetActive();
            return cost;
        }
    }
}
