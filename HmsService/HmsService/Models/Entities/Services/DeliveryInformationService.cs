﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IDeliveryInformationService
    {
        IQueryable<DeliveryInformation> GetAllDeliveryInforByUserId(string userId);
    }
    public partial class DeliveryInformationService
    {
        public IQueryable<DeliveryInformation> GetAllDeliveryInforByUserId(string userId)
        {
            return this.GetActive(q => q.UserId.Equals(userId));
        }
    }
}
