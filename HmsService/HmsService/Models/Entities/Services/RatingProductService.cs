﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IRatingProductService
    {
        bool CreateRating(RatingProduct rating);
    }
    public partial class RatingProductService
    {
        public bool CreateRating(RatingProduct rating)
        {
            try
            {
                this.Create(rating);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
