﻿using SkyWeb.DatVM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmsService.Sdk;
using AutoMapper.QueryableExtensions;
using SkyWeb.DatVM.Mvc.Autofac;
using HmsService.ViewModels;

namespace HmsService.Models.Entities.Services
{
    public partial interface IOrderService
    {
        IQueryable<Order> GetAdminByStoreWithFilter(int storeId,
            string keyword, KeyValuePair<string, bool> sortKeyAsc);
        OrderCustomEntity GetOrderById(int storeId, int id);
        Order GetOrderByIdAndBrandId(int brandId, int id);
        Task<bool> CreateOrderAsync(Order entity);
        IQueryable<Order> GetOrderByBrand(int brandId);
        Task<Order> GetOrderByIdAsync(int id);
        IQueryable<Order> GetOrderByUserName(string username);
        bool CreateOrder(Order order);
        int CreateOrderReturnId(OrderViewModel order);
        #region SystemReport
        IQueryable<Order> GetAllOrderByDate(DateTime from, DateTime to, int brandId);
        IQueryable<Order> GetAllFinishedOrderByDate(DateTime from, DateTime to, int brandId);
        IQueryable<Order> GetAllFinishedOrderByDateaAndCustomer(DateTime from, DateTime to, int brandId, int customerID);
        IQueryable<Order> GetAllOrderByDateWithCard(DateTime from, DateTime to, int brandId);
        IQueryable<Order> GetAllOrderByDateByPromotionId(DateTime from, DateTime to, int brandId, int PromotionId);
        IEnumerable<Order> GetAllOrderByDateByPromotionIdAndStoreId(DateTime from, DateTime to, int brandId, int promotionId, int storeId);
        IQueryable<Order> GetAllOrderByDateDifferentFromPromotionId(DateTime from, DateTime to, int brandId, int PromotionId);
        IQueryable<Order> GetTodayOrders(int brandId);
        IQueryable<Order> GetAllOrderByDate(DateTime from, DateTime to);
        IQueryable<Order> GetRentsByTimeRange(int storeID, DateTime startTime, DateTime endTime);
        IQueryable<Order> GetRentsByTimeRange(int storeID, DateTime startTime, DateTime endTime, int brandId);
        IQueryable<Order> GetRentsFinishByTimeRange(int storeID, DateTime startTime, DateTime endTime);
        IQueryable<Order> GetRentsByTime(DateTime startTime, DateTime endTime);
        IQueryable<Order> GetAllOrderByDateByPromotionIdOrderDetail(DateTime from, DateTime to, int brandId, int PromotionId);
        IQueryable<Order> GetRentsByTimeRangeAndHour(int storeID, DateTime startTime, DateTime endTime, int brandId, int startHour, int endHour);

        #endregion
        IEnumerable<Order> GetOrders();
        IQueryable<Order> GetOrdersByTimeRange(int storeID, DateTime startTime, DateTime endTime);
        IEnumerable<Order> GetAllHotelOrdersByCheckOutDate(DateTime from, DateTime to, int storeId);

        IQueryable<Order> GetOrdersByTimeRange(DateTime startTime, DateTime endTime, int brandId, int storeId);
        IQueryable<OrderForDashBoard> GetOrderForDashboard(DateTime startTime, DateTime endTime, int brandId, int storeId);
        Task UpdateOrderDeliveryAsync(int orderId, List<ProductWithExtras> listExtra);
        Task UpdateExtraParentId(int orderId, List<OrderDetailViewModel> listExtra);
        Task UpdateTmpDetailIdOrderDetailAsync(int orderId);
        IQueryable<int> GetRentIdsByTimeRange(int storeID, DateTime startTime, DateTime endTime, int brandId);

        IQueryable<Order> FixDB1();
        IQueryable<Order> FixDB2();
    }

    public partial class OrderService
    {
        private readonly OrderDetailService detailService;
        public IQueryable<int> GetRentIdsByTimeRange(int storeID, DateTime startTime, DateTime endTime, int brandId)
        {
            return GetRentsByTimeRange(storeID, startTime, endTime, brandId).Select(q => q.RentID);
        }
        public IEnumerable<Order> GetAllHotelOrdersByCheckOutDate(DateTime from, DateTime to, int storeId)
        {
            var rents = this
                .Get(r => ((r.CheckOutDate >= from
                                && r.CheckOutDate <= to && r.StoreID == storeId)));
            return rents;
        }

        public IQueryable<Order> GetOrdersByTimeRange(int storeID, DateTime startTime, DateTime endTime)
        {
            var rents = this.Get(r => (r.StoreID == storeID)
                              && ((r.CheckInDate >= startTime
                                   && r.CheckInDate <= endTime)));
            return rents;
        }
        public IQueryable<Order> GetAdminByStoreWithFilter(int storeId,
            string keyword, KeyValuePair<string, bool> sortKeyAsc)
        {

            var entities = this.GetActive(q =>
                q.StoreID == storeId &&
                (keyword == null
                || (!q.CustomerID.HasValue || q.Customer.Name.Contains(keyword))));

            entities = entities.OrderByDescending(q => q.CheckInDate);

            return entities;
        }

        public OrderCustomEntity GetOrderById(int storeId, int id)
        {
            var order = this.GetActive(a => a.StoreID == storeId && a.RentID == id).FirstOrDefault();
            return new OrderCustomEntity
            {
                Order = order,
                OrderDetails = order.OrderDetails,
                Customer = order.Customer
            };
        }

        public IQueryable<Order> GetOrderByUserName(string username)
        {
            var result = this.GetActive(q => q.CheckInPerson.Equals(username));
            return result;
        }

        public Order GetOrderByIdAndBrandId(int brandId, int id)
        {
            var result = this.Get(q => q.Store.BrandId == brandId && q.RentID == id).FirstOrDefault();
            return result;
        }
        public int CreateOrderReturnId(OrderViewModel order)
        {
            try
            {
                var entity = order.ToEntity();
                this.Create(entity);
                return entity.RentID;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        public bool CreateOrder(Order order)
        {
            try
            {
                this.Create(order);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> CreateOrderAsync(Order entity)
        {
            try
            {
                await this.CreateAsync(entity);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IQueryable<Order> GetOrderByBrand(int brandId)
        {
            var result = this.Get(q => q.Store.BrandId == brandId && q.OrderType == (int)OrderTypeEnum.Delivery &&
                (q.DeliveryAddress != null || (q.InvoiceID != null)));
            return result;
        }

        public async Task<Order> GetOrderByIdAsync(int id)
        {
            var result = await this.GetAsync(id);
            return result;
        }

        #region SystemReport
        public IQueryable<Order> GetAllOrderByDate(DateTime from, DateTime to)
        {
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to)));
            return result;
        }

        public IQueryable<Order> GetTodayOrders(int brandId)
        {
            var startDate = DateTime.Now.GetStartOfDate();
            var endDate = DateTime.Now.GetEndOfDate();
            var result = this.Get(r => (r.CheckInDate >= startDate && r.CheckInDate <= endDate && r.Store.BrandId == brandId));
            return result;
        }

        public IQueryable<Order> GetRentsByTimeRangeAndHour(int storeID, DateTime startTime, DateTime endTime, int brandId, int startHour, int endHour)
        {
            var result = Get(q => q.CheckInDate >= startTime && q.CheckInDate <= endTime && q.CheckinHour >= startHour && q.CheckinHour < endHour);
            if(storeID == 0)
            {
                return result.Where(q => q.Store.BrandId == brandId && q.Store.isAvailable.Value);
            }
            else
            {
                return result.Where(q => q.StoreID == storeID);
            }
        }

        public IQueryable<Order> GetAllOrderByDate(DateTime from, DateTime to, int brandId)
        {
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId));
            return result;
        }
        public IQueryable<Order> GetAllFinishedOrderByDate(DateTime from, DateTime to, int brandId)
        {
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId && r.OrderStatus==2));
            return result;
        }
        public IQueryable<Order> GetAllFinishedOrderByDateaAndCustomer(DateTime from, DateTime to, int brandId, int customerID)
        {
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId && r.OrderStatus == 2 && r.CustomerID == customerID));
            return result;
        }
        public IQueryable<Order> GetAllOrderByDateWithCard(DateTime from, DateTime to, int brandId)
        {
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId && r.Att1!=null));
            return result;
        }
        public IQueryable<Order> GetAllOrderByDateByPromotionId(DateTime from, DateTime to, int brandId, int PromotionId)
        {
            OrderPromotionMappingApi opApi = new OrderPromotionMappingApi();
            var opMapping = opApi.BaseService.GetActive(q => q.PromotionId == PromotionId).Select(q => q.OrderId);
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId) && opMapping.Contains(r.RentID));


            return result;
        }
        public IEnumerable<Order> GetAllOrderByDateByPromotionIdAndStoreId(DateTime from, DateTime to, int brandId, int promotionId, int storeId)
        {
            var opService = DependencyUtils.Resolve<IOrderPromotionMappingService>();
            var opMapping = opService.GetActive(q => q.PromotionId == promotionId).Select(q => new {
                OrderId= q.OrderId
            }
            );
            
            var resultTemp = this.Get(r => ((r.CheckInDate >= from
                             && r.CheckInDate <= to) && r.Store.BrandId == brandId && r.Att1!=null))
                             .Select(r => new { Att1 = r.Att1, RentID = r.RentID, TotalAmount = r.TotalAmount, FinalAmount = r.FinalAmount, Discount = r.Discount, StoreID = r.StoreID })
                             .Join(opMapping, p=> p.RentID, q => q.OrderId, (p,q) =>p);

            var result = from b in resultTemp.AsEnumerable()
                           select new Order
                           {
                               Att1 = b.Att1,
                               RentID = b.RentID,
                               TotalAmount = b.TotalAmount,
                               FinalAmount = b.FinalAmount,
                               Discount = b.Discount,
                               StoreID = b.StoreID
                           };
            if (storeId > 0)
            {
                result = result.Where(r => r.StoreID == storeId);
            }

            return result;
        }
        public IQueryable<Order> GetAllOrderByDateDifferentFromPromotionId(DateTime from, DateTime to, int brandId, int PromotionId)
        {
            OrderPromotionMappingApi opApi = new OrderPromotionMappingApi();
            var opMapping = opApi.BaseService.GetActive(q => q.PromotionId == PromotionId).Select(q => q.OrderId);
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId) && !opMapping.Contains(r.RentID));


            return result;
        }

        public IQueryable<Order> GetRentsByTimeRange(int storeID, DateTime startTime, DateTime endTime)
        {
            var result = this.Get(r => r.CheckInDate >= startTime
                                   && r.CheckInDate <= endTime
                                   && r.StoreID == storeID );           
            return result;
        }

        public IQueryable<Order> GetOrdersByTimeRange(DateTime startTime, DateTime endTime, int brandId, int storeId)
        {
            var result = this.Get(r => (r.CheckInDate >= startTime
                                   && r.CheckInDate <= endTime));
            if (storeId != 0)
            {
                result = result.Where(r => r.StoreID == storeId);
            }
            else
            {
                result = result.Where(r => r.Store.BrandId == brandId && r.Store.isAvailable.Value);
            }
            return result;
        }


        public IQueryable<OrderForDashBoard> GetOrderForDashboard(DateTime startTime, DateTime endTime, int brandId, int storeId)
        {
            var orders = GetOrdersByTimeRange(startTime, endTime, brandId, storeId);
                return orders.Select(q => new OrderForDashBoard
                {
                    StoreID = q.StoreID.Value,
                    StoreName = q.Store.Name,
                    StoreAbbr = q.Store.ShortName,
                    CheckInPerson = q.CheckInPerson,
                    Date = q.CheckInDate.Value,
                    CheckInHour = q.CheckinHour.Value,
                    TotalAmount = q.TotalAmount,
                    FinalAmount = q.FinalAmount,
                    Discount = q.Discount,
                    DiscountOrderDetail = q.DiscountOrderDetail,
                    TotalOrderDetails = q.OrderDetails.Count,
                    OrderType = q.OrderType,
                    OrderStatus = q.OrderStatus
                });
        }

        public IQueryable<Order> GetRentsByTimeRange(int storeID, DateTime startTime, DateTime endTime, int brandId)
        {
            var result = this.Get(r => ((r.CheckInDate >= startTime
                                   && r.CheckInDate <= endTime)) && r.Store.BrandId == brandId);
            if (storeID != 0)
            {
                result = result.Where(q => q.StoreID == storeID);
            }
            else
            {
                result = result.Where(q => (q.StoreID.HasValue ? q.Store.BrandId == brandId : false));
            }
            return result;
        }
        public IQueryable<Order> GetRentsByTime(DateTime startTime, DateTime endTime)
        {
            var orderDetails = detailService.GetOrderDetailsByTimeRange(startTime, endTime, 13)
                .Select(q => q.RentID)
                .Distinct();
            return this.Get(q => q.CheckInDate >= startTime && q.CheckInDate <= endTime && orderDetails.Contains(q.RentID));
        }
        public IQueryable<Order> GetRentsFinishByTimeRange(int storeID, DateTime startTime, DateTime endTime)
        {
            var result = this.Get(r => (r.CheckInDate >= startTime
                                   && r.CheckInDate <= endTime) && r.OrderStatus == (int)OrderStatusEnum.Finish);
            if (storeID != 0)
            {
                result = result.Where(r => r.StoreID == storeID);
            }
            return result;
        }

        public IQueryable<Order> GetAllOrderByDateByPromotionIdOrderDetail(DateTime from, DateTime to, int brandId, int PromotionId)
        {
            OrderDetailPromotionMappingApi opApi = new OrderDetailPromotionMappingApi();
            OrderDetailApi odApi = new OrderDetailApi();
            var mapping = opApi.BaseService.GetActive(q => q.PromotionId == PromotionId).Select(q => q.OrderDetailId);
            var orderDetails = odApi.BaseService.GetActive(q => q.OrderDate >= from && q.OrderDate <= to && mapping.Contains(q.OrderDetailID)).Select(q => q.RentID);
            var result = this.Get(r => ((r.CheckInDate >= from
                               && r.CheckInDate <= to) && r.Store.BrandId == brandId) && orderDetails.Contains(r.RentID));


            return result;
        }

        #endregion

        public IEnumerable<Order> GetOrders()
        {
            return this.GetActive();
        }

        public async Task UpdateOrderDeliveryAsync(int orderId, List<ProductWithExtras> listExtra)
        {
            var productService = DependencyUtils.Resolve<IProductService>();
            var order = await this.GetAsync(orderId);
            var listOrderDetail = order.OrderDetails;
            foreach (var item in listExtra)
            {
                var parentOrderDetail = listOrderDetail.Where(q => q.TmpDetailId == item.ParentId).FirstOrDefault();
                if (parentOrderDetail != null)
                {
                    Product product = await productService.GetAsync(item.ProductExtraId);
                    var orderDetail = new OrderDetail();
                    orderDetail.ProductID = item.ProductExtraId;
                    orderDetail.Quantity = item.Quantity * parentOrderDetail.Quantity;
                    orderDetail.TotalAmount = product.Price * item.Quantity;
                    orderDetail.OrderDate = DateTime.Now;
                    orderDetail.Status = 0; // Tình trạng normal của món hàng từ website xuống POS, (thay đổi giá trị 'Hủy' tại POS)
                    orderDetail.IsAddition = false;
                    orderDetail.UnitPrice = product.Price;
                    orderDetail.Discount = (product.Price - ((product.Price != 0 && product.DiscountPrice != 0) ? product.DiscountPrice : product.Price)) * item.Quantity;
                    orderDetail.StoreId = order.StoreID;
                    orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
                    orderDetail.ParentId = parentOrderDetail.OrderDetailID;
                    order.TotalAmount += orderDetail.TotalAmount;
                    order.DiscountOrderDetail += orderDetail.Discount;
                    orderDetail.ProductType = (int)ProductTypeEnum.Embroidery;
                    listOrderDetail.Add(orderDetail);
                }
            }
            await this.UpdateAsync(order);
        }
        public async Task UpdateExtraParentId(int orderId, List<OrderDetailViewModel> listExtra)
        {
            var productService = DependencyUtils.Resolve<IProductService>();
            var order = await this.GetAsync(orderId);
            var listOrderDetail = order.OrderDetails;
            foreach (var item in listExtra)
            {
                var orderDetail = listOrderDetail.Where(q => q.TmpDetailId == item.TmpDetailId && q.ProductID == item.ProductID).FirstOrDefault();
                var parentOrderDetail = listOrderDetail.Where(q => q.TmpDetailId == item.TmpDetailId && q.ProductID != item.ProductID).FirstOrDefault();
                if (parentOrderDetail != null && orderDetail != null)
                {
                    orderDetail.ParentId = parentOrderDetail.OrderDetailID;
                }
            }
            await this.UpdateAsync(order);
        }
        public async Task UpdateTmpDetailIdOrderDetailAsync(int orderId)
        {
            var order = await this.GetAsync(orderId);
            foreach (var item in order.OrderDetails)
            {
                item.TmpDetailId = item.OrderDetailID;
            }
            await this.UpdateAsync(order);
        }

        public IQueryable<Order> FixDB1()
        {
            return this.Get(q => q.Att1 != null
                    && (q.Discount > 0 || q.DiscountOrderDetail > 0)
                    && (q.CustomerID == null || q.CustomerID == 0));
        }

        public IQueryable<Order> FixDB2()
        {
            return this.Get(q => q.Att1 == null && q.CustomerID != null && q.CustomerID > 0);
        }
    }

    public class OrderCustomEntity : IEntity
    {
        public Order Order { get; set; }
        public IEnumerable<OrderDetail> OrderDetails { get; set; }
        public Customer Customer { get; set; }
    }

    public class OrderForDashBoard
    {
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string StoreAbbr { get; set; }
        public string CheckInPerson { get; set; }
        public int CheckInHour { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<double> FinalAmount { get; set; }
        public Nullable<double> Discount { get; set; }
        public Nullable<double> DiscountOrderDetail { get; set; }
        public int TotalOrderDetails { get; set; }
        public Nullable<int> OrderType { get; set; }
        public Nullable<int> OrderStatus { get; set; }

    }
}
