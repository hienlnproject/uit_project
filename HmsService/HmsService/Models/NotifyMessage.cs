﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models
{
    public class SkyPlusMessage
    {
        public int StoreId { get; set; }
        public string StoreName { get; set; }

    }

    public class NotifyMessage : SkyPlusMessage
    {
        public int NotifyType { get; set; }

        /// <summary>
        /// COntent = Null: call update all data change
        /// Order: Content = OrderId
        /// </summary>
        public string Content { get; set; }


    }

    public class NotifyOrder : SkyPlusMessage
    {
        public int OrderId { get; set; }
        public string Content { get; set; }
        public int NotifyType { get; set; }

    }


    public enum NotifyMessageType
    {
        NoThing = 0,
        AccountChange = 1,
        ProductChange = 2,
        CategoryChange = 3,
        OrderChange = 4,
        PromotionChange = 5,
    }
}
