﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using AutoMapper.QueryableExtensions;

namespace HmsService.Sdk
{
    public partial class DeliveryInformationApi
    {
        public IQueryable<DeliveryInformation> GetAllDeliveryInforByUserId(string userId)
        {
            return this.BaseService.GetAllDeliveryInforByUserId(userId);
        }
        public IEnumerable<DeliveryInformationViewModel> GetAllDeliveryInforByUserIdToViewModel(string userId)
        {
            return this.BaseService.GetAllDeliveryInforByUserId(userId).ProjectTo<DeliveryInformationViewModel>(this.AutoMapperConfig).ToList();
        }
    }
}
