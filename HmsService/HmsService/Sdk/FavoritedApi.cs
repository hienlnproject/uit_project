﻿using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class FavoritedApi
    {
        public IEnumerable<ProductViewModel> GetActiveFavoriteProduct(string userId)
        {
            return this.BaseService.Get(q => q.UserID == userId && q.Active == true && (q.FavoriteStt != null && q.FavoriteStt == true))
                .Select(q => q.Product).ProjectTo<ProductViewModel>(this.AutoMapperConfig)
                .ToList();
        }
    }
}
