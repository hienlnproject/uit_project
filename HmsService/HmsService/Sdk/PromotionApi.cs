﻿using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmsService.Models.Entities.Services;

namespace HmsService.Sdk
{
    public partial class PromotionApi
    {
        public IQueryable<PromotionViewModel> GetActivePromotion(int brandId)
        {
            return this.BaseService.GetActivePromotion()
                .Where(q => q.BrandId == brandId)
                .ProjectTo<PromotionViewModel>(this.AutoMapperConfig);
        }

        public IQueryable<PromotionViewModel> GetAllPromotion(int brandId)
        {
            return this.BaseService.Get()
                .Where(q => q.BrandId == brandId)
                .ProjectTo<PromotionViewModel>(this.AutoMapperConfig);
        }


        public PromotionViewModel GetPromotionById(int id)
        {
            var promotion = this.BaseService.GetPromotionById(id);
            if (promotion == null)
            {
                return null;
            }
            else
            {
                return new PromotionViewModel(promotion);
            }
        }

        public Promotion GetPromotionByIdReturnEntity(int id)
        {
            var promotion = this.BaseService.GetPromotionById(id);
            if (promotion == null)
            {
                return null;
            }
            else
            {
                return promotion;
            }
        }

        public async Task EditPromotionIsApplyOnce(int id, bool isApplyOnce)
        {
            var promotion = this.BaseService.GetPromotionById(id);
            promotion.IsApplyOnce = isApplyOnce;

            await this.BaseService.UpdateAsync(promotion);
        }

        public async Task CreatePromotion(Promotion entity)
        {
            await this.BaseService.CreatePromotion(entity);
        }

        public async Task<int> CreatePromotionApplyforStore(PromotionEditViewModel model, int[] storeIds)
        {
            try
            {
                var entity = model.ToEntity();
                await this.BaseService.CreatePromotion(entity);
                if (storeIds == null)
                {
                    return entity.PromotionID;
                }
                PromotionStoreMappingApi api = new PromotionStoreMappingApi();
                foreach (var item in storeIds)
                {
                    PromotionStoreMappingViewModel mappingModel = new PromotionStoreMappingViewModel();
                    mappingModel.PromotionId = entity.PromotionID;
                    mappingModel.StoreId = item;
                    mappingModel.Active = true;
                    await api.CreateAsync(mappingModel);
                }
                return entity.PromotionID;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task UpdatePromotion(Promotion entity)
        {
            await this.BaseService.UpdatePromotion(entity);
        }

        public void Update(PromotionViewModel model)
        {
            var entity = this.BaseService.Get(model.PromotionID);
            model.CopyToEntity(entity);
            this.BaseService.Update(entity);
        }

        public async Task DeactivePromotionAsync(int id)
        {
            var promotion = this.BaseService.GetPromotionById(id);
            promotion.Active = false;
            await this.BaseService.DeactivePromotionAsync(promotion);
        }
        public Promotion GetByIdBrandId(int id, int brandId)
        {
            var promotion = this.BaseService.GetByIdBrandId(id, brandId);
            if (promotion == null)
            {
                return null;
            }
            else
            {
                return promotion;
            }
        }
        public Promotion GetByPromoCode(string code)
        {
            var promotion = this.BaseService.GetByPromoCode(code);
            if (promotion == null)
            {
                return null;
            }
            else
            {
                return promotion;
            }
        }

        public IQueryable<Promotion> GetPromotionByBrandId(int brandId)
        {
            var result = this.BaseService.GetPromotionByIdTime(brandId);
            return result;
        }

        public IQueryable<Promotion> GetPromotionNotVoucherByStoreId(int storeId)
        {
            var result = this.BaseService.GetPromotionByStoreId(storeId).Where(q => q.IsVoucher == false);
            return result;
        }

        public IQueryable<PromotionEditViewModel> GetPromotionByIdTime(int brandId)
        {
            var result = this.BaseService.GetPromotionByIdTime(brandId).ProjectTo<PromotionEditViewModel>(this.AutoMapperConfig);
            return result;
        }

    }
}
