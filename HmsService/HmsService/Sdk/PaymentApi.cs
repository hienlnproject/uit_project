﻿using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class PaymentApi
    {
        public void CreatePayment(Payment payment)
        {
            this.BaseService.Create(payment);
        }

        public IEnumerable<PaymentViewModel> GetStorePaymentInDateRange(int storeId, DateTime startDate, DateTime endDate, int brandId)
        {
            return BaseService.GetStorePaymentByTimeRange(storeId, brandId, startDate, endDate)
                .ProjectTo<PaymentViewModel>(AutoMapperConfig).ToList();
        }

        public IEnumerable<Payment> GetPaymentByOrder(int orderId)
        {
            return this.BaseService.Get(q => q.ToRentID == orderId).Distinct();
        }
        /// <summary>
        /// Get payment by time range
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="brandId"></param>
        /// <returns>Entity Payment</returns>
        public IQueryable<Payment> GetStorePaymentByDateRange(int storeId, DateTime startDate, DateTime endDate, int brandId)
        {
            return BaseService.GetStorePaymentByTimeRange(storeId, brandId, startDate, endDate);
        }



        public IQueryable<PaymentViewModel> GetQueryStorePaymentInDateRange(int storeId, DateTime startDate, DateTime endDate, int brandId)
        {
            return BaseService.GetStorePaymentByTimeRange(storeId, brandId, startDate, endDate)
                .ProjectTo<PaymentViewModel>(AutoMapperConfig);
        }

     
        public IQueryable<Payment> GetEntityStorePaymentInDateRange(int storeId, DateTime startDate, DateTime endDate, int brandId)
        {
            return BaseService.GetStorePaymentByTimeRange(storeId, brandId, startDate, endDate);
        }

        public IQueryable<Payment> GetStorePaymentByTimeRangeAndType(int storeId, DateTime startDate, DateTime endDate, int brandId, int type)
        {
            return BaseService.GetStorePaymentByTimeRangeAndType(storeId, brandId, startDate, endDate, type);
        }

        public IQueryable<PaymentViewModel> GetStorePaymentVMInDateRange(int storeId, DateTime startDate, DateTime endDate, int brandId)
        {
            //return BaseService.GetStorePaymentVMByTimeRange(storeId, brandId, startDate, endDate);
            var entity = BaseService.GetPaymentByTimeRange(startDate, endDate);        
            var orderApi = new OrderApi();
            var orders = orderApi.GetAllOrderIdsByTimeRange(storeId, brandId, startDate, endDate);
            return entity.Join(orders, q => q.ToRentID, p => p, (q, p) => new PaymentViewModel
            {
                PaymentID = q.PaymentID,
                ToRentID = q.ToRentID,
                CardCode = q.CardCode,
                Amount = q.Amount,
                CurrencyCode = q.CurrencyCode,
                FCAmount = q.FCAmount,
                Notes = q.Notes,
                PayTime = q.PayTime,
                RealAmount = q.RealAmount,
                Status = q.Status,
                Type = q.Type
            });
        }
    }
}
