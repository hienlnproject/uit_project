﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmsService.ViewModels;
using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;

namespace HmsService.Sdk
{
    public partial class CostApi
    {
        #region CostManage
        public IEnumerable<CostViewModel> GetCostCategoriesByRangeTime(DateTime startDate, DateTime endDate, int storeId)
        {
            var costCategories = this.BaseService.GetCostCategoriesByRangeTime(startDate, endDate, storeId)
                .ProjectTo<CostViewModel>(this.AutoMapperConfig)
                .ToList();
            return costCategories;
        }
        public IEnumerable<CostViewModel> GetCostCategoriesByRangeTimeAndStatus(DateTime startDate, DateTime endDate, int storeId, int status)
        {
            var costCategories = this.BaseService.GetCostCategoriesByRangeTimeAndStatus(startDate, endDate, storeId, status)
                .ProjectTo<CostViewModel>(this.AutoMapperConfig)
                .ToList();
            return costCategories;
        }

        public IEnumerable<CostViewModel> GetCostCategoriesByRangeTimeBrandIdAndStatus(DateTime startDate, DateTime endDate, int brandId, int status)
        {
            var costCategories = this.BaseService.GetCostCategoriesByRangeTimeBrandIdAndStatus(startDate, endDate, brandId, status)
                .ProjectTo<CostViewModel>(this.AutoMapperConfig)
                .ToList();
            return costCategories;
        }

        public IQueryable<Cost> GetCostbyCostCategoryandBrand(int brandId, int categoryId)
        {
            return this.BaseService.GetCostbyCostCategoryandBrand(brandId, categoryId);
        }

        public IQueryable<Cost> getAllCost()
        {
            var listCost = this.BaseService.Get();
            return listCost;
        }
        
        #endregion
    }
}
