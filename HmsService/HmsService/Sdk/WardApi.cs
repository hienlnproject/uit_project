﻿using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class WardApi
    {
        public IEnumerable<WardViewModel> GetAllWard()
        {
            return this.Get().ToList();
        }
    }
}
