﻿CREATE TABLE [dbo].[DateProduct] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [Date]             DATETIME       NOT NULL,
    [ProductId]        INT            NOT NULL,
    [StoreID]          INT            NOT NULL,
    [Quantity]         INT            NOT NULL,
    [TotalAmount]      INT            NOT NULL,
    [Discount]         INT            NOT NULL,
    [FinalAmount]      INT            NOT NULL,
    [CategoryId_]      INT            NULL,
    [ProductName_]     NVARCHAR (100) NULL,
    [Time0Quantity]    INT            NULL,
    [Time1Quantity]    INT            NULL,
    [Time2Quantity]    INT            NULL,
    [Time3Quantity]    INT            NULL,
    [Time4Quantity]    INT            NULL,
    [Time5Quantity]    INT            NULL,
    [Time6Quantity]    INT            NULL,
    [Time7Quantity]    INT            NULL,
    [Time8Quantity]    INT            NULL,
    [Time9Quantity]    INT            NULL,
    [Time10Quantity]   INT            NULL,
    [Time11Quantity]   INT            NULL,
    [Time12Quantity]   INT            NULL,
    [Time13Quantity]   INT            NULL,
    [Time14Quantity]   INT            NULL,
    [Time15Quantity]   INT            NULL,
    [Time16Quantity]   INT            NULL,
    [Time17Quantity]   INT            NULL,
    [Time18Quantity]   INT            NULL,
    [Time19Quantity]   INT            NULL,
    [Time20Quantity]   INT            NULL,
    [Time21Quantity]   INT            NULL,
    [Time22Quantity]   INT            NULL,
    [Time23Quantity]   INT            NULL,
    [OrderQuantity]    INT            NULL,
    [QuantityAtStore]  INT            NULL,
    [QuantityTakeAway] INT            NULL,
    [QuantityDelivery] INT            NULL,
    CONSTRAINT [PK_DateProduct] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_DateProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductID]),
    CONSTRAINT [FK_DateProduct_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([ID])
);

