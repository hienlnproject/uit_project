﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyAdmin.Manage.Models
{
    public static class FeedStatus
    {
        public const int Showing = 1;
        public const int Warning = 2;
        public const int Approved = 3;
        public const int Hiding = 4;
        public const int Deleted = 5;
    }
}