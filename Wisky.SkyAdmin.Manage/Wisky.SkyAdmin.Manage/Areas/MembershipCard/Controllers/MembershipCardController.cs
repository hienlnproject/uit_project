﻿using HmsService.Models;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LinqToExcel;
using Wisky.SkyAdmin.Manage.Controllers;
using System.Data.Entity.Validation;
using CsvHelper;
using System.Globalization;
using HmsService.Models.Entities;
using Newtonsoft.Json;

namespace Wisky.SkyAdmin.Manage.Areas.MembershipCard.Controllers
{
    public class MembershipCardController : DomainBasedController
    {
        #region CreateMembershipCardStore
        public ActionResult CreateMembershipCardStore()
        {
            var brandId = int.Parse(RouteData.Values["brandId"]?.ToString());
            var customerTypeApi = new CustomerTypeApi();
            var model = new MembershipCardEditViewModel();
            model.Customer = new CustomerEditViewModel();
            model.Customer.AvailableType = customerTypeApi.GetAllCustomerTypes(brandId).Select(
                q => new SelectListItem
                {
                    Value = q.ID.ToString(),
                    Text = q.CustomerType1,
                    Selected = false
                });

            model.Customer.AvailableGender = new List<SelectListItem>();
            model.Customer.AvailableGender.Add(new SelectListItem()
            {
                Text = "Nam",
                Value = "true",
            });
            model.Customer.AvailableGender.Add(new SelectListItem()
            {
                Text = "Nữ",
                Value = "false",
            });
            return View(model);
        }
        public JsonResult CheckMembershipCard(string membershipCardCode)
        {
            MembershipCardApi cardApi = new MembershipCardApi();
            CustomerApi cusApi = new CustomerApi();
            bool accept = false;
            var card = cardApi.GetMembershipCardByCode(membershipCardCode);

            if (card != null)
            {
                if (card.CustomerId != null)
                {
                    var cus = cusApi.GetCustomerById((int)card.CustomerId);
                    accept = true;
                    if (card.Status == (int)MembershipStatusEnum.Suspensed)
                    {
                        return Json(new
                        {
                            checkCard = accept,
                            customerId = card.CustomerId,
                            customerName = cus.Name,
                            message = "Thẻ đã bị khóa"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new
                    {
                        checkCard = accept,
                        customerId = card.CustomerId,
                        customerName = cus.Name,
                        message = "Thẻ đã có chủ sở hữu"
                    }, JsonRequestBehavior.AllowGet);
                }
                else if (card.Active == false)
                {
                    accept = true;
                    return Json(new
                    {
                        customerId = -1,
                        customerName = "",
                        checkCard = accept,
                        message = "Thẻ đã bị hủy ! xin vui lòng liên hệ admin"
                    }, JsonRequestBehavior.AllowGet);
                }
                else if (card.Status == (int)MembershipStatusEnum.Suspensed)
                {
                    accept = true;
                    return Json(new
                    {
                        customerId = -1,
                        customerName = "",
                        checkCard = accept,
                        message = "Thẻ đã bị khóa"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                accept = false;
            }
            return Json(new
            {
                checkCard = accept,
                message = "Thẻ Trống ! Có thể sử dụng"
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetListCardSample(int brandId)
        {
            var membershipCardApi = new MembershipCardApi();
            var listCard = membershipCardApi.GetMembershipCardSample(brandId);
            var result = listCard.Select(q => new
            {
                Id = q.Id,
                Code = q.MembershipCardCode
            });
            return Json(new
            {
                listCard = result,
                success = true
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadAccountSample(string cardId)
        {
            try
            {
                var cardSampleId = int.Parse(cardId);
                var accountApi = new AccountApi();
                var listAccountSample = accountApi.GetAccountByMembershipId(cardSampleId).AsEnumerable();
                var result = listAccountSample.Select(q => new IConvertible[] {
                    q.Type,
                    q.Balance,
                    q.ProductCode
                });

                return Json(new
                {
                    success = true,
                    listAccount = result
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Có lỗi trong quá trình xử lý, vui lòng liên hệ Admin"
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CheckCustomer(int brandId , string keyData)
        {
            try
            {
                var customerApi = new CustomerApi();
                var customer = customerApi.GetCustomerByEmailOrPhone(keyData,brandId);
                if (customer != null)
                {
                    var type = "";
                    var typeApi = new CustomerTypeApi();
                    if (customer.CustomerTypeId != null)
                    {
                        type = typeApi.GetCustomerTypeById(customer.CustomerTypeId.Value).CustomerType1;
                    }
                    string gender = "Không xác định";
                    string date = "";
                    if (customer.Gender != null)
                    {
                        if (customer.Gender == true)
                            gender = "Nam";
                        else gender = "Nữ";
                    }
                    if (customer.BirthDay.HasValue)
                    {
                        date = customer.BirthDay.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        date = "N/A";
                    }
                    var cmnd = customer.IDCard;
                    var disctrict = customer.District;
                    var city = customer.City;
                    return Json(new
                    {
                        success = true,
                        customer = new
                        {
                            id = customer.CustomerID,
                            Name = customer.Name,
                            Gender = gender,
                            Type = type,
                            Address = customer.Address,
                            Phone = customer.Phone,
                            Email = customer.Email ?? "N/A",
                            Date = date,
                            CMND = cmnd,
                            District = disctrict,
                            City = city,
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Không tìm thấy thông tin khách hàng"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    success = false,
                    message = "Có lỗi trong quá trình xử lý ! xin vui lòng liên hệ admin"
                }, JsonRequestBehavior.AllowGet);
            }


        }

        public async Task<ActionResult> CreateMembershipCardAtStore(int storeId, string cardCode, string customerId, string cardSampleId)
        {
            try
            {
                var membershipCardApi = new MembershipCardApi();
                var customerApi = new CustomerApi();
                var accountApi = new AccountApi();
                bool success = true;
                string message = "Tạo thẻ thành công ! Liên hệ Admin hệ thống để kích hoạt thẻ";
                var membershipCard = membershipCardApi.GetMembershipCardByCode(cardCode);
                if (membershipCard != null)
                {
                    success = false;
                    message = "Thẻ đã tồn tại trong hệ thống";
                }
                else
                {
                    if (Utils.IsDigitsOnly(customerId))
                    {
                        var customer = customerApi.GetCustomerByID((int.Parse(customerId)));
                        if (customer != null)
                        {
                            if (Utils.IsDigitsOnly(cardSampleId))
                            {
                                var cardSample = membershipCardApi.GetMembershipCardBaseOnID(int.Parse(cardSampleId));
                                if (cardSample != null)
                                {
                                    int cardNewId = await membershipCardApi.AddMembershipCardBaseOnCardSample(cardCode, cardSample, int.Parse(customerId), storeId);

                                    if (cardNewId > 0)
                                    {
                                        var listAccountSample = accountApi.GetAccountsByCardId(int.Parse(cardSampleId));
                                        foreach (var item in listAccountSample)
                                        {
                                            var accountNew = new AccountViewModel()
                                            {
                                                AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                                                AccountName = customer.Name,
                                                BrandId = cardSample.BrandId,
                                                StartDate = Utils.GetCurrentDateTime(),
                                                Active = true,
                                                MembershipCardId = cardNewId,
                                                Balance = item.Balance,
                                                ProductCode = item.ProductCode,
                                                Type = item.Type,
                                                FinishDate = item.FinishDate,
                                                Level_ = item.Level_
                                            };
                                            await accountApi.CreateAsync(accountNew);
                                        }
                                    }
                                    else
                                    {
                                        success = false;
                                        message = "Tạo thẻ không thành công ! Liên hệ Admin để được xử lý";
                                    }

                                }
                                else
                                {
                                    success = false;
                                    message = "Hệ thống phát hiện truy cập không hợp lệ";
                                }
                            }
                            else
                            {
                                success = false;
                                message = "Hệ thống phát hiện truy cập không hợp lệ";
                            }

                        }
                        else
                        {
                            success = false;
                            message = "Hệ thống phát hiện truy cập không hợp lệ";
                        }
                    }
                    else
                    {
                        success = false;
                        message = "Hệ thống phát hiện truy cập không hợp lệ";
                    }

                }
                return Json(new
                {
                    success = success,
                    message = message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = "Có lỗi trong quá trình xử lý ! vui lòng liên hệ admin để được giải quyết"
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CreateCustomer(int brandId, CustomerViewModel model)
        {
            var customerApi = new CustomerApi();
            int id = 0;
            if (String.IsNullOrEmpty(model.Phone))
            {
                return Json(new { success = false, message = "Số điện thoại không được để trống" });
            }
            else
            {
                if (customerApi.GetCustomerByEmailOrPhone(model.Phone, brandId) != null)
                {
                    return Json(new { success = false, message = "SĐT đã tồn tại" });
                }
            }
            if (!String.IsNullOrEmpty(model.Email))
            {
                if (customerApi.GetCustomerByEmail(model.Email, brandId) != null)
                {
                    return Json(new { success = false, message = "Email đã tồn tại" });
                }
            }
            try
            {
                model.BrandId = brandId;
                if (model.birthDayString != null)
                {
                    model.BirthDay = model.birthDayString.ToDateTime();
                }
                id = customerApi.AddCustomer(model);
            }
            catch (Exception e)
            {

                return Json(new { success = false, message = Resources.Message_VI_Resources.CreateCustomerFailed });
            }

            return Json(new
            {
                success = true,
                message = Resources.Message_VI_Resources.CreateCustomerSuccessfully,
                customer = new
                {
                    id = id,
                    name = model.Name,
                    phone = model.Phone
                }
            });
        }
        #endregion
        // get: membershipcard/membershipcard
        public ActionResult index()
        {
            return View();
        }
        #region Load Datatable
        //TODO: MembershipTypeId chưa sửa
        [HttpGet]
        public JsonResult LoadDeactiveMembershipCard(JQueryDataTableParamModel param, int membershipTypeId, int brandId)
        {
            int count = 0;
            int totalRecords = 0;
            int totalDisplayRecords = 0;

            var cusApi = new CustomerApi();
            var membershipCardApi = new MembershipCardApi();
            count = param.iDisplayStart + 1;
            var rs = new object();

            if (membershipTypeId != -1)
            {
                var storeApi = new StoreApi();
                var card = membershipCardApi.GetDeactiveListByBrandAndType(brandId, membershipTypeId)
                                            .Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch) || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower()
                           .Contains(param.sSearch.ToLower()))).OrderByDescending(q => q.CreatedTime);

                rs = searchList.Skip(param.iDisplayStart)
                        .Take(param.iDisplayLength)
                        .ToList()
                        .Select(a => new object[]
                        {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CustomerId,
                                       a.Id,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,
                        });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }
            else
            {
                var storeApi = new StoreApi();
                var card = membershipCardApi.GetDeactiveListByBrandId(brandId).Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch) || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower()
                            .Contains(param.sSearch.ToLower())));
                rs = searchList.OrderByDescending(q => q.CreatedTime)
                        .Skip(param.iDisplayStart)
                        .Take(param.iDisplayLength)
                        .ToList()
                        .Select(a => new object[]
                        {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CustomerId,
                                       a.Id,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,
                                                                            
                        });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = rs
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LoadActiveMembershipCard(JQueryDataTableParamModel param, int membershipTypeId, int brandId)
        {
            int count = 0;
            int totalRecords = 0;
            int totalDisplayRecords = 0;

            var membershipCardApi = new MembershipCardApi();

            count = param.iDisplayStart + 1;
            var rs = new object();
            var storeApi = new StoreApi();
            if (membershipTypeId != -1)
            {
                var card = membershipCardApi.GetActiveListByBrandAndType(brandId, membershipTypeId).Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch)
                        || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower().Contains(param.sSearch.ToLower()))
                        || (!string.IsNullOrEmpty(param.sSearch) && a.Customer.Name.ToLower().Contains(param.sSearch.ToLower())))
                        .OrderByDescending(q => q.CreatedTime);

                rs = searchList.Skip(param.iDisplayStart)
                           .Take(param.iDisplayLength)
                           .ToList().Select(a => new object[]
                           {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       a.Id,
                                       a.CustomerId,
                                       a.Status,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,

                           });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }
            else
            {
                var card = membershipCardApi.GetActiveListByBrandId(brandId).Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch)
                           || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower().Contains(param.sSearch.ToLower()))
                           || (!string.IsNullOrEmpty(param.sSearch) && a.Customer.Name.ToLower().Contains(param.sSearch.ToLower())));
                rs = searchList.OrderByDescending(q => q.CreatedTime)
                           .Skip(param.iDisplayStart)
                           .Take(param.iDisplayLength)
                           .ToList().Select(a => new object[]
                           {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       a.Id,
                                       a.CustomerId,
                                       a.Status,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,

                            });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalDisplayRecords,
                aaData = rs
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LoadCloseMembershipCard(JQueryDataTableParamModel param, int membershipTypeId, int brandId)
        {
            int count = 0;
            int totalRecords = 0;
            int totalDisplayRecords = 0;

            var membershipCardApi = new MembershipCardApi();

            count = param.iDisplayStart + 1;
            var rs = new object();
            var storeApi = new StoreApi();
            if (membershipTypeId != -1)
            {
                var card = membershipCardApi.GetCloseListByBrandAndType(brandId, membershipTypeId).Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch)
                           || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower().Contains(param.sSearch.ToLower()))
                           || (!string.IsNullOrEmpty(param.sSearch) && a.Customer.Name.ToLower().Contains(param.sSearch.ToLower())));
                rs = searchList.OrderByDescending(q => q.CreatedTime)
                           .Skip(param.iDisplayStart)
                           .Take(param.iDisplayLength)
                           .ToList().Select(a => new object[]
                  {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       a.Id,
                                       a.CustomerId,
                                       a.Status,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,

                  });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }
            else
            {
                var card = membershipCardApi.GetCloseListByBrandId(brandId).Where(a => a.IsSample != true);
                var searchList = card.Where(a => string.IsNullOrEmpty(param.sSearch)
                || (!string.IsNullOrEmpty(param.sSearch) && a.MembershipCardCode.ToLower().Contains(param.sSearch.ToLower()))
                || (!string.IsNullOrEmpty(param.sSearch) && a.Customer.Name.ToLower().Contains(param.sSearch.ToLower())));

                rs = searchList.OrderByDescending(q => q.CreatedTime)
                           .Skip(param.iDisplayStart)
                           .Take(param.iDisplayLength)
                           .ToList().Select(a => new object[]
                  {
                                       count++,
                                       string.IsNullOrEmpty(a.MembershipCardCode) ? "---" : a.MembershipCardCode,
                                       (a.CustomerId == null || a.CustomerId <= 0 || string.IsNullOrEmpty(a.Customer.Name)) ? "---" : a.Customer.Name,
                                       a.CreatedTime.ToString("dd/MM/yyyy"),
                                       a.MembershipCardType == null ? "---" : a.MembershipCardType.TypeName,
                                       a.Id,
                                       a.CustomerId,
                                       a.Status,
                                       (a.StoreId == null || a.StoreId.Value == 0 ) ? "Hệ Thống" : storeApi.Get(a.StoreId.Value).Name,
                  });
                totalRecords = card.Count();
                totalDisplayRecords = searchList.Count();
            }

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalDisplayRecords,
                aaData = rs
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        //        #region Create
        public class productListAndMembershipTypeList
        {
            public IEnumerable<Product> productList;
            public IEnumerable<MembershipCardTypeViewModel> membershipTypeList;
            public productListAndMembershipTypeList()
            {
                productList = null;
                membershipTypeList = null;
            }
        }
        public ActionResult CreateMembershipCard(int brandId)
        {
            var productApi = new ProductApi();
            var membershipType = new MembershipCardTypeApi();
            var productList = productApi.GetAllProductsByBrand(brandId);
            var membershipTypeList = membershipType.GetMembershipCardTypeByBrand(brandId);
            var model = new productListAndMembershipTypeList();
            model.productList = productList;
            model.membershipTypeList = membershipTypeList;
            return View(model);
        }
        //        [HttpGet]
        //        public ActionResult Create(MembershipCardEditViewModel model, int brandId)
        //        {
        //            var customerApi = new CustomerApi();
        //            return PartialView("Create", model);

        //            await membershipCardTypeMappingApi.CreateAsync(MembershipCardTypeMapping);
        //        }
        //    }
        //    #endregion

        //    #region createAccount
        //    //Tài khoản thanh toán
        //    if (creditAccountName != null)
        //    {
        //        var Account = new AccountViewModel();
        //        Account.AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff"); //Get AccountCode theo thời gian
        //        Account.AccountName = creditAccountName.Trim();
        //        Account.Level_ = 0; //Tải khoản không có level
        //        Account.StartDate = DateTime.Now;
        //        Account.FinishDate = creditFinishDate.ToDateTime();
        //        Account.Balance = creditBalance; //Số tiền trong tài khoản
        //        Account.MembershipCardId = membershipCardApi.GetMembershipCardByCode(membershipCardId).Id;
        //        Account.BrandId = brandId;
        //        Account.AccountTypeId = 1; //Tài khoản thanh toán
        //        Account.Active = true;

        //        await accountApi.CreateAsync(Account);
        //    }
        //    //Tài khoản sản phẩm
        //    if (giftAccountName != null)
        //    {
        //        var Account = new AccountViewModel();
        //        var productApi = new ProductApi();
        //        Account.AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff");//Get AccountCode theo thời gian
        //        Account.AccountName = giftAccountName.Trim();
        //        Account.Level_ = 0; //Tải khoản không có level
        //        Account.StartDate = DateTime.Now;
        //        Account.FinishDate = giftFinishDate.ToDateTime();
        //        Account.Balance = giftBalance; // Số sản phẩm trong tài khoản
        //        var productcode = productApi.GetProductById((int)giftProductId).Code;
        //        Account.ProductCode = productcode; // Get ProductCode
        //        Account.BrandId = brandId;
        //        Account.AccountTypeId = 2; //Tài khoản sản phẩm
        //        Account.MembershipCardId = membershipCardApi.GetMembershipCardByCode(membershipCardId).Id;
        //        Account.Active = true;

        //        await accountApi.CreateAsync(Account);
        //    }
        //    //Tài khoản tích điểm
        //    if (memberAccountName != null)
        //    {
        //        var Account = new AccountViewModel();
        //        Account.AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff");//Get AccountCode theo thời gian
        //        Account.AccountName = memberAccountName.Trim();
        //        Account.StartDate = DateTime.Now;
        //        Account.FinishDate = memberFinishDate.ToDateTime();
        //        Account.Balance = memberBalance; // Số điểm trong tài khoản
        //        Account.Level_ = (short)memberLevel;
        //        Account.BrandId = brandId;
        //        Account.AccountTypeId = 3; //Tài khoản tích điểm
        //        Account.MembershipCardId = membershipCardApi.GetMembershipCardByCode(membershipCardId).Id;
        //        Account.Active = true;

        //        await accountApi.CreateAsync(Account);
        //    }
        //    #endregion
        //    return RedirectToAction("Index");

        //}

        //private void PrepareCreate(MembershipCardEditViewModel model, int brandId)
        //{
        //    var customerApi = new CustomerApi();
        //    var membershipTypeApi = new MembershipTypeApi();
        //    var listMembershipType = membershipTypeApi.GetMembershipTypeByBrand(brandId);

        //    model.ListTypeMembership = listMembershipType.Select(q => new SelectListItem()
        //    {
        //        Selected = model.MembershipTypeId.Equals(q.Id),
        //        Text = q.TypeName,
        //        Value = q.Id.ToString(),
        //    });

        //    //var customerList = customerApi.GetActive();
        //    var customerList = customerApi.GetCustomersByBrand(brandId);
        //    model.CustomerList = customerList.Select(q => new SelectListItem()
        //    {
        //        Selected = model.CustomerId.Equals(q.CustomerID),
        //        Text = q.Name,
        //        Value = q.CustomerID.ToString(),
        //    });
        //}
        #region Edit
        public ActionResult Edit(int id, int brandId)
        {
            var membershipCardApi = new MembershipCardApi();
            var entity = membershipCardApi.Get(id);
            var model = this.Mapper.Map<MembershipCardEditViewModel>(entity);
            PrepareEdit(model, brandId);
            return PartialView("Edit", model);

        }

        [HttpPost]
        public ActionResult Edit(MembershipCardEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            var membershipCardApi = new MembershipCardApi();
            membershipCardApi.UpdateMembershipCard(model);
            return RedirectToAction("Index");

        }
        private void PrepareEdit(MembershipCardEditViewModel model, int brandId)
        {
            var customerApi = new CustomerApi();
            var membershipTypeApi = new MembershipCardTypeApi();

            var listMembershipType = membershipTypeApi.GetMembershipCardTypeByBrand(brandId);

            model.ListTypeMembership = listMembershipType.Select(q => new SelectListItem()
            {
                Selected = model.MembershipTypeId.Equals(q.Id),
                Text = q.TypeName,
                Value = q.Id.ToString(),
            });

            var customerList = customerApi.GetActive();
            model.CustomerList = customerList.Select(q => new SelectListItem()
            {
                Selected = model.CustomerId.Equals(q.CustomerID),
                Text = q.Name,
                Value = q.CustomerID.ToString(),
            });
        }

        #endregion
        #region Activate Card
        public ActionResult ActivateCard(int id, int brandId)
        {
            var membershipCardApi = new MembershipCardApi();
            var customerTypeApi = new CustomerTypeApi();
            var entity = membershipCardApi.Get(id);
            var model = this.Mapper.Map<MembershipCardEditViewModel>(entity);
            model.Customer = new CustomerEditViewModel();
            model.Customer.AvailableType = customerTypeApi.GetAllCustomerTypes(brandId).Select(
                q => new SelectListItem
                {
                    Value = q.ID.ToString(),
                    Text = q.CustomerType1,
                    Selected = false
                });

            model.Customer.AvailableGender = new List<SelectListItem>();
            model.Customer.AvailableGender.Add(new SelectListItem()
            {
                Text = "Nam",
                Value = "true",
            });
            model.Customer.AvailableGender.Add(new SelectListItem()
            {
                Text = "Nữ",
                Value = "false",
            });

            //PrepareActivateCard(model, brandId, id);
            return PartialView("ActivateCard", model);
        }

        //private void PrepareActivateCard(MembershipCardEditViewModel model, int brandId, int cardId)
        //{
        //    var customerApi = new CustomerApi();
        //    var membershipTypeApi = new MembershipTypeApi();
        //    //var typeId = new MembershipCardTypeMappingApi().Get().Where(a => a.MembershipCardId == cardId).;
        //    //var typeId = new MembershipCardTypeMappingApi().Get(cardId).MembershipTypeId;
        //    var membershipCardService = this.Service<IMembershipCardTypeMappingService>();
        //    var typeList = membershipCardService.Get().Where(a => a.MembershipCardId == cardId).Select(b => new { Name = b.MembershipType.TypeName }).ToArray();
        //    List<string> nameList = new List<string>();
        //    foreach (var item in typeList)
        //    {
        //        nameList.Add(item.Name);
        //    }
        //    var customerList = customerApi.GetCustomersByBrand(brandId);
        //    int result = customerList.Count();
        //    model.CustomerList = customerList.Select(q => new SelectListItem()
        //    {
        //        Selected = model.CustomerId.Equals(q.CustomerID),
        //        Text = q.Name,
        //        Value = q.CustomerID.ToString(),
        //    });

        //    model.ListType = nameList;

        //}

        [HttpPost]
        public async Task<JsonResult> ActiveMembershipCard(string cardCode, int customerId)
        {
            bool check;
            try
            {
                CustomerApi cApi = new CustomerApi();
                MembershipCardApi mcApi = new MembershipCardApi();
                var card = mcApi.GetMembershipCardByCode(cardCode);
                var customer = cApi.GetCustomerById(customerId);
                card.CustomerId = customerId;
                //card.Customer.Name = customer.Name;
                card.Status = (int)MembershipStatusEnum.Active;// Kích hoạt
                MembershipCardViewModel model = new MembershipCardViewModel(card);
                await mcApi.EditAsync(model.Id, model);

                /// Create Transaction
                var accountApi = new AccountApi();
                var listAccount = accountApi.GetAccountsByCardId(model.Id);
                if (listAccount.Count > 0)
                {
                    var account = listAccount.Where(q => q.Active == true && q.Type == (int)AccountTypeEnum.CreditAccount).FirstOrDefault();
                    if (account != null)
                    {
                        if (account.Balance >= 0)
                        {
                            var transaction = new TransactionViewModel()
                            {
                                AccountId = account.AccountID,
                                Date = Utils.GetCurrentDateTime(),
                                StoreId = model.StoreId == null ? 0 : model.StoreId.Value,
                                BrandId = model.BrandId,
                                IsIncreaseTransaction = true,
                                Notes = "ActiveCard",
                                Status = (int)TransactionStatus.Approve,
                                Amount = account.Balance == null ? 0 : account.Balance.Value,
                            };
                            var transactionApi = new TransactionApi();
                            await transactionApi.CreateAsync(transaction);
                            var message = "Passio Coffee" + ". "
                                            + DateTime.Now.ToString("dd/MM/yyyy") + ";" + DateTime.Now.ToString("HH:mm") + ".  "
                                            + "Passio Coffee xin chao: " + customer.Name + "; "
                                            + "He thong Passio da kich hoat TK:" + model.MembershipCardCode+"; "
                                            + "So du tai khoan: " + Utils.ToMoney((double)account.Balance) + "VNĐ";
                            try
                            {
                                Utils.SendSMS(customer.Phone, message, model.BrandId);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }

                check = true;
            }
            catch (Exception)
            {
                check = false;
            }

            return Json(new
            {
                success = check,

            });
        }

        public async Task<bool> unActiveMembershipCard(string cardCode)
        {
            bool check;
            try
            {
                MembershipCardApi mcApi = new MembershipCardApi();
                var card = mcApi.GetMembershipCardByCode(cardCode);
                card.Status = 0;// Tắt kích hoạt
                MembershipCardViewModel model = new MembershipCardViewModel(card);
                await mcApi.EditAsync(model.Id, model);
                check = true;
            }
            catch (Exception)
            {
                check = false;
            }

            return check;
        }
        #endregion

        #region Delete
        public async Task<JsonResult> Delete(int id)
        {
            var MembershipCardApi = new MembershipCardApi();
            var model = MembershipCardApi.Get(id);
            if (model == null)
            {
                return Json(new { success = false });
            }
            try
            {
                await MembershipCardApi.DeleteMembershipCardAsync(model);
            }
            catch (System.Exception e)
            {
                throw e;

            }
            return Json(new { success = true });
        }
        #endregion
        #region Change Status

        public async Task<ActionResult> ChangeStatus(int id, int status)
        {
            var api = new MembershipCardApi();
            var model = await api.GetAsync(id);
            switch (status)
            {
                case 0:
                    status = (int)MembershipStatusEnum.Active;
                    /// Create Transaction
                    var accountApi = new AccountApi();
                    var listAccount = accountApi.GetAccountsByCardId(model.Id);
                    if (listAccount.Count > 0)
                    {
                        var account = listAccount.Where(q => q.Active == true && q.Type == (int)AccountTypeEnum.CreditAccount).FirstOrDefault();
                        if (account != null)
                        {
                            if (account.Balance >0)
                            {
                                var transaction = new TransactionViewModel()
                                {
                                    AccountId = account.AccountID,
                                    Date = Utils.GetCurrentDateTime(),
                                    StoreId = model.StoreId == null ? 0 : model.StoreId.Value,
                                    BrandId = model.BrandId,
                                    IsIncreaseTransaction = true,
                                    Notes = "ActiveCard",
                                    Status = (int)TransactionStatus.Approve,
                                    Amount = account.Balance == null ? 0 : account.Balance.Value,
                                };
                                var customerApi = new CustomerApi();
                                var customer = customerApi.Get(model.CustomerId);
                                var transactionApi = new TransactionApi();
                                await transactionApi.CreateAsync(transaction);
                                var message = "Passio Coffee" + ". "
                                              + DateTime.Now.ToString("dd/MM/yyyy") + ";" + DateTime.Now.ToString("HH:mm") + ".  "
                                              + "Passio Coffee xin chao: " + customer.Name + "; "
                                              + "He thong Passio da kich hoat TK:" + model.MembershipCardCode + "; "
                                              + "So du tai khoan: " + Utils.ToMoney((double)account.Balance) + "VNĐ";
                                try
                                {
                                    Utils.SendSMS(customer.Phone, message, model.BrandId);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                    }
                    break;
                case 1:
                    status = (int)MembershipStatusEnum.Suspensed;
                    break;
                default:
                    status = (int)MembershipStatusEnum.Active;
                    break;
            }
            model.Status = status;
            await api.EditAsync(model.Id, model);
            return this.RedirectToAction("Index");

        }
        #endregion

        #region CreateMembershipCard

        //public JsonResult GetGroupCard(int brandId)
        //{
        //    try
        //    {
        //        var cardGroupApi = new GroupMembershipCardApi();
        //        var cardGroup = cardGroupApi.GetAllActiveGroupByBrandId(brandId).ToList();
        //        List<object> listGroup = new List<object>();
        //        foreach (var item in cardGroup)
        //        {
        //            listGroup.Add(new
        //            {
        //                Name = item.GroupName,
        //                ID = item.GroudId,
        //            });
        //        }

        //        return Json(new { success = true, listGroup = listGroup }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch(Exception e)
        //    {
        //        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult GetSampleMembershipCard(int brandId, int membershipTypeId)
        {
            try
            {
                var membershipCardApi = new MembershipCardApi();

                var memCards = membershipCardApi.GetActiveListByBrandAndType(brandId, membershipTypeId)
                            .Where(q => q.IsSample == true).ToList()
                            .Select(q => new SelectListItem
                            {
                                Text = q.MembershipCardCode,
                                Value = q.Id.ToString()
                            });

                return Json(new { success = true, memCards = memCards }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadAllCustomer(int brandId)
        {
            var customerApi = new CustomerApi();
            var customers = customerApi.GetCustomersByBrand(brandId);
            return Json(new
            {
                success = true,
                data = customers.Select(a => new
                {
                    id = a.CustomerID,
                    text = a.Name,
                    phone = a.Phone
                })
            });
        }

        public JsonResult GetSampleAccounts(int membershipCardId)
        {
            try
            {
                var accountApi = new AccountApi();
                var productApi = new ProductApi();

                int count = 0;
                var accounts = accountApi.GetAllAccountsByMembershipId(membershipCardId)
                                .Where(q => q.Active == true).ToList()
                                .Select(q => new IConvertible[]
                                {
                                     ++count,
                                     q.AccountName,
                                     q.Type,
                                     q.Balance,
                                     q.ProductCode != null ? productApi.GetProductByCode(q.ProductCode).ProductName : "N/A",
                                     q.StartDate != null ? q.StartDate.Value.ToString("dd/MM/yyyy") : "N/A",
                                     q.Active,
                                     q.FinishDate != null ? q.FinishDate.Value.ToString("dd/MM/yyyy") : "",
                                     q.ProductCode,
                                     q.Level_
                                });

                return Json(new { success = true, accounts = accounts }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> CreateMembershipCardWithAccounts(int storeId, int brandId, string memCardCode, string createTime, int memTypeId, string listAccounts)
        {
            try
            {
                int memCardId;
                #region Create Membership Card
                var membershipCardApi = new MembershipCardApi();
                var accountApi = new AccountApi();
                var accounts = JsonConvert.DeserializeObject<List<dynamic>>(listAccounts);
                MembershipCardViewModel cardModel = new MembershipCardViewModel();

                var memCard = membershipCardApi.GetMembershipCardByCode(memCardCode);
                if (memCard == null)
                {
                    cardModel.MembershipCardCode = memCardCode;
                    cardModel.MembershipTypeId = memTypeId;
                    cardModel.CreatedTime = createTime.ToDateTime();
                    cardModel.BrandId = brandId;
                    cardModel.IsSample = false;
                    cardModel.StoreId = storeId;
                    memCardId = await membershipCardApi.CreateMembershipCardAsync(cardModel);
                }
                else
                {
                    return Json(new { success = false, message = "Mã thẻ đã tồn tại, xin nhập mã thẻ khác." });
                }

                #endregion

                #region Create Accounts

                foreach (var data in accounts)
                {
                    AccountViewModel newAccount = new AccountViewModel();
                    newAccount.AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    newAccount.AccountName = data[1];
                    newAccount.Type = data[2];

                    string accStartDate = data[5];
                    if (!string.IsNullOrEmpty(accStartDate) && !accStartDate.Equals("N/A"))
                    {
                        newAccount.StartDate = accStartDate.ToDateTime();
                    }

                    string accFinishDate = data[7];
                    if (!string.IsNullOrEmpty(accFinishDate))
                    {
                        newAccount.FinishDate = accFinishDate.ToDateTime().GetEndOfDate();
                    }

                    newAccount.Balance = data[3];

                    string productCode = data[8];
                    if (data[2] == (int)AccountTypeEnum.GiftAccount && !string.IsNullOrWhiteSpace(productCode))
                    {
                        newAccount.ProductCode = productCode;
                    }


                    string level = data[9];
                    if (data[2] == (int)AccountTypeEnum.PointAccount && !string.IsNullOrWhiteSpace(level))
                    {
                        newAccount.Level_ = data[9];
                    }
                    else
                    {
                        newAccount.Level_ = 0;
                    }

                    newAccount.MembershipCardId = memCardId;
                    newAccount.BrandId = brandId;
                    newAccount.Active = true;

                    await accountApi.CreateAsync(newAccount);
                }
                #endregion
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, vui lòng thử lại." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult GetAllCustomers(int brandId, string searchTokens, int page)
        {
            try
            {
                var customerApi = new CustomerApi();
                var result = customerApi.GetCustomersByBrandId(brandId);
                if (!string.IsNullOrWhiteSpace(searchTokens))
                {
                    result = result.Where(q => Utils.CustomContains(q.Name, searchTokens));
                }

                var list = result
                    .OrderBy(q => q.Name)
                    .Skip(page * 20)
                    .Take(20)
                    .Select(q => new
                    {
                        id = q.CustomerID,
                        text = q.Name
                    });

                var total = result.Count();

                return Json(new { success = true, list = list, total = total }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetCustomerById(int id)
        {
            try
            {
                var customerApi = new CustomerApi();
                var customer = customerApi.GetCustomerByID(id);
                var typeApi = new CustomerTypeApi();
                var type = "";
                if (customer.CustomerTypeId != null)
                {
                    type = typeApi.GetCustomerTypeById(customer.CustomerTypeId.Value).CustomerType1;
                }
                string gender = "Không xác định";
                string date = "";
                if (customer.Gender != null)
                {
                    if (customer.Gender == true)
                        gender = "Nam";
                    else gender = "Nữ";
                }
                if (customer.BirthDay.HasValue)
                {
                    date = customer.BirthDay.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    date = "N/A";
                }
                var cmnd = customer.IDCard;
                var disctrict = customer.District;
                var city = customer.City;
                if (customer != null)
                {
                    return Json(new
                    {
                        success = true,
                        customer = new
                        {
                            Name = customer.Name,
                            Gender = gender,
                            Type = type,
                            Address = customer.Address,
                            Phone = customer.Phone,
                            Email = customer.Email ?? "N/A",
                            Date = date,
                            CMND = cmnd,
                            District = disctrict,
                            City = city,
                        }
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Không tồn tại khách hàng"
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    message = "Có lỗi xảy ra"
                });
            }
        }
        #region lấy thông tin membership card
        public JsonResult GetInfoMembershipCard(int cardId)
        {
            try
            {
                var membershipCardApi = new MembershipCardApi();
                var list = membershipCardApi.GetMembershipCardById(cardId);
                if (list != null)
                {
                    return Json(new
                    {
                        success = true,
                        list = list,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, message = "Thẻ không tồn tại" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { success = false, message = "Có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllMembershipCardTypes(int brandId)
        {
            try
            {
                var membershipCardTypeApi = new MembershipCardTypeApi();
                var typeList = membershipCardTypeApi.GetMembershipCardTypeByBrand(brandId).ToArray();
                if (typeList != null)
                {
                    return Json(new
                    {
                        success = true,
                        list = typeList
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Danh sách loại thẻ thành viên không tồn tại"
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    message = "Có lỗi xảy ra",
                    exception = e
                });
            }
        }
        //public JsonResult LoadMembershipCardType(int membershipCardId)
        //{
        //    var accountApi = new AccountApi();
        //    List<int> listType = new List<int>();
        //    int credit = 0, gift = 0, member = 0;
        //    var accountType = accountApi.GetAccountByMembershipId(membershipCardId).Where(a => a.Active == true).Select(q => q.AccountTypeId).ToList();
        //    foreach (var item in accountType)
        //    {
        //        if (item == 1)
        //        {
        //            credit++;
        //        }
        //        if (item == 2)
        //        {
        //            gift++;
        //        }
        //        if (item == 3)
        //        {
        //            member++;
        //        }
        //    }
        //    listType.Add(credit);
        //    listType.Add(gift);
        //    listType.Add(member);

        //    return Json(new
        //    {
        //        listType = listType.ToArray(),
        //    }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public async Task<JsonResult> EditMembershipCardType()
        {
            try
            {
                var accountData = Request["listAccount"];
                var membershipCardId = int.Parse(Request["membershipCardCode"]);
                var membershipCardTypeId = int.Parse(Request["membershipCardTypeId"]);
                dynamic accountList = JsonConvert.DeserializeObject<dynamic>(accountData);
                dynamic deletedCodeList = JsonConvert.DeserializeObject<dynamic>(accountData);
                var accountApi = new AccountApi();
                var membershipCardApi = new MembershipCardApi();
                //var membershipCard = membershipCardApi.GetMembershipCardById(membershipCardId);
                var membershipCard = membershipCardApi.Get(membershipCardId);

                if (membershipCard != null)
                {
                    membershipCard.MembershipTypeId = membershipCardTypeId;
                    await membershipCardApi.UpdateMembershipCard(membershipCard);
                }

                //bool flagCredit = false;
                //bool flagGift = false;
                //bool flagMember = false;
                foreach (var item in accountList)
                {
                    //var typeIdtmp = item[8];
                    //var active = item[6];
                    //if (active == true && typeIdtmp == (int)AccountTypeEnum.CreditAccount)
                    //{
                    //    flagCredit = true;
                    //}

                    //if (active == true && typeIdtmp == (int)AccountTypeEnum.GiftAccount)
                    //{
                    //    flagGift = true;
                    //}

                    //if (active == true && typeIdtmp == (int)AccountTypeEnum.PointAccount)
                    //{
                    //    flagMember = true;
                    //}
                    string datatmp = item[1];
                    var currentAccount = await accountApi.GetAccountByAccountCode(datatmp);
                    if (currentAccount != null)
                    {
                        currentAccount.Active = item[7];
                        await accountApi.UpdateAccountAsync(currentAccount);
                    }
                    else
                    {
                        currentAccount = new AccountViewModel();
                        currentAccount.Active = item[7];
                        currentAccount.AccountCode = item[1];
                        currentAccount.AccountName = item[2];
                        currentAccount.Type = item[3];
                        currentAccount.Balance = item[4];
                        currentAccount.MembershipCardId = membershipCardId;
                        string productCode = item[9];
                        if (!string.IsNullOrWhiteSpace(productCode))
                        {
                            currentAccount.ProductCode = productCode;
                        }
                        string startDate = item[6];
                        string finishDate = item[8];
                        currentAccount.StartDate = startDate.ToDateTime().GetStartOfDate();
                        currentAccount.FinishDate = finishDate.ToDateTime().GetEndOfDate();
                        currentAccount.BrandId = int.Parse(RouteData.Values["brandId"].ToString());
                        string level = item[10];
                        if (!string.IsNullOrWhiteSpace(level))
                        {
                            currentAccount.Level_ = item[10];
                        }
                        await accountApi.CreateAsync(currentAccount);

                        //var mapping = membershiptypeMapping.Get().Where(a => a.MembershipCardId == membershipId && a.MembershipTypeId == typeMember && a.Active == true);//dang chenh nhau 3 dv
                        //if (mapping == null || mapping.Count() <= 0)
                        //{

                        //    currentAccount.MembershipCardId = membershipId;
                        //    currentAccount.Balance = item[4];
                        //    currentAccount.ProductCode = item[5];
                        //    currentAccount.Active = item[7];
                        //    string datetime = item[9];
                        //    currentAccount.FinishDate = datetime.ToDateTime().GetEndOfDate();
                        //    currentAccount.BrandId = int.Parse(RouteData.Values["brandId"].ToString());
                        //    await accountApi.CreateAsync(currentAccount);
                        //    MembershipCardTypeMappingViewModel memberCardMapping = new MembershipCardTypeMappingViewModel();
                        //    memberCardMapping.MembershipCardId = membershipId;
                        //    //đang code cứng dưới DB
                        //    if (typeId == 1)
                        //    {
                        //        memberCardMapping.MembershipTypeId = 4; // TODO;
                        //    }
                        //    if (typeId == 2)
                        //    {
                        //        memberCardMapping.MembershipTypeId = 5; // TODO;
                        //    }
                        //    if (typeId == 3)
                        //    {
                        //        memberCardMapping.MembershipTypeId = 6; // TODO;
                        //    }
                        //    memberCardMapping.Active = true;
                        //    await membershiptypeMapping.CreateAsync(memberCardMapping);
                        //}
                    }
                }


                //var mappingtmp = membershiptypeMapping.Get().Where(a => a.MembershipCardId == membershipCardId);

                //foreach (var item in mappingtmp)
                //{
                //    if (item.MembershipTypeId == 4)
                //    {
                //        if (item.Active != flagCredit)
                //        {
                //            item.Active = flagCredit;
                //            await membershiptypeMapping.EditAsync(item.Id, item);
                //        }
                //    }
                //    if (item.MembershipTypeId == 5)
                //    {
                //        if (item.Active != flagGift)
                //        {
                //            item.Active = flagGift;
                //            await membershiptypeMapping.EditAsync(item.Id, item);
                //        }
                //    }
                //    if (item.MembershipTypeId == 6)
                //    {
                //        if (item.Active != flagMember)
                //        {
                //            item.Active = flagMember;
                //            await membershiptypeMapping.EditAsync(item.Id, item);
                //        }
                //    }
                //}
                //return null;

                return Json(new { success = true });
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, vui lòng thử lại." });
            }
        }

        #endregion
        #region lấy thông tin account thuộc membership card
        //public JsonResult GetAccountMembershipCard(int accountId)
        //{
        //    try
        //    {
        //        var accountApi = new AccountApi();
        //        var listAccount = accountApi.GetAccountByMembershipId(accountId);
        //        if (listAccount != null)
        //        {
        //            int count = 1;
        //            var list = listAccount.ToList().Select(a => new IConvertible[] {
        //                count++,
        //                a.AccountCode,
        //                a.AccountName,
        //                //a.AccountType.Name,
        //                a.Balance,
        //                string.IsNullOrEmpty(a.ProductCode) ? "N/A" : a.ProductCode,
        //                a.Active,
        //                a.AccountID,
        //                //a.AccountTypeId,
        //                a.FinishDate,
        //            });
        //            return Json(new
        //            {
        //                success = true,
        //                list = list
        //            }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(new { success = true, message = "Thẻ chưa có tài khoản" }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch
        //    {
        //        return Json(new { success = false, message = "Có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion
        #region update active, deactive account
        [HttpPost]
        public async Task<JsonResult> ActiveAccount(int accountId, bool checkActive)
        {
            bool check;
            try
            {
                AccountApi accountApi = new AccountApi();
                var account = accountApi.GetAccountEntityById(accountId);
                account.AccountID = accountId;
                if (checkActive)
                {
                    account.Active = true;// Kích hoạt
                }
                else
                {
                    account.Active = false;// KHóa
                }
                AccountViewModel model = new AccountViewModel(account);
                await accountApi.EditAsync(model.AccountID, model);
                check = true;
            }
            catch (Exception)
            {
                check = false;
            }

            return Json(new
            {
                success = check,
            });
        }
        #endregion
        #region danh sách sản phẩm
        public JsonResult LoadProductList(int brandId)
        {
            var productApi = new ProductApi();
            var listPro1 = productApi.GetActiveProductsEntitybyBrandId(brandId).ToArray();
            List<object> listProduct = new List<object>();
            foreach (var i in listPro1)
            {
                listProduct.Add(new
                {
                    ProductName = i.ProductName,
                    ProductId = i.Code,
                });
            }
            var listPro2 = listProduct.ToArray();

            return Json(new
            {
                list = listPro2,
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region tạo account

        #endregion
        #region tạo thêm tài khoản membershipcard
        //[HttpPost]
        //public async Task<JsonResult> CreateNewTypeCard(string input_name, int input_AccountType, int input_ProductCode, int input_blance, string input_finishDate, int brandId, int input_codeID)
        //{
        //    bool check;
        //    var accountApi = new AccountApi();
        //    try
        //    {
        //        var Account = new AccountViewModel();
        //        Account.AccountCode = DateTime.Now.ToString("yyyyMMddHHmmssfff");//Get AccountCode theo thời gian
        //        Account.AccountName = input_name.Trim();
        //        Account.StartDate = DateTime.Now;
        //        Account.FinishDate = input_finishDate.ToDateTime();
        //        Account.Balance = input_blance; // Số điểm trong tài khoản
        //        Account.BrandId = brandId;
        //        Account.AccountTypeId = 3; //Tài khoản tích điểm
        //        Account.MembershipCardId = input_codeID;
        //        Account.Active = true;

        //        await accountApi.CreateAsync(Account);
        //        check = true;
        //    }
        //    catch (Exception)
        //    {
        //        check = false;
        //    }

        //    return Json(new
        //    {
        //        success = check,
        //    });
        //}
        #endregion
        public JsonResult CheckCode(string code, int brandId)
        {
            var membershipCardApi = new MembershipCardApi();
            var list = membershipCardApi.GetMembershipCardByCode(code);

            if (list != null)
            {
                return Json(new { success = false });
            }
            else
            {
                return Json(new { success = true });
            }
        }
        [HttpPost]
        public JsonResult LoadAllMembershipType(int brandID)
        {
            var api = new MembershipCardTypeApi();
            //get customer types

            //set brain id = 1 to view page
            var typeList = api.GetMembershipCardTypeByBrand(brandID).Select(a => new
            {
                MembershipType = a.Id,
                Name = a.TypeName
            }).ToList();
            return Json(typeList);
        }

        //public JsonResult LoadAllCustomer(int brandId)
        //{
        //    var customerApi = new CustomerApi();
        //    var customers = customerApi.GetCustomersByBrand(brandId);
        //    int result = customers.Count();
        //    var list = customers.Select(a => new
        //    {
        //        id = a.CustomerID,
        //        name = a.Name
        //    });
        //    return Json(list);
        //    //return Json(new
        //    //{
        //    //    success = true,
        //    //    data = customers.Select(a => new
        //    //    {
        //    //        id = a.CustomerID,
        //    //        text = a.Name
        //    //        //phone = a.Phone
        //    //    })
        //    //});
        //}
        //TODO: sửa lại membershipcardtypeID

        #region Upload excel
        [HttpPost]
        public async Task<JsonResult> UploadExcel(HttpPostedFileBase FileUpload, int brandId)
        {
            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string pathToExcelFile = "";
                    List<string> redundantCardCode = new List<string>();
                    try
                    {
                        string filename = FileUpload.FileName;
                        string targetpath = Server.MapPath("~/Doc/");
                        FileUpload.SaveAs(targetpath + filename);
                        pathToExcelFile = targetpath + filename;
                        var connectionString = "";
                        if (filename.EndsWith(".xls"))
                        {
                            connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                        }
                        else if (filename.EndsWith(".xlsx"))
                        {
                            connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                        }

                        var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                        var ds = new DataSet();

                        adapter.Fill(ds, "ExcelTable");

                        DataTable dtable = ds.Tables["ExcelTable"];

                        string sheetName = "Sheet1";
                        var membershipCardApi = new MembershipCardApi();
                        var memberTypeApi = new MembershipCardTypeApi();
                        //var membershipMappingApi = new MembershipCardTypeMappingApi();
                        //MembershipCardTypeMappingViewModel newMapping = new MembershipCardTypeMappingViewModel();
                        var accountApi = new AccountApi();

                        var excelFile = new ExcelQueryFactory(pathToExcelFile);
                        var artistAlbums = from a in excelFile.Worksheet<MembershipCardEditViewModel>(sheetName) select a;


                        foreach (var a in artistAlbums)
                        {
                            try
                            {
                                if ((a.MembershipCardCode != "" && a.MembershipCardCode != null) && (a.MembershipCardTypeName != "" && a.MembershipCardTypeName != null))
                                {
                                    MembershipCardViewModel newCard = new MembershipCardViewModel();
                                    var existMembershipCard = membershipCardApi.GetMembershipCardByCode(a.MembershipCardCode);
                                    if (existMembershipCard == null)
                                    {
                                        var memType = memberTypeApi.GetMembershipCardTypeByNameSync(a.MembershipCardTypeName);
                                        int memTypeId = 0;
                                        if (memType != null)
                                        {
                                            memTypeId = memType.Id;
                                            int memCardId = membershipCardApi.CreateMembershipCard(a.MembershipCardCode, memTypeId, brandId);
                                            if (a.AccountType != null)
                                            {
                                                try
                                                {
                                                    int accTypeId = int.Parse(a.AccountType);
                                                    if (accTypeId == (int)AccountTypeEnum.PointAccount || accTypeId == (int)AccountTypeEnum.CreditAccount)
                                                    {
                                                        accountApi.CreateAccountByMemCard(a.MembershipCardCode, a.DefaultBalance, brandId, memCardId, accTypeId);
                                                    }
                                                    else if (accTypeId == (int)AccountTypeEnum.GiftAccount && a.ProductCode != null && a.ProductCode != "")
                                                    {
                                                        var productApi = new ProductApi();
                                                        var product = productApi.GetProductByCode(a.ProductCode);
                                                        if (product != null)
                                                        {
                                                            accountApi.CreateGiftAccountByMemCard(a.MembershipCardCode, a.DefaultBalance, brandId, memCardId, accTypeId, a.ProductCode);
                                                        }
                                                    }
                                                }
                                                catch (Exception e)
                                                {

                                                }
                                            }
                                        }
                                        else
                                        {
                                            redundantCardCode.Add(a.MembershipCardCode);
                                        }
                                    }
                                    else
                                    {
                                        //int reduntdantCount = 0;
                                        redundantCardCode.Add(a.MembershipCardCode);
                                    }
                                }
                                else
                                {
                                    //if (a.MembershipCardCode == "" || a.MembershipCardCode == null)
                                    //    data.Add("Cần có cột MembershipCardCode trong file!");
                                    //if (a.MembershipTypeName == "" || a.MembershipTypeName == null)
                                    //    data.Add("Cần có cột MembershipTypeId trong file");
                                    //if (a.DefaultBalance == null)
                                    //    data.Add("Cần có cột Default Balance trong file");

                                    //data.ToArray();
                                    //return Json(new { success = false, data = data }, JsonRequestBehavior.AllowGet);
                                    redundantCardCode.Add(a.MembershipCardCode);
                                }
                            }
                            catch (DbEntityValidationException ex)
                            {
                                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                                {

                                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                                    {

                                        //Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                                        return Json(new
                                        {
                                            success = false,
                                            data = "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                            //deleting excel file from folder  
                            if ((System.IO.File.Exists(pathToExcelFile)))
                            {
                                System.IO.File.Delete(pathToExcelFile);
                            }
                        }
                        return Json(new
                        {
                            success = true,
                            redundantCardCodeList = redundantCardCode,
                        }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json(new
                        {
                            success = false,
                            data = "Xin kiểm tra lại tên sheet, tên các cột trong file excel theo đúng format định sẵn",
                        });
                    }
                }
                else
                {
                    //alert message for invalid file format  
                    data.Add("Chỉ được upload file Excel!");
                    data.ToArray();
                    return Json(new
                    {
                        success = false,
                        data = data
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                data.Add("Xin chọn file excel");
                data.ToArray();
                return Json(new
                {
                    success = false,
                    data = data
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult editcard()
        //{
        //    return View();
        //}
        [HttpGet]
        public ActionResult editcard(int brandId, int id)
        {
            //int Card_Id = int.Parse(Request["Card_Id"]);
            var membershipCardApi = new MembershipCardApi();
            var accountApi = new AccountApi();
            var productApi = new ProductApi();
            var card = membershipCardApi.GetMembershipCardById(id);
            List<Account> accounts = accountApi.GetAllAccountsByMembershipId(id).ToList();
            int count = 0;
            var dataTable = accounts.Select(q => new IConvertible[] {
                                     ++count,
                                     q.AccountCode,
                                     q.AccountName,
                                     q.Type,
                                     q.Balance,
                                     q.ProductCode != null ? productApi.GetProductByCode(q.ProductCode).ProductName : "N/A",
                                     q.StartDate != null ? q.StartDate.Value.ToString("dd/MM/yyyy") : "N/A",
                                     q.Active,
                                     q.FinishDate != null ? q.FinishDate.Value.ToString("dd/MM/yyyy") : "",
                                     q.ProductCode,
                                     q.Level_,
                                     true // bool để phân biệt account đã tạo và chưa tạo
                            });
            var model = new MembershipCardEditViewModel2
            {
                membershipCard = card,
                accountList = accounts,
                dataTable = dataTable.ToList()
            };
            //var membershipTypeApi = new MembershipTypeApi();
            //var accountTypeApi = new AccountTypeApi();
            //var types = membershipTypeApi.GetMembershipTypeByBrand(brandId);
            //var model = new membershipCardAndType();
            //model.membershipCard = card;
            //model.membershipTypeList = types;
            ViewBag.AccountTypes = accounts.Select(q => q.Type).ToList();
            var accountTypes = Enum.GetValues(typeof(AccountTypeEnum));
            var accountTypeList = new List<AccountTypeViewModel>();
            foreach (var type in accountTypes)
            {
                accountTypeList.Add(new AccountTypeViewModel
                {
                    Name = ((AccountTypeEnum)type).DisplayName(),
                    Value = (int)type
                });
            }
            ViewBag.AllAccountTypes = accountTypeList;
            return View("editcard", model);
        }
        public class MembershipCardEditViewModel2
        {
            public MembershipCardEditViewModel membershipCard { get; set; }
            public List<Account> accountList { get; set; }
            public List<IConvertible[]> dataTable { get; set; }
        }
        public class AccountTypeViewModel
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }
        //public class membershipCardAndType
        //{
        //    public MembershipCardEditViewModel membershipCard;
        //    public IEnumerable<MembershipTypeViewModel> membershipTypeList;
        //    public membershipCardAndType()
        //    {
        //        membershipCard = null;
        //        membershipTypeList = null;
        //    }
        //}
        [HttpPost]
        public async Task<JsonResult> UploadExcelActiveAll(HttpPostedFileBase FileUpload, int FilterMembershipCard, int brandId)
        {
            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string pathToExcelFile = "";
                    List<string> redundantCardCode = new List<string>();
                    StreamReader fileReader = null;
                    try
                    {
                        string filename = FileUpload.FileName;
                        string targetpath = Server.MapPath("~/Doc/");
                        if (System.IO.File.Exists(targetpath + filename))
                            System.IO.File.Delete(targetpath + filename);
                        FileUpload.SaveAs(targetpath + filename);
                        pathToExcelFile = targetpath + filename;
                        fileReader = System.IO.File.OpenText(pathToExcelFile);
                        CsvReader csv = new CsvReader(fileReader);
                        CustomerApi cApi = new CustomerApi();
                        MembershipCardApi mcApi = new MembershipCardApi();
                        List<CustomerExcel> customerRecords = new List<CustomerExcel>();
                        while (csv.Read())
                        {
                            var name = csv.GetField<string>("Name");
                            var email = csv.GetField<string>("Email");
                            var address = csv.GetField<string>("Address");
                            var gender = csv.GetField<string>("Gender").Equals("Nam");
                            var phone = '0' + csv.GetField<string>("Phone");
                            CustomerExcel c = new CustomerExcel(name, email, address, gender, phone);
                            customerRecords.Add(c);
                        }
                        List<HmsService.Models.Entities.MembershipCard> listDeactiveCard = new List<HmsService.Models.Entities.MembershipCard>();
                        if (FilterMembershipCard != -1)
                        {
                            listDeactiveCard = mcApi.GetListDeactiveByFilter(FilterMembershipCard, brandId).ToList();
                        }
                        else
                        {
                            listDeactiveCard = mcApi.GetMembershipCardDeactiveByBranId(brandId);
                        }
                        if (customerRecords.Count() > listDeactiveCard.Count())
                        {
                            return Json(new
                            {
                                success = false,
                                data = "Số lượng khách hàng vượt quá số lượng thẻ chưa kích hoạt",
                            });
                        }

                        for (int i = 0; i < customerRecords.Count(); i++)
                        {
                            var customerExist = cApi.GetCustomerByEmail(customerRecords[i].Email, brandId);
                            if (customerExist == null)
                            {
                                CustomerViewModel customer = new CustomerViewModel();
                                customer.Name = customerRecords[i].Name;
                                customer.Email = customerRecords[i].Email;
                                customer.Address = customerRecords[i].Address;
                                customer.Gender = customerRecords[i].Gender;
                                customer.Phone = customerRecords[i].Phone;
                                customer.Type = 1; // Tạm thời mặc định Customer Type = 1
                                customer.BrandId = brandId;
                                var customerReturn = cApi.CreateCustomer2(customer);

                                listDeactiveCard[i].CustomerId = customerReturn.CustomerID;
                                //listDeactiveCard[i].Customer.Name = customerReturn.Name; Dòng này để làm gì??????
                                listDeactiveCard[i].Status = 1;// Kích hoạt
                                MembershipCardViewModel model = new MembershipCardViewModel(listDeactiveCard[i]);
                                await mcApi.EditAsync(model.Id, model);
                            }
                            else
                            {
                                redundantCardCode.Add(customerRecords[i].Name + " - " + customerRecords[i].Email);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return Json(new
                        {
                            success = false,
                            data = "Xin kiểm tra lại tên sheet, tên các cột trong file excel theo đúng format định sẵn",
                        });
                    }
                    finally
                    {
                        fileReader.Dispose();
                        fileReader.Close();
                    }

                    //deleting excel file from folder  
                    if ((System.IO.File.Exists(pathToExcelFile)))
                    {
                        System.IO.File.Delete(pathToExcelFile);
                    }
                    return Json(new
                    {
                        success = true,
                        redundantCardCodeList = redundantCardCode,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //alert message for invalid file format  
                    data.Add("Chỉ được upload file Excel!");
                    data.ToArray();
                    return Json(new
                    {
                        success = false,
                        data = data
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (FileUpload == null) data.Add("Xin chọn file excel");
                data.ToArray();
                return Json(new
                {
                    success = false,
                    data = data
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

    public class CustomerExcel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public bool Gender;

        public string Phone { get; set; }


        public CustomerExcel(string name, string email, string address, bool gender, string phone)
        {
            this.Name = name;
            this.Email = email;
            this.Address = address;
            this.Gender = gender;
            this.Phone = phone;
        }
    }

}