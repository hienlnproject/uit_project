﻿using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.ReRunReport
{
    public class ReRunReportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ReRunReport";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ReRunReport_default",
                "{brandId}/ReRunReport/{storeId}/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}