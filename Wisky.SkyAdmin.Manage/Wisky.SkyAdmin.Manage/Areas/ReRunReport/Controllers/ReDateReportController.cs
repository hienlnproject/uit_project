﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.ReRunReport.Controllers
{
    public class ReDateReportController : DomainBasedController
    {
        // GET: Admin/ReRunDateReport
        public ActionResult Index(int brandId)
        {
            var storeApi = new StoreApi();
            var stores = new List<SelectListItem>();
            stores.Add(new SelectListItem { Value = "0", Text = "Tất cả cửa hàng" });
            stores.AddRange(storeApi.GetActiveStoreByBrandId(brandId)
                                .Select(q => new SelectListItem { Value = q.ID.ToString(), Text = q.Name }).ToList());
            ViewBag.Stores = stores;
            return this.View();
        }

        bool CheckDateReport(DateReport oldReport, DateReport checkReport)
        {
            return oldReport != null &&
                !(oldReport.TotalOrder != checkReport.TotalOrder
                || oldReport.FinalAmount != checkReport.FinalAmount
                || oldReport.Discount != checkReport.Discount
                || oldReport.TotalAmount != checkReport.TotalAmount
                || oldReport.TotalCash != checkReport.TotalCash
                || oldReport.TotalOrderAtStore != checkReport.TotalOrderAtStore
                || oldReport.TotalOrderDelivery != checkReport.TotalOrderDelivery
                || oldReport.TotalOrderTakeAway != checkReport.TotalOrderTakeAway
              );
        }

      
        public ActionResult ViewReport(string sDate, string eDate, /*string date,*/ int brandId, int storeId)
        {
            try
            {
                //var dateSearch = Utils.ToDateTime(date);
                var paymentService = DependencyUtils.Resolve<IPaymentService>();
                var reportDateApi = new DateReportApi();
                var orderApi = new OrderApi();
                var fromDate = Utils.ToDateTime(sDate).GetStartOfDate();
                var toDate = Utils.ToDateTime(eDate).GetEndOfDate();
                var reports = reportDateApi.GetDateReportByTimeRange(fromDate, toDate, brandId, storeId);
                var allOrders = orderApi.GetOrdersByTimeRange(storeId, fromDate, toDate, brandId);
                var allPayments = paymentService.GetStorePaymentByTimeRange(storeId, brandId, fromDate, toDate).ToList();
                List<DateCheckReport> listResults = new List<DateCheckReport>();
                foreach (var item in reports)
                {
                    var orders = allOrders.Where(q => q.CheckInDate == null ? false :
                                        q.CheckInDate.Value.GetEndOfDate() == item.Date.GetEndOfDate() && q.StoreID == item.StoreID).ToList();
                    var checkReport = GetCheckReport(orders, item.Store.Type, item.StoreID, item.Date);
                    var status = CheckDateReport(item, checkReport);
                    var resultReport = new DateCheckReport
                    {
                        ID = item.ID,
                        Date = item.Date,
                        Status = status,
                        Revenue = item.FinalAmount.Value,
                        RealRevenue = checkReport.FinalAmount.Value,
                        TotalOrders = item.TotalOrder,
                        RealTotalOrders = checkReport.TotalOrder,
                        TotalPayments = 0,
                        RealTotalPayments = 0,
                        StoreId = item.StoreID,
                        StoreName = item.Store.Name
                    };
                    listResults.Add(resultReport);
                }

                var returnResult = listResults.OrderBy(q => q.Status).ThenBy(q => q.Date).Select(q => new
                {
                    Id = q.ID,
                    Date = q.Date.ToString("dd/MM/yyyy"),
                    Status = q.Status,
                    Revenue = string.Format("{0:n0}", q.Revenue),
                    RealRevenue = string.Format("{0:n0}", q.RealRevenue),
                    TotalOrders = string.Format("{0:n0}", q.TotalOrders),
                    RealTotalOrders = string.Format("{0:n0}", q.RealTotalOrders),
                    StoreId = q.StoreId,
                    StoreName = q.StoreName
                }).ToList();

                //var listReport = reportDateApi.GetDateReportByDateAndBrand(dateSearch, brandId).ToList();

                //var returnResult = listReport.Select(q => new IConvertible[]{
                //    count++,
                //    q.StoreID,
                //    q.TotalOrder,
                //    orderApi.CountFinishOrderByDate(dateSearch, q.StoreID),
                //    0,  //true-false
                //    q.ID
                //}).ToList();
                return Json(new { success = true, listReport = returnResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        DateReport GetCheckReport(IEnumerable<Order> orders, int storeType, int storeID, DateTime reportDate)
        {
            IEnumerable<Order> finishedOrders;
            if (storeType == (int)StoreTypeEnum.Hotel)
            {
                finishedOrders = orders.Where(a => a.RentStatus == (int)RentStatusEnum.Leave);
            }
            else
            {
                finishedOrders = orders.Where(q => q.OrderType != (int)OrderTypeEnum.DropProduct
                                        && q.OrderStatus == (int)OrderStatusEnum.Finish);
            }
            var dateReport = new DateReport();
            if (storeType == (int)StoreTypeEnum.Hotel)
            {
                dateReport.StoreID = storeID;
                dateReport.CreateBy = "system";
                dateReport.Status = (int)DateReportStatusEnum.Approved;
                dateReport.Date = reportDate;
                dateReport.Discount = finishedOrders.Sum(a => a.Discount);
                dateReport.DiscountOrderDetail = finishedOrders.Sum(a => a.DiscountOrderDetail);
                dateReport.TotalAmount = finishedOrders.Sum(a => a.TotalAmount);
                dateReport.FinalAmount = finishedOrders.Sum(a => a.FinalAmount);
                dateReport.TotalCash = 0;
                dateReport.TotalOrder = finishedOrders.Count();
                dateReport.TotalOrderAtStore = 0;
                dateReport.TotalOrderTakeAway = 0;
                dateReport.TotalOrderDelivery = 0;
                dateReport.TotalOrderDetail = finishedOrders.Sum(a => a.OrderDetails.Sum(b => b.FinalAmount));
                dateReport.TotalOrderFeeItem = (int)finishedOrders.Sum(a => a.OrderFeeItems.Sum(b => b.TotalAmount));
            }
            else
            {
                dateReport.StoreID = storeID;
                dateReport.CreateBy = "system";
                dateReport.Status = (int)DateReportStatusEnum.Approved;
                dateReport.Date = reportDate;
                dateReport.Discount = finishedOrders.Sum(a => a.Discount);
                dateReport.DiscountOrderDetail = finishedOrders.Sum(a => a.DiscountOrderDetail);
                dateReport.TotalAmount = finishedOrders.Sum(a => a.TotalAmount);
                dateReport.FinalAmount = finishedOrders.Sum(a => a.FinalAmount);
                dateReport.TotalCash = 0;
                dateReport.TotalOrder = finishedOrders.Count();
                dateReport.TotalOrderAtStore = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.AtStore);
                dateReport.TotalOrderTakeAway = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.TakeAway);
                dateReport.TotalOrderDelivery = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.Delivery);
                dateReport.TotalOrderDetail = 0;
                dateReport.TotalOrderFeeItem = 0;
            }
            return dateReport;
        }

        public ActionResult ReCreateReport(string date, int reportId, int storeId)
        {
            try
            {
                var reportDate = Utils.ToDateTime(date).GetEndOfDate();
                var storeService = DependencyUtils.Resolve<IStoreService>();
                var orderService = DependencyUtils.Resolve<IOrderService>();
                var orderDetailService = DependencyUtils.Resolve<IOrderDetailService>();
                var productService = DependencyUtils.Resolve<IProductService>();
                var productItemService = DependencyUtils.Resolve<IProductItemService>();
                var inventoryReceiptService = DependencyUtils.Resolve<IInventoryReceiptService>();
                var reportService = DependencyUtils.Resolve<IReportService>();
                var dateReportApi = new DateReportApi();
                var dateProductApi = new DateProductApi();
                var paymentService = DependencyUtils.Resolve<IPaymentService>();

                //Set date
                var fromDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetStartOfDate();
                var toDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetEndOfDate();

                //Get store
                var store = storeService.GetStoreById(storeId);

                //Get orders
                IEnumerable<Order> orders;
                IEnumerable<Order> finishedOrders;
                if (store.Type == (int)StoreTypeEnum.Hotel)
                {
                    orders = orderService.GetAllHotelOrdersByCheckOutDate(fromDate, toDate, store.ID).ToList();
                    finishedOrders = orders.Where(a => a.RentStatus == (int)RentStatusEnum.Leave);
                }
                else
                {
                    orders = orderService.GetOrdersByTimeRange(store.ID, fromDate, toDate).ToList();
                    finishedOrders = orders.Where(q => q.OrderType != (int)OrderTypeEnum.DropProduct
                                            && q.OrderStatus == (int)OrderStatusEnum.Finish);
                }

                ////Get order details
                //var orderDetails =
                //    orderDetailService.GetOrderDetailsByTimeRange(fromDate, toDate, store.ID)
                //    .Where(a => a.Order.OrderType != (int)OrderTypeEnum.DropProduct && a.Order.OrderStatus == (int)OrderStatusEnum.Finish).ToList();

                ////Get products
                //var dateProducts =
                //    orderDetails.GroupBy(a => a.ProductID)
                //        .Join(productService.GetAllProducts(), a => a.Key, a => a.ProductID, (a, b) => new DateProduct()
                //        {
                //            ProductId = a.Key,
                //            StoreID = store.ID,
                //            Quantity = a.Sum(c => c.Quantity),
                //            Date = reportDate,
                //            TotalAmount = a.Sum(c => c.TotalAmount),
                //            FinalAmount = a.Sum(c => c.FinalAmount),
                //            Discount = a.Sum(c => c.Discount),
                //            ProductName_ = b.ProductName,
                //            Product = b,
                //            CategoryId_ = b.ProductCategory.CateID
                //        }).ToList();

                //Create DateReport
                var dateReport = new DateReport();
                if (store.Type == (int)StoreTypeEnum.Hotel)
                {
                    dateReport.StoreID = store.ID;
                    dateReport.CreateBy = "system";
                    dateReport.Status = (int)DateReportStatusEnum.Approved;
                    dateReport.Date = reportDate;
                    dateReport.Discount = finishedOrders.Sum(a => a.Discount);
                    dateReport.DiscountOrderDetail = finishedOrders.Sum(a => a.DiscountOrderDetail);
                    dateReport.TotalAmount = finishedOrders.Sum(a => a.TotalAmount);
                    dateReport.FinalAmount = finishedOrders.Sum(a => a.FinalAmount);
                    dateReport.TotalCash = 0;
                    dateReport.TotalOrder = finishedOrders.Count();
                    dateReport.TotalOrderAtStore = 0;
                    dateReport.TotalOrderTakeAway = 0;
                    dateReport.TotalOrderDelivery = 0;
                    dateReport.TotalOrderDetail = finishedOrders.Sum(a => a.OrderDetails.Sum(b => b.FinalAmount));
                    dateReport.TotalOrderFeeItem = (int)finishedOrders.Sum(a => a.OrderFeeItems.Sum(b => b.TotalAmount));
                }
                else
                {
                    dateReport.StoreID = store.ID;
                    dateReport.CreateBy = "system";
                    dateReport.Status = (int)DateReportStatusEnum.Approved;
                    dateReport.Date = reportDate;
                    dateReport.Discount = finishedOrders.Sum(a => a.Discount);
                    dateReport.DiscountOrderDetail = finishedOrders.Sum(a => a.DiscountOrderDetail);
                    dateReport.TotalAmount = finishedOrders.Sum(a => a.TotalAmount);
                    dateReport.FinalAmount = finishedOrders.Sum(a => a.FinalAmount);
                    dateReport.TotalCash = 0;
                    dateReport.TotalOrder = finishedOrders.Count();
                    dateReport.TotalOrderAtStore = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.AtStore);
                    dateReport.TotalOrderTakeAway = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.TakeAway);
                    dateReport.TotalOrderDelivery = finishedOrders.Count(a => a.OrderType == (int)OrderTypeEnum.Delivery);
                    dateReport.TotalOrderDetail = 0;
                    dateReport.TotalOrderFeeItem = 0;
                }

                var oldDateReport = dateReportApi.GetDateReportById(reportId);
                var listDateProduct = dateProductApi.GetDateProductByTimeRange(reportDate, storeId);

                var payments = paymentService.GetStorePaymentByTimeRange(store.ID, (int)store.BrandId, fromDate, toDate).ToList();




                //var reportTracking = new ReportTracking()
                //{
                //    Date = Utils.GetCurrentDateTime(),
                //    IsUpdate = false,
                //    UpdatePerson = "system",
                //    StoreId = store.ID,
                //};
                //var compositionsStatistic = dateProducts.SelectMany(a => a.Product.ProductItemCompositionMappings.Select(b => new Tuple<ProductItemCompositionMapping, int>(b, a.Quantity)))
                //    .GroupBy(a => a.Item1.ItemID);
                //var dateItemProduct = compositionsStatistic.Join(productItemService.GetProductItems(), a => a.Key, a => a.ItemID, (a, b) => new DateProductItem
                //{
                //    StoreId = store.ID,
                //    Date = reportDate,
                //    ProductItemID = a.Key,
                //    ProductItemName = b.ItemName,
                //    Quantity = (int)a.Sum(c => c.Item2 * c.Item1.Quantity),
                //    Unit = b.Unit
                //}).AsQueryable();
                //var inventoryReceipts = inventoryReceiptService.GetInventoryReceiptByTimeRange(store.ID, fromDate, toDate).AsQueryable();

                string user = "";
                try
                {
                    user = (System.Web.HttpContext.Current == null) ? "quanly" : System.Web.HttpContext.Current.User.Identity.Name;
                }
                catch (Exception)
                {
                    user = "noname";
                }

                reportService.UpdateDateReportOnly(oldDateReport, dateReport, store);


            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

       

        public ActionResult ReRunReport(int[] incorrectReports)
        {
            try
            {
                var dateReportApi = new DateReportApi();
                var reportService = DependencyUtils.Resolve<IReportService>();
                var orderService = DependencyUtils.Resolve<IOrderService>();
                var paymentService = DependencyUtils.Resolve<IPaymentService>();
                var reports = dateReportApi.BaseService.Get(q => incorrectReports.Contains(q.ID)).ToList();
                foreach (var oldReport in reports)
                {
                    var reportDate = oldReport.Date.GetEndOfDate();
                    var storeId = oldReport.StoreID;
                    var orders = orderService.GetOrdersByTimeRange(storeId, reportDate.GetStartOfDate(), reportDate).ToList();
                    var checkDateReport = GetCheckReport(orders, oldReport.Store.Type, oldReport.StoreID, oldReport.Date);
                    var success = reportService.UpdateDateReportOnly(oldReport, checkDateReport, oldReport.Store);
                    if (!success)
                    {
                        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}