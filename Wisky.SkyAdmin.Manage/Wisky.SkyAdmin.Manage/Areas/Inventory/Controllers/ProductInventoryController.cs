﻿using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.Models;
using System.Threading.Tasks;
using HmsService.ViewModels;
using Newtonsoft.Json;
using Wisky.SkyAdmin.Manage.Controllers;

namespace WiSky.SkyAdmin.Manage.Areas.Inventory.Controllers
{
    [Authorize(Roles = "BrandManager, Inventory, StoreManager, Manager")]
    public class ProductInventoryController : DomainBasedController
    {
        #region Nhập hàng
        public ActionResult ListImportInventory(string storeId)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetImportInventoryReceiptByStore(int.Parse(storeId));
            var model = new InventoryReceiptEditViewModel(inventoryReceipts, this.Mapper);
            ViewBag.storeId = storeId;
            return View(model);
        }

        public JsonResult LoadImportInventory(JQueryDataTableParamModel param, int? status, int storeId)
        {
            // TODO: remove hard-code - duydt
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetImportInventoryReceiptByStore(storeId).OrderByDescending(a => a.CreateDate);
            if (status != 3)
            {
                inventoryReceipts = api.GetImportInventoryReceiptByStore(storeId).Where(q => q.Status == status).OrderByDescending(a => a.CreateDate);
            }
            int count = 1;
            try
            {
                var rs = inventoryReceipts
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch.Trim().ToLower()))
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                        {
                        count++,
                        string.IsNullOrEmpty(a.Name) ? "Không xác định" : a.Name,
                        a.InvoiceNumber,
                        a.Creator,
                        a.CreateDate?.ToString("dd/MM/yyyy") ?? "---",
                        a.Status,
                        a.Notes,
                        a.Provider.ProviderName,
                        a.ReceiptID
                        });
                var totalRecords = inventoryReceipts.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = rs
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public async Task<ActionResult> ImportInventory(int brandId, string storeId)
        {
            ViewBag.storeId = storeId;
            var model = new InventoryReceiptEditViewModel();
            PrepareImportInventoryReceipt(model, brandId);
            return View(model);
        }
        #endregion

        #region Prepare
        private void PrepareImportInventoryReceipt(InventoryReceiptEditViewModel model, int brandId)
        {

            var providers = new ProviderApi()
                .GetProvidersByBrand(brandId);
            var creators = new AspNetUserApi().GetActive().Where(q => q.BrandId == brandId);
            var api = new ProviderProductItemMappingApi();
            List<ProviderViewModel> providerList = new List<ProviderViewModel>();
            List<AspNetUserViewModel> creatorList = new List<AspNetUserViewModel>();

           
            foreach (var item in providers)
            {
                var categories = api.GetProviderProductItems()
                    .Where(q => q.Active && q.ProviderID == item.Id && q.ProductItem.ItemCategory.BrandId == brandId);
                if (categories.Count() != 0)
                {
                    providerList.Add(item);
                }
            }
            foreach (var item in creators)
            {
                creatorList.Add(item);
            }
            model.AvailableCreator = creatorList
                .ToSelectList(q => q.UserName, q => q.UserName.ToString(), q => false);
            model.AvailableProvider = providerList
                .ToSelectList(q => q.ProviderName, q => q.Id.ToString(), q => false);
        }
        private void PrepareExportInventoryReceipt(InventoryReceiptEditViewModel model, int brandId, int storeId)
        {
            //Chỉ lấy những category có productitem
            var categories = new ProductItemCategoryApi()
                .GetItemCategoryByBrand(brandId).ToList();

            var creators = new AspNetUserApi().GetActive().Where(q => q.BrandId == brandId);
            var api = new ProductItemApi();
            List<ProductItemCategoryViewModel> categoriesList = new List<ProductItemCategoryViewModel>();
            List<AspNetUserViewModel> creatorList = new List<AspNetUserViewModel>();
            foreach (var item in categories)
            {
                var productItems = api.GetProductItemsByCategoryId(item.CateID);
                if (productItems.Count() != 0)
                {
                    categoriesList.Add(item);
                }
            }
            foreach (var item in creators)
            {
                creatorList.Add(item);
            }
            model.AvailableCreator = creatorList
                .ToSelectList(q => q.UserName, q => q.UserName.ToString(), q => false);

            model.AvailableItemCategory = categoriesList
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => false);

            model.AvailableStore = (new StoreApi()
                .GetAllStore(brandId).Where(q => q.isAvailable == true && q.ID != storeId))
                .ToSelectList(q => q.Name, q => q.ID.ToString(), q => false);
        }
        private void PrepareTransferInventoryReceipt(InventoryReceiptEditViewModel model, int brandId, int storeId)
        {
            //Chỉ lấy những category có productitem
            var categories = new ProductItemCategoryApi()
                .GetItemCategoryByBrand(brandId).ToList();
            var api = new ProductItemApi();
            List<ProductItemCategoryViewModel> categoriesList = new List<ProductItemCategoryViewModel>();
            foreach (var item in categories)
            {
                var productItems = api.GetProductItemsByCategoryId(item.CateID);
                if (productItems.Count() != 0)
                {
                    categoriesList.Add(item);
                }
            }
            model.AvailableItemCategory = categoriesList
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => false);
            model.AvailableStore = (new StoreApi()
                .GetAllStore(brandId).Where(q => q.isAvailable == true && q.ID != storeId))
                .ToSelectList(q => q.Name, q => q.ID.ToString(), q => false);
        }
        #endregion

        #region View detail of receipt
        public async Task<ActionResult> InventoryReceiptItem(int id, string storeId)
        {
            ViewBag.storeId = storeId;
            var api = new InventoryReceiptItemApi();
            var receiptApi = new InventoryReceiptApi();
            var storeApi = new StoreApi();
            var inventoryReceipt = await receiptApi.GetInventoryReceiptById(id);
            var invetoryReceiptItem = api.GetItemReceiptById(id);
            var model = new InventoryReceiptEditViewModel(inventoryReceipt, this.Mapper);
            model.InventoryReceiptItem = invetoryReceiptItem;
            if (inventoryReceipt.InStoreId != null)
            {
                model.InStoreName = storeApi.GetStoreNameByID((int)inventoryReceipt.InStoreId);
            }
            return View(model);
        }

        //Do chưa có đang nhập nên tạo 1 controller khác cho instore
        public async Task<ActionResult> InStoreInventoryReceiptItem(int id, string storeId)
        {
            var api = new InventoryReceiptItemApi();
            var receiptApi = new InventoryReceiptApi();
            var storeApi = new StoreApi();
            var inventoryReceipt = await receiptApi.GetInventoryReceiptById(id);
            var invetoryReceiptItem = api.GetItemReceiptById(id);
            var model = new InventoryReceiptEditViewModel(inventoryReceipt, this.Mapper);
            model.InventoryReceiptItem = invetoryReceiptItem;
            model.OutStoreName = storeApi.GetStoreNameByID((int)inventoryReceipt.OutStoreId);
            ViewBag.storeId = storeId;
            return View(model);
        }
        #endregion

        #region ChangeStatus of Import, ExportReceipt
        [HttpPost]
        public async Task<ActionResult> AcceptReceipt(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            if (inventoryReceipt == null)
            {
                return this.HttpNotFound();
            }
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.Approved;
            await api.EditAsync(id, inventoryReceipt);
            if (inventoryReceipt.ReceiptType == 0)
            {
                return this.RedirectToAction("ListImportInventory", "ProductInventory");
            }
            else if (inventoryReceipt.ReceiptType == 1)
            {
                return this.RedirectToAction("ListGetTransferInventory", "ProductInventory");
            }
            else if (inventoryReceipt.ReceiptType == 2)
            {
                return this.RedirectToAction("ListTransferInventory", "ProductInventory");
            }
            else
            {
                return this.RedirectToAction("ListExportInventory", "ProductInventory");
            }
        }

        [HttpPost]
        public async Task<ActionResult> RejectReceipt(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            if (inventoryReceipt == null)
            {
                return this.HttpNotFound();
            }
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.Reject;
            await api.EditAsync(id, inventoryReceipt);
            return this.RedirectToAction("ListImportInventory", "ProductInventory");
        }

        [HttpPost]
        public async Task<ActionResult> CancelReceipt(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.Canceled;
            await api.EditAsync(id, inventoryReceipt);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ChangeStatus of Transfer, GetTransferInventory
        [HttpPost]
        public async Task<ActionResult> AcceptCancel(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            if (inventoryReceipt == null)
            {
                return this.HttpNotFound();
            }
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.Canceled;
            await api.EditAsync(id, inventoryReceipt);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> InStoreCancelRequest(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            if (inventoryReceipt == null)
            {
                return this.HttpNotFound();
            }
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.InStoreCancelRequested;
            await api.EditAsync(id, inventoryReceipt);

            return this.RedirectToAction("ListTransferInventory", "ProductInventory");
        }

        [HttpPost]
        public async Task<ActionResult> OutStoreCancelRequest(int id)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipt = await api.GetInventoryReceiptById(id);
            if (inventoryReceipt == null)
            {
                return this.HttpNotFound();
            }
            inventoryReceipt.Status = (int)InventoryReceiptStatusEnum.OutStoreCancelRequested;
            await api.EditAsync(id, inventoryReceipt);

            return this.RedirectToAction("ListGetTransferInventory", "ProductInventory");
        }
        #endregion

        #region JsonImportInventory
        [HttpPost]
        public JsonResult GetCurrentProviderCategories(int ProviderId, int brandId)
        {
            //var count = 0;
            List<dynamic> listDt = new List<dynamic>();
            var api = new ProviderProductItemMappingApi();
            var categories = api.GetProviderProductItems()
                .Where(q => q.Active && q.ProviderID == ProviderId && q.ProductItem.ItemCategory.BrandId == brandId)
                .Select(q => new
                {
                    CategoryId = q.ProductItem.CatID,
                    CategoryName = q.ProductItem.ItemCategory.CateName,
                }).Distinct();

            return Json(new { categories = categories.ToArray() }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public JsonResult GetListProvider()
        {
            //var count = 0;
            List<dynamic> listDt = new List<dynamic>();
            var api = new ProviderApi();
            var providers = api.GetProviders()
                .Where(q => q.IsAvailable == true)
                .Select(q => new
                {
                    Value = q.Id,
                    Text = q.ProviderName,
                }).Distinct();

            return Json(new { providers = providers.ToArray() }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SelectChangeItemByProviderId(int itemCatId, int? ProviderId)
        {
            IEnumerable<ProviderProductItemMappingViewModel> ProductItems = null;

            var productItemApi = new ProductItemApi();
            var providerProductItemApi = new ProviderProductItemMappingApi();
            if (ProviderId == null)
            {
                //ProductItems = _providerProductItemService.GetProviderProductItems().Where(q => q.Active);
                ProductItems = productItemApi.GetProductItemsByCategoryId(itemCatId).Select(q => new ProviderProductItemMappingViewModel()
                {
                    ProductItemID = q.ItemID,
                    ProductItem = q,
                });
            }
            else
            {
                ProductItems = providerProductItemApi.GetProviderProductItems().Where(q => q.Active
                && q.ProviderID == ProviderId && q.ProductItem.CatID == itemCatId);
            }
            var data = ProductItems.Select(a => new
            {
                ItemId = a.ProductItemID,
                a.ProductItem.ItemName,
                a.ProductItem.Unit,
                a.ProductItem.Unit2,
                a.ProductItem.Price,
                a.ProductItem.UnitRate,
            });
            return Json(new
            {
                data = data.ToArray()
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SelectChangeItemByCategoryId(int itemCatId)
        {
            IEnumerable<ProductItemViewModel> ProductItems = null;
            var api = new ProductItemApi();
            ProductItems = api.GetProductItemsByCategoryId(itemCatId);
            var data = ProductItems.Select(a => new
            {
                ItemId = a.ItemID,
                a.ItemName,
                a.Unit,
                a.Unit2,
                a.Price,
                IndexPriority = a.IndexPriority ?? 1
            });
            return Json(new
            {
                data = data.ToArray()
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Xuất hàng
        public ActionResult ListExportInventory(string storeId)
        {
            ViewBag.storeId = storeId;
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetExportInventoryReceiptByStore(int.Parse(storeId));
            var model = new InventoryReceiptEditViewModel(inventoryReceipts, this.Mapper);

            return View(model);
        }

        public JsonResult LoadExportInventory(JQueryDataTableParamModel param, int? status, int storeId)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetExportInventoryReceiptByStore(storeId).OrderByDescending(a => a.CreateDate);
            if (status != 3)
            {
                inventoryReceipts = api.GetExportInventoryReceiptByStore(storeId).Where(q => q.Status == status).OrderByDescending(a => a.CreateDate);
            }
            int count = 1;
            try
            {
                var rs = inventoryReceipts
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch.Trim().ToLower()))
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                        {
                        count++,
                        string.IsNullOrEmpty(a.Name) ? "Không xác định" : a.Name,
                        a.Creator,
                        a.CreateDate?.ToString("dd/MM/yyyy") ?? "---",
                        a.Status,
                        a.Notes,
                        a.ReceiptType,
                        a.ReceiptID
                        });
                var totalRecords = inventoryReceipts.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = rs
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public async Task<ActionResult> ExportInventory(int brandId, string storeId)
        {
            ViewBag.storeId = storeId;
            var model = new InventoryReceiptEditViewModel();
            PrepareExportInventoryReceipt(model, brandId, int.Parse(storeId));
            return View(model);
        }
        #endregion

        #region Chuyển hàng đi
        public ActionResult ListTransferInventory(string storeId)
        {
            ViewBag.storeId = storeId;
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetOutStoreInventoryReceiptByStore(int.Parse(storeId));
            var model = new InventoryReceiptEditViewModel(inventoryReceipts, this.Mapper);

            return View(model);
        }

        public JsonResult LoadTransferInventory(JQueryDataTableParamModel param, int? status, int storeId)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetOutStoreInventoryReceiptByStore(storeId).OrderByDescending(a => a.CreateDate);
            if (status != 3)
            {
                inventoryReceipts = api.GetOutStoreInventoryReceiptByStore(storeId).Where(q => q.Status == status).OrderByDescending(a => a.CreateDate);
            }
            var storeApi = new StoreApi();
            string name = "";
            int count = 1;
            try
            {
                var rs = inventoryReceipts
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch.Trim().ToLower()))
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                        {
                        count++,
                        string.IsNullOrEmpty(a.Name) ? "Không xác định" : a.Name,
                        a.Creator,
                        a.CreateDate?.ToString("dd/MM/yyyy") ?? "---",
                        a.Status,
                        a.Notes,
                        name = storeApi.GetStoreNameByID((int)a.InStoreId).ToString(),
                        a.ReceiptType,
                        a.ReceiptID
                        });
                var totalRecords = inventoryReceipts.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = rs
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        public async Task<ActionResult> TransferInventory(int brandId, string storeId)
        {
            ViewBag.storeId = storeId;
            var model = new InventoryReceiptEditViewModel();
            PrepareTransferInventoryReceipt(model, brandId, int.Parse(storeId));
            return View(model);
        }
        #endregion

        #region Chuyển hàng đến
        public ActionResult ListGetTransferInventory(int storeId)
        {
            ViewBag.storeId = storeId.ToString();
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetInStoreInventoryReceiptByStore(storeId);
            var model = new InventoryReceiptEditViewModel(inventoryReceipts, this.Mapper);
            return View(model);
        }

        public JsonResult LoadGetTransferInventory(JQueryDataTableParamModel param, int status, int storeId)
        {
            var api = new InventoryReceiptApi();
            var inventoryReceipts = api.GetInStoreInventoryReceiptByStore(storeId).OrderByDescending(a => a.CreateDate);
            if (status != 3)
            {
                inventoryReceipts = api.GetInStoreInventoryReceiptByStore(storeId).Where(q => q.Status == status).OrderByDescending(a => a.CreateDate);
            }
            var storeApi = new StoreApi();
            int count = 1;
            try
            {
                var rs = inventoryReceipts
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch.Trim().ToLower()))
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                        {
                        count++,
                        string.IsNullOrEmpty(a.Name) ? "Không xác định" : a.Name,
                        a.Creator,
                        a.CreateDate?.ToString("dd/MM/yyyy") ?? "---",
                        a.Status,
                        a.Notes,
                        storeApi.GetStoreNameByID((int)a.OutStoreId),
                        a.ReceiptType,
                        a.ReceiptID
                        });
                var totalRecords = inventoryReceipts.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = rs
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "Error" });
            }
        }
        #endregion

        #region Thêm đơn hàng
        //Post import data in database
        [HttpPost]
        public JsonResult ImportInventory(string data, int storeId)
        {           
            var model = JsonConvert.DeserializeObject<ImportExportItemModel>(data);
            try
            {
                var api = new InventoryReceiptApi();
                var itemApi = new InventoryReceiptItemApi();
                //var id = Session["storeId"].ToString();
                //var storeId = Convert.ToInt32(id);
                var time = Utils.GetCurrentDateTime().Ticks;
                //Push InventoryReceipt
                var receiptInventory = new InventoryReceiptViewModel
                {
                    CreateDate = Utils.GetCurrentDateTime(),
                    ReceiptType = 0,
                    //ReceiptType = model.ReceiptTypeId == 2 ? 1 : model.ReceiptTypeId,
                    Status = (int)InventoryReceiptStatusEnum.New, //0 Giá trị mặc định là chờ duyệt,
                    Notes = model.Notes,
                    Name = "PNH-" + time,
                    InvoiceNumber = model.InvoiceNumber,
                    Creator = model.Creator,
                    StoreId = storeId,
                    ProviderId = model.ProviderId,
                    ChangeDate = Convert.ToDateTime(model.ImportDate)
                };
                api.Create(receiptInventory);
                //Push InventoryReceiptItem
                foreach (var item in model.ReceiptItems)
                {
                    var receiptItem = new InventoryReceiptItemViewModel()
                    {
                        ReceiptID = receiptInventory.ReceiptID,
                        ItemID = item.Id,
                        Quantity = (int)item.Quantity,
                        Price = item.Price
                    };
                    itemApi.Create(receiptItem);
                }
                //return RedirectToAction("ListImportInventory");
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, vui lòng thử lại!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExportInventory(string data, int storeId)
        {
            var model = JsonConvert.DeserializeObject<ImportExportItemModel>(data);
            try
            {
                var api = new InventoryReceiptApi();
                var itemApi = new InventoryReceiptItemApi();
                //var id = Session["storeId"].ToString();
                //var storeId = Convert.ToInt32(id);
                var time = Utils.GetCurrentDateTime().Ticks;
                //Push InventoryReceipt
                var receiptInventory = new InventoryReceiptViewModel
                {
                    CreateDate = Utils.GetCurrentDateTime(),
                    ReceiptType = model.ReceiptTypeId,
                    Status = (int)InventoryReceiptStatusEnum.New, //0 Giá trị mặc định là chờ duyệt,
                    Notes = model.Notes,
                    Name = "PNH-" + time,
                    Creator = model.Creator,
                    StoreId = storeId,
                    ChangeDate = Convert.ToDateTime(model.ExportDate)
                };
                api.Create(receiptInventory);
                //Push InventoryReceiptItem
                foreach (var item in model.ReceiptItems)
                {
                    var receiptItem = new InventoryReceiptItemViewModel()
                    {
                        ReceiptID = receiptInventory.ReceiptID,
                        ItemID = item.Id,
                        Quantity = (int)item.Quantity,
                        Price = item.Price
                    };
                    itemApi.Create(receiptItem);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, vui lòng thử lại!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult TransferInventory(string data, int storeId)
        {
            var model = JsonConvert.DeserializeObject<ImportExportItemModel>(data);
            try
            {
                var api = new InventoryReceiptApi();
                var itemApi = new InventoryReceiptItemApi();
                //var id = Session["storeId"].ToString();
                //var storeId = Convert.ToInt32(id);
                var time = Utils.GetCurrentDateTime().Ticks;
                //Push InventoryReceipt
                var receiptInventory = new InventoryReceiptViewModel
                {
                    CreateDate = Utils.GetCurrentDateTime(),
                    ReceiptType = 2,
                    Status = (int)InventoryReceiptStatusEnum.New, //0 Giá trị mặc định là chờ duyệt,
                    Notes = model.Notes,
                    Name = "PNH-" + time,
                    Creator = model.Creator,
                    StoreId = storeId,
                    InStoreId = model.InStoreId,
                    OutStoreId = storeId,
                    ChangeDate = Convert.ToDateTime(model.ExportDate)
                };
                api.Create(receiptInventory);
                //Push InventoryReceiptItem
                foreach (var item in model.ReceiptItems)
                {
                    var receiptItem = new InventoryReceiptItemViewModel()
                    {
                        ReceiptID = receiptInventory.ReceiptID,
                        ItemID = item.Id,
                        Quantity = (int)item.Quantity,
                        Price = item.Price
                    };
                    itemApi.Create(receiptItem);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, vui lòng thử lại!" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}