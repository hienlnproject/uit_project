﻿using Facebook;
using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.DashBoard.Controllers
{
    public class FacebookDashBoardController : Controller
    {
        // GET: DashBoard/FacebookDashBoard
        public ActionResult Index(int storeId, int brandId)
        {
            var storeApi = new StoreApi();

            if (storeId != 0)
            {
                var store = storeApi.Get(storeId);
                ViewBag.storeId = store.ID.ToString();
                ViewBag.storeName = store.Name;
                ViewBag.brandId = brandId;
                ViewBag.fbAppId = FacebookAppSetting.AppId;
                if (store.FbAccessToken != null)
                {
                    ViewBag.isConnected = true;
                    ViewBag.FbAccessToken = store.FbAccessToken;
                    FacebookClient fbApp = new FacebookClient(store.FbAccessToken);
                    dynamic param = new ExpandoObject();
                    param.fields = "single_line_address,phone,emails,website,about,description";
                    param.locale = "vi_vi";
                    try
                    {
                        dynamic result = fbApp.Get(store.FbShopId, param);
                        if (HasProperty(result, "single_line_address"))
                        {
                            ViewBag.Address = result.single_line_address;
                        }
                        if (HasProperty(result, "phone"))
                        {
                            ViewBag.Phone = result.phone;
                        }
                        if (HasProperty(result, "emails"))
                        {
                            ViewBag.Email = result.emails;
                        }
                        if (HasProperty(result, "website"))
                        {
                            ViewBag.Website = result.website;
                        }
                        if (HasProperty(result, "about"))
                        {
                            ViewBag.About = result.about;
                        }
                        if (HasProperty(result, "description"))
                        {
                            ViewBag.Description = result.description;
                        }

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Facebook Exception: " + e.Message);
                    }
                }
                else
                {
                    ViewBag.isConnected = false;
                };
                return View();
            }

            ViewBag.storeId = "0";
            ViewBag.storeName = "Tổng quan hệ thống";
            ViewBag.brandId = brandId;

            return View();
        }


        private bool HasProperty(dynamic obj, string name)
        {
            try
            {
                var prop = obj[name];
                return prop != null;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("Name = " + name);
                return false;
            }
        }

    }
}