﻿using HmsService.Sdk;
using HmsService.ViewModels;
using HmsService.Models;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Transaction.Controllers
{
    public class TransactionController : DomainBasedController
    {
        // GET: CRM/Transaction
        public ActionResult Index()
        {
            var transactionStatusList = new List<TransactionEnumViewModel>();
            var allTransactionStatus = Enum.GetValues(typeof(TransactionStatus));
            foreach (var status in allTransactionStatus)
            {
                transactionStatusList.Add(new TransactionEnumViewModel
                {
                    Name = ((TransactionStatus)status).DisplayName(),
                    Value = (int)status
                });
            }

            var transactionAmountList = new List<TransactionEnumViewModel>();
            var allTransactionAmount = Enum.GetValues(typeof(TransactionAmount));
            foreach (var amount in allTransactionAmount)
            {
                transactionAmountList.Add(new TransactionEnumViewModel
                {
                    Name = ((TransactionAmount)amount).DisplayName(),
                    Value = (int)amount
                });
            }

            ViewBag.TransactionStatusList = transactionStatusList;
            ViewBag.TransactionAmountList = transactionAmountList;

            return View();
        }

        public ActionResult GetAllTransactions(int storeId)
        {
            try
            {
                var transactionApi = new TransactionApi();

                var count = 1;
                var result = transactionApi.GetAllTransactionByStoreId(storeId).ToList().Select(q => new IConvertible[] {
                    count++,
                    q.Account.AccountName,
                    q.Amount,
                    q.Date.ToString("dd/MM/yyyy"),
                    q.Status
                });

                return Json(new { success = true, list = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, xin vui lòng liện hệ admin" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CreateTransaction(int brandId, int storeId, string membershipCardCode, decimal amount, int? type, string description)
        {
            try
            {
                var transactionApi = new TransactionApi();
                var membershipCardApi = new MembershipCardApi();
                var currentCard = membershipCardApi.GetMembershipCardByCode(membershipCardCode);
                var typeTrasaction = true;
                if (type !=null)
                {
                    if (type == 1)
                    {
                        typeTrasaction = true;
                    }
                    else
                    {
                        typeTrasaction = false;
                    }
                } else
                {
                    return Json(new { success = false, message = "Giao dịch thất bại!" });
                }
                foreach (var account in currentCard.Accounts)
                {
                    if (account.Type == (int)AccountTypeEnum.CreditAccount)
                    {
                        transactionApi.Create(new TransactionViewModel
                        {
                            AccountId = account.AccountID,
                            Amount = amount,
                            StoreId = storeId,
                            BrandId = brandId,
                            Date = DateTime.Now,
                            IsIncreaseTransaction = typeTrasaction,
                            Notes = description,
                            Status = (int) TransactionStatus.New
                        });

                        return Json(new { success = true, message = "Giao dịch thành công!" });
                    }
                }
                return Json(new { success = false, message = "Giao dịch thất bại!" });
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, xin vui lòng liện hệ admin" });
            }
        }

        public ActionResult CheckMembershipCardCode(int brandId, string membershipCardCode)
        {
            try
            {
                var membershipCardApi = new MembershipCardApi();
                var currentCard = membershipCardApi.GetMembershipCardByCode(membershipCardCode.Trim());

                if (currentCard != null && currentCard.BrandId == brandId)
                {
                    if (currentCard.Accounts != null && currentCard.Accounts.Count > 0)
                    {
                        foreach (var account in currentCard.Accounts)
                        {
                            if (account.Type == (int)AccountTypeEnum.CreditAccount)
                            {
                                var customer = currentCard.Customer;
                                return Json(new { success = true, AccountName = account.AccountName, Customer = new { Name = customer.Name, Phone = customer.Phone } }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    return Json(new { success = false, message = "Không tồn tại tài khoảng thanh toán!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Thẻ không tồn tại!" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "Có lỗi xảy ra, xin vui lòng liện hệ admin" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllTransactionsByDateRange(JQueryDataTableParamModel param, int storeId, string startTime, string endTime, int transactionStatus, int transactionType)
        {
            try
            {
                var transactionApi = new TransactionApi();
                var customerApi = new CustomerApi();
                var accountApi = new AccountApi();
                var membershipCardApi = new MembershipCardApi();

                DateTime sTime = startTime.ToDateTime().GetStartOfDate();
                DateTime eTime = endTime.ToDateTime().GetEndOfDate();
                var result = transactionApi.GetAllTransactionByStoreIdWithDateRangeEntity(storeId, sTime, eTime);
                var count = param.iDisplayStart + 1;

                if (transactionStatus != -1)
                {
                    result = result.Where(a => a.Status == transactionStatus).OrderBy(q => q.Date);
                }

                if (transactionType != -1)
                {
                    result = result.Where(a => a.IsIncreaseTransaction == (transactionType == 0)).OrderBy(q => q.Date);
                }

                var list = result
                    .OrderByDescending(q => q.Date)
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Account.AccountName.ToLower().Contains(param.sSearch.ToLower()))
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(q => new IConvertible[] {
                        count++,
                        //q.AccountId,
                        q.Account.AccountName,
                        //customerApi.Get(q.AccountId).Name,
                        membershipCardApi.GetMembershipCardById(accountApi.Get(q.AccountId)
                            .MembershipCardId.GetValueOrDefault()).CustomerName,
                        q.Amount,
                        q.Date.ToString("dd/MM/yyyy HH:mm:ss"),
                        q.Notes,
                        q.Status,
                        q.IsIncreaseTransaction
                    });
                var list2 = result
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Account.AccountName.ToLower().Contains(param.sSearch.ToLower()))
                    .ToList()
                    .Select(q => new IConvertible[] {
                        q.Amount,
                        q.Status,
                        q.IsIncreaseTransaction
                    });

                var totalRecords = result.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = list,
                    totalData = list2,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                //return Json(new { success = false, message = "Có lỗi xảy ra, xin vui lòng liện hệ admin" }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public ActionResult Create(int Id)
        {
            TransactionEditViewModel model = new TransactionEditViewModel();
            model.AccountId = Id;
            model.IsIncreaseTransaction = true;
            return View(model);
        }
        //TODO: AccountViewModel chưa có MembershipCard để lấy CustomerID
        //[HttpPost]
        //public async Task<ActionResult> Create(TransactionEditViewModel model)
        //{
        //    if (!this.ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var transactionApi = new TransactionApi();
        //    var accountApi = new AccountApi();
        //    await transactionApi.CreateTransactionAsync(model);
        //    var account = await accountApi.GetAsync(model.AccountId);
        //    return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
        //}

        public async Task<ActionResult> Edit(int Id)
        {
            var transactionApi = new TransactionApi();
            var model = new TransactionEditViewModel(await transactionApi.GetAsync(Id), this.Mapper);
            return View(model);
        }
        //TODO: accountviewmodel chua co Membershipcard
        //[HttpPost]
        //public async Task<ActionResult> Edit(TransactionEditViewModel model)
        //{
        //    if (!this.ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var transactionApi = new TransactionApi();
        //    var accountApi = new AccountApi();
        //    await transactionApi.EditTransactionAsync(model);
        //    var account = await accountApi.GetAsync(model.AccountId);
        //    return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
        //}
    }

    public class TransactionEnumViewModel
    {
        public dynamic Value { get; set; }
        public string Name { get; set; }
    }
}