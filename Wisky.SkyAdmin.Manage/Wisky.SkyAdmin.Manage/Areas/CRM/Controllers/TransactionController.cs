﻿using HmsService.Models;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.CRM.Controllers
{
    public class TransactionController : BaseController
    {
        // GET: CRM/Transaction
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransactionDetail(int id, int customerId)
        {
            var accApi = new AccountApi();
            var membershipCardApi = new MembershipCardApi();
            var customerApi = new CustomerApi();

            var customer = customerApi.GetCustomerById(customerId);
            CustomerEditViewModel model = new CustomerEditViewModel(customer.ToEntity());
            model.MembershipCard = membershipCardApi.GetMembershipCardById(id);
            model.AllAccounts = accApi.GetAccountsByMembershipCardId(id).Select(a => new SelectListItem
            {
                Text = a.AccountName,
                Value = a.AccountID.ToString(),
            });

            return View(model);
        }


        public JsonResult LoadTransaction(JQueryDataTableParamModel param, int brandId, int transactionStatus, int transactionType, string startDate, string endDate)
        {
            var transactionApi = new TransactionApi();
            var accountApi = new AccountApi();
            var customerApi = new CustomerApi();
            var membershipCardApi = new MembershipCardApi();


            var startTime = startDate.ToDateTime().GetStartOfDate();
            var endTime = endDate.ToDateTime().GetEndOfDate();
            var transaction = transactionApi.GetAllBrandByTimeRange(brandId, startTime, endTime).AsEnumerable<TransactionViewModel>();
            var customerAPi = new CustomerApi();
            //if (accountId != -1)
            //{
            //    transaction = transaction.Where(q => q.AccountId == accountId);
            //}
            try
            {
                var count = param.iDisplayStart + 1;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                   transaction = transaction.Where(a => membershipCardApi.Get(accountApi.Get(a.AccountId).MembershipCardId.Value).MembershipCardCode.Contains(param.sSearch.ToLower())
                    );
                }
                if (transactionStatus != -1)
                {
                    transaction = transaction.Where(a => a.Status == transactionStatus);
                }
                if (transactionType != -1)
                {
                    transaction = transaction.Where(a => a.IsIncreaseTransaction == (transactionType ==0));
                }
                var totalRecords = transaction.Count();
                var totalDisplayRecords = transaction.Count();
                var rs = transaction
                    .OrderByDescending(q => q.Date)
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                    {
                            count++,
                            membershipCardApi.GetMembershipCardById(accountApi.Get(a.AccountId)
                            .MembershipCardId.GetValueOrDefault()).MembershipCardCode,                      
                            membershipCardApi.GetMembershipCardById(accountApi.Get(a.AccountId)
                            .MembershipCardId.GetValueOrDefault()).CustomerName,
                            a.Amount,
                            a.Date.ToString("dd/MM/yyyy HH:mm:ss"),
                            string.IsNullOrWhiteSpace(a.Notes) ? "---" : a.Notes,
                            a.StoreId <=0 ? "Hệ Thống" : (Utils.GetStore(a.StoreId).Name), 
                            a.Status,
                            a.Id,
                            membershipCardApi.GetMembershipCardById(accountApi.Get(a.AccountId)
                            .MembershipCardId.GetValueOrDefault()).CustomerId,
                            a.IsIncreaseTransaction
                           // a.Id,
                            //a.Account.Type
                    });
                var list2 = transaction
                    .ToList()
                    .Select(a => new IConvertible[]
                    {
                            a.Amount,
                            a.Status,
                            a.IsIncreaseTransaction
                    });
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalDisplayRecords,
                    aaData = rs,
                    totalData = list2,
                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "Error" });
            }
        }

        //public ActionResult Create(int Id)
        //{
        //    TransactionEditViewModel model = new TransactionEditViewModel();
        //    model.AccountId = Id;
        //    model.IsIncreaseTransaction = true;
        //    return View(model);
        //}
        public ActionResult CreateByAccountId(int id)
        {
            var model = new TransactionEditViewModel();
            var accountApi = new AccountApi();
            var account = accountApi.GetAccountEntityById(id);
            model.AccountId = account.AccountID;
            model.Amount = (int)account.Balance;
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> CreateByAccountId(TransactionEditViewModel model)
        {
            var brandId = int.Parse(RouteData.Values["brandId"].ToString());
            var transactionApi = new TransactionApi();
            var accountApi = new AccountApi();
            var membershipCardApi = new MembershipCardApi();
            var account = await accountApi.GetAsync(model.AccountId);
            if (model.Amount == 0 || model.Amount.ToString().Length > 9)
            {
                return Json(new { success = false, message = "Số tiền tăng hoặc giảm cần lớn hơn 0 và cần ít hơn 9 chữ số!" },
                    JsonRequestBehavior.AllowGet);
            }
            model.Amount = (decimal)model.Amount;
            model.Status = (int)TransactionStatus.New;
            var customerId = membershipCardApi.GetMembershipCardById(account.MembershipCardId.GetValueOrDefault()).CustomerId;
            var customer = (new CustomerApi()).GetCustomerEntityById(customerId);
            await transactionApi.CreateTransactionAsync(model, customer, brandId);
            account = await accountApi.GetAsync(model.AccountId);
            return Json(new { success = true, message = "Tạo giao dịch thành công!", balance = account.Balance }, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Create(int Id)
        //{
        //    TransactionEditViewModel model = new TransactionEditViewModel();
        //    AccountApi accountApi = new AccountApi();
        //    model.ActiveAccounts = accountApi.GetActiveAccountByCusId(Id);
        //    model.IsIncreaseTransaction = true;
        //    return PartialView(model);
        //}

        public ActionResult Create(int id)
        {
            TransactionEditViewModel model = new TransactionEditViewModel();
            AccountApi accountApi = new AccountApi();
            model.ActiveAccounts = accountApi.GetActiveAccountByCardId(id);
            model.IsIncreaseTransaction = true;
            return PartialView(model);
        }

        //[HttpPost]
        //public async Task<ActionResult> Create(TransactionEditViewModel model)
        //{
        //    if (!this.ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var transactionApi = new TransactionApi();
        //    var accountApi = new AccountApi();
        //    var account = await accountApi.GetAsync(model.AccountId);
        //    if ((account.Balance == 0 || account.Balance==null ||account.Balance < model.Amount) && model.IsIncreaseTransaction==false)
        //    {
        //        TempData["msg"] = "<script>alert('Số dư của tài khoản hiện đang bé hơn số tiền giảm!');</script>";

        //        return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
        //    }
        //    await transactionApi.CreateTransactionAsync(model);
        //    return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
        //}

        [HttpPost]
        public async Task<JsonResult> Create(TransactionEditViewModel model)
        {
            var transactionApi = new TransactionApi();
            var accountApi = new AccountApi();
            var storeApi = new StoreApi();
            var store = storeApi.Get(model.StoreId);
            var account = await accountApi.GetAsync(model.AccountId);
            if (model.Amount == 0 || model.Amount.ToString().Length > 9)
            {
                return Json(new { success = false, message = "Số tiền tăng hoặc giảm cần lớn hơn 0 và cần ít hơn 9 chữ số!" },
                    JsonRequestBehavior.AllowGet);
            }
            //if ((account.Balance == null || account.Balance < double.Parse(model.Amount.ToString())) && model.IsIncreaseTransaction == false)
            //{
            //    return Json(new { success = false, message = "Số dư của tài khoản hiện đang bé hơn số tiền giảm!" },
            //        JsonRequestBehavior.AllowGet);
            //}
            model.Amount = (decimal)model.Amount;
            model.StoreId = store.ID;
            model.Status = (int)TransactionStatus.Approve;
            var customerId = (new MembershipCardApi()).GetMembershipCardById(account.MembershipCardId.GetValueOrDefault()).CustomerId;
            var customer = (new CustomerApi()).GetCustomerEntityById(customerId);
            await transactionApi.CreateTransactionAsync(model, customer, model.BrandId);
            account = await accountApi.GetAsync(model.AccountId);
            return Json(new { success = true, message = "Tạo giao dịch thành công!", balance = account.Balance }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int Id)
        {
            var transactionApi = new TransactionApi();
            var model = new TransactionEditViewModel(transactionApi.Get(Id), this.Mapper);
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> Approve(int transactionId, bool isApproved)
        {
            try
            {
                var transactionApi = new TransactionApi();
                var model = transactionApi.Get(transactionId);
                if (isApproved)
                {
                    model.Status = (int)TransactionStatus.Approve;
                    var accountApi = new AccountApi();
                    var account = accountApi.Get(model.AccountId);
                    var message = "";
                    var membershipCardApi = new MembershipCardApi();
                    if (account!= null)
                    {
                        var membershipCard = membershipCardApi.GetMembershipCardById(account.MembershipCardId.Value);
                        if (model.IsIncreaseTransaction)
                        {
                            account.Balance += model.Amount;
                            message = "Passio Coffee" + ". "
                            + DateTime.Now.ToShortDateString() + ";" + DateTime.Now.ToString("HH:mm") + ". "
                            + "TK: xxxxx" + membershipCard.MembershipCardCode.Substring(5, membershipCard.MembershipCardCode.Length - 5) + ". "
                            + "PS:" + "+"+ Utils.ToMoney((double)model.Amount) + " VNĐ" + "."
                            + "Tại: " + (new StoreApi().GetStoreById(model.StoreId)).Name + "."
                            + "Số dư tài khoản: " + Utils.ToMoney((double)account.Balance) + " VNĐ";
                        } else
                        {
                            account.Balance -= model.Amount;
                            message = "Passio Coffee" + ". "
                           + DateTime.Now.ToShortDateString() + ";" + DateTime.Now.ToString("HH:mm") + ". "
                           + "TK: xxxxx" + membershipCard.MembershipCardCode.Substring(5, membershipCard.MembershipCardCode.Length - 5) + ". "
                           + "PS:" + "-" + Utils.ToMoney((double)model.Amount) + " VNĐ" + "."
                           + "Tại: " + (new StoreApi().GetStoreById(model.StoreId)).Name + "."
                           + "Số dư tài khoản: " + Utils.ToMoney((double)account.Balance) + " VNĐ";
                        }
                        await accountApi.EditAsync(account.AccountID, account);
                        var customerApi = new CustomerApi();
                        var customer = customerApi.Get(membershipCard.CustomerId);
                        Utils.SendSMS(customer.Phone, message, model.BrandId);
                    }
                }
                else
                {
                    model.Status = (int)TransactionStatus.Cancel;
                }
                await transactionApi.EditAsync(transactionId, model);
                return Json(new { success = true });
            }
            catch (System.Exception e)
            {
                return Json(new { success = false });
            }
        }

        public JsonResult CheckAccount(int accountId)
        {
            var accountApi = new AccountApi();
            var result = accountApi.GetAccountEntityById(accountId);
            var balance = (int)result.Balance;
            return Json(new
            {
                typeId = result.Type,
                balance = balance
            }, JsonRequestBehavior.AllowGet);
        }
        //TODO: AccountViewModel chưa co membershipcard để lấy CustomerID
        [HttpPost]
        public async Task<ActionResult> Edit(TransactionEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            var transactionApi = new TransactionApi();
            var accountApi = new AccountApi();

            var account = await accountApi.GetAsync(model.AccountId);
            if (model.IsIncreaseTransaction == false && account.Balance < model.Amount)
            {
                //return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
                return RedirectToAction("Index", "Transaction");
            }
            await transactionApi.EditTransactionAsync(model);
            //return RedirectToAction("CustomerDetail", "Customer", new { Id = account.CustomerID });
            return RedirectToAction("Index", "Transaction");
        }
        public async Task<JsonResult> Delete(int? Id)
        {
            try
            {
                var transactionApi = new TransactionApi();
                var model = await transactionApi.GetAsync(Id);
                if (model == null)
                {
                    return Json(new { success = false });
                }
                await transactionApi.DeleteAsync(model);
                return Json(new { success = true });
            }
            catch (System.Exception e)
            {
                return Json(new { success = false });
            }
        }
    }
}