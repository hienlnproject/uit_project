﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.SysAdmin.Controllers
{
    public class ReDateReportController : Controller
    {
        // GET: SysAdmin/ReDateReport
        public ActionResult Index()
        {
            return this.View();
        }
        
        public ActionResult ViewReport(string date, string brandId)
        {
            try
            {
                int id = int.Parse(brandId);
                var dateSearch = Utils.ToDateTime(date);
                var reportDateApi = new DateReportApi();
                var orderApi = new OrderApi();

                var listReport = reportDateApi.GetDateReportByDateAndBrand(dateSearch.GetStartOfDate(), dateSearch.GetEndOfDate(), id).ToList();
                var count = 0;

                var returnResult = listReport.Select(q => new IConvertible[]{
                    count++,
                    q.StoreID,
                    q.TotalOrder,
                    orderApi.CountFinishOrderByDate(dateSearch.GetStartOfDate(), dateSearch.GetEndOfDate(), q.StoreID),
                    0,  //true-false
                    q.ID
                }).ToList();
                return Json(new { success = true, listReport = returnResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ReCreateReport(string date, string reportId, string storeId)
        {
            try
            {
                //Parse
                int reportIdint = int.Parse(reportId);
                int storeIdint = int.Parse(storeId);
                var reportDate = Utils.ToDateTime(date);

                var storeService = DependencyUtils.Resolve<IStoreService>();
                var orderService = DependencyUtils.Resolve<IOrderService>();
                var orderDetailService = DependencyUtils.Resolve<IOrderDetailService>();
                var productService = DependencyUtils.Resolve<IProductService>();
                var productItemService = DependencyUtils.Resolve<IProductItemService>();
                var inventoryReceiptService = DependencyUtils.Resolve<IInventoryReceiptService>();
                var reportService = DependencyUtils.Resolve<IReportService>();
                var dateReportApi = new DateReportApi();
                var dateProductApi = new DateProductApi();

                //Set date
                var fromDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetStartOfDate();
                var toDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetEndOfDate();

                //Get store
                var store = storeService.GetStoreById(storeIdint);

                //Get orders
                var orders = new List<Order>();
                if (store.Type == (int)StoreTypeEnum.Hotel)
                {
                    orders = orderService.GetAllHotelOrdersByCheckOutDate(fromDate, toDate, store.ID)
                    .Where(a => a.RentStatus == (int)RentStatusEnum.Leave).ToList();
                }
                else
                {
                    orders = orderService.GetOrdersByTimeRange(store.ID, fromDate, toDate)
                    .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).ToList();
                }

                //Get order details
                var orderDetails =
                    orderDetailService.GetOrderDetailsByTimeRange(fromDate, toDate, store.ID)
                    .Where(a => a.Order.OrderType != (int)OrderTypeEnum.DropProduct && a.Order.OrderStatus == (int)OrderStatusEnum.Finish).ToList();

                //Get products
                var dateProducts =
                    orderDetails.GroupBy(a => a.ProductID)
                        .Join(productService.GetAllProducts(), a => a.Key, a => a.ProductID, (a, b) => new DateProduct()
                        {
                            ProductId = a.Key,
                            StoreID = store.ID,
                            Quantity = a.Sum(c => c.Quantity),
                            Date = reportDate,
                            TotalAmount = a.Sum(c => c.TotalAmount),
                            FinalAmount = a.Sum(c => c.FinalAmount),
                            Discount = a.Sum(c => c.Discount),
                            ProductName_ = b.ProductName,
                            Product = b,
                            CategoryId_ = b.ProductCategory.CateID
                        }).ToList();

                //Create DateReport
                var dateReport = new DateReport();
                if (store.Type == (int)StoreTypeEnum.Hotel)
                {
                    dateReport.StoreID = store.ID;
                    dateReport.CreateBy = "system";
                    dateReport.Status = (int)DateReportStatusEnum.Approved;
                    dateReport.Date = reportDate.GetEndOfDate();
                    dateReport.Discount = orders.Sum(a => a.Discount);
                    dateReport.DiscountOrderDetail = orders.Sum(a => a.DiscountOrderDetail);
                    dateReport.TotalAmount = orders.Sum(a => a.TotalAmount);
                    dateReport.FinalAmount = orders.Sum(a => a.FinalAmount);
                    dateReport.TotalCash = 0;
                    dateReport.TotalOrder = orders.Count();
                    dateReport.TotalOrderAtStore = 0;
                    dateReport.TotalOrderTakeAway = 0;
                    dateReport.TotalOrderDelivery = 0;
                    dateReport.TotalOrderDetail = orders.Sum(a => a.OrderDetails.Sum(b => b.FinalAmount));
                    dateReport.TotalOrderFeeItem = (int)orders.Sum(a => a.OrderFeeItems.Sum(b => b.TotalAmount));
                }
                else
                {
                    dateReport.StoreID = store.ID;
                    dateReport.CreateBy = "system";
                    dateReport.Status = (int)DateReportStatusEnum.Approved;
                    dateReport.Date = reportDate.GetEndOfDate();
                    dateReport.Discount = orders.Sum(a => a.Discount);
                    dateReport.DiscountOrderDetail = orders.Sum(a => a.DiscountOrderDetail);
                    dateReport.TotalAmount = orders.Sum(a => a.TotalAmount);
                    dateReport.FinalAmount = orders.Sum(a => a.FinalAmount);
                    dateReport.TotalCash = 0;
                    dateReport.TotalOrder = orders.Count();
                    dateReport.TotalOrderAtStore = orders.Count(a => a.OrderType == (int)OrderTypeEnum.AtStore);
                    dateReport.TotalOrderTakeAway = orders.Count(a => a.OrderType == (int)OrderTypeEnum.TakeAway);
                    dateReport.TotalOrderDelivery = orders.Count(a => a.OrderType == (int)OrderTypeEnum.Delivery);
                    dateReport.TotalOrderDetail = 0;
                    dateReport.TotalOrderFeeItem = 0;
                }

                var oldDateReport = dateReportApi.GetDateReportById(reportIdint);
                var listDateProduct = dateProductApi.GetDateProductByTimeRange(reportDate, storeIdint);
                
                //var reportTracking = new ReportTracking()
                //{
                //    Date = Utils.GetCurrentDateTime(),
                //    IsUpdate = false,
                //    UpdatePerson = "system",
                //    StoreId = store.ID,
                //};
                //var compositionsStatistic = dateProducts.SelectMany(a => a.Product.ProductItemCompositionMappings.Select(b => new Tuple<ProductItemCompositionMapping, int>(b, a.Quantity)))
                //    .GroupBy(a => a.Item1.ItemID);
                //var dateItemProduct = compositionsStatistic.Join(productItemService.GetProductItems(), a => a.Key, a => a.ItemID, (a, b) => new DateProductItem
                //{
                //    StoreId = store.ID,
                //    Date = reportDate,
                //    ProductItemID = a.Key,
                //    ProductItemName = b.ItemName,
                //    Quantity = (int)a.Sum(c => c.Item2 * c.Item1.Quantity),
                //    Unit = b.Unit
                //}).AsQueryable();
                //var inventoryReceipts = inventoryReceiptService.GetInventoryReceiptByTimeRange(store.ID, fromDate, toDate).AsQueryable();

                string user = "";
                try
                {
                    user = (System.Web.HttpContext.Current == null) ? "quanly" : System.Web.HttpContext.Current.User.Identity.Name;
                }
                catch (Exception)
                {
                    user = "noname";
                }

                var result = reportService.ReUpdateDateReport(oldDateReport, dateReport, listDateProduct, orderDetails, store, orders, user);


            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}