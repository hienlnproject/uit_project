﻿using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook
{
    public class FacebookAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Facebook";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Facebook_default",
                "{brandId}/Facebook/{storeId}/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}