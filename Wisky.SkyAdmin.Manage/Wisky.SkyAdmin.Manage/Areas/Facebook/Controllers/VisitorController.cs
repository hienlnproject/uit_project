﻿using Facebook;
using HmsService.Models;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook.Controllers
{
    public class VisitorController : Controller
    {
        // GET: Facebook/Visitor
        StoreApi storeApi = new StoreApi();
        FacebookClient fbApp = new FacebookClient();
        CommentApi commentApi = new CommentApi();
        CustomerApi customerApi = new CustomerApi();
        public ActionResult Index(int storeId, int brandId)
        {
            if (storeId != 0)
            {
                var store = storeApi.Get(storeId);
                ViewBag.storeId = store.ID.ToString();
                ViewBag.storeName = store.Name;
                ViewBag.brandId = brandId;
                ViewBag.fbAppId = FacebookAppSetting.AppId;
                if (store.FbAccessToken != null)
                {
                    ViewBag.isConnected = true;
                    ViewBag.FbAccessToken = store.FbAccessToken;
                }
                else
                {
                    ViewBag.isConnected = false;
                };
                return View();
            }

            ViewBag.storeId = "0";
            ViewBag.storeName = "Tổng quan hệ thống";
            ViewBag.brandId = brandId;

            return View();
        }
        public JsonResult GetUserList(JQueryDataTableParamModel param, int storeId, DateTime? startDate, DateTime? endDate)
        {
            var shopId = storeApi.GetStoreById(storeId);
            var listUser = commentApi.GetCommentUserList(param, shopId.FbShopId, startDate, endDate);
            var totalRecords = listUser.Count();
            var data = listUser.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
            var displayRecords = data.Count();

            ShopViewModel shop = storeApi.GetShop(shopId.FbShopId);
            try
            {
                FacebookClient fbApp = new FacebookClient(shop.FbToken);
                dynamic fbUser;
                for (int i = 0; i < data.Count(); i++)
                {
                    try
                    {
                        fbUser = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].UserFBId).ToString());
                        data[i].UserName = fbUser.name;
                        try
                        {
                            CustomerViewModel registedCustomer = customerApi.GetCustomerByFacebookId(data[i].UserFBId, shopId.FbShopId);
                            if (registedCustomer.Name != null)
                            {
                                data[i].IsCustomer = true;
                                data[i].UserName = data[i].UserName + " (" + registedCustomer.Name + ")";
                            }
                        }
                        catch (Exception)
                        {
                            data[i].IsCustomer = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        data[i].UserName = "Người dùng không tồn tại";
                        Debug.WriteLine(ex.Message);
                    }
                }
                var dataList = data.Select(q => new {
                    q.UserName,
                    q.TotalComment,
                    q.UnreadComment,
                    q.ListIntentNumber,
                    q.ListStatusNumber,
                    q.UserFBId,
                });
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,//displayRecords,
                    aaData = dataList,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }


        }
    }
}