﻿using Facebook;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Wisky.SkyAdmin.Manage.Controllers;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook.Controllers
{
    public class MessengerController : DomainBasedController
    {
        FacebookClient fbApp = new FacebookClient();
        PostApi postApi = new PostApi();
        StoreApi storeApi = new StoreApi();
        CommentApi commentApi = new CommentApi();
        CustomerApi customerApi = new CustomerApi();

        // GET: Facebook/Messenger
        public ActionResult Index(int storeId, int brandId)
        {
            var store = storeApi.Get(storeId);
            ViewBag.storeId = store.ID.ToString();
            ViewBag.storeName = store.Name;
            ViewBag.brandId = brandId;

            return View();
        }

        #region Quang

        public JsonResult GetAllCommentByUser(JQueryDataTableParamModel param, string SenderId, int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            try
            {
                var listComment = commentApi.GetAllCommentByUser(param, SenderId);
                var totalRecords = listComment.Count();
                //var data = listComment.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                var data = listComment.ToList();

                ShopViewModel shop = storeApi.GetShop(FbshopId.FbShopId);

                if (shop != null)
                {
                    FacebookClient fbApp = new FacebookClient(shop.FbToken);
                    dynamic postContent;
                    dynamic fbUser;
                    dynamic commentContent;
                    CustomerViewModel registedCustomer;

                    for (int i = 0; i < data.Count(); i++)
                    {
                        //Get Post Content infor
                        if (data[i].PostContent != null && data[i].PostContent.Length != 0)
                        {
                            data[i].PostContent = TruncateLongString(data[i].PostContent, 40);
                        }
                        else
                        {
                            try
                            {
                                dynamic paramFB = new ExpandoObject();
                                paramFB.locale = "vi_VI";
                                postContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].PostId, paramFB).ToString());
                                if (postContent.story == null || postContent.message == null)
                                {
                                    data[i].PostContent = TruncateLongString(postContent.story + postContent.message, 40);
                                }
                                else
                                {
                                    data[i].PostContent = TruncateLongString(postContent.story + ": " + postContent.message, 40);
                                }
                            }
                            catch (Exception e)
                            {
                                data[i].PostContent = "Bài đăng không tồn tại.";
                                Debug.WriteLine(e.Message);
                            }
                        }


                        //Get comment infor

                        if (data[i].LastContent != null && data[i].LastContent.Length != 0)
                        {
                            data[i].CommentContent = TruncateLongString(data[i].LastContent,100);
                        }
                        else
                        {
                            try
                            {
                                dynamic paramFB = new ExpandoObject();
                                paramFB.fields = "from,id,message";
                                commentContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].Id, paramFB).ToString());
                                if (commentContent != null && !string.IsNullOrEmpty(commentContent.message))
                                {
                                    data[i].CommentContent = TruncateLongString(commentContent.message, 100);
                                }
                                else
                                {
                                    data[i].CommentContent = "Bấm để xem nội dung";
                                }

                            }
                            catch (Exception e)
                            {
                                data[i].CommentContent = "Bình luận không tồn tại.";
                                Debug.WriteLine(e.Message);
                            }
                        }

                        //Get Sender Information
                        registedCustomer = customerApi.GetCustomerByFacebookId(data[i].SenderFbId, FbshopId.FbShopId);


                        try
                        {
                            fbUser = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].SenderFbId).ToString());
                            data[i].SenderName = fbUser.name;
                        }
                        catch (Exception ex)
                        {
                            data[i].SenderName = "Người dùng không tồn tại";
                            Debug.WriteLine(ex.Message);
                        }
                        if (registedCustomer != null)
                        {
                            if (registedCustomer.Name != null)
                            {
                                data[i].SenderName = data[i].SenderName + " (" + registedCustomer.Name + ")";
                            }
                            data[i].IsCustomer = true;
                        }
                        else
                        {
                            data[i].IsCustomer = false;

                        }

                        //Get Intent infor
                        if (data[i].IntentId != null)
                        {
                            data[i].IntentId = data[i].IntentId;
                            data[i].IntentName = "hihi";//intentService.GetIntentNameById(data[i].IntentId.Value);                           
                        }
                        data[i].PostContent = TruncateLongString(data[i].PostContent, 10);
                        data[i].CommentContent = TruncateLongString(data[i].CommentContent, 100);
                        data[i].SenderName = TruncateLongString(data[i].SenderName, 10);
                    }

                }

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Json(new { success = false, e }, JsonRequestBehavior.AllowGet);
            }
        }

        //Cut long string
        public string TruncateLongString(string str, int maxLength)
        {
            if (str != null && str.Length > maxLength)
            {
                return str.Substring(0, maxLength) + " ...";
            }
            else
            {
                return str;
            }

        }

        public JsonResult ReplyComment(string commentId, string message, int storeId)
        {
            try
            {
                var FbshopId = storeApi.GetStoreById(storeId);
                dynamic param = new ExpandoObject();

                param.access_token = FbshopId.FbAccessToken;
                param.message = message;
                var result = fbApp.Post(commentId + "/comments", param);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetCommentRead(string commentId)
        {
            try
            {
                Debug.WriteLine("id of new comment " + commentId);
                var result = commentApi.SetIsRead(commentId);
                Debug.WriteLine("and result " + result);
                //SignalRAlert.AlertHub.SendNotification((string)Session["ShopId"]);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult DeleteComment(string commentId, int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            fbApp.AccessToken = FbshopId.FbAccessToken;
            try
            {
                List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId).ToList();
                if (commentList != null)
                {
                    foreach (Comment c in commentList)
                    {
                        commentApi.SetStatus(c.CommentId, (int)CommentStatus.DELETED);
                    }
                }
                var result = fbApp.Delete(commentId);
                commentApi.SetStatus(commentId, (int)CommentStatus.DELETED);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult HideComment(string commentId, int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            fbApp.AccessToken = FbshopId.FbAccessToken;
            dynamic param = new ExpandoObject();
            try
            {
                param.access_token = FbshopId.FbAccessToken;
                param.is_hidden = true;
                List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId).ToList();
                if (commentList != null)
                {
                    foreach (Comment c in commentList)
                    {
                        commentApi.SetStatus(c.CommentId, (int)CommentStatus.HIDDEN);
                    }
                }

                var result = fbApp.Post(commentId, param);
                commentApi.SetStatus(commentId, (int)CommentStatus.HIDDEN);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UnHideComment(string commentId, int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            fbApp.AccessToken = FbshopId.FbAccessToken;
            dynamic param = new ExpandoObject();
            try
            {
                param.access_token = FbshopId.FbAccessToken;
                param.is_hidden = false;
                List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId).ToList();
                if (commentList != null)
                {
                    foreach (Comment c in commentList)
                    {
                        commentApi.SetStatus(c.CommentId, (int)CommentStatus.SHOWING);
                    }
                }

                var result = fbApp.Post(commentId, param);
                commentApi.SetStatus(commentId, (int)CommentStatus.SHOWING);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetUserAvatar(string userid, int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            fbApp.AccessToken = FbshopId.FbAccessToken;
            dynamic param = new ExpandoObject();
            param.access_token = FbshopId.FbAccessToken;
            dynamic result = fbApp.Get(userid + "/picture?type=square&redirect=false", param);
            return Content(result.data.url);
        }

        public ActionResult GetPost(string postId, int storeId)
        {
            var Fbshop = storeApi.GetStoreById(storeId);
            string accessToken = Fbshop.FbAccessToken;
            dynamic postParam = new ExpandoObject();
            postParam.fields = "message,from,full_picture,story";
            postParam.locale = "vi_VI";
            try
            {

                FacebookClient fbAppWithAccTok = new FacebookClient(accessToken);

                //get post info

                var fbPost = fbAppWithAccTok.Get(postId, postParam);

                PostViewModel postmodel = new PostViewModel();
                postmodel.from = fbPost.from.name;
                //priority: message (attachment) > story (attachment) > attachment
                if (fbPost.message != null && fbPost.message != "") postmodel.message = fbPost.message;
                //message = null => picture or activity
                else
                {
                    postmodel.message = fbPost.story;
                    Debug.WriteLine("story post " + postmodel.message);
                }
                if (fbPost.full_picture != null && fbPost.full_picture != "") postmodel.imageContent = fbPost.full_picture;

                Debug.WriteLine("test post " + postmodel.message);
                return Json(postmodel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllPost(int skip, int take, int storeId)
        {
            try
            {
                dynamic postParam = new ExpandoObject();

                string accessToken = (storeApi.GetStoreById(storeId)).FbAccessToken;
                postParam.access_token = accessToken;
                postParam.fields = "message,from,full_picture,story";
                postParam.locale = "vi_VI";



                Debug.WriteLine("service " + DateTime.Now + " " + storeId);
                var rawPostList = postApi.GetAllPost((storeApi.GetStoreById(storeId)).FbShopId);
                List<PostWithLastestComment> postlist = rawPostList.Skip(skip).Take(take).ToList();

                Debug.WriteLine(postlist.Count() + " end service " + DateTime.Now);
                if (postlist != null)
                {
                    List<PostViewModel> postviewlist = new List<PostViewModel>();
                    PostListViewModel postlistviewmodel = new PostListViewModel();
                    PostViewModel postmodel;
                    postlistviewmodel.postQuan = rawPostList.Count();

                    foreach (PostWithLastestComment post in postlist)
                    {
                        postmodel = new PostViewModel();
                        postmodel.post = post;
                        Debug.WriteLine("last edit of post " + post.LastUpdate);
                        Debug.WriteLine("read of unread? " + post.IsRead);
                        Debug.WriteLine(post.Id + " start  fb " + DateTime.Now);
                        //post deleted

                        Debug.WriteLine("status = 5" + "__" + post.SenderFbId);

                        //priority: message (attachment) > story (attachment) > attachment
                        postmodel.message = post.LastContent;

                        if (post.status != (int)PostStatus.DELETED)
                        {
                            try
                            {
                                dynamic fbPost = fbApp.Get(post.Id, postParam);

                                postmodel.from = fbPost.from.name;
                                //priority: message (attachment) > story (attachment) > attachment
                                if (fbPost.message != null && fbPost.message != "") postmodel.message = fbPost.message;
                                //message = null => picture or activity
                                else
                                {

                                    postmodel.message = fbPost.story;
                                    Debug.WriteLine("story post " + postmodel.message);
                                }
                                if (fbPost.full_picture != null && fbPost.full_picture != "") postmodel.imageContent = fbPost.full_picture;
                            }
                            catch (FacebookApiException)
                            {
                                postApi.SetStatus(postmodel.post.Id, (int)PostStatus.DELETED);
                                dynamic userParam = new ExpandoObject();
                                userParam.access_token = accessToken;
                                userParam.fields = "name";
                                dynamic fbUser = fbApp.Get(post.SenderFbId, userParam);
                                postmodel.from = fbUser.name;
                                postmodel.post.status = (int)PostStatus.DELETED;
                            }
                        }
                        else
                        {
                            FacebookClient fbAppWithAccTok = new FacebookClient(accessToken);
                            dynamic userParam = new ExpandoObject();
                            userParam.access_token = accessToken;
                            userParam.fields = "name";
                            var fbUser = fbAppWithAccTok.Get(post.SenderFbId, userParam);
                            postmodel.from = fbUser.name;
                        }

                        Debug.WriteLine("test post " + postmodel.message);

                        postviewlist.Add(postmodel);
                    }
                    postlistviewmodel.postviewlist = postviewlist;
                    return Json(postlistviewmodel, JsonRequestBehavior.AllowGet);
                }
                else return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPostDetail(string postId, int skip, int take, int storeId)
        { //string shopId, int from, int quantity

            Debug.WriteLine("GetPostDetailFunction");
            var FbshopId = storeApi.GetStoreById(storeId);
            dynamic commentParam = new ExpandoObject();
            dynamic postParam = new ExpandoObject();

            try
            {
                string accessToken = FbshopId.FbAccessToken;
                postParam.fields = "full_picture,created_time,message,from{picture,name},story";
                commentParam.fields = "attachment,from{picture,name,id},message,created_time,can_hide,can_reply_privately";
                postParam.locale = "vi_VI";
                FacebookClient fbAppWithAccTok = new FacebookClient(accessToken);
                Debug.WriteLine("a ccess tok " + accessToken);
                dynamic userParam = new ExpandoObject();
                userParam.access_token = accessToken;
                userParam.fields = "name";


                Post selectedPost = postApi.GetPost(postId, FbshopId.FbShopId);
                if (selectedPost != null)
                {
                    //get post info

                    PostDetailModel postView = new PostDetailModel();
                    postView.SenderFbId = selectedPost.SenderId;
                    Debug.WriteLine("date of the post: " + selectedPost.DateCreated);
                    postView.LastUpdate = selectedPost.DateCreated;
                    postView.Id = postId;
                    postView.Status = selectedPost.Status;
                    postView.IntentId = selectedPost.IntentId;

                    if (postView.Status != (int)PostStatus.DELETED)
                    {
                        try
                        {
                            var fbPost = fbAppWithAccTok.Get(postId, postParam);
                            postView.from = fbPost.from.name;
                            if (postView.IntentId == null) postView.fromAvatar = "https://graph.facebook.com/" + FbshopId.FbShopId + "/picture?type=square";
                            else postView.fromAvatar = "https://graph.facebook.com/" + postView.SenderFbId + "/picture?type=square";
                            postView.postContent = fbPost.message;
                            postView.storyContent = fbPost.story;
                            //posted photo
                            postView.postImageContent = fbPost.full_picture;
                        }
                        catch (FacebookApiException)
                        {
                            postApi.SetStatus(postId, (int)CommentStatus.DELETED);
                            postView.fromAvatar = "https://graph.facebook.com/" + postView.SenderFbId + "/picture?type=square";
                            postView.postContent = selectedPost.LastContent;
                            postView.storyContent = null;
                            var fbUser = fbAppWithAccTok.Get(selectedPost.SenderId, userParam);
                            postView.from = fbUser.name;
                            postView.postImageContent = null;
                            postView.Status = (int)CommentStatus.DELETED;
                        }

                    }
                    else
                    {
                        postView.fromAvatar = "https://graph.facebook.com/" + postView.SenderFbId + "/picture?type=square";
                        postView.postContent = selectedPost.LastContent;
                        postView.storyContent = null;
                        var fbUser = fbAppWithAccTok.Get(selectedPost.SenderId, userParam);
                        postView.from = fbUser.name;
                        postView.postImageContent = null;
                    }
                    List<Comment> commentList = commentApi.GetCommentsParentWithPostId(postId, skip, take);

                    if (commentList != null && commentList.Count > 0)
                    {
                        List<CommentDetailModel> commentviewlist = new List<CommentDetailModel>();
                        CommentDetailModel commentdetailmodel;
                        CommentDetailModel commentdetailmodelRep;


                        #region Get Comment                       
                        foreach (var item in commentList)
                        {
                            commentdetailmodel = new CommentDetailModel();
                            commentdetailmodel.SenderFbId = item.SenderId;
                            commentdetailmodel.Id = item.CommentId;
                            commentdetailmodel.datacreated = item.DateCreated;
                            commentdetailmodel.Status = item.Status;
                            Debug.WriteLine("status of comment: " + commentdetailmodel.Status);
                            commentdetailmodel.IsRead = item.IsRead;
                            commentdetailmodel.IntentId = item.IntentId;
                            commentdetailmodel.nestedCommentQuan = commentApi.GetNestedCommentQuan(item.CommentId);
                            commentApi.SetIsRead(item.CommentId);
                            userParam.access_token = accessToken;


                            commentdetailmodel.commentImageContent = null;
                            commentdetailmodel.commentContent = item.LastContent;


                            //SignalRAlert.AlertHub.SendNotification((string)Session["ShopId"]);

                            commentdetailmodel.avatarUrl = "https://graph.facebook.com/" + item.SenderId + "/picture?type=square";

                            if (commentdetailmodel.Status != (int)CommentStatus.DELETED)
                            {
                                try
                                {
                                    //Debug.WriteLine("status of comment is not 5: " + commentdetailmodel.Status);
                                    var fbComment = fbAppWithAccTok.Get(item.CommentId, commentParam);
                                    if (fbComment.attachment == null) commentdetailmodel.commentImageContent = null;
                                    else commentdetailmodel.commentImageContent = fbComment.attachment.media.image.src;
                                    commentdetailmodel.from = fbComment.from.name;
                                    commentdetailmodel.commentContent = fbComment.message;
                                    commentdetailmodel.canHide = fbComment.can_hide;
                                    commentdetailmodel.canReply = fbComment.can_reply_privately;
                                }
                                catch (FacebookApiException)
                                {
                                    commentApi.SetStatus(commentdetailmodel.Id, (int)CommentStatus.DELETED);
                                    var fbUser = fbAppWithAccTok.Get(commentdetailmodel.SenderFbId, userParam);
                                    commentdetailmodel.from = fbUser.name;
                                    commentdetailmodel.Status = (int)CommentStatus.DELETED;
                                }

                            }
                            else
                            {
                                var fbUser = fbAppWithAccTok.Get(commentdetailmodel.SenderFbId, userParam);
                                commentdetailmodel.from = fbUser.name;
                            }
                            commentviewlist.Add(commentdetailmodel);
                        }
                        #endregion

                        #region Get Replies

                        foreach (var comment in commentviewlist)
                        {
                            List<Comment> nestedcommentlist = commentApi.GetRepliesWithPostId(postId, comment.Id, skip, take);
                            foreach (var replies in nestedcommentlist)
                            {
                                commentdetailmodelRep = new CommentDetailModel();
                                commentdetailmodelRep.Id = replies.CommentId;
                                commentdetailmodelRep.parentId = replies.ParentId;
                                commentdetailmodelRep.datacreated = replies.DateCreated;
                                commentdetailmodelRep.SenderFbId = replies.SenderId;
                                commentdetailmodelRep.Status = replies.Status;
                                commentdetailmodelRep.IsRead = replies.IsRead;
                                commentdetailmodelRep.IntentId = replies.IntentId;

                                commentApi.SetIsRead(replies.CommentId);
                                //SignalRAlert.AlertHub.SendNotification((string)Session["ShopId"]);
                                commentdetailmodelRep.isUnreadRepliesRemain = commentApi.CheckUnreadRemain(commentdetailmodelRep.parentId);
                                commentdetailmodelRep.avatarUrl = "https://graph.facebook.com/" + replies.SenderId + "/picture?type=square";

                                userParam.access_token = accessToken;


                                commentdetailmodelRep.commentContent = replies.LastContent;
                                commentdetailmodelRep.commentImageContent = null;

                                if (replies.Status != (int)CommentStatus.DELETED)
                                {
                                    try
                                    {
                                        var fbComment = fbAppWithAccTok.Get(replies.CommentId, commentParam);
                                        Debug.WriteLine("time  " + commentdetailmodelRep.datacreated);
                                        commentdetailmodelRep.from = fbComment.from.name;
                                        commentdetailmodelRep.commentContent = fbComment.message;
                                        if (fbComment.attachment == null) commentdetailmodelRep.commentImageContent = null;
                                        else commentdetailmodelRep.commentImageContent = fbComment.attachment.media.image.src;
                                        commentdetailmodelRep.canHide = fbComment.can_hide;
                                        commentdetailmodelRep.canReply = fbComment.can_reply_privately;
                                    }
                                    catch (FacebookApiException)
                                    {
                                        commentApi.SetStatus(commentdetailmodelRep.Id, (int)CommentStatus.DELETED);
                                        var fbUser = fbAppWithAccTok.Get(commentdetailmodelRep.SenderFbId, userParam);
                                        commentdetailmodelRep.from = fbUser.name;
                                        commentdetailmodelRep.Status = (int)CommentStatus.DELETED;
                                    }

                                }
                                else
                                {
                                    var fbUser = fbAppWithAccTok.Get(commentdetailmodelRep.SenderFbId, userParam);
                                    commentdetailmodelRep.from = fbUser.name;
                                }
                                comment.ListCommentReply.Add(commentdetailmodelRep);
                            }
                        }
                        #endregion

                        postView.Comments = commentviewlist;
                        //postView.nestedComments = ;
                        postView.commentQuan = commentApi.GetParentCommentQuan(postId);
                        postView.isUnreadParentRemain = commentApi.CheckUnreadParentComment(postId);

                        //check post read state by browsing comments, true = unread
                        if (commentApi.CheckPostUnread(selectedPost.PostId) == true)
                        {
                            postApi.SetPostIsUnread(selectedPost.PostId);
                            postView.isRead = false;
                        }
                        else
                        {
                            postApi.SetPostIsRead(selectedPost.PostId);
                            postView.isRead = true;
                            //SignalRAlert.AlertHub.SendNotification((string)Session["ShopId"]);
                        }
                        //Debug.WriteLine(postView.nestedComments.Count);
                        return Json(postView, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        postView.Comments = null;
                        if (commentApi.CheckPostUnread(postView.Id) == true)
                        {
                            postApi.SetPostIsUnread(postView.Id);
                            postView.isRead = false;
                        }
                        else
                        {
                            postApi.SetPostIsRead(postView.Id);
                            postView.isRead = true;
                        }
                        return Json(postView, JsonRequestBehavior.AllowGet);
                    }
                }
                else return null;
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region BaoTD
        [HttpPost]
        public JsonResult GetCustomerByFacebookId(int brandId, string facebookId)
        {
            CustomerApi customerApi = new CustomerApi();
            var customer = customerApi.GetCustomerByFacebookId(facebookId, brandId);

            if (customer == null)
            {
                return Json(new { success = false, message = "Khách hàng chưa tồn tại." });
            }

            return Json(new
            {
                success = true,
                customer = new
                {
                    Id = customer.CustomerID,
                    Name = customer.Name,
                    Address = customer.Address,
                    Phone = customer.Phone,
                    Email = customer.Email,
                    Description = customer.Description
                }
            });
        }

        [HttpPost]
        public async Task<JsonResult> CreateCustomer(int brandId, CustomerViewModel model)
        {
            int id = 0;
            try
            {
                model.BrandId = brandId;
                var customerApi = new CustomerApi();
                id = await customerApi.CreateCustomerReturnId(model);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = Resources.Message_VI_Resources.CreateCustomerFailed });
            }

            return Json(new
            {
                success = true,
                message = Resources.Message_VI_Resources.CreateCustomerSuccessfully,
                customer = new
                {
                    id = id,
                    name = model.Name,
                    description = model.Description
                }
            });
        }

        public async Task<JsonResult> CreateOrder(OrderViewModel order, int CustomerId, string OrderDetails, int storeId, int brandId)
        {
            var modelOrder = JsonConvert.DeserializeObject<List<OrderDetailViewModel>>(OrderDetails);
            CustomerApi customerApi = new CustomerApi();
            CustomerViewModel customerData = customerApi.GetCustomerByID(CustomerId);
            DateTime time = Utils.GetCurrentDateTime();
            if (string.IsNullOrEmpty(order.FacebookId))
            {
                return Json(new
                {
                    success = false,
                    msg = "Không tìm thấy khách hàng"
                });
            }

            if (order.DeliveryAddress == null)
            {
                return Json(new
                {
                    success = false,
                    msg = "Không tìm thấy địa chỉ"
                });
            }

            double tempTotalAmount = 0;
            double tempFinalAmount = 0;
            double discountOrderDetail = 0;
            foreach (var item in modelOrder)
            {
                tempFinalAmount += item.FinalAmount;
                tempTotalAmount += item.TotalAmount;
                discountOrderDetail += item.Discount;
                item.OrderDate = time;
            }
            order.Payments = new List<PaymentViewModel>();
            order.Payments.Add(new PaymentViewModel
            {
                Amount = tempTotalAmount,
                CurrencyCode = "VND",
                Status = (int)PaymentStatusEnum.New,
                Type = (int)PaymentTypeEnum.Cash,
                FCAmount = (decimal)tempFinalAmount,
                PayTime = time,
            });

            order.CheckInDate = time;
            order.CheckInPerson = User.Identity.Name;
            order.TotalAmount = tempTotalAmount;
            //Calculator VAT amount
            //var vatAmount = (tempFinalAmount * 10 / 100); //VAT 10%
            var vatAmount = 0; //VAT 10%
            order.FinalAmount = tempFinalAmount - vatAmount;
            order.DiscountOrderDetail = discountOrderDetail;
            order.DeliveryStatus = (int)DeliveryStatus.Assigned;
            order.OrderType = (int)OrderTypeEnum.Delivery;
            order.OrderStatus = (int)OrderStatusEnum.New;
            order.InvoiceID = Utils.GetCurrentDateTime().Ticks.ToString() + "-" + storeId;
            order.SourceType = (int)SourceTypeEnum.FaceBook; // Tam thoi de bang 0
            order.SourceID = storeId;
            order.CustomerID = CustomerId;
            order.DeliveryPhone = customerData.Phone;
            order.GroupPaymentStatus = 0; //Tạm thời chưa xài đến
            if (customerData != null)
            {
                customerData.BrandId = brandId;
            }
            var orderApi = new OrderApi();
            var storeApi = new StoreApi();
            OrderCustomEntityViewModel orderEntity = new OrderCustomEntityViewModel()
            {
                Order = order,
                OrderDetails = modelOrder,
                Customer = customerData,
            };
            var rs = 0;
            rs = orderApi.CreateOrderDelivery(orderEntity);
            //NotifyMessage sent Queue, and Pos
            var msg = new NotifyOrder()
            {
                StoreId = (int)order.StoreID,
                //StoreName = store.Name,
                NotifyType = (int)NotifyMessageType.OrderChange,
                Content = "Có đơn hàng mới",
                OrderId = rs,

            };
            await Utils.RequestOrderWebApi(msg);

            if (rs == 0)
            {
                return Json(new
                {
                    success = false,
                    msg = "Tạo đơn hàng không thành công"
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                success = true,
                msg = "Tạo đơn hàng thành công"
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadAllOrder(int brandId, string facebookId)
        {
            CustomerApi customerApi = new CustomerApi();
            OrderApi orderApi = new OrderApi();
            CustomerViewModel customer = customerApi.GetCustomerByFacebookIdBrandId(facebookId, brandId);
            if (customer != null)
            {
                var customerOrders = orderApi.GetCustomerOrder(brandId, customer.CustomerID);
                var result = customerOrders.OrderByDescending(a => a.CheckInDate).Select(a => new IConvertible[] {
                    a.InvoiceID,
                    a.CheckInDate
                });
                return Json(new { success = true, orders = result }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, orders = new List<IConvertible[]>() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult OrderDetail(string invoiceId)
        {
            var orderApi = new OrderApi();
            var order = orderApi.GetOrderViewModelByInvoiceId(invoiceId);

            var orderDetail = order.OrderDetails.OrderBy(q => q.OrderDate);
            int count = 1;
            var totalCount = orderDetail.Count();
            var list = orderDetail.Select(a => new IConvertible[]
            {
                count++,
                a.Product.ProductName,
                a.UnitPrice,
                a.Quantity,
                a.Status,
                a.Discount,
                a.FinalAmount,
                a.RentID,
            }).ToList();

            ViewBag.OrderDetails = Newtonsoft.Json.JsonConvert.SerializeObject(list).Replace("'", "\\'");

            return View(order);
        }
        #endregion

        #region Message
        [HttpGet]
        public ActionResult GetConversationPreviews()
        {
            var listConversationPreview = new List<ConversationPreviewViewModel>();
            var store = ((StoreViewModel)ViewBag.currentStore);
            int storeId = store.ID;
            ConversationApi conversationApi = new ConversationApi();

            //calling fb
            string accessToken = store.FbAccessToken;
            var listConversation = conversationApi.GetConversationsByShopId(store.FbShopId);

            foreach (var c in listConversation)
            {
                var preview = GetConversationPreview(accessToken, c);

                listConversationPreview.Add(preview);
            }

            return Json(listConversationPreview, JsonRequestBehavior.AllowGet);
        }

        private ConversationPreviewViewModel GetConversationPreview(string accessToken, ConversationViewModel c)
        {
            string shopId = ((StoreViewModel)ViewBag.currentStore).FbShopId;
            var preview = new ConversationPreviewViewModel();
            preview.ThreadId = c.Id;
            preview.IsRead = c.IsRead;

            dynamic param = new ExpandoObject();
            param.access_token = accessToken;
            param.fields = "from,created_time,message,attachments";
            dynamic result = fbApp.Get(c.Id + "/messages", param);

            var first = result.data[0];

            param = new ExpandoObject();
            param.access_token = accessToken;
            param.fields = "participants";
            var r = fbApp.Get(c.Id, param);

            var id = "";
            var name = "";
            foreach (var p in r.participants.data)
            {
                if (p.id != shopId)
                {
                    id = p.id;
                    name = p.name;
                }
            }

            preview.UserName = name;
            preview.UserFbId = id;
            preview.CreatedTime = DateTime.Parse(first.created_time);
            preview.RecentMess = first.message;
            if ((preview.RecentMess == null || preview.RecentMess == "") && first.attachments != null)
            {
                preview.AttachmentType = first.attachments.data.Count > 0 ? ("image".Equals(first.attachments.data[0].mime_type.Split('/')[0]) ? "img" : "other") : "";
            }
            //preview.IntentId = c.IntentId;
            preview.IntentId = 1; //Temporary hardcode

            param = new ExpandoObject();
            param.access_token = accessToken;
            param.fields = "";
            param.limit = 1;
            string url = preview.UserFbId + "/picture?type=square&redirect=false";
            dynamic result2 = fbApp.Get(url, param);
            preview.AvatarUrl = result2.data.url;

            return preview;
        }

        [HttpGet]
        public ActionResult GetConversationContent(string conversationId, string userFacebookId)
        {
            CustomerApi customerApi = new CustomerApi();
            var conversationContent = new ConversationContentViewModel();
            var store = ((StoreViewModel)ViewBag.currentStore);
            string accessToken = store.FbAccessToken;
            CustomerViewModel user = customerApi.GetCustomerByFacebookIdBrandId(userFacebookId, store.BrandId.Value);

            dynamic param = new ExpandoObject();
            param.fields = "from,created_time,message,attachments";
            param.locale = "vi-vn";
            fbApp.AccessToken = accessToken;
            dynamic result = fbApp.Get(conversationId + "/messages", param);

            foreach (var mess in result.data)
            {
                var mc = new MessageContentViewModel();
                mc.MessId = mess.id;
                mc.MessContent = mess.message;
                mc.DateCreated = DateTime.Parse(mess.created_time);
                mc.UserId = mess.from.id;
                mc.UserName = mess.from.name;

                if (mess.attachments != null)
                {
                    foreach (var att in mess.attachments.data)
                    {
                        string mime = att.mime_type;
                        string type = mime == null ? "image" : mime.Split('/')[0];
                        var attachment = new AttachmentViewModel();

                        if (type.Equals("image"))
                        {
                            attachment.Type = "img";
                            attachment.Url = att.image_data.url;
                        }
                        else
                        {
                            attachment.Type = "other";
                            attachment.Filename = att.name;
                            attachment.Url = att.file_url;
                        }

                        mc.Attachments.Add(attachment);
                    }
                }

                conversationContent.Messages.Add(mc);
            }

            if (conversationContent.Messages.Count == 25)
            {
                conversationContent.NextUrl = result.paging.next;
            }

            return Json(new { success = true, content = conversationContent, user = user }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetConversationContentNext(int storeId, string url)
        {
            var conversationContent = new ConversationContentViewModel();
            var store = (new StoreApi()).Get(storeId);
            string shopId = store.FbShopId;
            string accessToken = store.FbAccessToken;

            dynamic param = new ExpandoObject();
            param.access_token = accessToken;
            dynamic result = fbApp.Get(url, param);
            if (result.data.Count != 0)
            {
                foreach (var mess in result.data)
                {
                    var mc = new MessageContentViewModel();
                    mc.MessId = mess.id;
                    mc.MessContent = mess.message;
                    mc.DateCreated = DateTime.Parse(mess.created_time);
                    mc.UserId = mess.from.id;
                    mc.UserName = mess.from.name;

                    conversationContent.Messages.Add(mc);
                }

                if (conversationContent.Messages.Count == 25)
                {
                    conversationContent.NextUrl = result.paging.next;
                }
            }
            else
            {
                conversationContent.NextUrl = null;
            }

            return Json(conversationContent, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendMessage(string threadId, string message)
        {
            var store = ((StoreViewModel)ViewBag.currentStore);
            ConversationApi conversationApi = new ConversationApi();
            var conversation = conversationApi.Get(threadId);
            string accessToken = store.FbAccessToken;
            dynamic param = new ExpandoObject();
            try
            {
                param.access_token = accessToken;
                param.message = message;
                dynamic result = fbApp.Post(threadId + "/messages", param);
                MessageContentViewModel messageItem = null;
                if (result.id != null)
                {
                    messageItem = new MessageContentViewModel
                    {
                        Attachments = new List<AttachmentViewModel>(),
                        UserId = store.FbShopId,
                        MessContent = message,
                        DateCreated = DateTime.Now
                    };
                }
                return Json(new { success = true, id = result.id, message = messageItem }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> AddCustomer(int brandId, CustomerViewModel customer)
        {
            try
            {
                CustomerApi customerApi = new CustomerApi();

                await customerApi.CreateCustomer(customer);
                var model = customerApi.GetCustomerByFacebookIdBrandId(customer.FacebookId, brandId);

                return Json(new { success = true, customer = model });
            }
            catch (Exception e)
            {
                return Json(new { success = false, error = e.Message });
            }
        }

        [HttpPost]
        public JsonResult EditCustomer(int brandId, CustomerViewModel customer)
        {
            try
            {
                CustomerApi customerApi = new CustomerApi();
                var model = customerApi.GetCustomerByFacebookIdBrandId(customer.FacebookId, brandId);
                model.Name = customer.Name;
                model.Address = customer.Address;
                model.Phone = customer.Phone;
                model.Description = customer.Description;
                model.Email = customer.Email;

                customerApi.UpdateCustomer(model);

                return Json(new { success = true, customer = model });
            }
            catch (Exception e)
            {
                return Json(new { success = false, error = e.Message });
            }
        }
        #endregion

        public ActionResult LoadItemByCategory(int echo, int cateId, string pattern, int brandId)
        {
            pattern = (pattern ?? "").ToLower();
            var productApi = new ProductApi();
            var listProductCate =
                productApi.GetProductByBrand(brandId)
                    .Where(
                        p =>
                             (cateId <= 0 || p.CatID == cateId) &&
                             (pattern.IsNullOrWhiteSpace() || p.ProductName.ToLower().Contains(pattern.ToLower())));
            return Json(new
            {
                echo = echo,
                products = listProductCate.Select(a => new
                {
                    image = a.PicURL == null ? "Default_product_img.jpg" : "product/" + a.PicURL,
                    name = a.ProductName,
                    id = a.ProductID,
                    discount = a.DiscountPercent,
                    price = a.Price,
                    type = a.ProductType
                }).Take(100)
            });
        }

        public ActionResult getProductDetail(int brandId, int productId)
        {
            var productApi = new ProductApi();
            var product = productApi.GetProductById(productId);
            return PartialView("ProductDetailModal", product);
        }

        public JsonResult LoadCateExtra(int brandId)
        {
            var cateExtraApi = new ProductCategoryApi();
            var cateExtras = cateExtraApi.GetProductCategoriesByBrandId(brandId).Where(c => c.IsExtra == true).Select(q => new
            {
                CategoryId = q.CateID,
                Name = q.CateName,
            });
            return Json(cateExtras); ;
            //var cateExtraApi = new CategoryExtraMappingApi();
            //var categoryExtras = cateExtraApi.GetProductCategoryExtra(primaryProductId);
            //return Json(new
            //{
            //    extraGroup = categoryExtras.Select(a => new
            //    {
            //        groupId = a.CateID,
            //        groupName = a.CateName,
            //    })
            //});
        }

        public JsonResult LoadAllCategory(int brandId)
        {
            var productCategoryApi = new ProductCategoryApi();
            var productCategories = productCategoryApi.GetProductCategoriesByBrandId(brandId)
                .Select(q => new
                {
                    CategoryId = q.CateID,
                    Name = q.CateName,
                });
            return Json(productCategories);
        }

        public ActionResult ProductEditModal(int brandId)
        {
            return View();
        }
    }
}