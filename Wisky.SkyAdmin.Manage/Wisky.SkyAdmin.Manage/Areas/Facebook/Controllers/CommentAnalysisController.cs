﻿using Facebook;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook.Controllers
{
    public class CommentAnalysisController : Controller
    {
        FacebookClient fbApp = new FacebookClient();
        PostApi postApi = new PostApi();
        StoreApi storeApi = new StoreApi();
        CommentApi commentApi = new CommentApi();
        CustomerApi customerApi = new CustomerApi();

        // GET: Facebook/CommentAnalysis
        public ActionResult Index(int storeId, int brandId)
        {

            if (storeId != 0)
            {
                var store = storeApi.Get(storeId);
                ViewBag.storeId = store.ID.ToString();
                ViewBag.storeName = store.Name;
                ViewBag.brandId = brandId;
                ViewBag.fbAppId = FacebookAppSetting.AppId;
                if (store.FbAccessToken != null)
                {
                    ViewBag.isConnected = true;
                    ViewBag.FbAccessToken = store.FbAccessToken;
                }
                else
                {
                    ViewBag.isConnected = false;
                };
                return View();
            }

            ViewBag.storeId = "0";
            ViewBag.storeName = "Tổng quan hệ thống";
            ViewBag.brandId = brandId;
            return View();
        }

        public JsonResult GetCommentByShopAndCondition(JQueryDataTableParamModel param, string postId ,int storeId, string fbId, int? intentId, int? status, bool? isRead, DateTime? startDate, DateTime? endDate)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            try
            {
                var listModel = commentApi.GetCommentByShopAndCondition(param, postId, fbId, FbshopId.FbShopId, intentId, status, isRead, startDate, endDate);
                var totalRecords = listModel.Count();
                var data = listModel.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                //var displayRecords = data.Count();

                ShopViewModel shop = storeApi.GetShop(FbshopId.FbShopId);

                if (shop != null)
                {
                    FacebookClient fbApp = new FacebookClient(shop.FbToken);
                    dynamic postContent;
                    dynamic fbUser;
                    dynamic commentContent;
                    CustomerViewModel registedCustomer;

                    for (int i = 0; i < data.Count(); i++)
                    {
                        //Get Post Content infor
                        if (data[i].PostContent != null && data[i].PostContent.Length != 0)
                        {
                            data[i].PostContent = TruncateLongString(data[i].PostContent, 40);
                        }
                        else
                        {
                            try
                            {
                                dynamic paramFB = new ExpandoObject();
                                paramFB.locale = "vi_VI";
                                postContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].PostId, paramFB).ToString());
                                if (postContent.story == null || postContent.message == null)
                                {
                                    data[i].PostContent = TruncateLongString(postContent.story + postContent.message, 40);
                                }
                                else
                                {
                                    data[i].PostContent = TruncateLongString(postContent.story + ": " + postContent.message, 40);
                                }
                            }
                            catch (Exception e)
                            {
                                data[i].PostContent = "Bài đăng không tồn tại.";
                                Debug.WriteLine(e.Message);
                            }
                        }


                        //Get comment infor

                        if (data[i].LastContent != null && data[i].LastContent.Length != 0)
                        {
                            data[i].CommentContent = TruncateLongString(data[i].LastContent, 40);
                        }
                        else
                        {
                            try
                            {
                                dynamic paramFB = new ExpandoObject();
                                paramFB.fields = "from,id,message";
                                commentContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].Id, paramFB).ToString());
                                if (commentContent != null && !string.IsNullOrEmpty(commentContent.message))
                                {
                                    data[i].CommentContent = TruncateLongString(commentContent.message, 40);
                                }
                                else
                                {
                                    data[i].CommentContent = "Bấm để xem nội dung";
                                }

                            }
                            catch (Exception e)
                            {
                                data[i].CommentContent = "Bình luận không tồn tại.";
                                Debug.WriteLine(e.Message);
                            }
                        }

                        //Get Sender Information
                        registedCustomer = customerApi.GetCustomerByFacebookId(data[i].SenderFbId, FbshopId.FbShopId);


                        try
                        {
                            fbUser = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].SenderFbId).ToString());
                            data[i].SenderName = fbUser.name;
                        }
                        catch (Exception ex)
                        {
                            data[i].SenderName = "Người dùng không tồn tại";
                            Debug.WriteLine(ex.Message);
                        }
                        if (registedCustomer != null)
                        {
                            if (registedCustomer.Name != null)
                            {
                                data[i].SenderName = data[i].SenderName + " (" + registedCustomer.Name + ")";
                            }
                            data[i].IsCustomer = true;
                        }
                        else
                        {
                            data[i].IsCustomer = false;

                        }

                    //Get Intent infor
                        if (data[i].IntentId != null)
                        {
                            data[i].IntentId = data[i].IntentId;
                            data[i].IntentName = "hihi";//intentService.GetIntentNameById(data[i].IntentId.Value);                           
                        }
                        data[i].PostContent = TruncateLongString(data[i].PostContent, 10);
                        data[i].CommentContent = TruncateLongString(data[i].CommentContent, 10);
                        data[i].SenderName = TruncateLongString(data[i].SenderName, 10);
                    }
                }


                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Json(new { success = false, e }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult ReplyComment(string commentId, string message, int storeId)
        {
            try
            {
                string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
                dynamic param = new ExpandoObject();

                fbApp.AccessToken = accessToken;
                param.access_token = accessToken;
                param.message = message;
                var result = fbApp.Post(commentId + "/comments", param);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetCommentIsRead(string[] commentId)
        {
            try
            {
                for (int i = 0; i < commentId.Length; i++)
                {
                    var result = commentApi.SetIsRead(commentId[i]);
                }
                //SignalRAlert.AlertHub.SendNotification((string)Session["ShopId"]);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult ApprovalComment(string[] commentId)
        {
            try
            {
                for (int i = 0; i < commentId.Length; i++)
                {
                    commentApi.SetStatus(commentId[i], (int)CommentStatus.APPROVED);                  
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult DeleteComment(string[] commentId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            try
            {
                fbApp.AccessToken = accessToken;
                for (int i = 0; i < commentId.Length; i++)
                {
                    var result = fbApp.Delete(commentId[i]);
                    commentApi.SetStatus(commentId[i], (int)CommentStatus.DELETED);
                    List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId[i]).ToList();
                    if (commentList != null && commentList.Count() > 0)
                    {
                        foreach (Comment c in commentList)
                        {
                            commentApi.SetStatus(c.CommentId, (int)CommentStatus.DELETED);
                        }
                    }
                }                            
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult HideComment(string[] commentId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            dynamic param = new ExpandoObject();
            try
            {
                fbApp.AccessToken = accessToken;
                param.access_token = accessToken;
                param.is_hidden = true;
                for (int i = 0; i < commentId.Length; i++)
                {
                    var result = fbApp.Post(commentId[i], param);
                    commentApi.SetStatus(commentId[i], (int)CommentStatus.HIDDEN);
                    List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId[i]).ToList();
                    if (commentList != null && commentList.Count() > 0)
                    {
                        foreach (Comment c in commentList)
                        {
                            try
                            {
                                commentApi.SetStatus(c.CommentId, (int)CommentStatus.HIDDEN);
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }
                    }
                }                      
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UnHideComment(string[] commentId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            dynamic param = new ExpandoObject();
            try
            {
                fbApp.AccessToken = accessToken;
                param.access_token = accessToken;
                param.is_hidden = false;
                for (int i = 0; i < commentId.Length; i++)
                {
                    var result = fbApp.Post(commentId[i], param);
                    commentApi.SetStatus(commentId[i], (int)CommentStatus.SHOWING);
                    List<Comment> commentList = commentApi.GetAllCommentByParentId(commentId[i]).ToList();
                    if (commentList != null && commentList.Count() > 0)
                    {
                        foreach (Comment c in commentList)
                        {
                            commentApi.SetStatus(c.CommentId, (int)CommentStatus.SHOWING);
                        }
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult LoadAllPost(int storeId)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            var listPost = postApi.GetAllPost(FbshopId.FbShopId)
                .Select(q => new
                {
                    PostId = q.Id,
                    Name = TruncateLongString(q.LastContent, 30),
                });
            return Json(listPost);
        }


        public static DateTime ConvertDoubleToDatetime(double timestamp)
        {
            TimeSpan time = TimeSpan.FromSeconds(timestamp);
            DateTime date = new DateTime(1970, 1, 1) + time;
            return date;
        }

        public static double ConvertDatetimeToDouble(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        //Cut long string
        public string TruncateLongString(string str, int maxLength)
        {
            if (str !=null &&str.Length > maxLength)
            {
                return str.Substring(0, maxLength) + " ...";
            }
            else
            {
                return str;
            }

        }


    }
}