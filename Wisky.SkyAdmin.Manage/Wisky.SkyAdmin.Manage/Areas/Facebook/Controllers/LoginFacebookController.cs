﻿using Facebook;
using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook.Controllers
{
    public class LoginFacebookController : Controller
    {
        // GET: Facebook/LoginFacebook
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetLongLiveUserAccessToken(string shortToken)
        {
            FacebookClient fbApp = new FacebookClient(shortToken);
            dynamic parameters = new ExpandoObject();

            parameters.app_id = FacebookAppSetting.AppId;

            //get long-lived 
            dynamic getAccessParam = new ExpandoObject();
            getAccessParam.grant_type = "fb_exchange_token";
            getAccessParam.client_id = FacebookAppSetting.AppId;
            getAccessParam.client_secret = FacebookAppSetting.AppSecret;
            getAccessParam.fb_exchange_token = shortToken;
            var fbToken = fbApp.Get("oauth/access_token", getAccessParam);
            string longToken = fbToken["access_token"];

            return Json(new { longToken = longToken }, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> AddTab(string pageId, string pageAccessToken, int storeId)
        {

            FacebookClient fbApp = new FacebookClient(pageAccessToken);
            dynamic parameters = new ExpandoObject();
            parameters.app_id = FacebookAppSetting.AppId;


            // add tab
            dynamic fbAddTabResult = fbApp.Post(pageId + "/tabs", parameters);
            // add webhook
            var fbWebhookResult = fbApp.Post(pageId + "/subscribed_apps", parameters);

            if ((fbAddTabResult != null && fbAddTabResult["success"] == true) && (fbWebhookResult != null && fbWebhookResult["success"] == true))
            {
                if (storeId != 0)
                {
                    try
                    {
                        var storeApi = new StoreApi();
                        await storeApi.AddFbStore(pageId, pageAccessToken, storeId);
                        return Json(new { success = true, message = "Cập nhật access token thành công" });
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, message = "Edit thất bại thất bại" });
                    }
                }
                return Json(new { success = false, message = "Không tím thấy store" });
            }
            else
            {
                return Json(new { success = false, message = "Chưa tạo tab và đăng ký webhooks" });
            }
        }

    }
}