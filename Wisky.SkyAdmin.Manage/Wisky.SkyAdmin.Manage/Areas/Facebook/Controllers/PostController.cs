﻿using Facebook;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Facebook.Controllers
{
    public class PostController : Controller
    {
        PostApi postApi = new PostApi();
        FacebookClient fbApp = new FacebookClient();
        StoreApi storeApi = new StoreApi();
        CustomerApi customerApi = new CustomerApi();

        // GET: Facebook/Post
        public ActionResult Index(int storeId, int brandId)
        {
            if (storeId != 0)
            {
                var store = storeApi.Get(storeId);
                ViewBag.storeId = store.ID.ToString();
                ViewBag.storeName = store.Name;
                ViewBag.brandId = brandId;
                ViewBag.fbAppId = FacebookAppSetting.AppId;
                if (store.FbAccessToken != null)
                {
                    ViewBag.isConnected = true;
                    ViewBag.FbAccessToken = store.FbAccessToken;
                }
                else
                {
                    ViewBag.isConnected = false;
                };
                return View();
            }

            ViewBag.storeId = "0";
            ViewBag.storeName = "Tổng quan hệ thống";
            ViewBag.brandId = brandId;

            return View();
        }

        #region MyRegion

        public JsonResult GetListPost(JQueryDataTableParamModel param, int storeId, int? intentId, int? statusId, bool? isRead, DateTime? startDate, DateTime? endDate)
        {
            var FbshopId = storeApi.GetStoreById(storeId);

            //startDate = null;
            //endDate = null;
            var count = param.iDisplayStart + 1;
            var dataTotal = postApi.GetPostByStore(param, FbshopId.FbShopId, intentId, statusId, isRead, startDate, endDate);
            if (param.sSearch != null)
            {
                dataTotal = dataTotal.Where(a => a.Status != 5 && a.LastContent != null && a.LastContent.Contains(param.sSearch));
            }
            var totalRecords = dataTotal.Count();
            var data = dataTotal.Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength).ToList();
            if (FbshopId != null)
            {
                FacebookClient fbApp = new FacebookClient(FbshopId.FbAccessToken);
                dynamic postContent;
                dynamic fbUser;
                CustomerViewModel registedCustomer;
                List<AnalysisPostViewModel> listModel = new List<AnalysisPostViewModel>();
                AnalysisPostViewModel model;

                for (int i = 0; i < data.Count(); i++)
                {
                    model = new AnalysisPostViewModel();
                    model.PostId = data[i].PostId;
                    try
                    {
                        //Get Post Information
                        dynamic paramFB = new ExpandoObject();
                        paramFB.locale = "vi_VI";
                        paramFB.fields = "from,id,message,story,likes.summary(true),comments.summary(true),shares";
                        postContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].PostId, paramFB).ToString());
                        if (postContent.story == null || postContent.message == null)
                        {
                            model.PostContent = TruncateLongString(postContent.story + postContent.message, 40);
                        }
                        else
                        {
                            model.PostContent = TruncateLongString(postContent.story + ": " + postContent.message, 40);
                        };
                        if (postContent.likes != null)
                        {
                            model.LikeCount = postContent.likes.summary.total_count;
                        }
                        else
                        {
                            model.LikeCount = 0;
                        }
                        if (postContent.comments != null)
                        {
                            model.CommentCount = postContent.comments.summary.total_count;
                        }
                        else
                        {
                            model.CommentCount = 0;
                        }
                        if (postContent.shares != null)
                        {
                            model.ShareCount = postContent.shares.count;
                        }
                        else
                        {
                            model.ShareCount = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        if (data[i].LastContent != null)
                        {
                            model.PostContent = data[i].LastContent;
                        }
                        else
                        {
                            model.PostContent = "Bài đăng không tồn tại.";
                        }
                        model.LikeCount = 0;
                        model.CommentCount = 0;
                        model.ShareCount = 0;
                        Debug.WriteLine(e.Message);
                    }

                    //Get Sender Information
                    model.SenderFBId = data[i].SenderId;
                    registedCustomer = customerApi.GetCustomerByFacebookId(data[i].SenderId, storeId.ToString());
                    try
                    {
                        fbUser = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].SenderId).ToString());
                        model.SenderFBName = fbUser.name;
                    }
                    catch (Exception ex)
                    {
                        model.SenderFBName = "Người dùng không tồn tại";
                        Debug.WriteLine(ex.Message);
                    }
                    if (registedCustomer != null)
                    {
                        if (registedCustomer.Name != null)
                        {
                            model.SenderFBName = model.SenderFBName + " (" + registedCustomer.Name + ")";
                        }
                        model.IsCustomer = true;
                    }
                    else
                    {
                        model.IsCustomer = false;

                    }

                    model.ShopId = data[i].FbShopId;
                    model.Status = data[i].Status;
                    model.IsRead = data[i].IsRead;
                    model.IntentId = data[i].IntentId;
                    model.Rating = model.ShareCount * 5 + model.LikeCount * 2 + model.CommentCount * 3;
                    model.CreateDate = (data[i].DateCreated.ToShortDateString());
                    if (data[i].IntentId != null)
                    {
                        //model.IntentName = intentService.GetIntentNameById(data[i].IntentId);
                        model.IntentName = "abc";
                    }
                    listModel.Add(model);

                }
                var listModellist = listModel.Select(q => new IConvertible[] {
                            q.PostId, //0
                            q.PostContent, //1
                            q.SenderFBName, //2
                            q.LikeCount, //3
                            q.ShareCount, //4
                            q.CommentCount, //5
                            q.IntentName, //6
                            q.Status, //7
                            q.IsRead, //8
                            q.ShopId, //9
                            q.SenderFBId, //10
                            q.IsCustomer, //11
                            q.CreateDate, //12
                            q.Rating, //13
                             });
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,//displayRecords,
                    aaData = listModellist
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetListPostBySender(int storeId, int brandId)
        {
            if (storeId != 0)
            {
                var store = storeApi.Get(storeId);
                ViewBag.storeId = store.ID.ToString();
                ViewBag.storeName = store.Name;
                ViewBag.brandId = brandId;
                ViewBag.fbAppId = FacebookAppSetting.AppId;
                if (store.FbAccessToken != null)
                {
                    ViewBag.isConnected = true;
                    ViewBag.FbAccessToken = store.FbAccessToken;
                }
                else
                {
                    ViewBag.isConnected = false;
                };
                return View();
            }

            ViewBag.storeId = "0";
            ViewBag.storeName = "Tổng quan hệ thống";
            ViewBag.brandId = brandId;
            return View();
        }
        public JsonResult GetListPostByCustomer(JQueryDataTableParamModel param, int storeId, int? intentId, int? statusId, bool? isRead, DateTime? startDate, DateTime? endDate)
        {
            var FbshopId = storeApi.GetStoreById(storeId);
            var count = param.iDisplayStart + 1;
            var dataTotal = postApi.GetPostByCustomer(param, FbshopId.FbShopId, intentId, statusId, isRead, startDate, endDate);
            if (param.sSearch != null)
            {
                dataTotal = dataTotal.Where(a => a.Status != 5 && a.LastContent != null && a.LastContent.Contains(param.sSearch));
            }
            var totalRecords = dataTotal.Count();
            var data = dataTotal.Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength).ToList();
            if (FbshopId != null)
            {
                FacebookClient fbApp = new FacebookClient(FbshopId.FbAccessToken);
                dynamic postContent;
                dynamic fbUser;
                CustomerViewModel registedCustomer;
                List<AnalysisPostViewModel> listModel = new List<AnalysisPostViewModel>();
                AnalysisPostViewModel model;

                for (int i = 0; i < data.Count(); i++)
                {
                    model = new AnalysisPostViewModel();
                    model.PostId = data[i].PostId;
                    try
                    {
                        //Get Post Information
                        dynamic paramFB = new ExpandoObject();
                        paramFB.locale = "vi_VI";
                        paramFB.fields = "from,id,message,story,likes.summary(true),comments.summary(true),shares";
                        postContent = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].PostId, paramFB).ToString());
                        if (postContent.story == null || postContent.message == null)
                        {
                            model.PostContent = TruncateLongString(postContent.story + postContent.message, 40);
                        }
                        else
                        {
                            model.PostContent = TruncateLongString(postContent.story + ": " + postContent.message, 40);
                        };
                        if (postContent.likes != null)
                        {
                            model.LikeCount = postContent.likes.summary.total_count;
                        }
                        else
                        {
                            model.LikeCount = 0;
                        }
                        if (postContent.comments != null)
                        {
                            model.CommentCount = postContent.comments.summary.total_count;
                        }
                        else
                        {
                            model.CommentCount = 0;
                        }
                        if (postContent.shares != null)
                        {
                            model.ShareCount = postContent.shares.count;
                        }
                        else
                        {
                            model.ShareCount = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        if (data[i].LastContent != null)
                        {
                            model.PostContent = data[i].LastContent;
                        }
                        else
                        {
                            model.PostContent = "Bài đăng không tồn tại.";
                        }
                        model.LikeCount = 0;
                        model.CommentCount = 0;
                        model.ShareCount = 0;
                        Debug.WriteLine(e.Message);
                    }

                    //Get Sender Information
                    model.SenderFBId = data[i].SenderId;
                    registedCustomer = customerApi.GetCustomerByFacebookId(data[i].SenderId, storeId.ToString());
                    try
                    {
                        fbUser = System.Web.Helpers.Json.Decode(fbApp.Get(data[i].SenderId).ToString());
                        model.SenderFBName = fbUser.name;
                    }
                    catch (Exception ex)
                    {
                        model.SenderFBName = "Người dùng không tồn tại";
                        Debug.WriteLine(ex.Message);
                    }
                    if (registedCustomer != null)
                    {
                        if (registedCustomer.Name != null)
                        {
                            model.SenderFBName = model.SenderFBName + " (" + registedCustomer.Name + ")";
                        }
                        model.IsCustomer = true;
                    }
                    else
                    {
                        model.IsCustomer = false;

                    }

                    model.ShopId = data[i].FbShopId;
                    model.Status = data[i].Status;
                    model.IsRead = data[i].IsRead;
                    model.IntentId = data[i].IntentId;
                    model.Rating = model.ShareCount * 5 + model.LikeCount * 2 + model.CommentCount * 3;
                    model.CreateDate = (data[i].DateCreated.ToShortDateString());
                    if (data[i].IntentId != null)
                    {
                        //model.IntentName = intentService.GetIntentNameById(data[i].IntentId);
                        model.IntentName = "abc";
                    }
                    listModel.Add(model);

                }
                var listModellist = listModel.Select(q => new IConvertible[] {
                            q.PostId,
                            q.PostContent,
                            q.SenderFBName,
                            q.LikeCount,
                            q.ShareCount,
                            q.CommentCount,
                            q.IntentName,
                            q.Status,
                            q.IsRead,
                            q.ShopId,
                            q.SenderFBId,
                            q.IsCustomer,
                            q.CreateDate,
                            q.Rating,
                             });
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,//displayRecords,
                    aaData = listModellist
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeletePost(string[] postId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            fbApp.AccessToken = accessToken;
            dynamic param = new ExpandoObject();
            try
            {
                for (var i = 0; i < postId.Length; i++)
                {
                    var result = fbApp.Delete(postId[i]);
                    var updateStatus = postApi.SetStatusPost(postId[i]);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult HidePost(string[] postId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            Debug.WriteLine(accessToken);
            dynamic param = new ExpandoObject();
            try
            {
                param.access_token = accessToken;
                param.is_hidden = true;
                for (var i = 0; i < postId.Length; i++)
                {
                    var result = fbApp.Post(postId[i], param);
                    var resultList = postApi.SetHidePost(postId[i]);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ReadPost(string postId, int storeId)
        {
            try
            {
                var result = postApi.SetPostIsRead(postId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ApprovePost(string postId, int storeId)
        {
            try
            {
                var result = postApi.ApprovePost(postId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UnhidePost(string[] postId, int storeId)
        {
            string accessToken = storeApi.GetStoreById(storeId).FbAccessToken;
            Debug.WriteLine(accessToken);
            dynamic param = new ExpandoObject();
            try
            {
                param.access_token = accessToken;
                param.is_hidden = false;
                for (var i = 0; i < postId.Length; i++)
                {
                    var result = fbApp.Post(postId[i], param);
                    var resultList = postApi.SetUnhidePost(postId[i]);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return Json(new { success = false, e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string TruncateLongString(string str, int maxLength)
        {
            if (str.Length > maxLength)
            {
                return str.Substring(0, maxLength) + " ...";
            }
            else
            {
                return str;
            }
        }
    }
    #endregion
}
