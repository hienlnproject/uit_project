﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.CostManager.Controllers
{
    [Authorize(Roles = "BrandManager, Manager")]
    public class CostManageController : DomainBasedController
    {


        #region Cost Category
        // GET: CostManager/CostManage
        public ActionResult IndexCostManage(string storeId)
        {
            ViewBag.storeId = storeId;
            return View();
        }

        public JsonResult GetDataCostCategory(JQueryDataTableParamModel param, int? status, int? brandId)
        {
            var api = new CostCategoryApi();
            var listCategory = api.GetCostCategories().Where(q => q.Active == true && q.BrandId == brandId).OrderBy(q => q.Type);
            if (status != 3)
            {
                listCategory = listCategory.Where(q => q.Type == status && q.Active == true && q.BrandId==brandId).OrderBy(a => a.Type);
            }
            var search = listCategory
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.CatName.ToLower().Contains(param.sSearch.ToLower()));
            int count = 0;
            count = param.iDisplayStart + 1;
            try
            {
                var rs = search
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength)
                    .ToList()
                    .Select(a => new IConvertible[]
                        {
                        count++,
                        string.IsNullOrEmpty(a.CatName) ? "Không xác định" : a.CatName,
                        a.Type,
                        a.CatID
                        });
                var totalRecords = listCategory.Count();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = search.Count(),
                    aaData = rs
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #region Create
        public ActionResult Create()
        {
            var model = new CostCategoryViewModel();
            PrepareCreate(model);
            return PartialView("Create", model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CostCategoryViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            var api = new CostCategoryApi();
            await api.CreateAsync(model);
            return RedirectToAction("IndexCostManage", "CostManage");
        }
        private void PrepareCreate(CostCategoryViewModel model)
        {
            var customerApi = new CostCategoryApi();
            var costCategoryType = Enum.GetValues(typeof(CostTypeEnum)).Cast<CostTypeEnum>().ToList();

            model.ListType = new List<SelectListItem>();

            model.ListType.Add(new SelectListItem()
            {
                Text = "Thu",
                Value = "1",
                Selected = false
            });

            model.ListType.Add(new SelectListItem()
            {
                Text = "Chi",
                Value = "2",
                Selected = false
            });
        }
        #endregion

        #region Edit

        public async Task<ActionResult> Edit(int Id)
        {
            var api = new CostCategoryApi();
            var model = await api.GetAsync(Id);
            model.ListType = new List<SelectListItem>();

            model.ListType.Add(new SelectListItem()
            {
                Text = "Thu",
                Value = "1",
                Selected = false
            });

            model.ListType.Add(new SelectListItem()
            {
                Text = "Chi",
                Value = "2",
                Selected = false
            });
            return PartialView("Edit", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(CostCategoryViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            var api = new CostCategoryApi();
            await api.UpdateCostManageAsync(model);
            return RedirectToAction("IndexCostManage", "CostManage");
        }
        #endregion

        #region
        public async Task<ActionResult> Delete(int Id, int brandId)
        {
            var api = new CostCategoryApi();
            var costApi = new CostApi();
            var model = await api.GetAsync(Id);
            var costs = costApi.GetCostbyCostCategoryandBrand(brandId, Id);
            var checkedList = costs.Select(q => q.CostStatus != (int)CostStatusEnum.Deleted);
            if (checkedList.Count() == 0)
            {
                api.Deactivate(Id);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion
        #endregion

        #region Cost Management

        public ActionResult CostManagement()
        {
            //ViewBag.storeId = RouteData.Values["storeId"].ToString();
            //ViewBag.storeName = RouteData.Values["storeName"].ToString();
            //var categories = _costCategoryService.GetCostCategories();
            DateTime d = new DateTime(2016, 4, 28, 21, 21, 10);
            //var dateString = Utils.GetCurrentDateTime().Month + "/" + Utils.GetCurrentDateTime().Day + "/" + Utils.GetCurrentDateTime().Year + " " + "11" + ":" + "56";
            var date = Utils.GetCurrentDateTime() - d;
            var time = date.Hours * 3600 + date.Minutes * 60;
            ViewBag.Date = date;
            ViewBag.Time = time;
            return View();
        }

        public JsonResult GetData(string startTime, string endTime, int storeId, int brandId, int status)
        {
            var costApi = new CostApi();
            var categoryApi = new CostCategoryApi();
            var storeApi = new StoreApi();

            DateTime startDate, endDate;

            if (string.IsNullOrEmpty(startTime) || string.IsNullOrEmpty(endTime))
            {
                startDate = Utils.GetCurrentDateTime().GetStartOfDate();
                endDate = Utils.GetCurrentDateTime().GetEndOfDate();
            }
            else if (startTime.Equals(endTime))
            {
                startDate = startTime.ToDateTime().GetStartOfDate();
                endDate = endTime.ToDateTime().GetEndOfDate();
            }
            else
            {
                startDate = startTime.ToDateTime();
                endDate = endTime.ToDateTime();
            }
            IEnumerable<CostViewModel> costList;
            if (storeId != 0)
            {
                if (status != 3)
                {
                    costList = costApi.GetCostCategoriesByRangeTimeAndStatus(startDate, endDate, storeId, status);
                }
                else
                {
                    costList = costApi.GetCostCategoriesByRangeTime(startDate, endDate, storeId);
                }
            }
            else
            {
                costList = costApi.GetCostCategoriesByRangeTimeBrandIdAndStatus(startDate, endDate, brandId, status);
            }
            int count = 1;
            var category = categoryApi.GetCostCategories();
            var store = storeApi.Get();

            var list = costList
                .OrderByDescending(a => a.CostDate)
                .Select(q => new
                {
                    STT = count++,
                    Description = q.CostDescription,
                    Cate = category.Where(p => p.CatID == q.CatID).Select(p => p.CatName),
                    Amount = q.Amount.ToString("C0", System.Globalization.CultureInfo.GetCultureInfo("vi-VN")),
                    Date = q.CostDate.ToString("dd/MM/yyyy HH:mm"),
                    PaidPerson = q.PaidPerson,
                    LoggedUser = q.LoggedPerson,
                    ApprovedPerson = q.ApprovedPerson,
                    CostID = q.CostID,
                    CostCategoryType = q.CostCategoryType,
                    StoreName = store.Where(p => p.ID == q.StoreId).Select(p => p.Name),
                });
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        //Tạo phiếu chi
        public ActionResult CreatingSpendForm(string costCategoryType)
        {
            var categoryApi = new CostCategoryApi();

            var model = new CostViewModel();
            int type = Convert.ToInt32(costCategoryType);
            var categories = categoryApi.GetCostCategories().Where(a => a.Type == type);
            //model.CostDate = Utils.GetCurrentDateTime();
            model.CostCategoryType = type;
            model.Categories = categories;
            return PartialView("CreatingSpendForm", model);
        }

        //public ActionResult CostOverview(string startTime, string endTime, int status, int storeId)
        //{
        //    var model = new CostOverViewModel();
        //    DateTime startDate, endDate;
        //    if (string.IsNullOrEmpty(startTime) || string.IsNullOrEmpty(endTime))
        //    {
        //        startDate = DateTime.Now.GetStartOfDate();
        //        endDate = DateTime.Now.GetEndOfDate();
        //    }
        //    else if (startTime.Equals(endTime))
        //    {
        //        startDate = DateTime.Parse(startTime).GetStartOfDate();
        //        endDate = DateTime.Parse(endTime).GetEndOfDate();
        //    }
        //    else
        //    {
        //        startDate = DateTime.Parse(startTime);
        //        endDate = DateTime.Parse(endTime);
        //    }
        //    var costApi = new CostApi();
        //    var costList = costApi.GetCostCategoriesByRangeTime(startDate, endDate, storeId);
        //    var amountReceive = costList.Where(a => a.CostType == (int)CostTypeEnum.ReceiveCost).Sum(b => b.Amount);
        //    var amountSpend = costList.Where(a => a.CostType == (int)CostTypeEnum.SpendingCost).Sum(b => b.Amount);

        //    model.AmountReceipt = amountReceive;
        //    model.AmountSpend = amountSpend;
        //    model.Status = status;
        //    model.StartTime = startTime;
        //    model.EndTime = endTime;
        //    return PartialView("_CostOverview", model);
        //}

        [HttpPost]
        public ActionResult CreatingSpendForm(CostViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var api = new CostApi();
                model.CostDate = Utils.GetCurrentDateTime();
                api.Create(model);
                return Json(new
                {
                    success = true
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false
                });
            }

        }

        //Tạo phiếu thu
        public ActionResult CreatingReceiptForm(string costCategoryType)
        {
            var categoryApi = new CostCategoryApi();
            var model = new CostViewModel();
            int type = Convert.ToInt32(costCategoryType);
            var categories = categoryApi.GetCostCategories().Where(a => a.Type == type);
            model.CostCategoryType = type;
            model.Categories = categories;
            return PartialView("CreatingReceiptForm", model);
        }

        [HttpPost]
        public ActionResult CreatingReceiptForm(CostViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var api = new CostApi();
                model.CostDate = Utils.GetCurrentDateTime();
                api.Create(model);
                return Json(new
                {
                    success = true
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false
                });
            }
        }

        public JsonResult CancelCost(string CostID)
        {

            var costApi = new CostApi();
            int id = int.Parse(CostID);
            costApi.Delete(id);
            //_costService.DeleteCost(id);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            //return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}