﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Invoice.Controllers
{
    public class InvoiceController : Controller
    {
        // GET: Invoice/Invoice
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetData(string startTime, string endTime, int storeId, int brandId)
        {

            List<InvoiceViewModel> invoiceList = new List<InvoiceViewModel>();
            InvoiceApi api = new InvoiceApi();
            invoiceList = api.GetActive().ToList();
            var list = invoiceList
                .OrderByDescending(a => a.InvoiceDate.ToString())
                .Select(q => new
                {
                    invoiceDate=q.InvoiceDate.ToShortDateString(),
                    cusId=q.CustomerId,
                    proId=q.ProviderId,
                    type=q.Type,
                    amout=q.Amount,
                    status=q.Status,
                    storeId=q.StoreId
                });
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
    }
}