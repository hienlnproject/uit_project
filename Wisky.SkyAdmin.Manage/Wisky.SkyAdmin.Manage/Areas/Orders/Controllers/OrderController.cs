﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using SkyWeb.DatVM.Mvc;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Orders.Controllers
{
    public class OrderController : DomainBasedController
    {
        // GET: Orders/Order
        public ActionResult Index(int storeId)
        {
            ViewBag.storeId = storeId.ToString();
            return View();
        }

        public ActionResult UpdateOrderStatus(int id, string status)
        {
            var orderApi = new OrderApi();
            var order = orderApi.BaseService.Get(id);
            try
            {
                var orderstatus = int.Parse(status);
                if (orderstatus == (int)DeliveryStatus.Finish)
                {
                    order.OrderStatus = (int)OrderStatusEnum.Finish;
                    order.DeliveryStatus = (int)DeliveryStatus.Finish;
                    if (order.FinalAmount <= 0)
                    {
                        order.FinalAmount = order.TotalAmount - (order.Discount + order.DiscountOrderDetail);
                        foreach (var item in order.OrderDetails)
                        {
                            item.Status = (int)OrderDetailStatus.Finish;
                            item.FinalAmount = item.TotalAmount - item.Discount;
                        }
                        if (order.Payments != null)
                        {
                            if (order.Payments.Count > 1)
                            {
                                orderApi.BaseService.Update(order);
                                var paymentApi = new PaymentApi();
                                foreach (var item in order.Payments)
                                {
                                    paymentApi.BaseService.Delete(item);
                                }
                                var payment = new HmsService.Models.Entities.Payment();
                                payment.Amount = order.FinalAmount;
                                payment.CurrencyCode = "VNĐ";
                                payment.PayTime = Utils.GetCurrentDateTime();
                                payment.Status = (int)PaymentStatusEnum.Approved;
                                payment.ToRentID = order.RentID;
                                payment.Type = (int)PaymentTypeEnum.Cash;
                                paymentApi.BaseService.Create(payment);
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (order.Payments.Count() == 0)
                                {
                                    var paymentApi = new PaymentApi();
                                    var payment = new HmsService.Models.Entities.Payment();
                                    payment.Amount = order.FinalAmount;
                                    payment.CurrencyCode = "VNĐ";
                                    payment.PayTime = Utils.GetCurrentDateTime();
                                    payment.Status = (int)PaymentStatusEnum.Approved;
                                    payment.ToRentID = order.RentID;
                                    payment.Type = (int)PaymentTypeEnum.Cash;
                                    paymentApi.BaseService.Create(payment);
                                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    order.Payments.FirstOrDefault().Amount = order.FinalAmount;
                                }

                            }
                        }
                    }

                }
                else
                {
                    if (orderstatus == (int)DeliveryStatus.Fail)
                    {
                        order.DeliveryStatus = (int)DeliveryStatus.Fail;
                        order.OrderStatus = (int)OrderStatusEnum.Cancel;
                        order.FinalAmount = 0;
                        foreach (var item in order.OrderDetails)
                        {
                            item.Status = (int)OrderDetailStatusEnum.Cancel;
                            item.FinalAmount = 0;
                        }
                        foreach (var item in order.Payments)
                        {
                            item.Amount = 0;
                        }
                    }
                    else
                    {
                        order.DeliveryStatus = orderstatus;
                        order.OrderStatus = (int)OrderStatusEnum.Processing;
                    }
                }
                orderApi.BaseService.Update(order);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult UpdatePayment(int id, string type)
        {
            var paymentApi = new PaymentApi();
            var orderApi = new OrderApi();
            var order = orderApi.BaseService.Get(id);
            try
            {
                var typeUpdate = int.Parse(type);
                var payment = new HmsService.Models.Entities.Payment();
                payment.Amount = order.FinalAmount;
                payment.CurrencyCode = "VNĐ";
                payment.PayTime = Utils.GetCurrentDateTime();
                payment.Status = (int)PaymentStatusEnum.Approved;
                payment.ToRentID = order.RentID;
                payment.Type = int.Parse(type);
                paymentApi.BaseService.Create(payment);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadOrder(JQueryDataTableParamModel param, int brandId, int storeId, int selectedstoreId, string startDate, string endDate, int status, int type)
        {

            var orderApi = new OrderApi();
            IQueryable<Order> listOrder = null;
            var startTime = startDate.ToDateTime().GetStartOfDate();
            var endTime = endDate.ToDateTime().GetEndOfDate();
            //filter status and type
            if (storeId == 0)
            {
                listOrder = orderApi.GetRentsByTimeRange(selectedstoreId, startTime, endTime, brandId);
            }
            else
            {
                listOrder = orderApi.GetRentsByTimeRange(storeId, startTime, endTime);
            }

            if (status != 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status && q.OrderType == type);
            }
            else if (status == 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderType == type);
            }
            else if (type == 0 && status != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status);
            }

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                listOrder = listOrder.Where(q => q.InvoiceID.ToLower().Contains(param.sSearch.ToLower()));
            }
            int count = 0;
            count = param.iDisplayStart + 1;

            //try
            //{
            var result = listOrder
                .OrderByDescending(q => q.CheckInDate)
                .Skip(param.iDisplayStart)
                .Take(param.iDisplayLength)
                .ToList();
            var list = result.Select(a => new object[]
                    {
                        count++,
                        string.IsNullOrEmpty(a.InvoiceID) ? "Không xác định" : a.InvoiceID,
                        a.OrderDetailsTotalQuantity,
                        a.FinalAmount,
                        a.Payments.Select(q => ((PaymentTypeEnum) q.Type).DisplayName()).Distinct().ToArray(),
                        a.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                        a.DeliveryStatus,
                        a.CheckInPerson,
                        a.RentID,
                        a.Customer !=null ? a.Customer.Name : "",
                        a.DeliveryAddress!=null ? a.DeliveryAddress : "",
                        a.DeliveryPhone!=null ? a.DeliveryPhone : "",
                        a.Notes !=null? a.Notes : "",
                        a.TotalAmount,
                        (a.Discount + a.DiscountOrderDetail),
                        a.Store.Name,
                        a.DeliveryPayment,
                        a.DeliveryType,
                        a.Att1 == "1" ? "Có":"Không",
                    });
            var totalRecords = listOrder.Count();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = list
            }, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception e)
            //{
            //    Console.Write(e.Message);
            //    return Json(new
            //    {
            //        sEcho = param.sEcho,
            //        iTotalRecords = totalRecords,
            //        iTotalDisplayRecords = totalRecords,
            //        aaData = new List<dynamic>()
            //    }, JsonRequestBehavior.AllowGet);
            //}
        }

        //public ActionResult OrderDetail(int brandId, int Id)
        //{
        //    var orderApi = new OrderApi();
        //    var order = orderApi.GetOrderByIdAndBrandId(brandId,Id);
        //    return View(order);
        //}


        public JsonResult LoadOrderDetail(int id)
        {
            var orderlApi = new OrderApi();
            var order = orderlApi.Get(id);
            var orderDetail = order.OrderDetails.OrderBy(q => q.OrderDate);
            int count = 1;
            var totalCount = orderDetail.Count();
            var list = orderDetail.Select(a => new IConvertible[]
            {
                count++,
                a.Product.ProductName,
                a.OrderDetailAtt1,
                a.OrderDetailAtt2,
                a.OrderDetailAtt3,
                !string.IsNullOrWhiteSpace(a.OrderDetailAtt4)?a.OrderDetailAtt4:" ",
                a.UnitPrice,
                a.Quantity,
                a.Discount,
                a.FinalAmount,
                a.RentID,
                a.ProductType,
            });

            var lblData = new
            {
                cusName = order.DeliveryReceiver != null ? order.DeliveryReceiver : "N/A",
                cusAddr = string.IsNullOrEmpty(order.DeliveryAddress) ? "N/A" : order.DeliveryAddress,
                cusPhone = order.DeliveryPhone != null ? order.DeliveryPhone : "N/A",
                notes = order.Notes != null ? order.Notes : "",
                totalAmount = order.TotalAmount,
                totalDiscount = (order.Discount + order.DiscountOrderDetail),
                store = order.Store.Name,
                payment = order.Payments.GroupBy(q => q.Type)
                                .Select(a => new
                                {
                                    type = ((PaymentTypeEnum)a.Key).DisplayName(),
                                    amount = a.Sum(z => z.Amount)
                                }).ToArray()
            };

            return Json(new
            {
                dataTable = list,
                lblData = lblData
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadOrderDetailDataTable(int id)
        {
            var orderlApi = new OrderApi();
            var order = orderlApi.Get(id);
            var orderDetail = order.OrderDetails.OrderBy(q => q.OrderDate);
            int count = 1;
            var totalCount = orderDetail.Count();
            var list = orderDetail.Select(a => new IConvertible[]
            {
                count++,
                a.Product.ProductName,
                a.UnitPrice,
                a.Quantity,
                a.Status,
                a.Discount,
                a.FinalAmount,
                a.OrderDetailAtt1,
                a.OrderDetailAtt2,
                a.OrderDetailAtt3,
                a.RentID,
                a.ProductType,
            });

            //var lblData = new
            //{
            //    cusName = order.DeliveryReceiver != null ? order.DeliveryReceiver : "N/A",
            //    cusAddr = string.IsNullOrEmpty(order.DeliveryAddress) ? "N/A" : order.DeliveryAddress,
            //    cusPhone = order.DeliveryPhone != null ? order.DeliveryPhone : "N/A",
            //    notes = order.Notes != null ? order.Notes : "",
            //    totalAmount = order.TotalAmount,
            //    totalDiscount = (order.Discount + order.DiscountOrderDetail),
            //    store = order.Store.Name,
            //    payment = order.Payments.GroupBy(q => q.Type)
            //                    .Select(a => new
            //                    {
            //                        type = ((PaymentTypeEnum)a.Key).DisplayName(),
            //                        amount = a.Sum(z => z.Amount)
            //                    }).ToArray()
            //};

            return Json(new
            {
                aaData = list,
            }, JsonRequestBehavior.AllowGet);
        }

        // Hàm xuất file excel
        public ActionResult ExportOrderTableToExcel(int _id, int storeId, int brandId)
        {
            var storeService = this.Service<IStoreService>();
            var orderService = this.Service<IOrderService>();
            var orderDetailService = this.Service<IOrderDetailService>();
            var storeApi = new StoreApi();
            var orderApi = new OrderApi();
            var orderDetailApi = new OrderDetailApi();
            var storeName = "";
            if (storeId > 0)
            {
                storeName = storeService.Get(storeId).Name;
            }
            else
            {
                storeName = "Service";
            }

            if (storeId > 0)
            {
                var totalModel = orderDetailApi.GetOrderDetailsByRentId(_id)
                  .Select(g => new
                  {
                      productName = g.Product.ProductName,
                      Price = g.UnitPrice,
                      quality = g.Quantity,
                      discount = g.Discount,
                      finalAmount = g.FinalAmount
                  });
                //var totalModel = orderApi.GetRentsByTimeRange2(storeId, fromDate, toDate)
                //  .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish)
                //  .GroupBy(a => a.CheckInPerson)
                //  .Select(g => new
                //  {
                //      FullName = g.Key == null ? "N/A" : aspNetUserApi.GetUserByUsername(g.Key).FullName,
                //      Username = g.Key == null ? "N/A" : g.Key,
                //      TotalBill = g.Count(),
                //      FinalAmount = g.Sum(x => x.FinalAmount)
                //  });

                #region Export to Excel
                int count = 0;
                MemoryStream ms = new MemoryStream();
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("ChiTietHoaDon");
                    char StartHeaderChar = 'A';
                    int StartHeaderNumber = 1;
                    #region Headers
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "STT";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Tên sản phẩm";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Giá sản phẩm";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Số lượng";
                    ws.Cells["" + (StartHeaderChar) + (StartHeaderNumber)].Value = "Tình trạng";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Giảm giá";
                    ws.Cells["" + (StartHeaderChar) + (StartHeaderNumber)].Value = "Thanh toán";
                    var EndHeaderChar = StartHeaderChar;
                    var EndHeaderNumber = StartHeaderNumber;
                    StartHeaderChar = 'A';
                    StartHeaderNumber = 1;
                    #endregion
                    #region Set style for rows and columns
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()].Style.Font.Bold = true;
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()].AutoFitColumns();
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                        .Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                        .Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.GreenYellow);
                    ws.View.FreezePanes(2, 1);
                    #endregion
                    #region Set values for cells                
                    foreach (var data in totalModel)
                    {
                        ws.Cells["" + (StartHeaderChar++) + (++StartHeaderNumber)].Value = ++count;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.productName;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.Price);
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.quality;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.discount);
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.finalAmount);
                        StartHeaderChar = 'A';
                    }
                    #endregion

                    //Set style for excel
                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    ws.Cells[ws.Dimension.Address].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    package.SaveAs(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    var fileDownloadName = "ChiTietHoaDon_" + storeName + "_TổngQuanNgày.xlsx";
                    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    return this.File(ms, contentType, fileDownloadName);
                }
                #endregion
            }
            else
            {
                var totalModel = orderDetailApi.GetOrderDetailsByRentId(_id)
                  .Select(g => new
                  {
                      productName = g.Product.ProductName,
                      Price = g.UnitPrice,
                      quality = g.Quantity,
                      discount = g.Discount,
                      finalAmount = g.FinalAmount
                  });

                #region Export to Excel
                int count = 0;
                MemoryStream ms = new MemoryStream();
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("TongQuanNhanVien");
                    char StartHeaderChar = 'A';
                    int StartHeaderNumber = 1;
                    #region Headers
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "STT";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Tên sản phẩm";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Giá sản phẩm";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Số lượng";
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Giảm giá";
                    ws.Cells["" + (StartHeaderChar) + (StartHeaderNumber)].Value = "Thanh toán";
                    var EndHeaderChar = StartHeaderChar;
                    var EndHeaderNumber = StartHeaderNumber;
                    StartHeaderChar = 'A';
                    StartHeaderNumber = 1;
                    #endregion
                    #region Set style for rows and columns
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()].Style.Font.Bold = true;
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()].AutoFitColumns();
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                        .Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                        ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                        .Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.GreenYellow);
                    ws.View.FreezePanes(2, 1);
                    #endregion
                    #region Set values for cells                
                    foreach (var data in totalModel)
                    {
                        ws.Cells["" + (StartHeaderChar++) + (++StartHeaderNumber)].Value = ++count;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.productName;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.Price);
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.quality;
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.discount);
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.finalAmount);
                        StartHeaderChar = 'A';
                    }
                    #endregion

                    //Set style for excel
                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    ws.Cells[ws.Dimension.Address].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[ws.Dimension.Address].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    package.SaveAs(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    var fileDownloadName = "ChiTietHoaDon" + storeName + "_TổngQuanNgày.xlsx";
                    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    return this.File(ms, contentType, fileDownloadName);
                }
                #endregion
            }
        }

        //Export excel Order
        /// <summary>
        /// Export excel by datetime range
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="storeId"></param>
        /// <param name="selectedstoreId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult ExportOrderToExcel(int brandId, int storeId, int selectedstoreId, string startDate, string endDate, int status, int type)
        {
            var orderApi = new OrderApi();
            IEnumerable<Order> listOrder = null;
            var startTime = startDate.ToDateTime().GetStartOfDate();
            var endTime = endDate.ToDateTime().GetEndOfDate();
            //filter status and type
            if (storeId == 0)
            {
                listOrder = orderApi.GetRentsByTimeRange(selectedstoreId, startTime, endTime, brandId);
            }
            else
            {
                listOrder = orderApi.GetRentsByTimeRange(storeId, startTime, endTime);
            }

            if (status != 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status && q.OrderType == type);
            }
            else if (status == 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderType == type);
            }
            else if (type == 0 && status != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status);
            }
            var listOrderEnum = listOrder.Select(a => new
            {
                InvoiceID = string.IsNullOrEmpty(a.InvoiceID) ? "Không xác định" : a.InvoiceID,
                Quantity = a.OrderDetailsTotalQuantity,
                FinalAmount = a.FinalAmount,
                PaymentType = a.Payments.Select(q => ((PaymentTypeEnum)q.Type).DisplayName()).Distinct().ToArray(),
                PaymentTypeEnum = a.DeliveryPayment,
                CheckInDate = a.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                DeliveryStatus = a.DeliveryStatus,
                OrderStatus = ((OrderStatusEnum)a.OrderStatus).DisplayName(),
                CustomerName = a.Customer != null ? a.Customer.Name : "N/A",
                DeliveryAddress = a.Customer != null ? a.DeliveryAddress : "N/A",
                DeliveryPhone = a.Customer != null ? a.DeliveryPhone : "",
                Note = a.Notes != null ? a.Notes : "N/A",
            }).ToList();
            int count = 0;
            #region Export to Excel
            MemoryStream ms = new MemoryStream();
            using (ExcelPackage package = new ExcelPackage(ms))
            {
                #region Thanh toán tiền mặt
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Danh sách hóa đơn");
                char StartHeaderChar = 'A';
                int StartHeaderNumber = 1;
                #region Headers
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "STT";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Mã hóa đơn";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Số lượng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Tổng cộng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Thanh toán";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Loại hình thanh toán";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Tình Trạng Thanh Toán";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Thời gian đặt hàng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Tình trạng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Khách hàng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Địa chỉ giao hàng";
                ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Điện thoại";
                ws.Cells["" + (StartHeaderChar) + (StartHeaderNumber)].Value = "Ghi chú";
                var EndHeaderChar = StartHeaderChar;
                var EndHeaderNumber = StartHeaderNumber;
                StartHeaderChar = 'A';
                StartHeaderNumber = 1;
                #endregion
                #region Set style for rows and columns
                ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                    ":" + EndHeaderChar + EndHeaderNumber.ToString()].Style.Font.Bold = true;
                ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                    ":" + EndHeaderChar + EndHeaderNumber.ToString()].AutoFitColumns();
                ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                    ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                    .Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["" + StartHeaderChar + StartHeaderNumber.ToString() +
                    ":" + EndHeaderChar + EndHeaderNumber.ToString()]
                    .Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.GreenYellow);
                ws.View.FreezePanes(2, 1);
                #endregion
                #region Set values for cells                
                foreach (var data in listOrderEnum)
                {
                    ws.Cells["" + (StartHeaderChar++) + (++StartHeaderNumber)].Value = count++;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.InvoiceID;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.Quantity;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.FinalAmount);
                    if (data.PaymentType.Count() > 0)
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", data.FinalAmount);
                    }
                    else
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = string.Format(CultureInfo.InvariantCulture, "{0:0,0}", 0);
                    }
                    if (data.PaymentType.Count() > 0)
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.PaymentType;
                    }
                    else
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "Unpaid";
                    }
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = Utils.DisplayName((PaymentTypeEnum)data.PaymentTypeEnum);
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.CheckInDate;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.OrderStatus;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.CustomerName;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.DeliveryAddress;
                    ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.DeliveryPhone;
                    if (String.IsNullOrEmpty(data.Note))
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = "N/A";
                    }
                    else
                    {
                        ws.Cells["" + (StartHeaderChar++) + (StartHeaderNumber)].Value = data.Note;
                    }
                    StartHeaderChar = 'A';
                }
                #endregion

                //Set style for excel
                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                ws.Cells[ws.Dimension.Address].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells[ws.Dimension.Address].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ws.Cells[ws.Dimension.Address].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[ws.Dimension.Address].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                #endregion
                package.SaveAs(ms);
                ms.Seek(0, SeekOrigin.Begin);
                var storeApi = new StoreApi();
                var storeName = "";
                if (storeId == 0)
                {
                    if (selectedstoreId == 0)
                    {
                        storeName = "Hệ thống";
                    }
                    else
                    {
                        storeName = storeApi.GetStoreById(selectedstoreId).Name;
                    }
                }
                else
                {
                    storeName = storeApi.GetStoreById(storeId).Name;
                }
                var fileDownloadName = "Danh sách hóa đơn: " + storeName + " (" + startTime.ToString("dd-MM-yyyy") + " đến " + endTime.ToString("dd-MM-yyyy") + ").xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return this.File(ms, contentType, fileDownloadName);
            }
            #endregion

        }



        #region lấy danh sách các store
        public JsonResult LoadStoreList(int brandId)
        {
            var storeapi = new StoreApi();
            var stores = storeapi.GetActiveStoreByBrandId(brandId).ToArray();
            return Json(new
            {
                store = stores,
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadOrderByPromotion(JQueryDataTableParamModel param, int brandId, int storeId, int promotionId, string _date, int status, int type)
        {

            var orderApi = new OrderApi();
            var membershipCardApi = new MembershipCardApi();
            var customerApi = new CustomerApi();

            var rpDate = _date.ToDateTime();
            var startTime = rpDate.GetStartOfDate();
            var endTime = rpDate.GetEndOfDate();
            //filter status and type
            IEnumerable<Order> listOrder = orderApi.GetRentsByTimeRange(storeId, startTime, endTime, brandId)
                .Where(o => o.OrderPromotionMappings.Any(m => m.PromotionId == promotionId)).ToList();
            var totalRecords = listOrder.Count();

            if (status != 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status && q.OrderType == type);
            }
            else if (status == 0 && type != 0)
            {
                listOrder = listOrder.Where(q => q.OrderType == type);
            }
            else if (type == 0 && status != 0)
            {
                listOrder = listOrder.Where(q => q.OrderStatus == status);
            }

            int count = param.iDisplayStart + 1;
            try
            {
                var searchList = listOrder
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.InvoiceID.ToLower().Contains(param.sSearch.ToLower()));
                var totalDisplayRecords = searchList.Count();

                var rs = searchList
                    .OrderByDescending(q => q.CheckInDate)
                    .Skip(param.iDisplayStart)
                    .Take(param.iDisplayLength);

                //var list = searchList.Select(a => new IConvertible[]
                //        {
                //        count++,
                //        string.IsNullOrEmpty(a.InvoiceID) ? "Không xác định" : a.InvoiceID,
                //        a.OrderDetailsTotalQuantity,
                //        a.FinalAmount,
                //        a.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                //        a.OrderType,
                //        a.CheckInPerson,
                //        a.RentID,
                //        a.OrderStatus,
                //        //a.Customer!=null ? a.Customer.Name : "",
                //        //a.Customer!=null ? a.Customer.Address : "",
                //        //a.Customer!=null ? a.Customer.Phone : "",
                //        a.Notes !=null? a.Notes : "",
                //        a.TotalAmount,
                //        a.FinalAmount,
                //        (a.Discount + a.DiscountOrderDetail),
                //        });

                var list = new List<dynamic>();
                foreach (var a in rs)
                {
                    // Lấy membershipcard code từ Att1
                    var cardCode = "";
                    if (a.Att1 != null && a.Att1.Contains(":"))
                    {
                        cardCode = a.Att1.Substring(a.Att1.LastIndexOf(":") + 1);
                    }
                    var membershipCard = membershipCardApi.GetMembershipCardByCode(cardCode);
                    Customer customer = null;
                    if (membershipCard != null && membershipCard.Active == true
                    && membershipCard.Status == (int)MembershipStatusEnum.Active /* chưa kiểm tra membershipType */)
                    {
                        customer = customerApi.GetCustomerEntityById(membershipCard.CustomerId.GetValueOrDefault());
                    }

                    var rowData = new List<IConvertible>();

                    rowData.Add(count++); // 0
                    rowData.Add(string.IsNullOrEmpty(a.InvoiceID) ? "Không xác định" : a.InvoiceID); // 1
                    rowData.Add(a.OrderDetailsTotalQuantity); // 2
                    rowData.Add(a.FinalAmount); // 3
                    rowData.Add(a.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss")); // 4
                    rowData.Add(a.OrderType); // 5
                    rowData.Add(a.CheckInPerson); //6
                    rowData.Add(a.RentID); // 7
                    rowData.Add(a.OrderStatus); // 8
                    rowData.Add(customer != null ? customer.Name : ""); // 9
                    rowData.Add(customer != null ? customer.Address : ""); // 10
                    rowData.Add(customer != null ? customer.Phone : ""); // 11
                    rowData.Add(a.Notes != null ? a.Notes : ""); // 12
                    rowData.Add(a.TotalAmount); // 13
                    rowData.Add(a.FinalAmount); // 14
                    rowData.Add(a.Discount + a.DiscountOrderDetail); // 15

                    list.Add(rowData.ToArray());
                }


                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalDisplayRecords,
                    aaData = list
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}