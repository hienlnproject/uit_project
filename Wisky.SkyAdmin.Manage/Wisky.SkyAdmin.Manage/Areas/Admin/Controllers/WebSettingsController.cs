﻿using HmsService.Models;
using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{

    public class WebSettingsController : DomainBasedController
    {

        public ActionResult Index()
        {
            var model = new StoreWebSettingApi()
                .GetActiveByStore(this.CurrentStore.ID);

            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken(), ValidateInput(false)]
        public async Task<ActionResult> Index(HmsService.ViewModels.WebSettingsEditViewModel model)
        {
            if (model != null)
            {
                var settings = model.Pairs.Select(q => new KeyValuePair<int, string>(q.Id, q.Value));

                try
                {
                    await new StoreWebSettingApi().MassUpdate(settings, this.CurrentStore.ID);
                }
                catch (UnauthorizedAccessException)
                {
                    // This exception is thrown when something is not right:
                    // this store is trying to modify value of other store.
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

            }

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

    }
}