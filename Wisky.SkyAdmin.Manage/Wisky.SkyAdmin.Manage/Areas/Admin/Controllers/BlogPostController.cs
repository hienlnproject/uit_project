﻿using HmsService.Models;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{

    //[Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class BlogPostController : DomainBasedController
    {

        public ActionResult Index()
        {
            return this.View();
        }

        //public ActionResult IndexList(BootgridRequestViewModel request)
        //{
        //    var result = new BlogPostApi().GetAdminWithFilterAsync(
        //        this.CurrentStore.ID, request.searchPhrase,
        //        request.current, request.rowCount, request.FirstSortTerm);

        //    var model = new BootgridResponseViewModel<BlogPostDetailsViewModel>(result);
        //    return this.Json(model, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult IndexList(JQueryDataTableParamModel param, int storeId, int brandId)
        {
            var result = new BlogPostApi().GetAllBlogpostByStoreId(storeId);
            IEnumerable<BlogPostDetails> filterList;
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                filterList = result.Where(
                    c => (!string.IsNullOrWhiteSpace(c.BlogPost.Title) && c.BlogPost.Title.ToLower().Contains(param.sSearch.ToLower()))
                    || (!string.IsNullOrWhiteSpace(c.BlogPost.BlogContent) && c.BlogPost.BlogContent.ToLower().Contains(param.sSearch.ToLower()))
                    || (!string.IsNullOrWhiteSpace(c.BlogPost.Author) && c.BlogPost.Author.ToLower().Contains(param.sSearch.ToLower()))
                    );
            }
            else
            {
                filterList = result;
            }
            // Sort.
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc

            switch (sortColumnIndex)
            {
                case 1:
                    filterList = sortDirection == "asc"
                        ? filterList.OrderBy(c => c.BlogPost.Title)
                        : filterList.OrderByDescending(c => c.BlogPost.Title);
                    break;
                case 2:
                    filterList = sortDirection == "asc"
                        ? filterList.OrderBy(c => c.BlogPost.BlogCategory.Title)
                        : filterList.OrderByDescending(c => c.BlogPost.BlogCategory.Title);
                    break;
                case 3:
                    filterList = sortDirection == "asc"
                        ? filterList.OrderBy(c => c.BlogPost.CreatedTime)
                        : filterList.OrderByDescending(c => c.BlogPost.CreatedTime);
                    break;
                case 4:
                    filterList = sortDirection == "asc"
                        ? filterList.OrderBy(c => c.BlogPost.UpdatedTime)
                        : filterList.OrderByDescending(c => c.BlogPost.UpdatedTime);
                    break;
                case 5:
                    filterList = sortDirection == "asc"
                        ? filterList.OrderBy(c => c.BlogPost.Author)
                        : filterList.OrderByDescending(c => c.BlogPost.Author);
                    break;
            }
            
            var displayedList = filterList.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

            var returnResult = displayedList.Select(c => new IConvertible[] {
                c.BlogPost.Image,
                c.BlogPost.Title,
                c.BlogPostCollections.First().Name,
                c.BlogPost.CreatedTime.ToString("dd/MM/yyyy hh:mm"),
                c.BlogPost.UpdatedTime.ToString("dd/MM/yyyy hh:mm"),
                c.BlogPost.Author,

            });
            return Json(new
            {
                param.sEcho,
                iTotalRecords = result.Count(),
                iTotalDisplayRecords = displayedList.Count(),
                aaData = returnResult
            }, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> Create()
        {
            var model = new BlogPostEditViewModel()
            {
                BlogPost = new BlogPostViewModel(),
            };

            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Create(BlogPostEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }

            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.Active = true;
            model.BlogPost.Author = this.User.Identity.Name;

            var api = new BlogPostApi();
            await api.CreateAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0]);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(BlogPostEditViewModel model)
        {
            model.AvailableCollections = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = await new BlogPostApi()
                .GetDetailsByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new BlogPostEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogPostEditViewModel model)
        {
            var api = new BlogPostApi();
            // Validate
            var info = await api
                .GetByStoreIdAsync(model.BlogPost.Id, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }

            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.CreatedTime = info.CreatedTime;
            model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.Active = true;
            await api.EditAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0]);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(BlogPostEditViewModel model)
        {
            model.AvailableCollections = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedBlogPostCollections.Contains(q.Id));
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var productApi = new BlogPostApi();
            var info = await productApi
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            await productApi.DeactivateAsync(id.Value);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

    }
}