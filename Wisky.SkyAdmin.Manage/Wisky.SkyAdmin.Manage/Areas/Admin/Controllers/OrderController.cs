﻿using HmsService.Models;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    [Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class OrderController : DomainBasedController
    {
        // GET: Admin/Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new OrderApi().GetAdminWithFilterAsync(
                this.CurrentStore.ID, request.searchPhrase,
                request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<OrderViewModel>(result);
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderDetail(int id)
        {
            var result = new OrderApi().GetOrderById(this.CurrentStore.ID, id);
            if (result == null)
            {
                return Json(new
                {
                    success = false
                });
            }
            return Json(new
            {
                success = true,
                data = result
            });
        }
    }
}