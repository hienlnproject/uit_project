﻿using Autofac;
using Autofac.Core.Lifetime;
using Autofac.Integration.Mvc;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Repositories;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using SkyWeb.DatVM.Data;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;

namespace Wisky.SkyAdmin.Manage.Automation
{
    public class DateReportExecuter
    {

        public string Url { get; set; }

        public DateReportExecuter(string url)
        {
            this.Url = url;
        }

        public void Start()
        {
            var t = new Thread(Loop);
            t.Start();
        }

        private void Loop()
        {
            while (true)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.DownloadData(this.Url);
                    }
                }
                catch
                {

                }
                Thread.Sleep(3600000);
            }
        }

        public static void PerformWork()
        {
            //var storeApi = DependencyUtils.Resolve<StoreApi>();
            //var stores = storeApi.GetStoresByBrandId(1);
            //var storeService = DependencyUtils.Resolve<IStoreService>();
            //var stores = storeService.GetStoreByBrandId(1);
            var storeApi = new StoreApi();
            var stores = storeApi.GetStores();
            foreach (var store in stores)
            {
                if (!store.ReportDate.HasValue)
                {
                    if (store.Orders.Count > 0)
                    {
                        ReCalculate(store.ID);
                    }
                }
                else
                {
                    Calculate(store.ID);
                }
            }
        }

        public static void Calculate(int storeId)
        {
            var storeService = DependencyUtils.Resolve<IStoreService>();
            var store = storeService.GetStoreById(storeId);
            //var storeApi = DependencyUtils.Resolve<StoreApi>();
            //var store = storeApi.GetStoreById(storeId);
            if (store.ReportDate.Value.AddDays(1) < Utils.GetCurrentDateTime())
            {
                var time = Utils.GetCurrentDateTime();
                var days = (time - store.ReportDate.Value).Days;
                for (int i = 0; i < days; i++)
                {
                    StoreCalculate(store);
                }
            }
        }

        private static void ReCalculate(int storeId)
        {
            var time = Utils.GetCurrentDateTime();
            //var storeApi = DependencyUtils.Resolve<StoreApi>();
            //var store = storeApi.GetStoreById(storeId);
            var storeService = DependencyUtils.Resolve<IStoreService>();
            var store = storeService.GetStoreById(storeId);
            if (store.Orders.Count <= 0)
            {
                return;
            }
            var firstOrderDate = store.Orders.Where(a => a.CheckInDate.HasValue).Min(a => a.CheckInDate.Value);
            store.ReportDate = firstOrderDate.GetEndOfDate().AddDays(-1);
            var timeCount = (time - firstOrderDate).Days;
            for (int i = 0; i < timeCount; i++)
            {
                StoreCalculate(store);
            }
        }

        public static void StoreCalculate(Store store)
        {
            Debug.WriteLine("Start update store {0} in date {1} - {2}", store.Name,
                store.ReportDate.Value.ToString("dd/MM/yyyy"), Utils.GetCurrentDateTime().ToString("HH:mm:ss"));
            var reportDate = store.ReportDate.Value.AddDays(1);
            var orderDetailService = DependencyUtils.Resolve<IOrderDetailService>();
            var orderService = DependencyUtils.Resolve<IOrderService>();
            var dateProductService = DependencyUtils.Resolve<IDateProductService>();
            var dateProductItemService = DependencyUtils.Resolve<IDateProductItemService>();
            var dateReportService = DependencyUtils.Resolve<IDateReportService>();
            var storeService = DependencyUtils.Resolve<IStoreService>();
            var productService = DependencyUtils.Resolve<IProductService>();
            var productItemService = DependencyUtils.Resolve<IProductItemService>();
            var dateProductRepo = DependencyUtils.Resolve<IDateProductRepository>();
            var dateProductItemRepo = DependencyUtils.Resolve<IDateProductItemRepository>();
            var reportTrackingService = DependencyUtils.Resolve<IReportTrackingService>();
            var reportService = DependencyUtils.Resolve<IReportService>();
            var inventoryReceiptService = DependencyUtils.Resolve<IInventoryReceiptService>();
            var uow = DependencyUtils.Resolve<IUnitOfWork>();

            var fromDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetStartOfDate();
            var toDate = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day).GetEndOfDate();
            //Get OrderDetail
            var orderDetails =
                orderDetailService.GetOrderDetailsByTimeRange(fromDate, toDate, store.ID)
                .Where(a => a.Order.OrderType != (int)OrderTypeEnum.DropProduct && a.Order.OrderStatus == (int)OrderStatusEnum.Finish).ToList();
            //Get Rent
            var orders = new List<Order>();
            if (store.Type == (int)StoreTypeEnum.Hotel)
            {
                orders = orderService.GetAllHotelOrdersByCheckOutDate(fromDate, toDate, store.ID)
            .Where(a => a.RentStatus == (int)RentStatusEnum.Leave).ToList();
            }
            else
            {
                orders = orderService.GetOrdersByTimeRange(store.ID, fromDate, toDate)
            .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).ToList();
            }

            var dateProducts =
                orderDetails.GroupBy(a => a.ProductID)
                    .Join(productService.GetAllProducts(), a => a.Key, a => a.ProductID, (a, b) => new DateProduct()
                    {
                        ProductId = a.Key,
                        StoreID = store.ID,
                        Quantity = a.Sum(c => c.Quantity),
                        Date = reportDate,
                        TotalAmount = a.Sum(c => c.TotalAmount),
                        FinalAmount = a.Sum(c => c.FinalAmount),
                        Discount = a.Sum(c => c.Discount),
                        ProductName_ = b.ProductName,
                        Product = b,                        
                        CategoryId_ = b.ProductCategory.CateID
                    }).ToList();

            var dateReport = new DateReport();
            if (store.Type == (int)StoreTypeEnum.Hotel)
            {   
                dateReport.StoreID = store.ID;
                dateReport.CreateBy = "system";
                dateReport.Status = (int)DateReportStatusEnum.Approved;
                dateReport.Date = reportDate;
                dateReport.Discount = orders.Sum(a => a.Discount);
                dateReport.DiscountOrderDetail = orders.Sum(a => a.DiscountOrderDetail);
                dateReport.TotalAmount = orders.Sum(a => a.TotalAmount);
                dateReport.FinalAmount = orders.Sum(a => a.FinalAmount);
                dateReport.TotalCash = 0;
                dateReport.TotalOrder = orders.Count();
                dateReport.TotalOrderAtStore = 0;
                dateReport.TotalOrderTakeAway = 0;
                dateReport.TotalOrderDelivery = 0;
                dateReport.TotalOrderDetail = orders.Sum(a => a.OrderDetails.Sum(b => b.FinalAmount));
                dateReport.TotalOrderFeeItem = (int)orders.Sum(a => a.OrderFeeItems.Sum(b => b.TotalAmount));
            }
            else
            {
                dateReport.StoreID = store.ID;
                dateReport.CreateBy = "system";
                dateReport.Status = (int)DateReportStatusEnum.Approved;
                dateReport.Date = reportDate;
                dateReport.Discount = orders.Sum(a => a.Discount);
                dateReport.DiscountOrderDetail = orders.Sum(a => a.DiscountOrderDetail);
                dateReport.TotalAmount = orders.Sum(a => a.TotalAmount);
                dateReport.FinalAmount = orders.Sum(a => a.FinalAmount);
                dateReport.TotalCash = 0;
                dateReport.TotalOrder = orders.Count();
                dateReport.TotalOrderAtStore = orders.Count(a => a.OrderType == (int)OrderTypeEnum.AtStore);
                dateReport.TotalOrderTakeAway = orders.Count(a => a.OrderType == (int)OrderTypeEnum.TakeAway);
                dateReport.TotalOrderDelivery = orders.Count(a => a.OrderType == (int)OrderTypeEnum.Delivery);
                dateReport.TotalOrderDetail = 0;
                dateReport.TotalOrderFeeItem = 0;
            }


            var reportTracking = new ReportTracking()
            {
                Date = Utils.GetCurrentDateTime(),
                IsUpdate = false,
                UpdatePerson = "system",
                StoreId = store.ID,
            };
            var compositionsStatistic = dateProducts.SelectMany(a => a.Product.ProductItemCompositionMappings.Select(b => new Tuple<ProductItemCompositionMapping, int>(b, a.Quantity)))
                .GroupBy(a => a.Item1.ItemID);

            var dateItemProduct = compositionsStatistic.Join(productItemService.GetProductItems(), a => a.Key, a => a.ItemID, (a, b) => new DateProductItem
            {
                StoreId = store.ID,
                Date = reportDate,
                ProductItemID = a.Key,
                ProductItemName = b.ItemName,
                Quantity = (int)a.Sum(c => c.Item2 * c.Item1.Quantity),
                Unit = b.Unit
            }).AsQueryable();

            var inventoryReceipts = inventoryReceiptService.GetInventoryReceiptByTimeRange(store.ID, fromDate, toDate).AsQueryable();
            string user = "";
            try
            {
                user = (System.Web.HttpContext.Current == null) ? "quanly" : System.Web.HttpContext.Current.User.Identity.Name;
            }
            catch (Exception)
            {
                user = "noname";
            }
            //System.Security.Principal.IIdentity user = null;
            var result = reportService.CreateDateReport(dateReport, orderDetails, dateItemProduct, store, orders, inventoryReceipts, user);

        }
    }
    internal class ProductReportStatistic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int Amount { get; set; }
    }
}