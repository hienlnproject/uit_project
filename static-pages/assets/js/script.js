$(document).ready(function() {
    var swiperMainSlider = new Swiper('#main-slider .swiper-container', {
        pagination: '#main-slider .swiper-pagination',
        paginationClickable: true,
        nextButton: '#main-slider .swiper-button-next',
        prevButton: '#main-slider .swiper-button-prev',
        spaceBetween: 30
    });
    var swiperProducts = new Swiper('.component-product-slider .swiper-container', {
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        nextButton: '.component-product-slider .swiper-button-next',
        prevButton: '.component-product-slider .swiper-button-prev',
        breakpoints: {
            1200: {
                spaceBetween: 30
            },
            992: {
                spaceBetween: 60
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            360: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });
    var swiperCustomers = new Swiper('#customers .swiper-container', {
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '#customers .swiper-button-next',
        prevButton: '#customers .swiper-button-prev',
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    $('#navbar-collapse li').hover(function() {
        // debugger
        var hasNav = $(this).children('div.navigator');
        if (hasNav.length > 0) {
            $('.dark-bg').show(0);
            $(this).find('.navigator').show(0);
        }
    });

    $('#navbar-collapse li').mouseleave(function() {
        // debugger
        var hasNav = $(this).children('div.navigator');
        if (hasNav.length > 0 && hasNav.outerHeight() > 0) {
            $('.dark-bg').hide(0);
            $(this).find('.navigator').hide(0);
            $(this).find('.navigator').finish();
        }
    });


    translation();

    function translation() {
        if (typeof(Storage) !== "undefined") {
            var langMode = localStorage.getItem('langMode');
            if (langMode != null) {
                langObj = lang[langMode];
                $('.lang').text(langMode === "en"? "VI" : "ENG");
            }else {
              langObj = lang['vi'];
            }
        }
        // Set inner HTML
        $('[data-lang]').each(function() {
            var s = $(this).data('lang');
            $(this).html(langObj[s]);
        });
        // Set placeholder
        $('[data-lang-placeholder]').each(function() {
            var s = $(this).data('lang-placeholder');
            $(this).attr('placeholder', langObj[s]);
        });
        // Set value
        $('[data-lang-value]').each(function() {
            var s = $(this).data('lang-value');
            $(this).attr('value', langObj[s]);
        });
    };

    $('.lang').click(function() {
        // debugger
        var langMode = $(this).text();
        if (langMode === "ENG") {
            $(this).text("VI");
            langMode = "en";
        } else {
            $(this).text("ENG");
            langMode = "vi";
        }
        localStorage.setItem('langMode', langMode);
        translation();
    });
});
