﻿/*
	Add to cart fly effect with jQuery. - May 05, 2013
	(c) 2013 @ElmahdiMahmoud - fikra-masri.by
	license: http://www.opensource.org/licenses/mit-license.php
    Modified by DucBM
*/


//function initFlyToCartEffect() {
//    console.log("Init fly to cart effect");
//    $('.cp-button').click(function () {

//        console.log("Init fly to cart effect -- onclick");
//        var cart = $('.cp-cart-icon').eq(0);
//        console.log(cart);
//        var imgtodrag = $(this).closest('.cp-item').find(".cp-image").eq(0);

//        if (cart == undefined) {
//            console.log('.cp-cart-icon is not found');
//            return;
//        }
//        if (imgtodrag == undefined) {
//            console.log('.cp-image is not found');
//            return;
//        }
//        console.log("Init fly to cart effect -- img to drag");
//        if (imgtodrag) {
//            var imgclone = imgtodrag.clone()
//                .offset({
//                    top: imgtodrag.offset().top,
//                    left: imgtodrag.offset().left
//                })
//                .css({
//                    'opacity': '0.5',
//                    'position': 'absolute',
//                    'height': '150px',
//                    'width': '150px',
//                    'z-index': '100000',
//                    //'transform': 'translate(-50%, -50%)'
//                })
//                .appendTo($('body'))
//                .animate({
//                    'top': cart.offset().top + cart.height() / 2,
//                    'left': cart.offset().left + cart.width() / 2,
//                    'width': 75,
//                    'height': 75
//                }, 100, 'swing');

//            console.log("Init fly to cart effect -- shake");
//            setTimeout(function () {
//                var cart = $('.cp-cart-icon').eq(0);
//                cart.effect("shake", {
//                    times: 2,
//                    direction: 'left',
//                }, 200);
//            }, 1500);

//            imgclone.animate({
//                'width': 0,
//                'height': 0
//            }, function () {
//                $(this).detach()
//            });
//        }

//        console.log('Init fly to cart effect -- done');
//    })
//}

$(document).ready(function () {
    console.log('effect init');
    flyToCart = function (This) {
        var cart = $('.cp-cart-icon').eq(0);
        var imgtodrag = $(This).closest('.cp-item').find(".cp-image").eq(0);

        if (cart == undefined) {
            console.log('.cp-cart-icon is not found');
            return;
        }
        if (imgtodrag == undefined) {
            console.log('.cp-image is not found');
            return;
        }

        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left,
                })
                .width(imgtodrag.width())
                .height(imgtodrag.height())
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'z-index': '100000',
                    'transform': 'translate(0, 0)',
                    'border-radius': '50%'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + cart.height() / 2,
                    'left': cart.offset().left + cart.width() / 2,
                    'width': 35,
                    'height': 35,
                    'transform': 'translate(-50%, -50%)',
                    'border-radius': '50%'
                }, 600, 'swing');

            setTimeout(function () {
                cart.stop().effect('shake', {
                    direction: 'right',
                    times: 1
                }, 200);
            }, 600);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
    }
})