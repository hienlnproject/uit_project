/*global angular */
angular.module('OrderApp', [
        'ngRoute',
        'ngStorage',
        'ui.bootstrap',
        'angularUtils.directives.dirPagination'
    ])
    .config(function (paginationTemplateProvider) {
        paginationTemplateProvider.setPath('/Content/Stores/SW.SaiGonVat/assets/angularjs/pagination/dirPagination.tpl.html');
    });