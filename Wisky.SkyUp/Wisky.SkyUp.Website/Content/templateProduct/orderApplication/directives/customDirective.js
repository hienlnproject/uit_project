/*global angular */

/**
 * Directive that executes an expression when the element it is applied to gets
 * an `escape` keydown event.
 */
angular.module('OrderApp')
	.directive('validQuantity', function () {
	    return {
	        require: '?ngModel',
	        link: function (scope, element, attrs, ngModelCtrl) {
	            if (!ngModelCtrl) {
	                return;
	            }

	            ngModelCtrl.$parsers.push(function (val) {
	                var check = false;
	                if (angular.isUndefined(val) || val == "") {
	                    val = 1;
	                    check = true;
	                }
	                if (val >= 9999) {
	                    val = 9999;
	                }
	                var sVal = val + '';
	                var clean = sVal.replace(/[^0-9]+/g, '');
	                if (sVal !== clean) {
	                    if (check || clean == "") {
	                        clean = '1';
	                    }
	                    ngModelCtrl.$setViewValue(clean);
	                    ngModelCtrl.$render();
	                }
	                return parseInt(clean);
	            });

	            element.bind('keypress', function (event) {
	                if (event.keyCode === 32) {
	                    event.preventDefault();
	                }
	            });
	        }
	    };
	});
