angular.module('OrderApp').factory('extraService', function ($http, $q) {
    var service = {

        /*
         * author: TrungNDT
         * method: [Private] 
         */
        loadExtraGroups: function (primaryProductId) {
            // var deferred = $q.defer();
            // $http({
                // url: '/SWCart/GetCategoryExtra',
                // method: 'POST',
            // }).success(function (result) {
                // console.log(result);
                // deferred.resolve(result.extraGroup);
            // });
            // return deferred.promise;
			var deferred = $q.defer();
            $http({
                url: '/SWCart/GetGroupExtraByProductPrimaryId',
                method: 'POST',
                params: { primaryProductId: primaryProductId },
            }).success(function (result) {
                console.log(result);
                deferred.resolve(result.extraGroup);
            });
            return deferred.promise;
        },

        /*
         * author: TrungNDT
         * method: [Private] 
         */
        loadExtraByGroupId: function (cateId) {
            var deferred = $q.defer();
            $http({
                url: '/SWCart/GetExtra',
                method: 'POST',
                params: { cateId: cateId },
            }).success(function (result) {
                deferred.resolve(result.productExtra);
            }).error(function (error) {
                console.log(error);
            });
            return deferred.promise;
        },

        /*
         * author: TrungNDT
         * method: [Private] 
         */
        loadGeneralByProductId: function (productId) {
            var deferred = $q.defer();
            $http({
                url: '/SWCart/GetGeneral',
                method: 'GET',
                params: { productId: productId },
            }).success(function (result) {
                deferred.resolve(result.productGeneral);
            }).error(function (error) {
                console.log(error);
            });
            return deferred.promise;
        },
    };
        
    return service;
});
