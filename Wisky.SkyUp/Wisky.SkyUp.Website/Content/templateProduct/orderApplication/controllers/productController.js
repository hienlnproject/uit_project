﻿angular.module('OrderApp').controller('productController',function ($scope, $modal, productService) {
    var vm = this;

    // Variables
    vm.order = {};
    vm.productList = [];
    vm.categories = [];
	vm.hotProducts=[];
	vm.bestSaleProducts = [];
	vm.product = {};
    //For filter and sort
    vm.filter = {
        name: '' //This value must exist in list option
    };
    vm.sortBy = '';

    /*
     * author: TrungNDT
     * method: Load all products
     */
    vm.getAllProducts = function () {
        //return productService.getProducts();
        var promise = productService.loadAllProducts();
        promise.then(function (products) {
            vm.productList = products;
        });
    }

	/*PhuongTA - load products in HotProducts*/
	vm.getHotProducts = function(){
		var promise = productService.loadHotProducts();
		promise.then(function (products){
			vm.hotProducts=products;
		});
	}
	
	/*PhuongTA - load products in bestSaleProducts*/
	vm.getBestSaleProducts = function(){
		var promise = productService.loadBestSaleProducts();
		promise.then(function (products){
			vm.bestSaleProducts=products;
		});
	}

	vm.getProductById = function (productId) {
	    for (var i = 0 ; i < vm.productList.length; i++)
	        if (vm.productList[i].id == productId) return angular.copy(vm.productList[i]);
	    return null;
	}

	//vm.addProductToCart = function (product) {
	//    productService.addProductToCart(product);
	//    angular.copy(product || {});
	//};

	vm.addProductToCartById = function (id, quantity) {
	    var product = vm.getProductById(id);
	    product.quantity = quantity;
	    vm.addProductToCart(product);
	};

    //Author: Phu
	//vm.getAllProductsByCategory = function (cateId) {
	//    var promise = productService.loadAllProductsInCategory(cateId);
	//    promise.then(function (products) {
	//        vm.productList = products;
	//    });
	//};

	//vm.getAllProductsInCategory = function () {
	//    var promise = productService.loadAllProductsInCategory(vm.categoryId);
	//    promise.then(function (products) {
	//        vm.productList = products;
	//    });
	//};

	//$scope.init = function (cateId) {
	//    vm.categoryId = cateId;
	//    vm.getAllProductsInCategory();
	//};
    /*
     * author: TrungNDT
     * method: Load all categories
     */
	//vm.getAllCategories = function () {
	//    //return productService.getCategories();
	//    var promise = productService.loadAllCategories();
	//    promise.then(function (categories) {
	//        vm.categories = categories;
	//    });
	//};

    /*
     * author: TrungNDT
     * method: View selected product in popup modal
     */
    vm.viewProduct = function (product) {
        var modalInstance = $modal.open({
            controller: 'modalController',
            controllerAs: 'vm',
            templateUrl: 'modalContent.html',
            animation: true,
            resolve: {
				// vm.product = product,
                product: function () {
                    return angular.copy(product || {});
                },
                isNewMode: function () {
                    return true;
                }
            }
        });
    };

    /*
     * author: TrungNDT
     * method: Add given product to cart. Count up if product exist
     */
    vm.addProductToCart = function (product) {
        productService.addProductToCart(product);
    };

    vm.getAllProducts();
    //vm.getAllCategories();
	vm.getHotProducts();
	vm.getBestSaleProducts();

	//$scope.priceRange = function (product) {
	//    return (product.price >= parseInt($scope.lowerBound) && product.price <= parseInt($scope.upperBound));
	//}
});