﻿var flagEdit = false;
var idEdit = -1;
$(document).ready(function () {
    $(document).bind('ajaxStart', function () {
        $('.spinner-group').show();
    }).bind('ajaxStop', function () {
        $('.spinner-group').hide();
    });
    var swiperMainSlider = new Swiper('#main-slider .swiper-container', {
        pagination: '#main-slider .swiper-pagination',
        paginationClickable: true,
        nextButton: '#main-slider .swiper-button-next',
        prevButton: '#main-slider .swiper-button-prev',
        spaceBetween: 30,
        autoHeight: true,
        autoplay: 10000,
        speed: 500
    });
    var swiperProducts = new Swiper('.component-product-slider .swiper-container', {
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        nextButton: '.component-product-slider .swiper-button-next',
        prevButton: '.component-product-slider .swiper-button-prev',
        breakpoints: {
            1200: {
                spaceBetween: 30
            },
            992: {
                spaceBetween: 60
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        },
        loop: true,
        autoplay: 5000,
        speed: 500,
        autoplayDisableOnInteraction: false
    });
    var swiperCustomers = new Swiper('#customers .swiper-container', {
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '#customers .swiper-button-next',
        prevButton: '#customers .swiper-button-prev',
        loop: true,
        autoplay: 7000,
        speed: 600,
        autoplayDisableOnInteraction: false,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });
    // show sub-menu
    $('#navbar-collapse li.dropdown').hover(function () {
        // debugger
        var parent = $(this).closest('#navbar-collapse');
        var hasNav = $(this).find('div.navigator');
        if (hasNav.length > 0) {
            $('.dark-bg').show(0);
            parent.find('.navigator').hide(0);
            $(this).find('.navigator').show(0);
        }
    });
    // hide sub-menu
    $('#navbar-collapse').mouseleave(function () {
        // debugger
        //var hasNav = $(this).find('div.navigator');
        //if (hasNav.length > 0 && hasNav.outerHeight() > 0) {
        $('.dark-bg').hide(0);
        $(this).find('.navigator').hide(0);
        //$(this).find('.navigator').finish();
        // }
    });
    // hide sub-menu
    $('#navbar-collapse > ul > li:not(.dropdown)').hover(function () {
        $('.dark-bg').hide(0);
        $('#navbar-collapse').find('.navigator').hide(0);
    });

    // search bar event
    $('.search-bar').each(function () {
        var input = $(this).find('input');
        var btn = $(this).find('.btn-search');
        var hidden = input.css('display') == 'none';
        btn.click(function () {
            input.fadeToggle(0, function () {
                if (hidden) {
                    input.focus();
                }
            });
        });
    })

    // submit search-form
    $('#search-form').submit(function (event) {
        event.preventDefault();
        $('.spinner-group').show();
        var inputText = $(this).find('input[type="text"]').val();
        window.location.href = "/tim-kiem/" + inputText;
    })

    $('#search-form1').submit(function (event) {
        event.preventDefault();
        $('.spinner-group').show();
        var inputText = $(this).find('input[type="text"]').val();
        window.location.href = "/tim-kiem/" + inputText;
    })
    $('#search-form2').submit(function (event) {
        event.preventDefault();
        $('.spinner-group').show();
        var inputText = $(this).find('input[type="text"]').val();
        window.location.href = "/tim-kiem/" + inputText;
    })
    //// show search bar 
    //$('.search-bar .btn-search:not(.active)').click(function () {
    //    var input = $(this).siblings('input');
    //    input.fadeIn(100, function () {
    //        input.focus();
    //    });
    //    $(this).addClass('active');
    //})
    //// hide search bar
    //$('.search-bar input').blur(function () {
    //    $(this).fadeOut(100);
    //})
    //// submit search form
    //$('.search-bar .btn-search.active').click(function () {
    //    var form = $(this).closest('form');
    //    form.submit();
    //})

    $(window).scroll(function () {

        scrollDownloadBtn();
    });
    translation();

    function translation() {
        if (typeof (Storage) !== "undefined") {
            var langMode = localStorage.getItem('langMode');
            if (langMode != null) {
                langObj = lang[langMode];
                $('.lang').text(langMode === "en" ? "VI" : "ENG");
            } else {
                langObj = lang['vi'];
            }
        }
        // Set inner HTML
        $('[data-lang]').each(function () {
            var s = $(this).data('lang');
            $(this).html(langObj[s]);
        });
        // Set placeholder
        $('[data-lang-placeholder]').each(function () {
            var s = $(this).data('lang-placeholder');
            $(this).attr('placeholder', langObj[s]);
        });
        // Set value
        $('[data-lang-value]').each(function () {
            var s = $(this).data('lang-value');
            $(this).attr('value', langObj[s]);
        });
    };

    $('.lang').click(function () {
        var langMode = $(this).text();
        if (langMode === "ENG") {
            $(this).text("VI");
            langMode = "en";
        } else {
            $(this).text("ENG");
            langMode = "vi";
        }
        localStorage.setItem('langMode', langMode);
        document.cookie = "language=" + langMode + "; expires=Thu, 1 Dec 3000 12:00:00 UTC;path=/";
        //translation();
        location.reload();
    });

    function scrollDownloadBtn() {
        var y = $('body').scrollTop();
        var height = $('body').height();
        var heightFooter = $('#footer').outerHeight() + $('#copyright').outerHeight() + $('#download-cata').outerWidth() / 2 + 60;
        if (height - y <= 1180) {           
            $('#download-cata').addClass("fixedScroll");
            $('#download-cata').css("top", ((height - 240)- heightFooter ) + "px");
            //$('.callCenter').addClass("fixedScroll");
            //$('.callCenter').css("top", ((height - '80%') - heightFooter) + "px");
        }
        else {
            $('#download-cata').removeClass("fixedScroll");
            $('#download-cata').css("top", "50%");
            //$('.callCenter').removeClass("fixedScroll");
            //$('.callCenter').css("top", "80%");
        }
    }
   
  

    $('input.toggle-radio').change(function () {
        var open = $(this).prop('checked');
        var targetSelector = $(this).data('target');
        var target = $(targetSelector);
        if (open) {
            target.slideDown(200);
        } else {
            target.slideUp(200);
        }
    })


    // Navigator: category hover events - DucBM
    $('#nav ul.nav > li > a').hover(function () {
        var navigator = $(this).siblings('.navigator');
        navigator.find('.col-center > div').hide();
        navigator.find('.col-right > div').hide();
        navigator.find('.col-center > div:last-child').show();
        navigator.find('.col-right > div:last-child').show();
        
    })
    // Navigator: sub cate hover events - DucBM
    $('.navigator .col-left a').hover(function () {        
        var href = $(this).attr('href');
        var navigator = $(this).closest('.navigator');


        navigator.find('.col-center .sub-cate').hide();
        navigator.find('.col-center [href="' + href + '"]').closest('.sub-cate').show();
        navigator.find('.col-right .image').hide();
        navigator.find('.col-right .image[data-href*="' + href + '"]').show();       
        
        
    })
    
    // Delivery Info module
    
    $("#orderbtn").on("click", function () {
        var isValid = true;

        var name = $("#nameInput").val().trim();
        if (name == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập họ tên",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#nameInput").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var phone = $("#phoneInput").val().trim();
        if (!phone.match(/^((\+84|0)\d{9,10})$/)) {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Số điện thoại không hợp lệ",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#phoneInput").focus();
                }, 100);
                return;
            });
            return;
        }
        
        isValid = true;
        var city = $('#cityInput').val().trim();
        var ProvinceCode = $('#cityInput option:selected').data("id");
        if (city == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập thành phố",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#cityInput").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var district = $('#districtInput').val().trim();
        var districtCode = $('#districtInput option:selected').data("id");
        if (district == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập quận/huyện",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#districtInput").focus();
                }, 100);
                return;
            });
            return;
        }
        
        isValid = true;
        var ward = $('#wardInput').val().trim();
        if (ward == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập tên đường",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#wardInput").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var addr = $("#addressInput").val().trim();
        if (addr == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập địa chỉ",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#addressInput").focus();
                }, 100);
                return;
            });
            return;
        }
        
        // 3 field tren chua validate

        var object = {
            ID: null,
            UserId: USER_ID,
            Name: name,
            Phone: phone,
            City: city,
            District: district,
            Ward: ward,
            Address: addr,
            ProvinceCode: ProvinceCode,
            DistrictCode: districtCode,
            TypeAddress: $('#typeAdd1').prop('checked'),
            IsDefault: $('#addDefault').is(":checked")
        }

        if (flagEdit == false) {
            $.ajax({
                url: "/SWCart/CreateDeliveryInfor",
                method: "POST",
                data: {
                    model: object
                },
                success: function (result) {
                    $('#addForm').hide();
                    $("#addForm form")[0].reset();
                    angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
                }
            });
        } else {
            if (idEdit != -1) {
                object.ID = idEdit;
                $.ajax({
                    url: "/SWCart/UpdateDeliveryInfor",
                    method: "POST",
                    data: {
                        model: object
                    },
                    success: function (result) {
                        $('#addForm').hide();
                        $("#addForm form")[0].reset();
                        angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
                    }
                });
            }
        }

    });
    $('#addDeliveryInfo').click(function () {
        var flagEdit = false;
        idEdit = -1;
        $("#addForm form")[0].reset();
        $('#addForm').show();
    })
    $('#cancelbtn').click(function () {
        $('#addForm').hide();
    })

    //Mobile

    $("#orderbtnmb").on("click", function () {
        var isValid = true;

        var name = $("#nameInput2").val().trim();
        if (name == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập họ tên",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#nameInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var phone = $("#phoneInput2").val().trim();
        if (!phone.match(/^((\+84|0)\d{9,10})$/)) {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Số điện thoại không hợp lệ",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#phoneInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var city = $('#cityInput2').val().trim();
        if (city == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập thành phố",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#cityInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var district = $('#districtInput2').val().trim();
        if (district == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập quận/huyện",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#districtInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var ward = $('#wardInput2').val().trim();
        if (ward == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập tên đường",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#wardInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        isValid = true;
        var addr = $("#addressInput2").val().trim();
        if (addr == "") {
            isValid = false;
            swal({
                title: "Lỗi",
                text: "Bạn chưa nhập địa chỉ",
                type: "error"
            }, function () {
                setTimeout(function () {
                    $("#addressInput2").focus();
                }, 100);
                return;
            });
            return;
        }

        // 3 field tren chua validate

        var object = {
            ID: null,
            UserId: USER_ID,
            Name: name,
            Phone: phone,
            City: city,
            District: district,
            Ward: ward,
            Address: addr,
            TypeAddress: $('#typeAdd1m').prop('checked'),
            IsDefault: $('#addDefaultm').is(":checked")
        }

        if (flagEdit == false) {
            $.ajax({
                url: "/SWCart/CreateDeliveryInfor",
                method: "POST",
                data: {
                    model: object
                },
                success: function (result) {
                    $('#addFormmb').hide();
                    $("#addFormmb form")[0].reset();
                    angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
                }
            });
        } else {
            if (idEdit != -1) {
                object.ID = idEdit;
                $.ajax({
                    url: "/SWCart/UpdateDeliveryInfor",
                    method: "POST",
                    data: {
                        model: object
                    },
                    success: function (result) {
                        $('#addFormmb').hide();
                        $("#addFormmb form")[0].reset();
                        angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
                    }
                });
            }
        }

    });
    $('#addDeliveryInfomb').click(function () {
        var flagEdit = false;
        idEdit = -1;
        $("#addFormmb form")[0].reset();
        $('#addFormmb').show();
    })
    $('#cancelbtnmb').click(function () {
        $('#addFormmb').hide();
    })

});

var province = [];
var district = [];
var ward = [];

function loadAddress() {
    $.ajax({
        url: '/SWCart/LoadAddress',
        type: 'GET',
        success: function (result) {
            if (result.success) {
                province = result.province;
                district = result.district;
                ward = result.ward;
                displayProvince();
                displayProvince2();
            } else {
                console.log("Error while loading address");
            }
        },
        error: function () {
            console.log("Error while loading address");
        }
    });
}


function displayProvince() {
    $("#cityInput").empty();
    $("#cityInput").append("<option value=''>Vui lòng chọn tỉnh/thành phố</option>");
    for (var i = 0; i < province.length; i++) {
        $("#cityInput").append("<option value='" + province[i].ProvinceType + " " + province[i].ProvinceName + "' data-id='" + province[i].ProvinceCode + "'>"
            + province[i].ProvinceType + " " + province[i].ProvinceName + "</option>")
    }
}

$("#cityInput").on('change', function () {
    var provinceCode = $("#cityInput").find(":selected").data('id');
    var districtFiltered = district.filter(q => q.ProvinceCode == provinceCode);
    displayDistrict(districtFiltered);
});

function displayDistrict(districtFiltered) {
    $("#districtInput").empty();
    $("#districtInput").append("<option value=''>Vui lòng chọn quận/huyện</option>");
    for (var i = 0; i < districtFiltered.length; i++) {
        $("#districtInput").append("<option value='" + districtFiltered[i].DistrictType + " " + districtFiltered[i].DistrictName + "'data-id='" + districtFiltered[i].DistrictCode + "'>"
            + districtFiltered[i].DistrictType + " " + districtFiltered[i].DistrictName + "</option>")
    }
    displayWard([]);
}

$("#districtInput").on('change', function () {
    var districtCode = $("#districtInput").find(":selected").data('id');
    var wardFiltered = ward.filter(q => q.DistrictCode == districtCode);
    displayWard(wardFiltered);
});

function displayWard(wardFiltered) {
    $("#wardInput").empty();
    $("#wardInput").append("<option value=''>Vui lòng chọn phường/xã</option>");
    for (var i = 0; i < wardFiltered.length; i++) {
        $("#wardInput").append("<option value='" + wardFiltered[i].WardType + " " + wardFiltered[i].WardName + "'>"
            + wardFiltered[i].WardType + " " + wardFiltered[i].WardName + "</option>")
    }
}

function swalCommingSoon() {
    swal({
        confirmButtonColor: "#235b57",
        title: "Coming soon",
        type: "info"
    });
}

// Delivery Info module
$(document).on('click', '.btn-edit', function () {
    $('.spinner-group').show();
    var id = $(this).data('id');
    idEdit = id;
    flagEdit = true;
    $.ajax({
        url: "/SWCart/GetDeliveryInfor",
        method: "POST",
        data: {
            deliveryId: id
        },
        success: function (result) {
            if (result.success == true) {
                var deliveryInfo = result.deliveryInfo;
                $('#nameInput').val(deliveryInfo.Name);
                $('#phoneInput').val(deliveryInfo.Phone);

                //City
                //$('#cityInput').val(deliveryInfo.City);
                $("#cityInput option:selected").removeAttr("selected");
                $("#cityInput option[value='" + deliveryInfo.City + "']").prop('selected', true);
                $("#cityInput").trigger("change");

                //District
               // $('#districtInput').val(deliveryInfo.District);
                $("#districtInput option:selected").removeAttr("selected");
                $("#districtInput option[value='" + deliveryInfo.District + "']").prop('selected', true);
                $("#districtInput").trigger("change");

                //Ward
                //$('#wardInput').val(deliveryInfo.Ward);
                $("#wardInput option:selected").removeAttr("selected");
                $("#wardInput option[value='" + deliveryInfo.Ward + "']").prop('selected', true);
                //$("#wardInput").trigger("change");

                $('#addressInput').val(deliveryInfo.Address);
                if (deliveryInfo.TypeAddress) {
                    $('#typeAdd1').prop('checked', true);
                    $('#typeAdd2').prop('checked', false);
                } else {
                    $('#typeAdd1').prop('checked', false);
                    $('#typeAdd2').prop('checked', true);
                }
                if (deliveryInfo.IsDefault) {
                    $("#addDefault").attr('checked', false);
                    $("#checkDefault").click();
                } else {
                    $("#addDefault").attr('checked', false);
                }
                $('#addForm').show();
            }
        }
    });
});
$(document).on('click', '.btn-delete', function () {
    $('.spinner-group').show();
    var id = $(this).data('id');
    $.ajax({
        url: "/SWCart/DeleteDeliveryInfor",
        method: "POST",
        data: {
            deliveryId: id
        },
        success: function (result) {
            angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
            $('.spinner-group').hide();
        }
    });
});

//mobile


function displayProvince2() {
    $("#cityInput2").empty();
    $("#cityInput2").append("<option value=''>Vui lòng chọn tỉnh/thành phố</option>");
    for (var i = 0; i < province.length; i++) {
        $("#cityInput2").append("<option value='" + province[i].ProvinceType + " " + province[i].ProvinceName + "' data-id='" + province[i].ProvinceCode + "'>"
            + province[i].ProvinceType + " " + province[i].ProvinceName + "</option>")
    }
}

$("#cityInput2").on('change', function () {
    var provinceCode = $("#cityInput2").find(":selected").data('id');
    var districtFiltered = district.filter(q => q.ProvinceCode == provinceCode);
    displayDistrict2(districtFiltered);
});

function displayDistrict2(districtFiltered) {
    $("#districtInput2").empty();
    $("#districtInput2").append("<option value=''>Vui lòng chọn quận/huyện</option>");
    for (var i = 0; i < districtFiltered.length; i++) {
        $("#districtInput2").append("<option value='" + districtFiltered[i].DistrictType + " " + districtFiltered[i].DistrictName + "'data-id='" + districtFiltered[i].DistrictCode + "'>"
            + districtFiltered[i].DistrictType + " " + districtFiltered[i].DistrictName + "</option>")
    }
    displayWard2([]);
}

$("#districtInput2").on('change', function () {
    var districtCode = $("#districtInput2").find(":selected").data('id');
    var wardFiltered = ward.filter(q => q.DistrictCode == districtCode);
    displayWard2(wardFiltered);
});

function displayWard2(wardFiltered) {
    $("#wardInput2").empty();
    $("#wardInput2").append("<option value=''>Vui lòng chọn phường/xã</option>");
    for (var i = 0; i < wardFiltered.length; i++) {
        $("#wardInput2").append("<option value='" + wardFiltered[i].WardType + " " + wardFiltered[i].WardName + "'>"
            + wardFiltered[i].WardType + " " + wardFiltered[i].WardName + "</option>")
    }
}

// Delivery Info module
$(document).on('click', '.btn-editmb', function () {
    $('.spinner-group').show();
    var id = $(this).data('id');
    idEdit = id;
    flagEdit = true;
    $.ajax({
        url: "/SWCart/GetDeliveryInfor",
        method: "POST",
        data: {
            deliveryId: id
        },
        success: function (result) {
            if (result.success == true) {
                var deliveryInfo = result.deliveryInfo;
                $('#nameInput2').val(deliveryInfo.Name);
                $('#phoneInput2').val(deliveryInfo.Phone);

                //City
                //$('#cityInput').val(deliveryInfo.City);
                $("#cityInput2 option:selected").removeAttr("selected");
                $("#cityInput2 option[value='" + deliveryInfo.City + "']").prop('selected', true);
                $("#cityInput2").trigger("change");

                //District
                // $('#districtInput').val(deliveryInfo.District);
                $("#districtInput2 option:selected").removeAttr("selected");
                $("#districtInput2 option[value='" + deliveryInfo.District + "']").prop('selected', true);
                $("#districtInput2").trigger("change");

                //Ward
                //$('#wardInput').val(deliveryInfo.Ward);
                $("#wardInput2 option:selected").removeAttr("selected");
                $("#wardInput2 option[value='" + deliveryInfo.Ward + "']").prop('selected', true);
                //$("#wardInput").trigger("change");

                $('#addressInput2').val(deliveryInfo.Address);
                if (deliveryInfo.TypeAddress) {
                    $('#typeAdd1m').prop('checked', true);
                    $('#typeAdd2m').prop('checked', false);
                } else {
                    $('#typeAdd1m').prop('checked', false);
                    $('#typeAdd2m').prop('checked', true);
                }
                if (deliveryInfo.IsDefault) {
                    $("#addDefaultm").attr('checked', false);
                    $("#checkDefaultm").click();
                } else {
                    $("#addDefaultm").attr('checked', false);
                }
                $('#addFormmb').show();
            }
        }
    });
});
$(document).on('click', '.btn-delete', function () {
    $('.spinner-group').show();
    var id = $(this).data('id');
    $.ajax({
        url: "/SWCart/DeleteDeliveryInfor",
        method: "POST",
        data: {
            deliveryId: id
        },
        success: function (result) {
            angular.element('.order-controller').scope().vm.loadAllUserDeliveryInfor(USER_ID);
            $('.spinner-group').hide();
        }
    });
});