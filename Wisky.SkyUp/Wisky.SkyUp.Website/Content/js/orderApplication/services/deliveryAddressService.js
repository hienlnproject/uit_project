﻿angular.module('OrderApp').factory('deliveryAddress', function ($http, $q) {
    var service = {

        /*
         * author: HieuTN
         * method: load all delivery address from database
         */
        initialize: function (){
            service.getAllDeliveryAddress();
        },

        /*
         * author: HieuTN
         * method: Load All delivery address
         */
        getAllDeliveryAddress: function () {
            var deferred = $q.defer();

            $http({
                url: '/SWCart/LoadAllDeliveryAddress',
                method: 'GET',
            }).success(function (data) {
                deferred.resolve(data.deliveryAddress);
            }).error(function () {
            });
            return deferred.promise;
        },
    }
});