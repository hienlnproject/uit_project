angular.module('OrderApp').factory('productService', function ($http, $localStorage, $q) {
    $localStorage.$default({
        categories: [],
        products: [],
        orders: [],
        lastPayment: 0,
        lastOrders: [],
        lastTotalDiscount: 0,
        msg: "",
        checkVoucher: false,
        deliveryInfo: {
            deliveryType: '',
            paymentMethod: '',
            name: '',
            email: '',
            phone: '',
            address: '',
            city: '',
            district: '',
            ward: '',
            typeAddress: '',
            fee: '',
            brandId: '',
            note: '',
            voucherCode: '',
            requiredCustomer: '',
            checkInPerson: "",
            deliveryCost: 0,
            aggrement: false,
            areaId: 0,
        }
    });

    var service = {
        /*
         * author: TrungNDT
         * method: Loadd all products and categories from database
         */
        initialize: function () {
            //service.loadAllProducts();
            //service.loadAllCategories();
            //service.loadHotProducts();
            //service.loadBestSaleProducts();
            //service.getDeliveryInfo();
        },

        /*
         * author: TrungNDT
         * method: Get product list in localstorage
         */
        getProducts: function () {
            return $localStorage.products;
        },

        clearStorageAfterViewResult: function () {
            $localStorage.lastPayment = 0;
            $localStorage.lastOrders = [];
            $localStorage.checkVoucher = false;
        },

        /*
         * author: TrungNDT
         * method: Get order list in localstorage
         */
        getOrders: function () {
            return $localStorage.orders;
        },
        /*
         * author: BaoTD
         * method: Get last checkout orders
         */
        getLastOrders: function () {
            return $localStorage.lastOrders;
        },

        /*
         * author: BaoTD
         * method: Get last checkout orders
         */
        getLastTotalCost: function () {
            return $localStorage.lastPayment;
        },

        /*
         * author: PHUONGTA
         * method: Get number products of orders in localstorage
         */
        getNumInOrders: function () {
            var list = $localStorage.orders;
            return list.length;
        },

        /*
         * author: TrungNDT
         * method: Get product categories list
         */
        getCategories: function () {
            return $localStorage.categories;
        },

        /*
         * author: TrungNDT
         * method: Get delivery info
         */
        getDeliveryInfo: function (z) {
            return $localStorage.deliveryInfo;
        },

        /*
         * author: PhuDCQ
         * method: Add given product to cart. Count up if product exist
         */
        addProductToCart: function (product, idGen) {
            if (product == null) {
                alert("Xin vui lòng thử lại. Cảm ơn!")
            }

            product.tempId = idGen;
            if (service.isSingleItem(product)) {
                //var orderIndex = service.getOrderIndex(product.id);
                var order = service.getExistingProduct(product);
                // Order exist
                // debugger
                if (order != null) {
                    order.quantity += product.quantity; // changed by DucBM
                    //alertify.message('<b>' + product.name + '</b>' + ' đã được thêm vào giỏ hàng. <b> (x' + order.quantity + ') </b>');
                } else { // New order
                    product.orderId = $localStorage.orders.length;
                    $localStorage.orders.push(angular.copy(product));
                    //alertify.message('<b>' + product.name + '</b>' + ' đã được thêm vào giỏ hàng. <b>(x1)</b>');
                }
            } else {
                service.shortenExtraName(product);
                product.orderId = $localStorage.orders.length;
                $localStorage.orders.push(angular.copy(product));
            }



            product = {};
        },


        //
        getOrderQuantity: function (productId) {
            var orderIndex = service.getOrderIndex(productId);
            if (orderIndex > -1) {
                return $localStorage.orders[orderIndex].quantity;
            }
        },
        /*
         * author: TrungNDT
         * method: update existing order by given information
         */
        updateOrder: function (order) {
            var index = service.getOrderIndexByOrderIdUpdate(order.tempId);
            if (index > -1) {
                $localStorage.orders.splice(index, 1, order);
            }
        },

        /*
         * author: TrungNDT
         * method: remove given order from cart
         */
        removeOrder: function (order) {
            var index = $localStorage.orders.indexOf(order);
            if (index > -1) {
                $localStorage.orders.splice(index, 1);
            }

        },

        submitVoucher: function () {
            //var matcher = {
            //    'Voucher': 'voucher',
            //}
            $('.spinner-group').fadeIn();
            if ($localStorage.msg != "") {
                $localStorage.msg = "";
            }
            var voucherCode = angular.copy($localStorage.deliveryInfo.voucherCode);
            //var brandId = angular.copy($localStorage.deliveryInfo.brandId);
            $http.post('/SWCart/ApplyVoucher', {
                voucherCode: voucherCode,
                //brandId: JSON.stringify(brandId)
            }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (result) {
                    $('.spinner-group').fadeOut();
                    $localStorage.msg = angular.copy(result.msg);
                    $localStorage.checkVoucher = angular.copy(result.checkVoucher);
                    if (result.checkVoucher) {
                        $('#validVoucher').show();
                        $('#invalidVoucher').hide();
                    } else {
                        $localStorage.deliveryInfo.voucherCode = "";
                        $('#invalidVoucher').show();
                        $('#validVoucher').hide();
                    }

                    return true;
                });
        },

        getResultSubmitVoucher: function () {
            var check = false;
            if ($localStorage.checkVoucher && !$localStorage.deliveryInfo.voucherCode == '') {
                check = true;
            }
            return check;
        },

        checkDiscountNoVoucher: function () {
            //setTimeout(function () {
            //    $('.spinner-group').fadeIn()
            //}, 0);

            var matcher = {
                'DeliveryType': 'deliveryType',
                'DeliveryPayment': 'paymentMethod',
                'CheckInPerson': 'checkInPerson',
                'CustomerName': 'name',
                'Phone': 'phone',
                'Email': 'email',
                'Note': 'note',
                'DeliveryAddress': 'address',
                'City': 'city',
                'District': 'district',
                'Ward': 'ward',
                'TypeAddress': 'typeAddress',
                'StoreName': 'store',
                'OrderDate': 'orderDate',
                'Voucher': 'voucherCode',
                'DeliveryCost': 'deliveryCost',
                'AreaId': 'areaId',
                'list': {
                    'name': 'productList',
                    'matchWith': 'ProductList',
                    'data': {
                        'ProductId': 'id',
                        'Quantity': 'quantity',
                        'ProductCode': 'productCode',
                        'Price': 'price',
                        'Size': 'size',
                        'Color': 'color',
                        'TempId': 'tempId',
                        'list': {
                            'name': 'extraList',
                            'matchWith': 'ProductWithExtras',
                            'ProductWithExtras': 'extraList',
                            'data': {
                                'ProductExtraId': 'id',
                                'Price': 'price',
                                'ProductCode': 'productCode',
                                'Quantity': 'quantity',
                                'ParentId': 'parentId'
                            }
                        }
                    }
                }
            };
            //$localStorage.deliveryInfo.address = "aaaaa";
            $localStorage.deliveryInfo = JSON.parse(localStorage["ngStorage-deliveryInfo"]);
            var temp = angular.copy($localStorage.deliveryInfo);
            //var brandId = angular.copy($localStorage.deliveryInfo.brandId);
            temp.orderDate = new Date();
            temp.productList = [];

            var index = 0;



            // Convert order (order with extra list) into "same level" structure
            angular.forEach(angular.copy($localStorage.orders), function (e, i) {
                // Clone main order items
                temp.productList.push({
                    id: e.id,
                    name: e.name,
                    price: e.price,
                    quantity: e.quantity,
                    size: e.size,
                    color: e.color,
                    tempId: e.tempId,
                });
                if (e.extraList != undefined) {
                    // Clone extra items
                    temp.productList[index].extraList = [];
                    angular.forEach(e.extraList, function (k, j) {
                        if (k.quantity > 0) {
                            temp.productList[index].extraList.push({
                                id: k.id,
                                name: k.name,
                                price: k.price,
                                quantity: k.quantity,
                                parentId: k.parentId
                            });
                        }
                    });
                }
                index = index + 1;
            });

            if (temp.productList.length <= 0) {
                swal({
                    title: "Lỗi",
                    text: "Chưa có sản phẩm nào trong đơn hàng",
                    type: "error"
                }, function () {
                    //setTimeout(function () {
                    //    $('.spinner-group').fadeOut()
                    //}, 10);
                    window.location.href = "/";
                    return;
                });
                //setTimeout(function () {
                //    $('.spinner-group').fadeOut()
                //}, 10);
                return;
            }
            //if ($localStorage.deliveryInfo.deliveryType == 2) {
            //    temp.productList.push({
            //        id: 163,
            //        name: "FastDelivery",
            //        price: 50000,
            //        quantity: 1,
            //        size: null,
            //        color: null,
            //        tempId: 0
            //    })
            //}

            var order = {};
            JsonMatch(order, temp, matcher);

            // $http.post('/SWCart/CreateOrder', {
            // str: JSON.stringify(order)
            // }, {
            // headers: {
            // 'Content-Type': 'application/json'
            // }
            // });
            $http.post('/SWCart/CheckDiscount', {
                str: JSON.stringify(order),
                //brandId: JSON.stringify(brandId)
            }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (result) {
                    setTimeout(function () {
                        $('.spinner-group').fadeOut()
                    }, 10);
                    if (result.success) {
                        $localStorage.lastTotalDiscount = result.totalDiscount;
                        console.log(result.totalDiscount);
                        $localStorage.msg = angular.copy(result.msg);
                        $localStorage.checkVoucher = angular.copy(result.checkVoucher);
                        //if (result.checkVoucher) {
                        //    $('#validVoucher').show();
                        //    $('#invalidVoucher').hide();
                        //} else {
                        //    $localStorage.deliveryInfo.voucherCode = "";
                        //    $('#invalidVoucher').show();
                        //    $('#validVoucher').hide();
                        //}
                        return result.totalDiscount;
                    } else {
                        $localStorage.lastTotalDiscount = 0;
                        $localStorage.msg = angular.copy(result.msg);
                        $localStorage.checkVoucher = angular.copy(result.checkVoucher);
                        //if (result.checkVoucher) {
                        //    $('#validVoucher').show();
                        //    $('#invalidVoucher').hide();
                        //} else {
                        //    $localStorage.deliveryInfo.voucherCode = "";
                        //    $('#invalidVoucher').show();
                        //    $('#validVoucher').hide();
                        //}
                        //$localStorage.deliveryInfo.voucherCode = "";
                        return 0;
                        console.log('Xin quý khách vui lòng thử lại.');
                    }

                }).error(function (result) {
                    $localStorage.deliveryInfo.voucherCode = "";
                    console.log('Xin quý khách vui lòng thử lại.');
                });
        },

        checkDiscount: function () {
            setTimeout(function () {
                $('.spinner-group').fadeIn()
            }, 0);

            var matcher = {
                'DeliveryType': 'deliveryType',
                'DeliveryPayment': 'paymentMethod',
                'CheckInPerson': 'checkInPerson',
                'CustomerName': 'name',
                'Phone': 'phone',
                'Email': 'email',
                'Note': 'note',
                'DeliveryAddress': 'address',
                'City': 'city',
                'District': 'district',
                'Ward': 'ward',
                'TypeAddress': 'typeAddress',
                'StoreName': 'store',
                'OrderDate': 'orderDate',
                'Voucher': 'voucherCode',
                'DeliveryCost': 'deliveryCost',
                'AreaId': 'areaId',
                'list': {
                    'name': 'productList',
                    'matchWith': 'ProductList',
                    'data': {
                        'ProductId': 'id',
                        'Quantity': 'quantity',
                        'ProductCode': 'productCode',
                        'Price': 'price',
                        'Size': 'size',
                        'Color': 'color',
                        'TempId': 'tempId',
                        'list': {
                            'name': 'extraList',
                            'matchWith': 'ProductWithExtras',
                            'ProductWithExtras': 'extraList',
                            'data': {
                                'ProductExtraId': 'id',
                                'Price': 'price',
                                'ProductCode': 'productCode',
                                'Quantity': 'quantity',
                                'ParentId': 'parentId'
                            }
                        }
                    }
                }
            };
            //$localStorage.deliveryInfo.address = "aaaaa";
            $localStorage.deliveryInfo = JSON.parse(localStorage["ngStorage-deliveryInfo"]);
            var temp = angular.copy($localStorage.deliveryInfo);
            //var brandId = angular.copy($localStorage.deliveryInfo.brandId);
            temp.orderDate = new Date();
            temp.productList = [];

            var index = 0;



            // Convert order (order with extra list) into "same level" structure
            angular.forEach(angular.copy($localStorage.orders), function (e, i) {
                // Clone main order items
                temp.productList.push({
                    id: e.id,
                    name: e.name,
                    price: e.price,
                    quantity: e.quantity,
                    size: e.size,
                    color: e.color,
                    tempId: e.tempId,
                });
                if (e.extraList != undefined) {
                    // Clone extra items
                    temp.productList[index].extraList = [];
                    angular.forEach(e.extraList, function (k, j) {
                        if (k.quantity > 0) {
                            temp.productList[index].extraList.push({
                                id: k.id,
                                name: k.name,
                                price: k.price,
                                quantity: k.quantity,
                                parentId: k.parentId
                            });
                        }
                    });
                }
                index = index + 1;
            });

            if (temp.productList.length <= 0) {
                swal({
                    title: "Lỗi",
                    text: "Chưa có sản phẩm nào trong đơn hàng",
                    type: "error"
                }, function () {
                    setTimeout(function () {
                        $('.spinner-group').fadeOut()
                    }, 10);
                    window.location.href = "/";
                    return;
                });
                setTimeout(function () {
                    $('.spinner-group').fadeOut()
                }, 10);
                return;
            }
            //if ($localStorage.deliveryInfo.deliveryType == 2) {
            //    temp.productList.push({
            //        id: 163,
            //        name: "FastDelivery",
            //        price: 50000,
            //        quantity: 1,
            //        size: null,
            //        color: null,
            //        tempId: 0
            //    })
            //}

            var order = {};
            JsonMatch(order, temp, matcher);

            // $http.post('/SWCart/CreateOrder', {
            // str: JSON.stringify(order)
            // }, {
            // headers: {
            // 'Content-Type': 'application/json'
            // }
            // });
            $http.post('/SWCart/CheckDiscount', {
                str: JSON.stringify(order),
                //brandId: JSON.stringify(brandId)
            }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (result) {
                    setTimeout(function () {
                        $('.spinner-group').fadeOut()
                    }, 10);
                    if (result.success) {
                        $localStorage.lastTotalDiscount = result.totalDiscount;
                        console.log(result.totalDiscount);
                        $localStorage.msg = angular.copy(result.msg);
                        $localStorage.checkVoucher = angular.copy(result.checkVoucher);
                        if (result.checkVoucher) {
                            $('#validVoucher').show();
                            $('#invalidVoucher').hide();
                        } else {
                            $localStorage.deliveryInfo.voucherCode = "";
                            $('#invalidVoucher').show();
                            $('#validVoucher').hide();
                        }
                        return result.totalDiscount;
                    } else {
                        $localStorage.lastTotalDiscount = 0;
                        $localStorage.msg = angular.copy(result.msg);
                        $localStorage.checkVoucher = angular.copy(result.checkVoucher);
                        if (result.checkVoucher) {
                            $('#validVoucher').show();
                            $('#invalidVoucher').hide();
                        } else {
                            $localStorage.deliveryInfo.voucherCode = "";
                            $('#invalidVoucher').show();
                            $('#validVoucher').hide();
                        }
                        //$localStorage.deliveryInfo.voucherCode = "";
                        return 0;
                        console.log('Xin quý khách vui lòng thử lại.');
                    }

                }).error(function (result) {
                    $localStorage.deliveryInfo.voucherCode = "";
                    console.log('Xin quý khách vui lòng thử lại.');
                });
        },

        getLastTotalDiscount: function () {
            return $localStorage.lastTotalDiscount;
        },

        clearLastTotalDiscount: function () {
            $localStorage.lastTotalDiscount = 0;
        },
        /*
         * author: TrungNDT
         * method: Submit new order & save to database.
         *          Also clear shopping cart and keep delivery info
         */
        submitOrder: function () {
            setTimeout(function () {
                $('.spinner-group').fadeIn()
            }, 0);

            var matcher = {
                'DeliveryType': 'deliveryType',
                'DeliveryPayment': 'paymentMethod',
                'CheckInPerson': 'checkInPerson',
                'CustomerName': 'name',
                'Phone': 'phone',
                'Email': 'email',
                'Note': 'note',
                'DeliveryAddress': 'address',
                'City': 'city',
                'District': 'district',
                'Ward': 'ward',
                'TypeAddress': 'typeAddress',
                'StoreName': 'store',
                'OrderDate': 'orderDate',
                'Voucher': 'voucherCode',
                'DeliveryCost': 'deliveryCost',
                'RequiredCustomer': 'requiredCustomer',
                'AreaId': 'areaId',
                'list': {
                    'name': 'productList',
                    'matchWith': 'ProductList',
                    'data': {
                        'ProductId': 'id',
                        'Quantity': 'quantity',
                        'ProductCode': 'productCode',
                        'Price': 'price',
                        'Size': 'size',
                        'Color': 'color',
                        'Price': 'price',
                        'Dispercent': 'dispercent',
                        'Price': 'price',
                        'TempId': 'tempId',
                        'list': {
                            'name': 'extraList',
                            'matchWith': 'ProductWithExtras',
                            'ProductWithExtras': 'extraList',
                            'data': {
                                'ProductExtraId': 'id',
                                'Price': 'price',
                                'ProductCode': 'productCode',
                                'Quantity': 'quantity',
                                'ProductCode': 'productCode',
                                'Text': 'text',
                                'Color': 'color',
                                'Style': 'style',
                                'ParentId': 'parentId',
                                'ImageUrl':'imageUrl',
                            }
                        }
                    }
                }
            };
            //$localStorage.deliveryInfo.address = "aaaaa";
            $localStorage.deliveryInfo = JSON.parse(localStorage["ngStorage-deliveryInfo"]);
            var temp = angular.copy($localStorage.deliveryInfo);
            //var brandId = angular.copy($localStorage.deliveryInfo.brandId);
            temp.orderDate = new Date();
            temp.productList = [];

            var index = 0;



            // Convert order (order with extra list) into "same level" structure
            angular.forEach(angular.copy($localStorage.orders), function (e, i) {
                // Clone main order items
                var finalPrice = e.price - (e.price * e.dispercent / 100);
                temp.productList.push({
                    id: e.id,
                    name: e.name,
                    price: e.finalPrices,
                    quantity: e.quantity,
                    size: e.size,
                    color: e.color,
                    tempId: e.tempId,
                });
                if (e.extraList != undefined) {
                    // Clone extra items
                    temp.productList[index].extraList = [];
                    angular.forEach(e.extraList, function (k, j) {
                        if (k.quantity > 0) {
                            temp.productList[index].extraList.push({
                                id: k.id,
                                name: k.name,
                                price: k.price,
                                quantity: k.quantity,
                                text: k.text,
                                color: k.color,
                                style: k.style,
                                imageUrl: k.imageUrl,
                                parentId: temp.productList[index].tempId,
                            });
                        }
                    });
                }
                index = index + 1;
            });

            if (temp.productList.length <= 0) {
                swal({
                    title: "Lỗi",
                    text: "Chưa có sản phẩm nào trong đơn hàng",
                    type: "error"
                }, function () {
                    setTimeout(function () {
                        $('.spinner-group').fadeOut()
                    }, 10);
                    window.location.href = "/";
                    return;
                });
                setTimeout(function () {
                    $('.spinner-group').fadeOut()
                }, 10);
                return;
            }
            //if ($localStorage.deliveryInfo.deliveryType == 2) {
            //    temp.productList.push({
            //        id: 163,
            //        name: "FastDelivery",
            //        price: 50000,
            //        quantity: 1,
            //        size: null,
            //        color: null,
            //        tempId: 0
            //    })
            //}

            var order = {};
            JsonMatch(order, temp, matcher);

            // $http.post('/SWCart/CreateOrder', {
            // str: JSON.stringify(order)
            // }, {
            // headers: {
            // 'Content-Type': 'application/json'
            // }
            // });
            $http.post('/SWCart/SendOrderWithVoucherToServer', {
                str: JSON.stringify(order),
                //brandId: JSON.stringify(brandId)
            }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (result) {
                    setTimeout(function () {
                        $('.spinner-group').fadeOut()
                    }, 10);
                    if (result.success) {
                        $localStorage.lastPayment = angular.copy(service.getTotalCost());
                        $localStorage.lastOrders = angular.copy($localStorage.orders);
                        service.clearStorage();
                        //window.location.href = '/thanh-toan';
                        swal({
                            title: "Thành công",
                            text: "Tạo đơn hàng thành công! Cảm ơn quý khách vì đã sử dụng dịch vụ của UniChef",
                            type: "success"
                        }, function () {
                            window.location.href = "/";
                        });
                    } else {
                        alert('Xin quý khách vui lòng thử lại.');
                    }
                });

            //$.ajax({
            //    type: "POST",
            //    url: "/SWCart/SendOrderWithVoucherToServer",
            //    data: {
            //        str: JSON.stringify(order),
            //    },
            //    success: function (result) {
            //        if (result.success) {
            //            $localStorage.lastPayment = angular.copy(service.getTotalCost());
            //            $localStorage.lastOrders = angular.copy($localStorage.orders);
            //            service.clearStorage();
            //            //window.location.href = '/thanh-toan';
            //            swal({
            //                title: "Thành công",
            //                text: "Tạo đơn hàng thành công! Cảm ơn quý khách vì đã sử dụng dịch vụ của UniChef",
            //                type: "success"
            //            }, function () {
            //                window.location.href = "/";
            //            });
            //        } else {
            //            alert('Xin quý khách vui lòng thử lại.');
            //        }
            //    }
            //});
        },
        clearStorage: function () {
            $localStorage.orders = [];
            $localStorage.deliveryInfo = {};
            $localStorage.checkVoucher = false;
            $localStorage.clearLastTotalDiscount = 0;
        },
        clearDeliveryInfor: function () {
            $localStorage.deliveryInfo.address = "";
            $localStorage.deliveryInfo.deliveryType = "";
            $localStorage.deliveryInfo.paymentMethod = "";
            $localStorage.deliveryInfo.voucherCode = "";
            $localStorage.deliveryInfo.deliveryCost = 0;
            $localStorage.lastTotalDiscount = 0;
        },

        /*
         * author: TrungNDT
         * method: check if given order has extra or not
         */
        isSingleItem: function (order) {
            var isSingle = true;
            if (order.extraList != undefined)
                angular.forEach(order.extraList, function (e, i) {
                    var quantity = e.quantity || 0;
                    if (quantity > 0)
                        isSingle = false;
                });
            return isSingle;
        },

        /*
         * author: TrungNDT
         * method: [Use for Expression] remove "Extra " string from extra name
         */
        shortenExtraName: function (order) {
            angular.forEach(order.extraList, function (e, i) {
                e.name = e.name.replace('Extra ', '');
            });
        },

        /*
         * author: TrungNDT
         * method: [Private] Get index of given product
         * params: {Number} product id
         * return: {Number} Product index. Return -1 if product not found.
         */
        getOrderIndex: function (productId) {
            var pos = -1;
            for (var i = 0, l = $localStorage.orders.length; i < l && pos < 0; i++)
                if ($localStorage.orders[i].id == productId
                    && service.isSingleItem($localStorage.orders[i])) pos = i;
            return pos;
        },
        getExtraIndex: function (productId, extraId) {
            var pos = -1;
            for (var i = 0, l = $localStorage.orders.length; i < l && pos < 0; i++)
                if ($localStorage.orders[i].id == productId) {
                    for (var j = 0; j < $localStorage.orders[i].extraList.length; j++) {
                        if ($localStorage.orders[i].extraList[j].id = extraId) {
                            pos = j;
                        }
                    }
                }
            return pos;
        },
        /*
         * author: DucBM
         * method: [Private] Get the existing product
         * params: {Number} product
         * return: {Number} The existing product in localStorage. Return null if product not found.
         */
        getExistingProduct: function (product) {
            var result = null;
            for (var i = 0; i < $localStorage.orders.length; i++)
                if ($localStorage.orders[i].id == product.id
                    && $localStorage.orders[i].size == product.size
                    && $localStorage.orders[i].color == product.color) {
                    result = $localStorage.orders[i];
                    break;
                }
            return result;
        },

        getOrderByTempId: function (tempId) {
            var order;
            for (var i = 0; i < $localStorage.orders.length; i++)
                if ($localStorage.orders[i].tempId == tempId)
                    //&& service.isSingleItem($localStorage.orders[i])) 
                    order = $localStorage.orders[i];
            return order;
        },

        isEmptyExtraList: function (tempId) {
            var isEmpty = true;
            var order = service.getOrderByTempId(tempId);
            angular.forEach(order.extraList, function (e, i) {
                if (e.quantity > 0) {
                    return isEmpty = false;
                }
            });
            return isEmpty;
        },

        getLastOrderByTempId: function (tempId) {
            var order;
            var orders = service.getLastOrders();
            for (var i = 0; i < orders.length; i++)
                if (orders[i].tempId == tempId)
                    //&& service.isSingleItem($localStorage.orders[i])) 
                    order = orders[i];
            return order;
        },

        isEmptyExtraListLastOrder: function (tempId) {
            var isEmpty = true;
            var order = service.getLastOrderByTempId(tempId);
            angular.forEach(order.extraList, function (e, i) {
                if (e.quantity > 0) {
                    return isEmpty = false;
                }
            });
            return isEmpty;
        },

        /*
         * author: TrungNDT
         * method: [Private] 
         */
        getOrderIndexByOrderId: function (orderId) {
            var pos = -1;
            for (var i = 0, l = $localStorage.orders.length; i < l && pos < 0; i++)
                if ($localStorage.orders[i].id == productId
                    && service.isSingleItem($localStorage.orders[i])) pos = i;
            return pos;
        },

        getOrderIndexByOrderIdUpdate: function (idGen) {
            var pos = -1;
            for (var i = 0, l = $localStorage.orders.length; i < l && pos < 0; i++)
                if ($localStorage.orders[i].tempId == idGen)
                    //&& service.isSingleItem($localStorage.orders[i])) 
                    pos = i;
            return pos;
        },

        /*
         * author: TrungNDT
         * method: Get total cost base on price, quantity & extra
         * return: {Number} order cost
         */
        /* getOrderCost: function (order) {
            var extraCost = 0;
            // Calculate extra cost
            angular.forEach(order.extraList, function (e, i) {
                extraCost += e.price * (e.quantity || 0);
            })
            // Calculate TOTAL cost
            var totalCost = (order.price + extraCost) * order.quantity;
            return totalCost;
        }, */

        //PHUONGTA cost with discount
        getOrderCost: function (order) {
            var extraCost = 0;
            // Calculate extra cost
            angular.forEach(order.extraList, function (e, i) {
                extraCost += e.price * (e.quantity || 0);
            })
            // Calculate TOTAL cost
            if (order.dispercent == 0) {
                var totalCost = (order.price + extraCost) * order.quantity;
            } else {
                var totalCost = (order.price - (order.price * order.dispercent / 100) + extraCost) * order.quantity;
            }

            return totalCost;
        },

        /*
        * author: TrungNDT
        * method: [EVENT: CHANGE] Plus or minus product quantity
        */
        changeProductQuantity: function (isPlus) {
            if (isPlus) {
                order.quantity++;
            } else {
                if (order.quantity > 1)
                    order.quantity--;
            }
        },

        /*
         * author: BaoTD
         * method: Get delivery fee
         * return: Delivery cost based on distance
         */
        getDeliveryFee: function () {
            return $localStorage.deliveryInfo.distance * $localStorage.deliveryInfo.fee;
        },


        /*
         * author: TrungNDT
         * method: Calculate entire cart's cost
         * return: {Number} total cost
         */
        getTotalCost: function () {
            var totalCost = 0;
            angular.forEach($localStorage.orders, function (e, i) {
                totalCost += service.getOrderCost(e);
            });
            return totalCost;
        },

        /*
         * author: TrungNDT
         * method: Clear shopping cart
         */
        emptyCart: function () {
            $localStorage.orders = [];
        },

        /*
         * author: TrungNDT
         * method: [Private] Load all product from database
         */
        loadAllProducts: function (cateId) {
            var deferred = $q.defer();
            if (cateId == null) {
                cateId = 0;
            }
            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadItemByCategory',
                method: 'GET',
                params: { cateId: cateId }
            }).success(function (data) {
                deferred.resolve(data.products);
                //$localStorage.products = data.products;
            }).error(function () {
            });
            return deferred.promise;
            //} else {
            //    //return false;
            //}
        },

        loadAllProductsOfCateParent: function (cateId) {
            var deferred = $q.defer();

            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadItemByCategoryId',
                method: 'GET',
                params: { cateId: cateId }
            }).success(function (data) {
                deferred.resolve(data.products);
            }).error(function () {
            });
            return deferred.promise;

        },
        /*
         * author: BaoTD
         * method: Load all product in a category
         */
        loadAllProductsInCategory: function (categoryId) {
            var deferred = $q.defer();
            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadItemByCategory',
                method: 'GET',
                params: { cateId: categoryId }
            }).success(function (data) {
                deferred.resolve(data.products);
            }).error(function () {
            });
            return deferred.promise;
        },

        /*PHUONGTA - load products by collection [HotProducts]*/
        loadHotProducts: function () {
            var deferred = $q.defer();
            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadProductCollection',
                method: 'GET',
                params: { collectionId: 1 }//HOtCollection in ProductCollection database
            }).success(function (data) {
                deferred.resolve(data.products);
            }).error(function () {
            });
            return deferred.promise;
        },

        /*PHUONGTA - load products by collection [BestSaleProducts]*/
        loadBestSaleProducts: function () {
            var deferred = $q.defer();
            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadProductCollection',
                method: 'GET',
                params: { collectionId: 2 }
            }).success(function (data) {
                deferred.resolve(data.products);
            }).error(function () {
            });
            return deferred.promise;
        },



        loadAllProductsInCategory: function (categoryId) {
            var deferred = $q.defer();
            //if (!$localStorage.products.length) {
            $http({
                url: '/SWCart/LoadItemByCategory',
                method: 'GET',
                params: { cateId: categoryId }
            }).success(function (data) {
                deferred.resolve(data.products);
            }).error(function () {
            });
            return deferred.promise;
        },

        /*
         * author: BaoTD
         * method: Load all images of a product
         */
        loadImagesByProductId: function (productId) {
            var deferred = $q.defer();
            $http({
                url: '/SWCart/LoadProductImages',
                method: 'GET',
                params: { productId: productId }
            }).success(function (result) {
                deferred.resolve(result.images);
            }).error(function (error) {
            });
            return deferred.promise;
        },

        /*
         * author: DucBM
         * method: Load all user's addresses
         */
        loadAllUserDeliveryInfor: function (userId) {
            var deferred = $q.defer();
            $http({
                url: '/SWCart/LoadAllUserDeliveryInfor',
                method: 'POST',
                params: { userId: userId }
            }).success(function (result) {
                deferred.resolve(result.deliveryInfo); // a list of delivery infos
            }).error(function (error) {
                console.log('Error occurs: ' + error);
            });
            return deferred.promise;
        },

        /*
         * author: DucBM
         * method: Get total quantity
         */
        getTotalQuantity: function () {
            var orders = $localStorage.orders;
            var totalQuantity = 0;
            angular.forEach(orders, function (value, key) {
                totalQuantity += value.quantity;
            });
            return totalQuantity;
        },
        getOrdered: function (username) {
            var deferred = $q.defer();
            $http({
                url: '/Login/LoadOrder',
                method: 'POST',
                params: { username: username }
            }).success(function (result) {
                deferred.resolve(result.orderList);
            }).error(function (error) {
                console.log('Error occurs: ' + error);
            });
            return deferred.promise;
        },

        getFavoriteProductActive: function (username) {
            var deferred = $q.defer();
            $http({
                url: '/Favorite/GetAllProductAndIsActive',
                method: 'POST',
                params: { username: username }
            }).success(function (result) {
                if (result.success) {
                    deferred.resolve(result.result);
                } else {
                    console.log('Không thể lấy dữ liệu sản phẩm yêu thích');
                }
            }).error(function (error) {
                console.log('Error occurs: ' + error);
            });
            return deferred.promise;
        },

    };
    //service.initialize();

    return service;
});

JsonMatch = function (target, source, matcher) {
    // mVal stands for matcherValue
    angular.forEach(matcher, function (mVal, mKey) {
        if (mKey == 'list') {
            target[mVal.matchWith] = [];
            angular.forEach(source[mVal.name], function (aItem, aIndex) {
                var _temp = {};
                JsonMatch(_temp, aItem, mVal.data);
                target[mVal.matchWith].push(_temp);
            });
        } else {
            target[mKey] = source[mVal];
        }
    });
};