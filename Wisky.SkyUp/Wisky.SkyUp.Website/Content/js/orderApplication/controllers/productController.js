﻿angular.module('OrderApp').controller('productController', function ($scope, $modal, productService) {
    var vm = this;

    // Variables
    vm.order = {};
    vm.productList = [];
    vm.productListOfCateParent = [];
    vm.categories = [];
    vm.hotProducts = [];
    vm.bestSaleProducts = [];
    vm.product = {};
    //For filter and sort
    vm.filter = {
        name: '' //This value must exist in list option
    };
    vm.sortBy = '';
    vm.lower_price_bound = 0;
    vm.upper_price_bound = 200000;

    vm.slider = {
        min: 0,
        max: 400000,
        options: {
            floor: 0,
            ceil: 400000,
            id: 'slider'
        }
    };

    

    //vm.filterProductSpecification = function (product, coat) {
    //    angular.forEach(product.specification, function (e, i) {
    //        if (e.name == name) {
    //            return product;
    //        }
    //    });
    //},
    /*
     * author: TrungNDT
     * method: Load all products
     */
    vm.getAllProducts = function () {
        //return productService.getProducts();
        var promise = productService.loadAllProducts();
        promise.then(function (products) {
            vm.productList = products;
        });
    }

    vm.getAllProductsOfCateParent = function (cateId) {
        var promise = productService.loadAllProductsOfCateParent(cateId);
        promise.then(function (products) {
            vm.productListOfCateParent = products;
        });
    }

    vm.getProductOrderQuantity = function (productId) {
        var orderQuantity = productService.getOrderQuantity(productId);
        return orderQuantity;
    }

    /*PhuongTA - load products in HotProducts*/
    //vm.getHotProducts = function () {
    //    var promise = productService.loadHotProducts();
    //    promise.then(function (products) {
    //        vm.hotProducts = products;
    //    });
    //}

    /*PhuongTA - load products in bestSaleProducts*/
    //vm.getBestSaleProducts = function () {
    //    var promise = productService.loadBestSaleProducts();
    //    promise.then(function (products) {
    //        vm.bestSaleProducts = products;
    //    });
    //}

    vm.getProductById = function (productId) {
        for (var i = 0 ; i < vm.productList.length; i++)
            if (vm.productList[i].id == productId) return angular.copy(vm.productList[i]);
        return null;
    }

    //vm.addProductToCart = function (product) {
    //    productService.addProductToCart(product);
    //    angular.copy(product || {});
    //};

    vm.addProductToCartById = function (id, quantity) {
        var product = vm.getProductById(id);
        product.quantity = quantity;
        vm.addProductToCart(product);
    };

    vm.addProductToCartUseId = function (id) {
        var product = vm.getProductById(id);
        vm.addProductToCart(product);
    };

    //Author: Phu
    //vm.getAllProductsByCategory = function (cateId) {
    //    var promise = productService.loadAllProductsInCategory(cateId);
    //    promise.then(function (products) {
    //        vm.productList = products;
    //    });
    //};

    //vm.getAllProductsInCategory = function () {
    //    var promise = productService.loadAllProductsInCategory(vm.categoryId);
    //    promise.then(function (products) {
    //        vm.productList = products;
    //    });
    //};

    //$scope.init = function (cateId) {
    //    vm.categoryId = cateId;
    //    vm.getAllProductsInCategory();
    //};
    /*
     * author: TrungNDT
     * method: Load all categories
     */
    //vm.getAllCategories = function () {
    //    //return productService.getCategories();
    //    var promise = productService.loadAllCategories();
    //    promise.then(function (categories) {
    //        vm.categories = categories;
    //    });
    //};

    vm.viewProductUseId = function (id) {
        var product = vm.getProductById(id);
        vm.viewProduct(product);
    }


    /*
     * author: TrungNDT
     * method: View selected product in popup modal
     */
    vm.viewProduct = function (product) {
        var modalInstance = $modal.open({
            controller: 'modalController',
            controllerAs: 'vm',
            templateUrl: 'modalContent.html',
            animation: true,
            resolve: {
                // vm.product = product,
                product: function () {
                    return angular.copy(product || {});
                },
                isNewMode: function () {
                    return true;
                }
            }
        });
    };

    generalId = function () {
        var id = 0;
        var orders = productService.getOrders();
        if (orders != null && orders.length > 0) {
            id = orders[orders.length - 1].tempId + 1;
        }
        return id;
    };

    /*
     * author: TrungNDT
     * method: Add given product to cart. Count up if product exist
     */
    vm.addProductToCart = function (product) {
        var idGen = generalId();
        productService.addProductToCart(product, idGen);
    };


    vm.getAllProducts();
    //vm.getAllCategories();
    //vm.getHotProducts();
    //vm.getBestSaleProducts();

    //$scope.priceRange = function (product) {
    //    return (product.price >= parseInt($scope.lowerBound) && product.price <= parseInt($scope.upperBound));
    //}
});