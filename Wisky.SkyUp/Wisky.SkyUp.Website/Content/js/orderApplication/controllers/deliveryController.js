﻿angular.module('OrderApp').controller('deliveryController', function (productService) {
    var vm = this;
    //var voucherCode = "";
    vm.getDeliveryInfo = function () {
        return productService.getDeliveryInfo();
    }

    vm.submitOrder = function () {
        productService.submitOrder();
    }

    vm.submitVoucher = function () {
        if (productService.getDeliveryInfo().address === undefined || productService.getDeliveryInfo().address === null || productService.getDeliveryInfo().address == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn địa chỉ giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }

        if (productService.getDeliveryInfo().deliveryType === undefined || productService.getDeliveryInfo().deliveryType === null || productService.getDeliveryInfo().deliveryType == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn hình thức giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        if (productService.getDeliveryInfo().paymentMethod === undefined || productService.getDeliveryInfo().paymentMethod === null || productService.getDeliveryInfo().paymentMethod == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn loại hình thanh toán",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        productService.checkDiscount();
        //productService.submitVoucher();
    }
    vm.submitVoucherNoVoucher = function () {
        setTimeout(function () {
            if (productService.getDeliveryInfo().address === undefined || productService.getDeliveryInfo().address === null || productService.getDeliveryInfo().address == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn địa chỉ giao hàng",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }

            if (productService.getDeliveryInfo().deliveryType === undefined || productService.getDeliveryInfo().deliveryType === null || productService.getDeliveryInfo().deliveryType == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn hình thức giao hàng",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }
            if (productService.getDeliveryInfo().paymentMethod === undefined || productService.getDeliveryInfo().paymentMethod === null || productService.getDeliveryInfo().paymentMethod == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn loại hình thanh toán",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }

            productService.checkDiscountNoVoucher();
        }, 700);

    }
    vm.isVoucherApply = function () {
        return productService.getResultSubmitVoucher();
    }

    vm.getLastTotalCost = function () {
        return productService.getLastTotalCost();
    }

    vm.getLastOrders = function () {
        return productService.getLastOrders();
    }
    vm.clearStorage = function () {
        productService.clearStorageAfterViewResult();
    }

    vm.isEmptyExtraList = function (tempId) {
        return productService.isEmptyExtraListLastOrder(tempId);
    }

    vm.checkDiscount = function () {
        return productService.checkDiscount();
    }
    vm.getLastTotalDiscount = function () {
        return productService.getLastTotalDiscount();
    }

    vm.clearLastTotalDiscount = function () {
        return productService.clearLastTotalDiscount();
    }

});
