﻿angular.module('OrderApp').controller('orderController', function ($modal, $scope, productService) {
    var vm = this;
    vm.product = {};
    vm.order = [];
    vm.userDeliveryInfors = [];
    vm.selectedUserDeliveryInforIndex = -1;
    vm.ordered = {};
    vm.delivery = {};
    vm.favoriteProduct = {};
    vm.deliveryCost = 0;
    /*
     * author: PhuDCQ
     * method: get order in cart by id
     */
    vm.getOrderById = function (tempId) {
        for (var i = 0; i < vm.order.length; i++) {
            if (vm.order[i].tempId == tempId) {
                return vm.order[i];
            }
        }
        return null;
    }

    vm.getDeliveryInfo = function () {
        return productService.getDeliveryInfo();
    }


    vm.getProductQuantity = function (tempId) {
        var order = vm.getOrderById(tempId);
        return order.quantity;
    }

    vm.isEmptyExtraList = function (tempId) {
        return productService.isEmptyExtraList(tempId);
    }

    /*
     * author: TrungNDT
     * method: Check if shopping cart is empty
     */
    vm.isShoppingCartEmpty = function () {
        return productService.getOrders().length == 0;
    },

        /*
         * author: PhuDCQ
         * method: Get change quantity when click button plus/minus
         */

        vm.changeProductQuantity = function (isPlus, tempId) {
            var order = vm.getOrderById(tempId);
            if (isPlus) {
                order.quantity++;
            } else {
                if (order.quantity > 1)
                    order.quantity--;
            }
        };

    /*
     * author: TrungNDT
     * method: Get shopping cart list
     */
    vm.getShoppingCart = function () {
        vm.order = productService.getOrders();
        return vm.order;
    },

        vm.showShoppingCartByNumberElement = function (number) {
            if (number == 0) {
                vm.getShoppingCart();
            } else {

            }

        },

        /*
         * author: PHUONGTA
         * method: Getnumber product in cart list
         */
        vm.getNumberOfProducts = function () {
            return productService.getNumInOrders()
        }

    /*
    * author: PhuDCQ
    * method: View product
    * param: product id
    */
    vm.viewOrderUseId = function (tempId) {
        var order = vm.getOrderById(tempId);
        vm.viewOrder(order);
    }


    /*
     * author: PhuDCQ
     * method: View selected product in popup modal
     */
    vm.viewOrder = function (product) {
        var modalInstance = $modal.open({
            controller: 'modalController',
            controllerAs: 'vm',
            templateUrl: 'modalContent.html',
            animation: true,
            resolve: {
                product: function () {
                    return angular.copy(product || {});
                },
                isNewMode: function () {
                    return false;
                }
            }
        });
    };

    /*
     * author: TrungNDT
     * method: update existing cart item
     */
    vm.saveOrderChange = function (order) {
        productService.updateOrder(order);
        $('#modalEditOrderItem').modal('hide');
    }

    /*
     * author: TrungNDT
     * method: Remove given order from cart
     */
    vm.removeOrder = function (order) {
        productService.removeOrder(order);
    }

    /*
     * author: TrungNDT
     * method: Get total cost base on price, quantity & extra
     * return: {Number} order cost
     */
    vm.getOrderCost = function (order) {
        return productService.getOrderCost(order);
    }

    /*
     * author: TrungNDT
     * method: Calculate entire cart's cost
     * return: {Number} total cost
     */
    vm.getTotalCost = function () {
        return productService.getTotalCost();
    }

    /*
     * author: TrungNDT
     * method: [Use for Filter] filter extra item has quatity > 0
     */
    vm.hasExtra = function () {
        return function (item) {
            return item.quantity > 0;
        }
    }

    vm.emptyShoppingCart = function () {
        productService.emptyCart();
    }

    vm.printCart = function () {
        //console.log($localStorage.orders);
    };

    /*
     * author: DucBM
     * method: [Use for Checkout page] get all user's addresses
     */
    vm.loadAllUserDeliveryInfor = function (userId) {
        var promise = productService.loadAllUserDeliveryInfor(userId);
        promise.then(function (result) {
            vm.userDeliveryInfors = result;
            $('.spinner-group').hide();
        });
    }

    /*
     * author: DucBM
     * method: [Use for Checkout page] get delivery info
     */
    vm.getDeliveryInfo = function () {
        return productService.getDeliveryInfo();
    }

    /*
     * author: DucBM
     * method: [Use for Checkout page] save order to DB
     */
    vm.submitOrder = function (brandId) {
        productService.getDeliveryInfo().brandId = brandId;

        if (productService.getDeliveryInfo().address === undefined || productService.getDeliveryInfo().address === null || productService.getDeliveryInfo().address == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn địa chỉ giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }

        if (productService.getDeliveryInfo().deliveryType === undefined || productService.getDeliveryInfo().deliveryType === null || productService.getDeliveryInfo().deliveryType == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn hình thức giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        if (productService.getDeliveryInfo().paymentMethod === undefined || productService.getDeliveryInfo().paymentMethod === null || productService.getDeliveryInfo().paymentMethod == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn loại hình thanh toán",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        if (productService.getDeliveryInfo().aggrement === undefined || productService.getDeliveryInfo().aggrement === null || productService.getDeliveryInfo().aggrement == "") {
            swal({
                title: "Thông báo",
                text: "Bạn đồng ý với điều khoản sử dụng của UniChef",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        
        productService.submitOrder();
    }

    /*
     * author: DucBM
     * method: [Use for Layout] get total quantity
     */
    vm.getTotalQuantity = function () {
        return productService.getTotalQuantity();
    }

    vm.getListOrdered = function (username) {
        var promise = productService.getOrdered(username);
        //console.log(promise);
        promise.then(function (result) {
            vm.ordered = result;
        });
        return vm.ordered;
    }

    vm.getFavoriteProductActive = function (username) {
        var promise = productService.getFavoriteProductActive(username);
        //console.log(promise);
        promise.then(function (result) {
            vm.favoriteProduct = result;
        });
        return vm.favoriteProduct;
    }



    vm.viewOrderDetail = function (orderId, listOrderDetail, finalAmount, discountAmount) {
        $('#orderDetail .modal-title').text("");
        $('#orderDetail .modal-title').text("Chi tiết đơn hàng " + orderId);
        $('#orderDetail tbody').empty();
        for (var i = 0; i < listOrderDetail.length; i++) {
            $('#orderDetail tbody').append(
                "<tr>" +
                "<td class='center-format-col'>" + listOrderDetail[i].product + "</td>" +
                "<td class='center-format-col text-center'>" + listOrderDetail[i].quantity + "</td>" +
                "<td class='center-format-col text-right'>" + listOrderDetail[i].price + " VND</td>" +
                "<td class='center-format-col text-right'>" + listOrderDetail[i].amount + " VND</td>" +
                "</tr>");
        }
        $('#orderDetail tbody').append(
            "<tr>" +
            "<td class='center-format-col' colspan='3'>Giảm giá</td>" +
            "<td class='center-format-col text-right'>" + discountAmount + " VND</td>" +
            "</tr>");
        $('#orderDetail tbody').append(
            "<tr>" +
            "<td class='center-format-col' colspan='3'>Tổng cộng</td>" +
            "<td class='center-format-col text-right'>" + finalAmount + " VND</td>" +
            "</tr>");

        $('#orderDetail').modal('show')
    }

    //Addtocart section

    vm.sizeArray = ['M', 'L', 'XL', 'XXL', 'XXXL'];
    vm.isUpdatingCart = false;
    vm.product = {
        id: null,
        quantity: 1,
        size: vm.sizeArray[0],
        color: null,
        discount: 0,
        extraList: [],
        seoName: null,
        tempId: null,
        dispercent: 0
    };
    vm.quantityArray = [];
    for (var i = 0; i < 10; i++) {
        vm.quantityArray.push(i + 1);
    }

    vm.initProduct = function (id, name, dispercent, image, code, price, seoName, tempId) {
        vm.product.id = id;
        vm.product.name = name;
        vm.product.dispercent = dispercent;
        vm.product.image = image;
        vm.product.code = code;
        vm.product.price = price;
        vm.product.seoName = seoName;
        vm.product.tempId = tempId;
        if (tempId != undefined && tempId != null) {
            var order = productService.getOrderByTempId(tempId);
            if (order != undefined && order != null) {
                for (var i = 0; i < vm.sizeArray.length; i++) {
                    if (vm.sizeArray[i] == order.size)
                        vm.product.size = vm.sizeArray[i];
                }

                vm.product.color = order.color;
                vm.product.quantity = order.quantity;
                vm.isUpdatingCart = true;
            }
        }
    };

    generalId = function () {
        var id = 0;
        var orders = productService.getOrders();
        if (orders != null && orders.length > 0) {
            id = orders[orders.length - 1].tempId + 1;
        }
        return id;
    };

    vm.addToCart = function (id, name, dispercent, image, code, price, seoName, tempId, formattedPrice) {
        vm.initProduct(id, name, dispercent, image, code, price, seoName, tempId);
        var idGen = generalId();
        productService.addProductToCart(vm.product, idGen);
        var timeout;
        clearTimeout(timeout);
        $('#myPopup img').removeAttr('src');
        $('#myPopup img').attr('src', image);
        $('#myPopup span').eq(0).text(code);
        $('#myPopup span').eq(1).text(name);
        $('#myPopup span').eq(2).text(formattedPrice + " VND");
        if ($("#myPopup").hasClass('show'))
            document.getElementById("myPopup").classList.toggle('show');

        if (!$("#myPopup").hasClass('show')) {
            document.getElementById("myPopup").classList.toggle('show');
            window.scrollTo(0, 0);
            timeout = setTimeout(function () {
                document.getElementById("myPopup").classList.toggle('show');
            }, 3000);
        }
    }

    vm.updateCart = function () {
        productService.updateOrder(vm.product);
    }

    vm.submitVoucher = function () {

        if (productService.getDeliveryInfo().address === undefined || productService.getDeliveryInfo().address === null || productService.getDeliveryInfo().address == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn địa chỉ giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }

        if (productService.getDeliveryInfo().deliveryType === undefined || productService.getDeliveryInfo().deliveryType === null || productService.getDeliveryInfo().deliveryType == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn hình thức giao hàng",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        if (productService.getDeliveryInfo().paymentMethod === undefined || productService.getDeliveryInfo().paymentMethod === null || productService.getDeliveryInfo().paymentMethod == "") {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn loại hình thanh toán",
                type: "info"
            }, function () {
                return;
            });
            return;
        }
        //if (productService.getDeliveryInfo().aggrement === undefined || productService.getDeliveryInfo().aggrement === null || productService.getDeliveryInfo().aggrement == "") {
        //    swal({
        //        title: "Thông báo",
        //        text: "Bạn đồng ý với điều khoản sử dụng của UniChef",
        //        type: "info"
        //    }, function () {
        //        return;
        //    });
        //    return;
        //}
        productService.checkDiscount();
        //productService.submitVoucher();
    }
    vm.submitVoucherNoVoucher = function () {
        setTimeout(function () {
            if (productService.getDeliveryInfo().address === undefined || productService.getDeliveryInfo().address === null || productService.getDeliveryInfo().address == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn địa chỉ giao hàng",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }

            if (productService.getDeliveryInfo().deliveryType === undefined || productService.getDeliveryInfo().deliveryType === null || productService.getDeliveryInfo().deliveryType == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn hình thức giao hàng",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }
            if (productService.getDeliveryInfo().paymentMethod === undefined || productService.getDeliveryInfo().paymentMethod === null || productService.getDeliveryInfo().paymentMethod == "") {
                swal({
                    title: "Thông báo",
                    text: "Bạn chưa chọn loại hình thanh toán",
                    type: "info"
                }, function () {
                    return;
                });
                return;
            }

            productService.checkDiscountNoVoucher();
        }, 700);
        
    }
    vm.getLastTotalDiscount = function () {
        return productService.getLastTotalDiscount();
    }

    vm.clearLastTotalDiscount = function () {
        return productService.clearLastTotalDiscount();
    }
    vm.clearDeliveryInfor = function () {
        return productService.clearDeliveryInfor();
    }
    
});