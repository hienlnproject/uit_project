﻿angular.module('OrderApp').controller('productDetailController', function ($scope, $modal, productService) {
    var vm = this;
    vm.sizeArray = ['M', 'L', 'XL', 'XXL', 'XXXL'];
    vm.isUpdatingCart = false;
    vm.product = {
        id: null,
        quantity: 1,
        size: vm.sizeArray[0],
        color: null,
        discount: 0,
        extraList: [],
        seoName: null,
        tempId: null,
        colorUrl: null,
        dispercent: 0
    };
    vm.quantityArray = [];
    for (var i = 0; i < 10; i++) {
        vm.quantityArray.push(i + 1);
    }

    vm.initProduct = function (id, name, dispercent, image, code, price, seoName, tempId, colorUrl, color) {
        vm.product.id = id;
        vm.product.name = name;
        vm.product.dispercent = dispercent;
        vm.product.image = image;
        vm.product.code = code;
        vm.product.price = price;
        vm.product.seoName = seoName;
        vm.product.tempId = tempId;
        vm.product.colorUrl = colorUrl;
        vm.product.color = color;

        if (tempId != undefined && tempId != null) {
            var order = productService.getOrderByTempId(tempId);
            if (order != undefined && order != null) {
                for (var i = 0; i < vm.sizeArray.length; i++) {
                    if (vm.sizeArray[i] == order.size)
                        vm.product.size = vm.sizeArray[i];
                }
                vm.product.extraList = order.extraList;
                vm.product.color = order.color;
                vm.product.quantity = order.quantity;
                vm.isUpdatingCart = true;
            }
        }
    };

    generalId = function () {
        var id = 0;
        var orders = productService.getOrders();
        if (orders != null && orders.length > 0) {
            id = orders[orders.length - 1].tempId + 1;
        }
        return id;
    };

    vm.addToCart = function () {
        if (vm.product.color != null) {
            var idGen = generalId();
            productService.addProductToCart(vm.product, idGen);
        } else {
            swal({
                title: "Thông báo",
                text: "Bạn chưa chọn màu sắc",
                type: "info"
            }, function () {
                return;
            });
            return;
        }

    };

    vm.updateCart = function () {
        productService.updateOrder(vm.product);
    };

    vm.addExtraToProduct = function (id, name, price, code, quantity, text, color, style, imageUrl) {
        if (text != undefined && text != null && text != "" && text.length > 17) {
            text = text.substring(0, 17);;
        }
        var isExist = false;
        var pos = -1;
        for (var i = 0; i < vm.product.extraList.length; i++) {
            if (vm.product.extraList[i].id == id) {
                isExist = true;
                pos = i;
            }
        }
        if (text == null || text.length == 0) {
            text = "n/a";
        }
        if (color == null || color.length == 0) {
            color = "n/a";
        }
        if (style == null || style.length == 0) {
            style = "n/a";
        }
        if (!isExist) {
            vm.product.extraList.push(
                {
                    id: id,
                    name: name,
                    price: price,
                    productCode: code,
                    quantity: quantity,
                    text: text,
                    color: color,
                    style: style,
                    imageUrl: imageUrl
                }
            )
        } else {
            vm.product.extraList.splice(pos, 1);
        }

    };

    vm.getProductExtra = function (id) {
        var pos = -1;
        for (var i = 0; i < vm.product.extraList.length; i++) {
            if (vm.product.extraList[i].id == id) {
                isExist = true;
                pos = i;
            }
        }
        if (pos != -1) {
            return vm.product.extraList[pos];
        }
        return null;
    };

    vm.addToCartFullInfo = function (id, name, dispercent, image, code, price, seoName, tempId, colorUrl, color, formattedPrice) {
        vm.initProduct(id, name, dispercent, image, code, price, seoName, tempId, colorUrl, color);
        var idGen = generalId();
        productService.addProductToCart(vm.product, idGen);
        var timeout;
        clearTimeout(timeout);
        $('#myPopup img').removeAttr('src');
        $('#myPopup img').attr('src', image);
        $('#myPopup span').eq(0).text(code);
        $('#myPopup span').eq(1).text(name);
        $('#myPopup span').eq(2).text(formattedPrice + " VND");
        if ($("#myPopup").hasClass('show'))
            document.getElementById("myPopup").classList.toggle('show');

        if (!$("#myPopup").hasClass('show')) {
            document.getElementById("myPopup").classList.toggle('show');
            window.scrollTo(0, 0);
            timeout = setTimeout(function () {
                document.getElementById("myPopup").classList.toggle('show');
            }, 3000);
        }
    }
});