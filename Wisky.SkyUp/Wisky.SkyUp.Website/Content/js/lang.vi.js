if (typeof (lang) == 'undefined') {
    lang = {}
}

lang.vi = {

    SignIn: 'Đăng nhập',
    SignOut: 'Đăng xuất',
    Download: 'Tải về CATALOGUE',
    ShopNow: 'Mua ngay',
    TopProducts: 'Sản phẩm hàng đầu',
    Customers: 'Khách hàng',
    B2b_design: 'Thiết kế B2B',

    // Footer
    Address: '138/66 Trương Công Định, Phường 14,<br> Quận Tân Bình, TP.HCM',
    Company: 'Công ty',
    Introduction: 'Giới thiệu',
    Value: 'Giá trị M.A.C',
    Partner: 'Đối tác',
    Customer: 'Khách hàng',
    MAC_Careers: 'Nghề nghiệp M.A.C',
    Buy: 'Mua hàng',
    Custom_Requirement: 'Yêu cầu riêng',
    Size_Table: 'Bảng kích cỡ',
    Deal: 'Ưu đãi',
    Get_Catalog: 'Nhận Catalog',
    Customer_Service: 'Dịch vụ khách hàng',
    Contact: 'Liên hệ',
    Transportation: 'Vận chuyển',
    Account_Information: 'Thông tin khách hàng',
    Order_Status: 'Tình trạng đơn hàng',
    Get_News: 'Nhận tin tức mới',
    Copyright: '&copy 2016 Bản quyền thuộc về UniChef Corp.',
    EmailAddress: "Địa chỉ e-mail",

    // Contact 
    Contact_Information: 'thông tin liên lạc',
    Nam_An_Company: 'Công ty TNHH TMDV SXPT NAM AN',
    Sales_Office: 'Văn phòng giao dịch',
    Showroom: 'Cửa hàng trưng bày (sắp khai trương)',
    Showroom_Address: '36/6 Bàu Cát 1, Phường 14, Quận Tân Bình, TP HCM',
    Office: 'Văn phòng',
    Office_Address: '7 Hoàng Diệu, Phường 10, Quận Phú Nhuận, TP HCM',
    Business_Code: 'Mã số doanh nghiệp',
    Phone: 'Điện Thoại',
    Hotline: 'Đường dây nóng',
    Requirement_Information: 'Thông tin yêu cầu',
    Fullname: 'Họ và tên',
    Fullname_required: 'Họ tên*',
    Phone_Number: 'Số điện thoại',
    Phone_Number_required: 'Số điện thoại*',
    Order_Number: 'Đơn hàng số (Nếu có)',
    Requirement_Type: 'Loại yêu cầu',
    Order: 'Đặt hàng',
    Message: 'Tin nhắn',
    SEND: 'GỬI',

    // Product Detail 
    Product_Detail: 'Chi tiết sản phẩm',
    Color: 'Màu sắc',
    Size: 'Kích thước',
    Size_Table: 'Bảng kích cỡ',
    Quantity: 'Số lượng',
    Add_To_Cart: 'Thêm vào giỏ hàng',
    Product_Information: 'Thông tin sản phẩm',
    Specification: 'Thông số kỹ thuật',
    Comments: 'Đóng góp ý kiến',
    Return_Policy: 'Chính sách đổi trả',
    Terms_and_policies: 'Điều khoản và chính sách',
    Delivery_Information: 'Thông tin giao hàng',
    Maybe_You_Will_Like: 'Có thể bạn sẽ thích',
    All_Positions: 'Tất cả vị trí',
    You_might_be_like: 'Có thể bạn sẽ thích',
    Close: 'Đóng',
    addcartsuccess: 'SẢN PHẨM ĐÃ ĐƯỢC THÊM THÀNH CÔNG',
    updatecartsuccess: 'SẢN PHẨM ĐÃ ĐƯỢC CẬP NHẬT THÀNH CÔNG',
    Comment_intro: 'Hãy là người đầu tiên chia sẻ suy nghĩa của bạn về sản phẩm và Dịch vụ của chúng tôi.',
    Your_comment: 'Nhận xét của bạn',
    Product_Rating: 'Xếp hạng sản phẩm',
    Content: 'Nội dung',
    Seemore: 'Xem thêm',
    Share: 'Chia sẻ',
    // Product
    Filter: 'Bộ lọc',
    Sort: 'Sắp xếp',
    Showing: 'Hiển thị',
    Price: 'Giá',
    From_To: 'Từ - đến',
    Color: 'Màu sắc',
    Shirt_Style: 'Kiểu áo',
    Pants_Style: 'Kiểu quần',
    Headwear_style: "Kiểu nón",
    Department: "Loại phụ kiện",
    Product_Line: 'Dòng sản phẩm',
    Aprons_Style: "Kiểu tạp dề",
    Material: 'Loại vải ',
    Sleeve_Length: 'Độ dài tay',
    Choice: 'Sự lựa chọn',
    Search: 'Tìm kiếm',
    Price_Low_High: 'VND thấp - cao',
    Price_High_Low: 'VND cao - thấp',
    Updating_product: 'Sản phẩm đang được cập nhật',

    // Sign In
    I_Have_Registered: 'Tôi đã đăng ký tài khoản',
    Username_Email: 'Tên đăng nhập',
    Password: 'Mật khẩu',
    Remember_Me: 'Ghi nhớ',
    Forgot_Password: 'Quên mật khẩu?',
    No_Account: 'Bạn chưa có tài khoản?',
    Create_Account_Description: 'Bằng cách tạo một tài khoản với cửa hàng của chúng tôi, bạn sẽ có thể di chuyển qua các quy trình kiểm tra nhanh hơn, lưu trữ nhiều địa chỉ gửi hàng, xem và theo dõi đơn đặt hàng của bạn trong tài khoản của bạn và nhiều hơn nữa.',
    Create_A_New_Account: 'Tạo một tài khoản mới',
    Sign_In_Via_Facebook: 'Đăng nhập bằng Facebook',
    Sign_In_Via_Google: 'Đăng nhập bằng Google',

    // Shopping Cart
    Shopping_Cart: 'Giỏ hàng',
    Product: 'Sản phẩm',
    Unit_Price: 'Đơn giá',
    Quantity: 'Số lượng',
    Price: 'Giá',
    View: 'Xem',
    Hide: 'Ẩn',
    Edit: 'Chỉnh sửa',
    Delete: 'Xóa',
    Gift_Code_Voucher: 'Gift code hoặc voucher',
    Apply: 'Áp dụng',
    Subtotal: 'Tạm tính',
    Shipping_Fee: 'Phí vận chuyển',
    Total: 'Tổng cộng',
    TotalDiscount: 'Tổng giảm giá',
    Proceed_To_Checkout: 'Tiến hành thanh toán',
    Valid_voucher: 'Voucher hợp lệ',
    Invalid_voucher: 'Voucher không hợp lệ',
    Checkout: 'Thanh toán',
    Checkout_SignIn: '1. Đăng nhập',
    Checkout_DeliveryAddress: '2. Địa chỉ giao hàng',
    Checkout_Checkout: '3. Thanh toán',
    SigninIntro: 'Bạn đã có tài khoản? Hãy đăng nhập để lưu đơn hàng và tiến hành thanh toán.',
    DeliveryAddress: 'Địa chỉ giao hàng',
    NoAddress: 'Bạn chưa có địa chỉ nào.',
    ChooseAddress: 'Chọn địa chỉ giao hàng có sẵn bên dưới:',
    PleaseChooseAddress:'Vui lòng bấm chọn địa chỉ giao hàng để tiến hành thanh toán',
    ViewCart: 'Xem giỏ hàng',
    Customer_Address: 'Địa chỉ',
    Customer_Address_required: 'Địa chỉ',
    Customer_Phone: 'Điện thoại',
    Deliver_to_other_address: 'Bạn muốn giao hàng đến địa chỉ khác?',
    Add_new_address: 'Thêm địa chỉ mới',
    Province: 'Tỉnh/Thành phố',
    District: 'Quận/Huyện',
    Ward: 'Phường/Xã',
    Input_fullname: 'Nhập họ tên',
    Input_phone: 'Nhập số điện thoại',
    Input_address: 'Nhập địa chỉ',
    Address_type: 'Để nhận hàng thuận tiện hơn vui lòng cho UniChef biết loại địa chỉ nhận hàng',
    Type_of_address: 'Loại địa chỉ',
    House: 'Nhà riêng / Chung cư',
    Company_receive: 'Cơ quan',
    Set_as_default: 'Đặt làm mặc định',
    Cancel: 'HỦY BỎ',
    Proceed_to_payment: 'TIẾN HÀNH ĐẶT MUA',
    Gift_code: 'Gift code hoặc voucher',
    Form_of_delivery: 'Hình thức giao hàng',
    Standard_delivery: 'Giao hàng tiêu chuẩn',
    Three_days: 'Dự kiến giao hàng vào 3 ngày sau khi đặt hàng.',
    Fast_delivery: 'Giao hàng hoả tốc (trong 24h)',
    Next_day: 'Dự kiến giao hàng từ 9:00 - 12:00 ngày hôm sau.',
    Form_of_payment: 'HÌNH THỨC THANH TOÁN',
    COD: 'Thanh toán tiền mặt khi nhận hàng (Ship COD)',
    Personal_transfer: 'Chuyển khoản cá nhân',
    Required_information_to_transfer: 'Để hoàn thành thủ tục thanh toán, xin quý khách vui lòng chuyển khoản theo thông tin sau đây:',
    Bank_name: '[Tên ngân hàng - Chi nhánh ngân hàng]',
    Account_number: 'Số Tài Khoản',
    Account_number2: '[số tài khoản]',
    Account_holder: 'Chủ Tài Khoản',
    Fullname_account_holder: '[Họ và tên chủ tài khoản]',
    Info: 'Sau khi giao dịch thành công, UniChef sẽ liên hệ với quý khách bằng điện thoại trong vòng 3 ngày để xác nhận đơn hàng. Cảm ơn quý khách đã tin tưởng và sử dụng dịch vụ của UniChef.',
    Internet_banking: 'Thanh toán qua thẻ ATM đăng ký Internet Banking (Miễn phí thanh toán)',
    Visa: 'Thanh toán qua thẻ thanh toán Quốc tế (Visa, Master Card v…v)',
    Customer_information: 'THÔNG TIN NGƯỜI MUA',
    Address_using: 'Sử dụng họ tên và số điện thoại của địa chỉ giao hàng.',
    Red_invoice: 'Yêu cầu xuất hoá đơn đỏ.',
    Note: '(Lưu ý: Vui lòng kiểm tra đơn hàng kĩ trước khi đặt mua)',
    Ordered_product: 'SẢN PHẨM ĐÃ ĐẶT',
    Show_all: 'Xem tất cả',
    Hide2: 'Thu gọn',
    Total_order_value: 'TỔNG GIÁ TRỊ ĐƠN HÀNG',
    Discount: 'Giảm giá',
    Address2: 'Địa chỉ:',
    Phone2: 'Điện thoại:',
    Aggreement: 'Tôi đồng ý thực hiện mọi giao dịch mua bán theo điều kiện sử dụng và chính sách của UniChef',

    //My Account
    My_account: 'TÀI KHOẢN CỦA TÔI',
    Add_product_success: 'SẢN PHẨM ĐÃ ĐƯỢC THÊM THÀNH CÔNG',
    View_cart: 'XEM GIỎ HÀNG',
    Hi: 'Xin chào',
    Change_password: 'Thay đổi mật khẩu',
    Order_management: 'Quản lý đơn hàng',
    Addresses: 'Số địa chỉ',
    Favorite_products: 'Sản phẩm yêu thích',
    Fullname2: 'Họ tên',
    Gender: 'Giới tính',
    Male: 'Nam',
    Female: 'Nữ',
    Date_of_birth: 'Ngày sinh',
    Optional: '(không bắt buộc)',
    Update: 'CẬP NHẬT',
    Old_password: 'Mật khẩu cũ',
    New_password: 'Mật khẩu mới',
    Confirm_password: 'Nhập lại mật khẩu',
    Code_order: 'Mã đơn hàng',
    Date: 'Ngày mua',
    Total2: 'Tổng tiền',
    Order_status2: 'Trạng thái đơn hàng',
    Checkout: 'Thanh toán',
    No_order: 'Chưa có đơn hàng',
    No_address: 'Bạn chưa có địa chỉ nào.',
    Select_address: 'Chọn địa chỉ giao hàng có sẵn bên dưới:',
    Cellphone: 'Điện thoại di động*',
    Input_district: 'Vui lòng chọn quận/huyện',
    Input_ward: 'Vui lòng chọn phường/xã',
    Save: 'LƯU',
    Product_code: 'Mã sản phẩm',
    Product_price: 'Đơn giá',
    No_favorite_product: 'Chưa có sản phẩm yêu thích nào',

    Product_name: 'Tên sản phẩm',
    Close: 'Đóng',

    Date2: 'Ngày đặt hàng:',
    Status: 'Trạng thái:',
    Input_province: 'Nhập tên tỉnh/thành phố',
    Input_district_name: 'Nhập tên quận/huyện',
    Input_ward_name: 'Nhập tên phường/xã',
    Address_required: 'Địa chỉ*',
    Price2: 'Thành tiền',

    //B2B
    IntentDeliveryArea: 'Khu vực giao hàng dự kiến',
    EmployeeNumber: 'Số lượng nhân sự',
    UniformNumber: "Số lượng đồng phục/ nhân sự",
    BuyUniformTime: 'Số lần bạn mua đồng phục trong một năm',
    BuyUniformCost: 'Ngân sách cho một bộ đồng phục (áo & quần)',
    HowDoYouKnownUs: 'Làm thế nào bạn biết đến chúng tôi',
    Friend: 'Bạn bè',
    SearchOnInternet: 'Tìm kiếm trên internet',
    Other: 'Khác',
    CanWeHelpYou: 'Chúng tôi có thể giúp gì cho bạn?',
    SendUsInformation: 'Hãy gửi thông tin cho chúng tôi',

    moneyPerLine: '100.000 VND mỗi dòng',
    Embroidered_name_letter: 'Thêu tên/chữ',
    Embroidered_notice: 'Văn bản thêu sẽ được xuất hiện chính xác như đánh máy',
    Embroidered_flag: 'Thêu cờ',
    Embroidered_flag_description:'120.000 VND /cờ. Kích thước 2x3.2cm',
    Embroidered_contact:'Đội ngũ tư vấn thiết kế của UniChef luôn sẵn sàng hỗ trợ Quý khách cho việc thiết kế in/thêu logo trên trang phục. Nếu Quý khách có sẵn logo, vui lòng gửi email về địa chỉ mail sales@unichef.vn hoặc liên hệ Hotline 0966.290.290 để được hỗ trợ tư vấn cho việc đặt hàng.',
}