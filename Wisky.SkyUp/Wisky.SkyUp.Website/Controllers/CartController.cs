﻿using HmsService.Sdk;
using HmsService.Models.Entities.Services;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models.ViewModels;
using System.Web.Script.Serialization;
using System.IO;
using WiSky.SkyUp.Website.Models;
using System.Threading.Tasks;
using System.Threading;
using HmsService.Models;
using Newtonsoft.Json;
using Wisky.SkyUp.Website.Models;
using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;
using System.Net.Mail;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;
using HtmlAgilityPack;

namespace Wisky.SkyUp.Website.Controllers
{
    public class CartController : DomainBasedController
    {
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> Add(int productId, string url, int quantity)
        {
            url = await ProcessCreate(productId, url, quantity);
            return RedirectToAction("Index", "Routing", new { parameters = url });
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> AddNoReload(int productId, string url, int quantity)
        {
            await ProcessCreate(productId, url, quantity);

            return Json(new { success = true });
            //return RedirectToAction("Index", "Routing", new { parameters = url });
        }

        public async System.Threading.Tasks.Task<ActionResult> Remove(int productId, string url)
        {
            if (url != null && url.Length > 0 && url[0] == '/')
            {
                url = url.Substring(1);
            }
            CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
            if (cart != null)
            {
                var product = await new ProductApi().GetProductDetailsAsync(productId, CurrentStore.ID);
                if (product != null)
                {
                    cart.RemoveItem(product.Product);
                    Session["cart$" + CurrentStore.ID] = cart;
                }
            }
            return RedirectToAction("Index", "Routing", new { parameters = url });
        }


        [HttpPost]
        public async Task<ActionResult> RemoveNoReload(int productId, string url)
        {
            if (url != null && url.Length > 0 && url[0] == '/')
            {
                url = url.Substring(1);
            }
            CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
            if (cart != null)
            {
                var product = await new ProductApi().GetProductDetailsAsync(productId, CurrentStore.ID);
                if (product != null)
                {
                    cart.RemoveItem(product.Product);
                    Session["cart$" + CurrentStore.ID] = cart;
                }
            }
            return Json(new { success = true });
        }

        public ActionResult Update(List<CartItem> updateData, string url)
        {
            if (url != null && url.Length > 0 && url[0] == '/')
            {
                url = url.Substring(1);
            }
            if (updateData != null && updateData.Count > 0)
            {
                CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
                if (cart != null)
                {
                    cart.UpdateCart(updateData.Select(a => new KeyValuePair<int, int>(a.Product.ProductID, a.Quantity)).ToList());
                    Session["cart$" + CurrentStore.ID] = cart;
                }
            }
            return RedirectToAction("Index", "Routing", new { parameters = url });
        }

        public ActionResult LoadCart(string cartJson)
        {
            CartJSON cart = new JavaScriptSerializer().Deserialize<CartJSON>(cartJson);
            CartItemsViewModel model = new CartItemsViewModel();
            if (cart != null)
            {
                foreach (var item in cart.Items)
                {
                    CartItem cartItem = new CartItem();
                    cartItem.Product = new ProductApi().Get(item.id);
                    cartItem.Quantity = item.quantity;
                    cartItem.Total = cartItem.Quantity * cartItem.Product.Price;
                    model.CartItems.Add(cartItem);
                    model.Total += cartItem.Total;
                }
            }
            return PartialView("_LoadCart", model);
        }

        public ActionResult CheckoutCart(string fullname, string phone, string email, string paymentType, string address, string receiver,
            string receiverPhone, string receiverAddress, string note, string returnUrl)
        {
            CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
            if (cart != null)
            {
                CustomerViewModel customer;
                customer = new CustomerApi().GetCustomerByEmail(email, CurrentStore.BrandId.Value);
                if (customer != null)
                {
                    customer.Name = fullname;
                    customer.Address = address;
                    customer.Phone = phone;
                    new CustomerApi().Edit(customer.CustomerID, customer);
                }
                else
                {
                    customer = new CustomerViewModel();
                    customer.Name = fullname;
                    customer.Email = email;
                    customer.Address = address;
                    customer.Phone = phone;
                    customer.Type = 1;
                    new CustomerApi().Create(customer);
                }
                OrderCustomEntityViewModel rent = new OrderCustomEntityViewModel();
                rent.Order = new OrderViewModel();
                rent.Order.CheckInDate = DateTime.Now;
                rent.Order.TotalAmount = 0;
                rent.Order.Discount = 0; // Chưa xử lý
                rent.Order.DiscountOrderDetail = 0;
                var orderDetails = new List<OrderDetailViewModel>();
                foreach (var item in cart.CartItems)
                {
                    ProductViewModel product = new ProductApi().Get(item.Product.ProductID);
                    OrderDetailViewModel orderDetail = new OrderDetailViewModel();
                    orderDetail.ProductID = item.Product.ProductID;
                    orderDetail.Quantity = item.Quantity;
                    orderDetail.TotalAmount = product.Price * item.Quantity;
                    orderDetail.OrderDate = DateTime.Now;
                    orderDetail.Status = 0;
                    orderDetail.IsAddition = false;
                    orderDetail.UnitPrice = product.Price;
                    orderDetail.Discount = (product.Price - product.PrimaryPrice) * item.Quantity;
                    orderDetail.StoreId = CurrentStore.ID;
                    orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
                    rent.Order.TotalAmount += orderDetail.TotalAmount;
                    rent.Order.DiscountOrderDetail += orderDetail.Discount;
                    orderDetails.Add(orderDetail);

                }
                rent.OrderDetails = orderDetails;
                rent.Order.FinalAmount = rent.Order.TotalAmount - rent.Order.Discount - rent.Order.DiscountOrderDetail;
                rent.Order.OrderStatus = 14;
                rent.Order.OrderType = 1;
                rent.Order.Notes = note;
                rent.Order.StoreID = CurrentStore.ID;
                rent.Order.RentType = 0;
                rent.Order.DeliveryStatus = (int)Enums.StatusOrder.New;
                rent.Order.DeliveryAddress = receiverAddress;
                rent.Order.DeliveryPhone = receiverPhone;
                rent.Order.DeliveryReceiver = receiver;
                rent.Order.Att1 = paymentType;
                rent.Order.IsFixedPrice = false; // Chưa xử lý
                rent.Order.CustomerID = customer.CustomerID;
                rent.Order.SourceType = 2;
                new OrderApi().Create(rent);
                Thread.Sleep(500);
                Noty(rent, cart);
                Session["cart$" + CurrentStore.ID] = null;
            }
            return Redirect(returnUrl);
        }

        //private async Task Noty(RentDetailsViewModel rent, CartItemsViewModel cart)
        private void Noty(OrderCustomEntityViewModel rent, CartItemsViewModel cart)
        {
            var storeSettingApi = new StoreWebSettingApi();

            var storeSettings = storeSettingApi.GetActiveByStore(CurrentStore.ID);
            var viewModel = new Dictionary<string, string>();

            var mailContent = "";
            switch (CurrentStore.ID)
            {
                case 73://GMP
                    var senderGMP73 = new EmailSender(storeSettings.ToArray());
                    ViewBag.Order = rent;
                    ViewBag.Cart = cart;
                    viewModel.Add("Fullname", Request["fullname"]);
                    viewModel.Add("Phone", Request["phone"]);
                    viewModel.Add("Email", Request["email"]);
                    viewModel.Add("PaymentType", Request["paymentType"]);
                    viewModel.Add("Address", Request["address"]);
                    viewModel.Add("Receiver", Request["receiver"]);
                    viewModel.Add("ReceiverPhone", Request["receiverPhone"]);
                    viewModel.Add("ReceiverAddress", Request["receiverAddress"]);
                    viewModel.Add("Note", Request["note"]);

                    var titleEmailVi = "Đơn đặt hàng số #" + rent.Order.RentID;

                    mailContent = RenderRazorViewToString("~/Views/SW.GMP/OrderAdminEmailTemplate.cshtml", viewModel);
                    var receiverVi = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận đơn đặt hàng")?.Value) ?? "info@gmpvn.com";
                    senderGMP73.Send(receiverVi, titleEmailVi, mailContent);

                    mailContent = RenderRazorViewToString("~/Views/SW.GMP/OrderCustomerEmailTemplate.cshtml", viewModel);
                    //var receiverVi = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận đơn đặt hàng")?.Value) ?? "gmp@mailinator.com";
                    senderGMP73.Send(Request["email"], titleEmailVi, mailContent);
                    break;
                case 74:
                    var senderGMP74 = new EmailSender(storeSettings.ToArray());
                    ViewBag.Order = rent;
                    ViewBag.Cart = cart;
                    viewModel.Add("Fullname", Request["fullname"]);
                    viewModel.Add("Phone", Request["phone"]);
                    viewModel.Add("Email", Request["email"]);
                    viewModel.Add("PaymentType", Request["paymentType"]);
                    viewModel.Add("Address", Request["address"]);
                    viewModel.Add("Receiver", Request["receiver"]);
                    viewModel.Add("ReceiverPhone", Request["receiverPhone"]);
                    viewModel.Add("ReceiverAddress", Request["receiverAddress"]);
                    viewModel.Add("Note", Request["note"]);

                    var titleEmailEn = "New order number # " + rent.Order.RentID;

                    mailContent = RenderRazorViewToString("~/Views/SW.GMP/OrderAdminEmailTemplateEn.cshtml", viewModel);
                    var receiverEn = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận đơn đặt hàng")?.Value) ?? "info@gmpvn.com";
                    senderGMP74.Send(receiverEn, titleEmailEn, mailContent);

                    mailContent = RenderRazorViewToString("~/Views/SW.GMP/OrderCustomerEmailTemplateEn.cshtml", viewModel);
                    //receiverEn = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận đơn đặt hàng")?.Value) ?? "info@gmpvn.com";
                    senderGMP74.Send(Request["email"], titleEmailEn, mailContent);

                    break;
            }
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private async Task<string> ProcessCreate(int productId, string url, int quantity)
        {
            if (url != null && url.Length > 0 && url[0] == '/')
            {
                url = url.Substring(1);
            }
            CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
            if (cart == null)
            {
                cart = new CartItemsViewModel();
            }
            var product = await new ProductApi().GetProductDetailsAsync(productId, CurrentStore.ID);
            if (product != null)
            {
                cart.AddItem(product.Product, quantity);
                Session["cart$" + CurrentStore.ID] = cart;
            }

            return url;
        }

        // Author: DucBM
        [HttpPost]
        public JsonResult LoadAllUserDeliveryInfor(string userId)
        {
            var deliveryInformationApi = new DeliveryInformationApi();
            var information = deliveryInformationApi.GetAllDeliveryInforByUserId(userId).OrderByDescending(q => q.IsDefault);
            var deliveryInfor = information.Select(a => new
            {
                id = a.ID,
                name = a.Name != null ? a.Name : "",
                phone = a.Phone != null ? a.Phone.ToString() : "",
                address = a.Address != null ? a.Address : "",
                city = a.City != null ? a.City : "",
                district = a.District != null ? a.District : "",
                ward = a.Ward != null ? a.Ward : "",
                typeAdress = a.TypeAddress != null ? a.TypeAddress.ToString() : "",
                isDefault = a.IsDefault,
                deliveryCost = a.District1.PriceDelivery != null ? a.District1.PriceDelivery
                : (a.Province.BasePriceDelivery != null
                ? a.Province.BasePriceDelivery : a.Province.AreaDelivery.PriceDelivery),
                areaId = a.District1.AreaDelivery != null ? a.District1.AreaDelivery.Id
                : (a.Province.AreaDelivery != null ? a.Province.AreaDelivery.Id : 0),
            }).ToList();
            return Json(new { deliveryInfo = deliveryInfor }, JsonRequestBehavior.AllowGet);
        }

        // Author: DucBM
        [HttpPost]
        public JsonResult CreateDeliveryInfor(DeliveryInformationViewModel model)
        {
            try
            {
                var deliveryInformationApi = new DeliveryInformationApi();
                if (model.IsDefault.Value)
                {
                    var listDelivery = deliveryInformationApi.GetAllDeliveryInforByUserIdToViewModel(model.UserId).Where(q => q.IsDefault == true).ToList();
                    foreach (var item in listDelivery)
                    {
                        item.IsDefault = false;
                        deliveryInformationApi.Edit(item.ID, item);
                    }
                }
                deliveryInformationApi.Create(model);
                if (model.IsDefault.Value)
                {
                    var aspNetUserApi = new AspNetUserApi();
                    var user = aspNetUserApi.GetUserById(model.UserId);
                    user.Customer.Phone = model.Phone;
                    user.Customer.Address = model.Address + ", " + model.Ward + ", " + model.District + ", " + model.City;
                    aspNetUserApi.BaseService.Update(user);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // Author: DucBM
        [HttpPost]
        public JsonResult DeleteDeliveryInfor(int deliveryId)
        {
            try
            {
                var deliveryInformationApi = new DeliveryInformationApi();
                deliveryInformationApi.Delete(deliveryId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // Author: DucBM
        [HttpPost]
        public JsonResult UpdateDeliveryInfor(DeliveryInformationViewModel model)
        {
            try
            {
                var deliveryInformationApi = new DeliveryInformationApi();
                model.Active = true;
                if (model.IsDefault.Value)
                {
                    var listDelivery = deliveryInformationApi.GetAllDeliveryInforByUserIdToViewModel(model.UserId).Where(q => q.IsDefault == true).ToList();
                    foreach (var item in listDelivery)
                    {
                        item.IsDefault = false;
                        deliveryInformationApi.Edit(item.ID, item);
                    }
                }
                deliveryInformationApi.Edit(model.ID, model);
                if (model.IsDefault.Value)
                {
                    var aspNetUserApi = new AspNetUserApi();
                    var user = aspNetUserApi.GetUserById(model.UserId);
                    user.Customer.Phone = model.Phone;
                    user.Customer.Address = model.Address + ", " + model.Ward + ", " + model.District + ", " + model.City;
                    aspNetUserApi.BaseService.Update(user);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // Author: DucBM
        [HttpPost]
        public JsonResult GetDeliveryInfor(int deliveryId)
        {
            try
            {
                var deliveryInformationApi = new DeliveryInformationApi();
                var deliveryInfo = deliveryInformationApi.Get(deliveryId);
                return Json(new { success = true, deliveryInfo = deliveryInfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Cart With Angular

        public async Task<ActionResult> LoadItemByCategory(int cateId, string pattern)
        {
            var productApi = new ProductApi();
            var productCollectionApi = new ProductCollectionApi();
            var storeApi = new StoreApi();
            var brandId = storeApi.Get(CurrentStore.ID)?.BrandId;
            var productSpe = new ProductSpecificationApi();

            pattern = (pattern ?? "").ToLower();
            var products = await productApi.GetActiveByBrandIdAndCateIdAsyncEntity(brandId.Value, cateId);

            var rs = products.Select(a => new
            {
                image = a.PicURL != null ? a.PicURL : "/Content/Stores/SW.SaiGonVat/images/order/default_product.jpg",
                name = a.ProductName != null ? a.ProductName : "Default Product",
                nameEng = a.ProductNameEng != null ? a.ProductNameEng : "Default Product",
                id = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).ProductID : a.ProductID,
                discount = a.DiscountPrice,
                price = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).Price : a.Price,
                category = a.CatID,
                hasGeneral = a.ProductType == (int)ProductTypeEnum.General ? true : false,
                quantity = 1,
                specification = a.ProductSpecifications.Where(q => q.Active == true).Select(t => new { name = (t.Name == null ? "" : t.Name.ToLower()), value = t.Value.ToLower(), ImageUrl = t.ImageUrl }).ToList(),
                att1 = a.Att1,
                att2 = a.Att2,
                att3 = a.Att3,
                att4 = a.Att4,
                att5 = a.Att5,
                att6 = a.Att6,
                att7 = a.Att7,
                att8 = a.Att8,
                att9 = a.Att9,
                att10 = a.Att10,
                //label = a.ProductCollectionItemMappings.Select(b => b.ProductCollectionId).LastOrDefault(),
                note = "",
                seoName = a.SeoName,
                description = a.Description,
                createdDate = a.CreateTime,
                hasExtra = a.HasExtra,
                code = a.Code,
                dispercent = a.DiscountPercent
            }).ToList();

            #region Add 10 attribute to specification

            foreach (var item in rs)
            {
                if (!String.IsNullOrWhiteSpace(item.att1) && item.att1.Split(':') != null)
                {
                    foreach (var att in item.att1.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att1.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att2) && item.att2.Split(':') != null)
                {
                    foreach (var att in item.att2.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att2.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att3) && item.att3.Split(':') != null)
                {
                    foreach (var att in item.att3.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att3.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att4) && item.att4.Split(':') != null)
                {
                    foreach (var att in item.att4.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att4.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att5) && item.att5.Split(':') != null)
                {
                    foreach (var att in item.att5.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att5.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att6) && item.att6.Split(':') != null)
                {
                    foreach (var att in item.att6.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att6.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att7) && item.att7.Split(':') != null)
                {
                    foreach (var att in item.att7.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att7.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att8) && item.att8.Split(':') != null)
                {
                    foreach (var att in item.att8.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att8.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att9) && item.att9.Split(':') != null)
                {
                    foreach (var att in item.att9.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att9.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att10) && item.att10.Split(':') != null)
                {
                    foreach (var att in item.att10.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att10.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
            }
            #endregion

            return Json(new
            {
                products = rs
            }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> LoadItemByCategoryId(int cateId, string pattern)
        {
            var productApi = new ProductApi();
            var productCollectionApi = new ProductCollectionApi();
            var storeApi = new StoreApi();
            var brandId = storeApi.Get(CurrentStore.ID)?.BrandId;
            var storeId = CurrentStore.ID;
            var productSpe = new ProductSpecificationApi();
            var productCateApi = new ProductCategoryApi();
            var productOfCateParent = productCateApi.BaseService.GetSubCategoriesById(cateId, storeId);
            pattern = (pattern ?? "").ToLower();
            IEnumerable<Product> products = new List<Product>();
            foreach (var item in productOfCateParent)
            {
                products = products.Concat(item.Products);
            }
            foreach (var item in products)
            {
                if (item.ProductSpecifications.Where(q => !String.IsNullOrEmpty(q.ImageUrl)).FirstOrDefault() != null)
                {
                    var a = item;
                }
            }
            var rs = products.Select(a => new
            {
                image = a.PicURL != null ? a.PicURL : "/Content/Stores/SW.SaiGonVat/images/order/default_product.jpg",
                name = a.ProductName != null ? a.ProductName : "Default Product",
                nameEng = a.ProductNameEng != null ? a.ProductNameEng : "Default Product",
                id = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).ProductID : a.ProductID,
                discount = a.DiscountPrice,
                price = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).Price : a.Price,
                category = a.CatID,
                hasGeneral = a.ProductType == (int)ProductTypeEnum.General ? true : false,
                quantity = 1,
                specification = a.ProductSpecifications.Where(q => q.Active == true).Select(t => new { name = (t.Name == null ? "" : t.Name.ToLower()), value = t.Value.ToLower(), ImageUrl = t.ImageUrl }).ToList(),
                att1 = a.Att1,
                att2 = a.Att2,
                att3 = a.Att3,
                att4 = a.Att4,
                att5 = a.Att5,
                att6 = a.Att6,
                att7 = a.Att7,
                att8 = a.Att8,
                att9 = a.Att9,
                att10 = a.Att10,
                note = "",
                seoName = a.SeoName,
                description = a.Description,
                createdDate = a.CreateTime,
                hasExtra = a.HasExtra,
                code = a.Code,
                dispercent = a.DiscountPercent
            }).ToList();
            #region Add 10 attribute to specification

            foreach (var item in rs)
            {
                if (!String.IsNullOrWhiteSpace(item.att1) && item.att1.Split(':') != null)
                {
                    foreach (var att in item.att1.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att1.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att2) && item.att2.Split(':') != null)
                {
                    foreach (var att in item.att2.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att2.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att3) && item.att3.Split(':') != null)
                {
                    foreach (var att in item.att3.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att3.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att4) && item.att4.Split(':') != null)
                {
                    foreach (var att in item.att4.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att4.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att5) && item.att5.Split(':') != null)
                {
                    foreach (var att in item.att5.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att5.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att6) && item.att6.Split(':') != null)
                {
                    foreach (var att in item.att6.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att6.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att7) && item.att7.Split(':') != null)
                {
                    foreach (var att in item.att7.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att7.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att8) && item.att8.Split(':') != null)
                {
                    foreach (var att in item.att8.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att8.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att9) && item.att9.Split(':') != null)
                {
                    foreach (var att in item.att9.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att9.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
                if (!String.IsNullOrWhiteSpace(item.att10) && item.att10.Split(':') != null)
                {
                    foreach (var att in item.att10.Split(':')[1].Split(';'))
                    {
                        item.specification.Add(new { name = item.att10.Split(':')[0], value = att, ImageUrl = "" });
                    }
                }
            }
            #endregion
            return Json(new
            {
                products = rs
            }, JsonRequestBehavior.AllowGet);
        }

        public async System.Threading.Tasks.Task<ActionResult> LoadProductCollection(int collectionId, string pattern)//SaiGonVat : HotProducts: 33 ;  BestSaleProducts: 34 ; NewProducts: 35 - storeId 83 - pattern : empty
        {
            //Load collection như bình thường PHUONGTA
            var productCollectionApi = new ProductCollectionApi();
            pattern = (pattern ?? "").ToLower();
            var collection = await productCollectionApi.GetDetails(collectionId);
            if (collection == null || !collection.ProductCollection.Active || collection.ProductCollection.StoreId != CurrentStore.ID)// currentStore: 83(SaiGonVat)
            {
                return null;
            }

            var collectionItems = collection.Items;
            return Json(new
            {
                products = collectionItems.Select(a => new
                {
                    image = a.Product.PicURL,
                    name = a.Product.ProductName,
                    id = a.Product.ProductID,//without ProductApi.General()
                    discount = a.Product.DiscountPrice,
                    price = a.Product.Price,
                    category = a.Product.CatID,
                    hasGeneral = a.Product.ProductType == (int)ProductTypeEnum.General ? true : false,
                    quantity = 1,
                    note = "",
                    seoName = a.Product.SeoName,
                    createdDate = a.Product.CreateTime,
                    code = a.Product.Code,
                })
            }, JsonRequestBehavior.AllowGet);
        }

        /*
         * Author: BaoTD
         * Method: Load product collection using its name (must be unique in every store)
         * Params: name - product collection's unique name
         * Return: Product collection if any
         */
        public async System.Threading.Tasks.Task<ActionResult> LoadProductCollectionByName(string name)
        {
            var productCollectionApi = new ProductCollectionApi();
            var collection = await productCollectionApi.GetDetailsByName(name, CurrentStore.BrandId.Value);
            if (collection == null || !collection.ProductCollection.Active || collection.ProductCollection.BrandId.Value != CurrentStore.BrandId.Value)
            {
                return null;
            }

            var collectionItems = collection.Items;
            return Json(new
            {
                products = collectionItems.Select(a => new
                {
                    image = a.Product.PicURL,
                    name = a.Product.ProductName,
                    id = a.Product.ProductID,
                    discount = a.Product.DiscountPrice,
                    price = a.Product.Price,
                    category = a.Product.CatID,
                    hasGeneral = a.Product.ProductType == (int)ProductTypeEnum.General ? true : false,
                    quantity = 1,
                    note = "",
                    seoName = a.Product.SeoName,
                    createdDate = a.Product.CreateTime,
                })
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// HiepBP-PhuongTA
        /// Thêm bảng CategoryExtra
        /// </summary>
        /// <param name="primaryProductId"></param>
        /// <returns></returns>
        public JsonResult GetGroupExtraByProductPrimaryId(int primaryProductId)
        {
            var cateExtraApi = new CategoryExtraMappingApi();
            var categoryExtras = cateExtraApi.GetProductCategoryExtra(primaryProductId);
            return Json(new
            {
                extraGroup = categoryExtras.Select(a => new
                {
                    groupId = a.CateID,
                    groupName = a.CateName,
                })
            });
        }


        public JsonResult LoadCategories()
        {
            var cateApi = new ProductCategoryApi();
            var categories = cateApi.Get().Where(a => a.IsDisplayed && a.IsExtra == false && a.StoreId == CurrentStore.ID).ToList();
            return Json(new
            {
                categories = categories.Select(a => new { id = a.CateID, name = a.CateName, parentId = a.ParentCateId == null ? 0 : a.ParentCateId })
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExtra(int cateId)
        {
            var productCate = new ProductCategoryApi();
            //Get product Extra
            var productExtra = productCate.GetProductExtra(cateId);

            //Get product Parent of extra
            return Json(new
            {
                productExtra = productExtra.Select(a => new
                {
                    id = a.ProductID,
                    name = a.ProductName,
                    price = a.Price
                })
                //productParent = productParent
            });
        }

        [HttpPost]
        public JsonResult GetCategoryExtra()
        {
            var productCateApi = new ProductCategoryApi();
            var categories = productCateApi.GetProductCategorieExtra().Where(a => a.StoreId == CurrentStore.ID);//new line : add where store ID
            return Json(new
            {
                extraGroup = categories.Select(a => new
                {
                    groupId = a.CateID,
                    groupName = a.CateName,
                    //parentId = a.ParentCateId == null ? 1 : a.ParentCateId
                })
            });
        }

        [HttpGet]
        public JsonResult GetGeneral(int productId)
        {
            var productApi = new ProductApi();
            //Get product child of product general
            var productGeneral = productApi.GetAllProductGeneral(productId);

            //Get product general
            return Json(new
            {
                productGeneral = productGeneral.Select(a => new
                {
                    id = a.ProductID,
                    att = a.Att1,
                    price = a.Price,
                    code = a.Code,
                    discount = a.DiscountPrice
                })
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// HiepBP-PhuongTA
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        //public JsonResult GetExtraOfGeneral(string productId)
        //{
        //    var productApi = new ProductApi();
        //    var productExtraApi = new ProductExtraApi();
        //    int proId = Convert.ToInt32(productId);
        //    //Get product extra of product child general
        //    var productExtra = productExtraApi.GetProductExtraByProductAsyn(proId);

        //    //Get product child general
        //    var productGeneral = productApi.Get(proId);
        //    return Json(new
        //    {
        //        productExtra = productExtra,
        //        productGeneral = productGeneral
        //    });
        //}
        public JsonResult UpdateOrderExtra(int orderId)
        {
            var orderDetailApi = new OrderDetailApi();
            var orderDetail = orderDetailApi.OrderMaster(orderId);
            //Get order detail has type is Master
            var orderMaster =
                 orderDetail.OrderDetail1.FirstOrDefault(a => a.ProductOrderType == (int)ProductOrderType.Master);
            //Get order detail has type is Child
            var orderChild = orderDetail.OrderDetail1.Where(a => a.ProductOrderType == (int)ProductOrderType.Child);
            return Json(new
            {
                orderMaster = orderMaster,
                orderChild = orderChild
            });
        }
        public JsonResult UpdateOrderSingle(int orderId)
        {
            var orderDetailApi = new OrderDetailApi();
            var orderSingle = orderDetailApi.Get(orderId);
            return Json(new
            {
                orderSingle = orderSingle
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ApplyVoucher(string voucherCode)
        {
            var promotionApi = new PromotionApi();
            var voucherApi = new VoucherApi();
            var promotionDetailApi = new PromotionDetailApi();

            VoucherViewModel voucher = null;
            Promotion promotion = null;
            PromotionDetail promotionDetail = null;

            if (!string.IsNullOrWhiteSpace(voucherCode))
            {
                voucher = voucherApi.GetVoucherByCode(voucherCode);
                if (voucher != null)
                {
                    promotion = promotionApi.GetPromotionByIdReturnEntity(voucher.PromotionID);
                }
                if (promotion != null)
                {
                    promotionDetail = promotionDetailApi.GetDetailByCode(promotion.PromotionCode).FirstOrDefault();
                }

            }

            DateTime now = DateTime.Now;
            bool isValidVoucher = voucher != null && promotionDetail != null && promotion != null
                && voucher.Quantity > voucher.UsedQuantity
                && (!promotion.IsVoucher.HasValue || promotion.IsVoucher.Value)
                && promotion.Active
                && now >= promotion.FromDate.GetStartOfDate() && now <= promotion.ToDate.GetEndOfDate()
                && now.Hour >= promotion.ApplyFromTime && now.Hour <= promotion.ApplyToTime
                && promotion.VoucherQuantity > promotion.VoucherUsedQuantity;

            var msg = "Apply Failed!";
            if (isValidVoucher)
            {
                msg = "Apply Success!";
            }

            return Json(new
            {
                msg = msg,
                checkVoucher = isValidVoucher,
            });
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrderToAdmin(string str, int selectedStoreId)//param: storeId
        {
            //int storeId = 1; // SGV
            var productApi = new ProductApi();
            OrderOnlineViewModel order = JsonConvert.DeserializeObject<OrderOnlineViewModel>(str);
            if (order != null)
            {
                int brandId = (new StoreApi()).Get(selectedStoreId).BrandId.Value;
                CustomerViewModel customer;
                customer = new CustomerApi().GetCustomerByEmail(order.Email, brandId);
                if (customer != null)
                {
                    customer.Name = order.CustomerName;
                    customer.Address = order.DeliveryAddress;
                    customer.Phone = order.Phone;
                    new CustomerApi().Edit(customer.CustomerID, customer);
                }
                else
                {
                    customer = new CustomerViewModel();
                    customer.Name = order.CustomerName;
                    customer.Email = order.Email;
                    customer.Address = order.DeliveryAddress;
                    customer.Phone = order.Phone;
                    customer.Type = 1; //CustomerType table - Quản lý khách hàng, apply khi đặt hàng trên website
                    new CustomerApi().Create(customer);
                }
                OrderCustomEntityViewModel rent = new OrderCustomEntityViewModel();
                rent.Order = new OrderViewModel();
                rent.Order.CheckInDate = DateTime.Now;
                rent.Order.TotalAmount = 0; //Giá trị khởi tạo: 0
                rent.Order.Discount = 0; //Giá trị khởi tạo: 0
                rent.Order.DiscountOrderDetail = 0; //Giá trị khởi tạo: 0
                var orderDetails = new List<OrderDetailViewModel>();
                foreach (var item in order.ProductList)
                {
                    ProductViewModel product = new ProductApi().Get(item.ProductId);
                    OrderDetailViewModel orderDetail = new OrderDetailViewModel();
                    orderDetail.ProductID = item.ProductId;
                    orderDetail.Quantity = item.Quantity;
                    orderDetail.TotalAmount = product.Price * item.Quantity;
                    orderDetail.OrderDate = DateTime.Now;
                    orderDetail.Status = 0; // Tình trạng normal của món hàng từ website xuống POS, (thay đổi giá trị 'Hủy' tại POS)
                    orderDetail.IsAddition = false;
                    orderDetail.UnitPrice = product.Price;
                    orderDetail.Discount = (product.Price - product.PrimaryPrice) * item.Quantity;
                    orderDetail.StoreId = CurrentStore.ID;
                    orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
                    rent.Order.TotalAmount += orderDetail.TotalAmount;
                    rent.Order.DiscountOrderDetail += orderDetail.Discount;
                    orderDetail.ProductType = productApi.Get(item.ProductId).ProductType;
                    orderDetails.Add(orderDetail);

                }
                rent.Order.InvoiceID = "SGV-1-DE-" + HmsService.Models.Utils.GenerateInvoiceCode();// Thêm quy tắc chung : HiệpPB
                rent.OrderDetails = orderDetails;
                rent.Order.FinalAmount = rent.Order.TotalAmount - rent.Order.Discount - rent.Order.DiscountOrderDetail + order.Fee;
                rent.Order.DeliveryStatus = (int)Enums.StatusOrder.New;
                rent.Order.CheckInPerson = "Website";
                rent.Order.Notes = order.Note;
                rent.Order.OrderStatus = (int)OrderStatusEnum.New;
                rent.Order.OrderType = (int)OrderTypeEnum.Delivery;
                rent.Order.StoreID = selectedStoreId; //storeId của cửa hàng khách hàng chọn đơn hàng đến. (đơn hàng xuống máy POS)
                rent.Order.RentType = (int)RentTypeEnum.Default;
                rent.Order.DeliveryAddress = order.DeliveryAddress;
                rent.Order.DeliveryPhone = order.Phone;
                rent.Order.DeliveryReceiver = order.CustomerName;
                rent.Order.Att1 = string.Format("{0}", order.Fee);
                rent.Order.IsFixedPrice = false;
                rent.Order.CustomerID = customer.CustomerID;
                rent.Order.SourceType = 2;
                //new OrderApi().Create(rent);//Create and return rentId như hàng dưới)
                var rentId = new OrderApi().CreateOrderDelivery(rent);

                var msg = new NotifyOrder()
                {
                    StoreId = selectedStoreId,
                    NotifyType = (int)NotifyMessageType.OrderChange,
                    Content = "Đơn hàng mới",
                    OrderId = rentId
                };
                await HmsService.Models.Utils.RequestOrderWebApi(msg);
                Thread.Sleep(500);
                order = null;
            }
            return Json(new
            {
                success = true
            });
        }

        public int getLastStore(int brandId)
        {
            var storeApi = new StoreApi();
            var storeId = storeApi.GetListStoreByBrandId(brandId).Select(q => q.ID).FirstOrDefault();
            return storeId;
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrderToAdminByLastStore(string str, int brandId)//param: brandId
        {
            var productApi = new ProductApi();
            var storeId = getLastStore(brandId);
            OrderOnlineViewModel order = JsonConvert.DeserializeObject<OrderOnlineViewModel>(str);
            var orderApi = new OrderApi();
            if (order != null)
            {
                CustomerViewModel customer;
                customer = new CustomerApi().GetCustomerByEmail(order.Email, brandId);
                if (customer != null)
                {
                    customer.Name = order.CustomerName;
                    customer.Address = order.DeliveryAddress;
                    customer.Phone = order.Phone;
                    new CustomerApi().Edit(customer.CustomerID, customer);
                }
                else
                {
                    customer = new CustomerViewModel();
                    customer.Name = order.CustomerName;
                    customer.Email = order.Email;
                    customer.Address = order.DeliveryAddress;
                    customer.Phone = order.Phone;
                    customer.Type = 1; //CustomerType table - Quản lý khách hàng, apply khi đặt hàng trên website
                    new CustomerApi().Create(customer);
                }
                OrderCustomEntityViewModel rent = new OrderCustomEntityViewModel();
                rent.Order = new OrderViewModel();
                rent.Order.CheckInDate = DateTime.Now;
                rent.Order.TotalAmount = 0; //Giá trị khởi tạo: 0
                rent.Order.Discount = 0; //Giá trị khởi tạo: 0
                rent.Order.DiscountOrderDetail = 0; //Giá trị khởi tạo: 0
                var orderDetails = new List<OrderDetailViewModel>();
                List<ProductWithExtras> listExtra = new List<ProductWithExtras>();
                foreach (var item in order.ProductList)
                {
                    ProductViewModel product = new ProductApi().Get(item.ProductId);
                    OrderDetailViewModel orderDetail = new OrderDetailViewModel();
                    orderDetail.ProductID = item.ProductId;
                    orderDetail.Quantity = item.Quantity;
                    orderDetail.TotalAmount = product.Price * item.Quantity;
                    orderDetail.OrderDate = DateTime.Now;
                    orderDetail.Status = 0; // Tình trạng normal của món hàng từ website xuống POS, (thay đổi giá trị 'Hủy' tại POS)
                    orderDetail.IsAddition = false;
                    orderDetail.UnitPrice = product.Price;
                    orderDetail.Discount = (product.Price - product.PrimaryPrice) * item.Quantity;
                    orderDetail.StoreId = CurrentStore.ID;
                    orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
                    orderDetail.TmpDetailId = item.TempId;
                    rent.Order.TotalAmount += orderDetail.TotalAmount;
                    rent.Order.DiscountOrderDetail += orderDetail.Discount;
                    orderDetail.ProductType = productApi.Get(item.ProductId).ProductType;
                    orderDetails.Add(orderDetail);
                    if (item.ProductWithExtras != null)
                    {
                        foreach (var productExtra in item.ProductWithExtras)
                        {
                            listExtra.Add(productExtra);
                        }
                    }
                }
                rent.Order.InvoiceID = "UniSpace-7-" + HmsService.Models.Utils.GenerateInvoiceCode();// Thêm quy tắc chung : HiệpPB
                rent.OrderDetails = orderDetails;
                rent.Order.FinalAmount = rent.Order.TotalAmount - rent.Order.Discount - rent.Order.DiscountOrderDetail + order.Fee;
                rent.Order.DeliveryStatus = (int)Enums.StatusOrder.New;
                rent.Order.CheckInPerson = "Website";
                rent.Order.Notes = order.Note;
                rent.Order.OrderStatus = (int)OrderStatusEnum.New;
                rent.Order.OrderType = (int)OrderTypeEnum.Delivery;
                rent.Order.StoreID = storeId;//storeId của cửa hàng khách hàng chọn đơn hàng đến. (đơn hàng xuống máy POS)
                rent.Order.RentType = (int)RentTypeEnum.Default;
                rent.Order.DeliveryAddress = order.DeliveryAddress;
                rent.Order.DeliveryPhone = order.Phone;
                rent.Order.DeliveryReceiver = order.CustomerName;
                rent.Order.Att1 = string.Format("{0}", order.Fee);
                rent.Order.IsFixedPrice = false;
                rent.Order.CustomerID = customer.CustomerID;
                rent.Order.SourceType = 2;
                //new OrderApi().Create(rent);//Create and return rentId như hàng dưới)
                if (rent.Order.CustomerID == null)
                {
                    var rentId = orderApi.CreateOrderDelivery(rent);
                    await orderApi.UpdateOrderDelivery(rentId, listExtra);
                    await orderApi.UpdateTmpDetailIdOrderDetailAsync(rentId);
                    var msg = new NotifyOrder()
                    {
                        StoreId = storeId,
                        NotifyType = (int)NotifyMessageType.OrderChange,
                        Content = "Đơn hàng mới",
                        OrderId = rentId
                    };
                    await HmsService.Models.Utils.RequestOrderWebApi(msg);
                    Thread.Sleep(500);
                    order = null;
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                    });
                }
            }
            return Json(new
            {
                success = true
            });
        }

        [HttpPost]
        public async Task<ActionResult> SendOrderWithVoucherToServer(string str)
        {
            try
            {
                var datenow = HmsService.Models.Utils.GetCurrentDateTime().ToString("dd_MM_yy");
                //Tạo api
                var promotionApi = new PromotionApi();
                var promotionDetailApi = new PromotionDetailApi();
                var voucherApi = new VoucherApi();
                var orderDetailPromotionMappingApi = new OrderDetailPromotionMappingApi();
                var orderPromotionMappingApi = new OrderPromotionMappingApi();
                var orderApi = new OrderApi();
                var orderDetailApi = new OrderDetailApi();
                var productApi = new ProductApi();
                var paymentApi = new PaymentApi();

                //Lấy thông tin người dùng đang login
                var currentUser = HttpContext.User.Identity.Name;
                var aspNetUserApi = new AspNetUserApi();
                var userLoggin = aspNetUserApi.GetUserByUsername(currentUser);

                var storeId = this.CurrentStore.ID;
                DateTime now = DateTime.Now;
                var isDiscountDelivery = false;

                //Biến lưu product extra
                List<OrderDetailViewModel> listExtra = new List<OrderDetailViewModel>();

                //Convert order từ json sang model
                OrderOnlineViewModel orderOnline = JsonConvert.DeserializeObject<OrderOnlineViewModel>(str);

                //Nếu convert thất bại
                if (orderOnline == null)
                {
                    return Json(new
                    {
                        success = false,
                    });
                }
                //Convert thành công
                else
                {
                    #region Lưu thông tin order (chưa giảm giá)

                    //Tạo biến ordertail
                    ICollection<OrderDetailViewModel> orderDetailList = new List<OrderDetailViewModel>();

                    //Lưu thông tin order

                    //Duyệt từng order detail luu vào order (chưa áp dụng giảm giá)
                    foreach (var itemDetail in orderOnline.ProductList)
                    {
                        //Lấy product trong database
                        var productDetail = productApi.GetProductById(itemDetail.ProductId);
                        //Thêm product vào orderdetail với thông tin lấy từ server
                        var orderDetail = new OrderDetailViewModel()
                        {
                            TotalAmount = productDetail.Price * itemDetail.Quantity,
                            FinalAmount = (productDetail.Price * (100 - productDetail.DiscountPercent) / 100) * itemDetail.Quantity,
                            Discount = (productDetail.Price * (productDetail.DiscountPercent) / 100) * itemDetail.Quantity,
                            ProductID = productDetail.ProductID,
                            ProductCode = productDetail.Code,
                            Quantity = itemDetail.Quantity,
                            ItemQuantity = 0,
                            Status = (int)OrderDetailStatusEnum.New,
                            UnitPrice = (productDetail.Price * (100 - productDetail.DiscountPercent) / 100),
                            OrderDate = DateTime.Now,
                            ProductType = productDetail.ProductType,
                            StoreId = storeId,
                            OrderDetailPromotionMappingId = itemDetail.OrderDetailPromotionMappingId,
                            OrderPromotionMappingId = itemDetail.OrderPromotionMappingId,
                            TmpDetailId = itemDetail.TempId,
                            OrderDetailAtt1 = itemDetail.Color,
                            OrderDetailAtt2 = itemDetail.Size,
                        };
                        orderDetailList.Add(orderDetail);
                        if (itemDetail.ProductWithExtras != null)
                        {
                            foreach (var productExtra in itemDetail.ProductWithExtras)
                            {
                                var productEx = productApi.GetProductById(productExtra.ProductExtraId);
                                var orderDetailEx = new OrderDetailViewModel()
                                {
                                    Discount = 0,
                                    TotalAmount = (productEx.Price * (100 - productEx.DiscountPercent) / 100) * (productExtra.Quantity * itemDetail.Quantity),
                                    FinalAmount = productExtra.FinalAmount != 0 ? productExtra.FinalAmount : (productEx.Price * (100 - productEx.DiscountPercent) / 100) * (productExtra.Quantity * itemDetail.Quantity),
                                    ProductID = productEx.ProductID,
                                    ProductCode = productEx.Code,
                                    Quantity = (productExtra.Quantity * itemDetail.Quantity),
                                    ItemQuantity = 0,
                                    Status = (int)OrderDetailStatusEnum.New,
                                    UnitPrice = (productEx.Price * (100 - productEx.DiscountPercent) / 100),
                                    OrderDate = DateTime.Now,
                                    TmpDetailId = itemDetail.TempId,
                                    StoreId = storeId,
                                    OrderDetailPromotionMappingId = null,
                                    OrderPromotionMappingId = null,
                                    OrderDetailAtt4 = productExtra.ImageUrl,
                                };
                                orderDetailList.Add(orderDetailEx);
                                listExtra.Add(orderDetailEx);
                            }
                        }
                    }
                    //Them san pham delivery
                    var deliveryProduct = productApi.GetProductByProductType((int)ProductTypeEnum.Delivery).FirstOrDefault();
                    if (deliveryProduct != null)
                    {
                        if (orderOnline.DeliveryCost != 0)
                        {
                            var orderDetailDelivery = new OrderDetailViewModel()
                            {
                                Discount = 0,
                                TotalAmount = orderOnline.DeliveryCost,
                                FinalAmount = orderOnline.DeliveryCost,
                                ProductID = deliveryProduct.ProductID,
                                ProductCode = deliveryProduct.Code,
                                Quantity = 1,
                                ItemQuantity = 0,
                                Status = (int)OrderDetailStatusEnum.New,
                                UnitPrice = orderOnline.DeliveryCost,
                                OrderDate = DateTime.Now,
                                ProductType = deliveryProduct.ProductType,
                                StoreId = storeId,
                            };
                            orderDetailList.Add(orderDetailDelivery);
                        }
                    }
                    //Tạo thông tin order (đã có giảm giá tại sản phẩm
                    var order = new OrderViewModel()
                    {
                        CheckInDate = DateTime.Now,
                        CustomerID = userLoggin.CustomerId,
                        TotalAmount = orderDetailList.Sum(q => q.TotalAmount),
                        FinalAmount = orderDetailList.Sum(q => q.FinalAmount),
                        Discount = 0, //TODO
                        DiscountOrderDetail = 0, //TODO
                        OrderStatus = (int)OrderStatusEnum.New,
                        OrderType = (int)OrderTypeEnum.Delivery,
                        StoreID = storeId,
                        SourceID = storeId,
                        Notes = orderOnline.Note, //TODO
                        IsFixedPrice = true, //TODO
                        InvoiceID = datenow + "-" + HmsService.Models.Utils.GenerateInvoiceCode(), //TODO
                        DeliveryStatus = (int)DeliveryStatus.New,
                        CheckInPerson = orderOnline.CheckInPerson,
                        VAT = 0,
                        VATAmount = 0,
                        RentType = (int)RentTypeEnum.Default,
                        DeliveryAddress = orderOnline.DeliveryAddress,
                        DeliveryPhone = orderOnline.Phone,
                        DeliveryReceiver = orderOnline.CustomerName,
                        DeliveryType = orderOnline.DeliveryType,
                        DeliveryPayment = orderOnline.DeliveryPayment,
                        SourceType = (int)OrderSourceEnum.Website,
                        Att1 = orderOnline.RequiredCustomer,
                        //PaymentStatus = (int)PaymentStatusEnum.New,
                    };

                    #endregion

                    var finalAmountOrder = orderDetailList.Where(k => k.ProductType != (int)ProductTypeEnum.Delivery).Sum(q => q.FinalAmount);

                    #region Tìm promotion của voucher code

                    //Tạo biến lưu promotion của voucher code
                    VoucherViewModel voucher = null;
                    Promotion promotionVoucher = null;
                    PromotionDetail promotionVoucherDetail = null;
                    bool checkValidVoucher = false;

                    //Nếu order có voucher code thì lấy promotion và promotion detail
                    if (!string.IsNullOrWhiteSpace(orderOnline.Voucher))
                    {
                        voucher = voucherApi.GetVoucherByCode(orderOnline.Voucher);
                        if (voucher != null)
                        {
                            promotionVoucher = promotionApi.GetPromotionByIdReturnEntity(voucher.PromotionID);
                        }
                        if (promotionVoucher != null)
                        {
                            promotionVoucherDetail = promotionDetailApi.GetDetailByCode(promotionVoucher.PromotionCode).FirstOrDefault();
                        }
                        checkValidVoucher = promotionVoucherDetail != null && promotionVoucher != null
                                    && voucher.Quantity > voucher.UsedQuantity
                                    && (!promotionVoucher.IsVoucher.HasValue || promotionVoucher.IsVoucher.Value)
                                    && promotionVoucher.Active
                                    && now >= promotionVoucher.FromDate.GetStartOfDate() && now <= promotionVoucher.ToDate.GetEndOfDate()
                                    && now.Hour >= promotionVoucher.ApplyFromTime && now.Hour <= promotionVoucher.ApplyToTime
                                    && promotionVoucher.VoucherQuantity > promotionVoucher.VoucherUsedQuantity;
                    }

                    #endregion

                    #region Lấy các promotion đang active
                    //Get all active promotion
                    var listPromotion = promotionApi.GetPromotionNotVoucherByStoreId(this.CurrentStore.ID).OrderBy(q => q.ApplyLevel).ToList();

                    //Add promotion của voucher vào list nếu có
                    if (promotionVoucher != null)
                    {
                        listPromotion.Add(promotionVoucher);
                    }
                    #endregion


                    #region Duyệt từng promotion để áp dụng

                    //Tạo list check promotion đã được áp dụng chưa và list promotion detail
                    //List<bool> listIsApplyPromotion = new List<bool>();
                    //List<PromotionDetail> listPromotionDetail = new List<PromotionDetail>();
                    List<PromotionWithDetail> listPromotionApplied = new List<PromotionWithDetail>();

                    foreach (var promotion in listPromotion)
                    {

                        if (promotion != null)
                        {
                            listPromotionApplied.Add(new PromotionWithDetail
                            {
                                Promotion = promotion,
                                ListPromotionDetail = new List<PromotionDetail>(),
                            });
                            var promotionDetailList = promotionDetailApi.GetDetailByCode(promotion.PromotionCode).ToList();
                            foreach (var promotionDetail in promotionDetailList)
                            {
                                bool isUsed = false;
                                //listPromotionDetail.Add(promotionDetail);
                                if (promotionDetail != null)
                                {
                                    //Kiểm tra voucher có hợp lệ không
                                    bool isValidVoucher = false;
                                    if (promotion.IsVoucher == true)
                                    {
                                        isValidVoucher = promotionDetail != null && promotion != null
                                        && voucher.Quantity > voucher.UsedQuantity
                                        && (!promotion.IsVoucher.HasValue || promotion.IsVoucher.Value)
                                        && promotion.Active
                                        && now >= promotion.FromDate.GetStartOfDate() && now <= promotion.ToDate.GetEndOfDate()
                                        && now.Hour >= promotion.ApplyFromTime && now.Hour <= promotion.ApplyToTime
                                        && promotion.VoucherQuantity > promotion.VoucherUsedQuantity;
                                    }
                                    else
                                    {
                                        isValidVoucher = promotionDetail != null && promotion != null
                                        && promotion.Active
                                        && now >= promotion.FromDate.GetStartOfDate() && now <= promotion.ToDate.GetEndOfDate()
                                        && now.Hour >= promotion.ApplyFromTime && now.Hour <= promotion.ApplyToTime;
                                    }

                                    double discountOrder = 0, discountOrderDetail = 0;
                                    //List lưu các sản phẩm được tặng
                                    List<OrderDetailViewModel> listGiftOrderDetail = new List<OrderDetailViewModel>();
                                    var promotionGiftType = -1;

                                    //Lấy ra loại quà tặng promotionGiftType của promotion hiện tại
                                    if (promotion != null && promotionDetail != null)
                                    {
                                        promotionGiftType = (int)(PromotionGiftTypeEnum)promotion.GiftType;
                                    }

                                    #region Lấy sẩn phẩm quà tặng và tiền giảm giá
                                    //Tạo biến lưu sản phẩm quà tặng
                                    Product giftProduct = null;
                                    //Số tiền discount
                                    double discount = 0;
                                    //Nếu loại quà tặng là giảm giá theo số tiền hoặc theo %
                                    if (promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate)
                                    {
                                        //Nếu giảm giá ở order detail (chưa test)
                                        if (promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetail)
                                        {
                                            //Lấy thông tin sản phẩm quà tặng và tiền giảm giá
                                            var discountProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                            giftProduct = productApi.GetProductByCode_All(discountProductCode);
                                            //Lấy ra số tiền đưuọc giảm
                                            discount = promotionDetail.DiscountAmount == null ? (promotionDetail.DiscountRate.Value / 100) : (double)promotionDetail.DiscountAmount.Value;
                                        }
                                        //Nếu không thì tính số tiên giảm giá
                                        else
                                        {
                                            giftProduct = null;
                                            discount = promotionDetail.DiscountAmount == null ? (promotionDetail.DiscountRate.Value / 100) : (double)promotionDetail.DiscountAmount.Value;
                                        }
                                    }
                                    //Nếu loại quà tặng là sản phẩm quà tặng (chưa test)
                                    else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                    {
                                        //Lấy sản phẩm quà tặng
                                        var discountProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                        giftProduct = productApi.GetProductByCode_All(discountProductCode);
                                        discount = 0;
                                    }
                                    #endregion

                                    //Duyệt từng orderdetail để áp dụng promotion (chưa sử dụng tới

                                    foreach (var orderDetailItem in orderDetailList)
                                    {
                                        //Áp dụng promotion mức sản phẩm
                                        if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetail && orderDetailItem.ProductCode == promotionDetail.BuyProductCode && isValidVoucher)
                                        {
                                            //Kiểm tra số lượng sản phẩm cần mua để áp dụng promotion
                                            bool isInRange = true;

                                            //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                            isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= orderDetailItem.Quantity) && isInRange;
                                            //So sánh nếu có max quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                            isInRange = (!promotionDetail.MaxBuyQuantity.HasValue || promotionDetail.MaxBuyQuantity.Value >= orderDetailItem.Quantity) && isInRange;

                                            //Promotion giảm giá cho sản phẩm theo số tiền hoặc %
                                            if (promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate)
                                            {
                                                //Lấy sản phẩm được giảm giá
                                                var giftProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                                //Lấy product trong database
                                                //var productDetail = productApi.GetProductById(orderDetailItem.ProductID);
                                                var discountDetail = orderDetailList.Where(q => q.ProductCode == giftProductCode).FirstOrDefault();
                                                var discountProduct = productApi.GetProductById(discountDetail.ProductID);
                                                //Chỉ áp dụng giảm giá nếu có mua sản phẩm và số lượng mua đủ
                                                if (discountDetail != null && isInRange)
                                                {
                                                    //Chỉ giảm giá đủ số lượng tối đa của promotion nếu mua vượt quá số lượng đó
                                                    if (promotionDetail.GiftQuantity.HasValue && discountDetail.Quantity > promotionDetail.GiftQuantity.Value)
                                                    {
                                                        //Lấy số lượng sản phẩm sẽ ko dc giảm
                                                        discountDetail.Quantity = discountDetail.Quantity - promotionDetail.GiftQuantity.Value;
                                                        //Lấy product trong database
                                                        var discountDetailFromDatabase = productApi.GetProductById(discountDetail.ProductID);
                                                        //Update lại quantity và giá của sản phẩm ko hưởng từ discount
                                                        discountDetail.TotalAmount = discountDetailFromDatabase.Price * discountDetail.Quantity;
                                                        discountDetail.FinalAmount = (discountDetailFromDatabase.Price * (100 - discountDetailFromDatabase.DiscountPercent) / 100) * discountDetail.Quantity;
                                                        discountDetail.Discount = (discountDetailFromDatabase.Price * (discountDetailFromDatabase.DiscountPercent) / 100) * discountDetail.Quantity;

                                                        discountOrderDetail += discountDetail.Discount;

                                                        //add thông tin sản phẩm đã đưuọc giảm giá với số lượng bằng số lượng đc cho phép
                                                        orderDetailList.Add(new OrderDetailViewModel()
                                                        {
                                                            TotalAmount = (discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * promotionDetail.GiftQuantity.Value,
                                                            FinalAmount = promotionDetail.DiscountAmount == null ? ((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * (1 - discount) * promotionDetail.GiftQuantity.Value) : (((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) - discount) * promotionDetail.GiftQuantity.Value),
                                                            Discount = 0,
                                                            ProductID = giftProduct.ProductID,
                                                            ProductCode = giftProduct.Code,
                                                            Quantity = promotionDetail.GiftQuantity.Value,
                                                            ItemQuantity = 0,
                                                            Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                            UnitPrice = (discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100),
                                                            OrderDate = DateTime.Now,
                                                            StoreId = storeId,
                                                            OrderDetailPromotionMappingId = null,
                                                            OrderPromotionMappingId = null
                                                        });


                                                        isUsed = true;

                                                        //Cộng số tiền đã discount của sản phẩm để lưu thông tin vào order
                                                        var curDetail = orderDetailList.Last();
                                                        curDetail.Discount = curDetail.TotalAmount - curDetail.FinalAmount;
                                                        discountOrderDetail += curDetail.Discount;
                                                    }
                                                    //Giảm giá toàn bộ khi không có quy định số lượng hoặc có nhưng trong giới hạn
                                                    else
                                                    {
                                                        //Giảm giá %
                                                        if (promotionDetail.DiscountAmount == null)
                                                        {
                                                            discountDetail.FinalAmount = ((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * (1 - discount) * discountDetail.Quantity);
                                                        }
                                                        //Giảm giá trực tiếp
                                                        else
                                                        {
                                                            discountDetail.FinalAmount = (((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) - discount) * discountDetail.Quantity);
                                                        }
                                                        isUsed = true;
                                                    }
                                                }
                                            }
                                            //Promotion tặng thêm product
                                            else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                            {
                                                //Nếu fail số lượng min max quantity thì không áp dụng promotion
                                                if (isInRange)
                                                {
                                                    orderDetailList.Add(new OrderDetailViewModel()
                                                    {
                                                        Discount = 0,
                                                        TotalAmount = (giftProduct.Price * (100 - giftProduct.DiscountPercent) / 100) * promotionDetail.GiftQuantity.Value,
                                                        FinalAmount = 0, //Free gift
                                                        ProductID = giftProduct.ProductID,
                                                        ProductCode = giftProduct.Code,
                                                        Quantity = promotionDetail.GiftQuantity.Value,
                                                        ItemQuantity = 0,
                                                        Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                        UnitPrice = (giftProduct.Price * (100 - giftProduct.DiscountPercent) / 100),
                                                        OrderDate = DateTime.Now,
                                                        StoreId = storeId,
                                                        OrderDetailPromotionMappingId = null,
                                                        OrderPromotionMappingId = null
                                                    });

                                                    var curDetail = orderDetailList.Last();
                                                    curDetail.Discount = curDetail.TotalAmount - curDetail.FinalAmount;
                                                    discountOrderDetail += curDetail.Discount;

                                                    isUsed = true;
                                                }
                                            }
                                        }

                                    }


                                    //Promotion mức order
                                    if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.Order && isValidVoucher)
                                    {
                                        //Kiểm tra số tiền hóa đơn cần đạt để áp dụng promotion
                                        bool isInRange = true;

                                        //Kiểm 
                                        isInRange = (!promotionDetail.MinOrderAmount.HasValue || promotionDetail.MinOrderAmount.Value <= finalAmountOrder) && isInRange;
                                        isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= finalAmountOrder) && isInRange;

                                        //Promotion giảm giá
                                        if ((promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate) && isInRange)
                                        {
                                            finalAmountOrder = promotionDetail.DiscountAmount == null ? finalAmountOrder * (1 - discount) : finalAmountOrder - discount;
                                            isUsed = true;
                                        }
                                        //Promotion tặng quà (chưa test)
                                        else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                        {
                                            var boughtOrderDetail = promotionDetail.BuyProductCode == null ? null : orderOnline.ProductList.Where(q => q.ProductCode == promotionDetail.BuyProductCode).FirstOrDefault();
                                            if (promotionDetail.BuyProductCode == null || boughtOrderDetail != null) { }
                                            {
                                                //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                                isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= boughtOrderDetail.Quantity) && isInRange;
                                                isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= boughtOrderDetail.Quantity) && isInRange;
                                            }

                                            //2 trường hợp so sánh nếu fail bất kỳ thì không áp dụng promotion
                                            if (isInRange)
                                            {
                                                orderDetailList.Add(new OrderDetailViewModel()
                                                {
                                                    Discount = 0,
                                                    TotalAmount = giftProduct.Price * promotionDetail.GiftQuantity.Value,
                                                    FinalAmount = 0, //Free gift
                                                    ProductID = giftProduct.ProductID,
                                                    ProductCode = giftProduct.Code,
                                                    Quantity = promotionDetail.GiftQuantity.Value,
                                                    ItemQuantity = 0,
                                                    Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                    UnitPrice = giftProduct.Price,
                                                    OrderDate = DateTime.Now,
                                                    StoreId = storeId,
                                                    OrderDetailPromotionMappingId = null,
                                                    OrderPromotionMappingId = null
                                                });

                                                isUsed = true;
                                            }
                                        }
                                    }
                                    //Promotion giảm giá cho delivery (type orderdetailpercent)
                                    else if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetailPercent && isValidVoucher && orderOnline.AreaId == promotion.AreaId)
                                    {
                                        //Kiểm tra số tiền hóa đơn cần đạt để áp dụng promotion
                                        bool isInRange = true;

                                        //So sánh nếu có max quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                        isInRange = (!promotionDetail.MinOrderAmount.HasValue || promotionDetail.MinOrderAmount.Value <= finalAmountOrder) && isInRange;
                                        isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= finalAmountOrder) && isInRange;
                                        //Kiếm sản phẩn delivery
                                        if (promotionDetail.BuyProductCode != null)
                                        {
                                            var BuyProduct = productApi.GetProductByCode_All(promotionDetail.BuyProductCode);
                                            var boughtOrderDetail = orderDetailList.Where(q => q.ProductID == BuyProduct.ProductID).FirstOrDefault();
                                            if (boughtOrderDetail != null)
                                            {
                                                //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                                isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= boughtOrderDetail.Quantity) && isInRange;
                                                //2 trường hợp so sánh nếu fail bất kỳ thì không áp dụng promotion
                                                if (isInRange)
                                                {
                                                    //finalAmountOrder = finalAmountOrder - boughtOrderDetail.FinalAmount;
                                                    if (promotionDetail.DiscountRate != null && promotionDetail.DiscountRate.HasValue)
                                                    {
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount * (100 - promotionDetail.DiscountRate.Value) / 100;
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount < 0 ? 0 : boughtOrderDetail.FinalAmount;
                                                    }
                                                    else if (promotionDetail.DiscountAmount != null && promotionDetail.DiscountAmount.HasValue)
                                                    {
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount - (double)promotionDetail.DiscountAmount;
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount < 0 ? 0 : boughtOrderDetail.FinalAmount;
                                                    }
                                                    finalAmountOrder = finalAmountOrder + boughtOrderDetail.FinalAmount;
                                                    isUsed = true;
                                                    isDiscountDelivery = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (isUsed)
                                {
                                    listPromotionApplied.Last().ListPromotionDetail.Add(promotionDetail);
                                }
                            }

                        }

                    }
                    #endregion
                    order.OrderDetails = orderDetailList;
                    if (!isDiscountDelivery)
                    {
                        if (order.OrderDetails.Where(q => q.ProductType == (int)ProductTypeEnum.Delivery).FirstOrDefault() != null)
                        {
                            order.FinalAmount = finalAmountOrder + order.OrderDetails.Where(q => q.ProductType == (int)ProductTypeEnum.Delivery).FirstOrDefault().FinalAmount;
                        }
                    }
                    else
                    {
                        order.FinalAmount = finalAmountOrder;
                    }
                    order.Discount = order.TotalAmount - order.FinalAmount;

                    orderApi.Create(order);
                    var orderId = order.RentID;
                    await orderApi.UpdateExtraParentId(orderId, listExtra);

                    //Tạo mapping và gửi email xác nhận thành công
                    if (orderId > 0)
                    {
                        //TODO: create mapping, update voucher and promotion detail, check voucher before use
                        foreach (var appliedPromotion in listPromotionApplied)
                        {
                            var currentPromotion = appliedPromotion.Promotion;
                            foreach (var currentPromotionDetail in appliedPromotion.ListPromotionDetail)
                            {
                                if (currentPromotion != null & currentPromotionDetail != null)
                                {
                                    //(chưa test)
                                    if (currentPromotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetail)
                                    {
                                        var promoOrderDetail = order.OrderDetails.Where(q => q.ProductCode == currentPromotionDetail.BuyProductCode).FirstOrDefault();
                                        if (currentPromotionDetail.DiscountAmount != null || currentPromotionDetail.DiscountRate != null)
                                        {
                                            var orderDetailMappingId = orderDetailPromotionMappingApi.CreateAndReturnId(new OrderDetailPromotionMappingViewModel
                                            {
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = currentPromotionDetail.DiscountAmount.HasValue ? currentPromotionDetail.DiscountAmount.Value : (decimal)(promoOrderDetail.TotalAmount - promoOrderDetail.FinalAmount),
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true,
                                                OrderDetailId = promoOrderDetail.OrderDetailID,
                                            });

                                            var orderMappingId = orderPromotionMappingApi.CreateAndReturnId(new OrderPromotionMappingViewModel
                                            {
                                                OrderId = order.RentID,
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = 0,
                                                DiscountRate = 0,
                                                Active = true
                                            });
                                            var giftProductCode = currentPromotionDetail.GiftProductCode != null ? currentPromotionDetail.GiftProductCode : currentPromotionDetail.BuyProductCode;
                                            var giftOrderDetail = order.OrderDetails.Where(q => q.ProductCode == giftProductCode).FirstOrDefault();
                                            if (orderDetailMappingId != -1 && orderMappingId != -1)
                                            {
                                                giftOrderDetail.OrderDetailPromotionMappingId = orderDetailMappingId;
                                                giftOrderDetail.OrderPromotionMappingId = orderMappingId;
                                                orderDetailApi.UpdateOrderDetail(giftOrderDetail);
                                            }
                                            else
                                            {
                                                return Json(new { success = false, message = "Tạo mapping thất bại" });
                                            }
                                        }
                                        else if (!String.IsNullOrEmpty(currentPromotionDetail.GiftProductCode.Trim()))
                                        {
                                            var orderDetailMappingId = orderDetailPromotionMappingApi.CreateAndReturnId(new OrderDetailPromotionMappingViewModel
                                            {
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = 0,
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true,
                                                OrderDetailId = promoOrderDetail.OrderDetailID,
                                            });

                                            var orderMappingId = orderPromotionMappingApi.CreateAndReturnId(new OrderPromotionMappingViewModel
                                            {
                                                OrderId = order.RentID,
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = currentPromotionDetail.DiscountAmount.HasValue ? currentPromotionDetail.DiscountAmount.Value : 0,
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true
                                            });
                                            var giftProductCode = currentPromotionDetail.GiftProductCode != null ? currentPromotionDetail.GiftProductCode : currentPromotionDetail.BuyProductCode;
                                            var giftOrderDetail = order.OrderDetails.Where(q => q.ProductCode == giftProductCode && q.FinalAmount == 0).FirstOrDefault();
                                            if (orderDetailMappingId != -1)
                                            {
                                                giftOrderDetail.OrderDetailPromotionMappingId = orderDetailMappingId;
                                                giftOrderDetail.OrderPromotionMappingId = orderMappingId;
                                                orderDetailApi.UpdateOrderDetail(giftOrderDetail);
                                            }
                                            else
                                            {
                                                return Json(new { success = false, message = "Tạo mapping thất bại" });
                                            }
                                        }
                                    }
                                    else if (currentPromotion != null && currentPromotion.ApplyLevel == (int)PromotionApplyLevelEnum.Order)
                                    {
                                        var promoOrderDetail = order.OrderDetails.Where(q => q.ProductCode == currentPromotionDetail.BuyProductCode).FirstOrDefault();
                                        if (currentPromotionDetail.DiscountAmount != null || currentPromotionDetail.DiscountRate != null)
                                        {
                                            var orderMappingId = orderPromotionMappingApi.CreateAndReturnId(new OrderPromotionMappingViewModel
                                            {
                                                OrderId = order.RentID,
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = currentPromotionDetail.DiscountAmount.HasValue ? currentPromotionDetail.DiscountAmount.Value : 0,
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true
                                            });
                                            //var giftProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                            //if (orderMappingId != -1 && !string.IsNullOrEmpty(giftProductCode))
                                            //{
                                            //    var giftOrderDetail = order.OrderDetails.Where(q => q.Product.Code == giftProductCode && q.FinalAmount == 0).FirstOrDefault();
                                            //    giftOrderDetail.OrderPromotionMappingId = orderMappingId;
                                            //    orderDetailApi.BaseService.Update(giftOrderDetail);
                                            //}
                                            //else
                                            //{
                                            //    return Json(new { success = false, message = "Tạo mapping thất bại" });
                                            //}
                                        }
                                        //(chưa test)
                                        else if (currentPromotion.GiftType == (int)PromotionGiftTypeEnum.Gift)
                                        {
                                            var orderMappingId = orderPromotionMappingApi.CreateAndReturnId(new OrderPromotionMappingViewModel
                                            {
                                                OrderId = order.RentID,
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = currentPromotionDetail.DiscountAmount.HasValue ? currentPromotionDetail.DiscountAmount.Value : 0,
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true
                                            });

                                            var giftProductCode = currentPromotionDetail.GiftProductCode != null ? currentPromotionDetail.GiftProductCode : currentPromotionDetail.BuyProductCode;
                                            var giftOrderDetail = order.OrderDetails.Where(q => q.ProductCode == giftProductCode && q.FinalAmount == 0).FirstOrDefault();
                                            if (orderMappingId != -1)
                                            {
                                                giftOrderDetail.OrderPromotionMappingId = orderMappingId;
                                                orderDetailApi.UpdateOrderDetail(giftOrderDetail);
                                            }
                                            else
                                            {
                                                return Json(new { success = false, message = "Tạo mapping thất bại" });
                                            }
                                        }
                                    }
                                    else if (currentPromotion != null && currentPromotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetailPercent && orderOnline.AreaId == currentPromotion.AreaId)
                                    {
                                        var BuyProduct = productApi.GetProductByCode_All(currentPromotionDetail.BuyProductCode);
                                        var promoOrderDetail = order.OrderDetails.Where(q => q.ProductID == BuyProduct.ProductID).FirstOrDefault();
                                        if (currentPromotionDetail.DiscountAmount != null || currentPromotionDetail.DiscountRate != null)
                                        {
                                            var orderDetailMappingId = orderDetailPromotionMappingApi.CreateAndReturnId(new OrderDetailPromotionMappingViewModel
                                            {
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = currentPromotionDetail.DiscountAmount.HasValue ? currentPromotionDetail.DiscountAmount.Value : (decimal)(promoOrderDetail.TotalAmount - promoOrderDetail.FinalAmount),
                                                DiscountRate = currentPromotionDetail.DiscountRate,
                                                Active = true,
                                                OrderDetailId = promoOrderDetail.OrderDetailID,
                                            });

                                            var orderMappingId = orderPromotionMappingApi.CreateAndReturnId(new OrderPromotionMappingViewModel
                                            {
                                                OrderId = order.RentID,
                                                PromotionDetailId = currentPromotionDetail.PromotionDetailID,
                                                PromotionId = currentPromotion.PromotionID,
                                                DiscountAmount = 0,
                                                DiscountRate = 0,
                                                Active = true
                                            });
                                            var codeMapping = string.IsNullOrEmpty(currentPromotionDetail.GiftProductCode) ? currentPromotionDetail.BuyProductCode : currentPromotionDetail.GiftProductCode;
                                            var giftOrderDetail = order.OrderDetails.Where(q => q.ProductCode == codeMapping).FirstOrDefault();
                                            if (orderDetailMappingId != -1 && orderMappingId != -1)
                                            {
                                                giftOrderDetail.OrderDetailPromotionMappingId = orderDetailMappingId;
                                                giftOrderDetail.OrderPromotionMappingId = orderMappingId;
                                                orderDetailApi.Edit(giftOrderDetail.OrderDetailID, giftOrderDetail);
                                            }
                                            else
                                            {
                                                return Json(new { success = false, message = "Tạo mapping thất bại" });
                                            }
                                        }
                                    }

                                    if (voucher != null)
                                    {
                                        voucher.UsedQuantity += 1;
                                        voucherApi.Update(voucher);
                                    }

                                    if (currentPromotion != null)
                                    {
                                        currentPromotion.VoucherUsedQuantity = currentPromotion.VoucherUsedQuantity == null ? null : currentPromotion.VoucherUsedQuantity + 1;
                                    }



                                    await promotionApi.UpdatePromotion(currentPromotion);
                                }
                            }

                        }



                        #region Gửi email

                        var emailTitle = "Xác nhận đơn hàng thành công";
                        var emailContent = "Xác nhận đơn hàng thành công";

                        try
                        {
                            var webpageApi = new WebPageApi();
                            var emailTemplate = webpageApi.GetByStoreAndTitle(this.CurrentStore.ID, "Order email confirm");
                            var hotline = new StoreWebSettingApi().BaseService.GetActiveByStoreAndName(this.CurrentStore.ID, "Phone").Value;
                            if (emailTemplate != null)
                            {
                                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                                string a = double.Parse("12345").ToString("#,###", cul.NumberFormat);
                                emailTitle = emailTemplate.PageTitle;
                                emailContent = emailTemplate.PageContent;
                                emailContent = emailContent.Replace("@username", order.CheckInPerson);
                                emailContent = emailContent.Replace("@userPhone", order.DeliveryPhone);
                                emailContent = emailContent.Replace("@invoiceId", order.InvoiceID);
                                emailContent = emailContent.Replace("@paymentType", ((PaymentTypeEnum)order.DeliveryPayment).DisplayName());
                                emailContent = emailContent.Replace("@deliveryAddress", order.DeliveryAddress);
                                emailContent = emailContent.Replace("@totalAmount", double.Parse(order.TotalAmount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@discountAmount", double.Parse(order.Discount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@finalAmount", double.Parse(order.FinalAmount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@deliveryTime", ConfigurationManager.AppSettings["DeliveryTime"] + " ngày");
                                emailContent = emailContent.Replace("@hotline", hotline);
                                var tbodyIndex = emailContent.IndexOf("<tbody>") + "<tbody>".Length;
                                var bodyContent = "";
                                foreach (var productItem in order.OrderDetails)
                                {
                                    var rowData = "<tr>";
                                    rowData = rowData + "<td style='vertical-align:top;width:200pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Product.ProductName + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Product.Code + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt2 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt1 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt3 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Quantity + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + double.Parse(productItem.UnitPrice.ToString()).ToString("#,###", cul.NumberFormat) + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + double.Parse(productItem.TotalAmount.ToString()).ToString("#,###", cul.NumberFormat) + "</p></td>";
                                    //rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + (productItem.Discount == 0 ? "0" : double.Parse(productItem.Discount.ToString()).ToString("#,###", cul.NumberFormat)) + "</p></td>";
                                    //rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + (productItem.FinalAmount == 0 ? "0" : double.Parse(productItem.FinalAmount.ToString()).ToString("#,###", cul.NumberFormat)) + "</p></td>";
                                    rowData = rowData + "</tr>";
                                    bodyContent = bodyContent + rowData;
                                }
                                emailContent = emailContent.Insert(tbodyIndex, bodyContent);
                            }
                            SendEmailAsync(userLoggin.Email, emailTitle, emailContent);
                        }
                        catch (Exception e)
                        {

                        }

                        //mail cho admin

                        emailTitle = "Thông báo đơn hàng mới";
                        emailContent = "Thông báo đơn hàng mới";
                        try
                        {
                            var webpageApi = new WebPageApi();
                            var emailTemplate = webpageApi.GetByStoreAndTitle(this.CurrentStore.ID, "Order email to admin");
                            var hotline = new StoreWebSettingApi().BaseService.GetActiveByStoreAndName(this.CurrentStore.ID, "Phone").Value;
                            if (emailTemplate != null)
                            {
                                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                                emailTitle = emailTemplate.PageTitle;
                                emailContent = emailTemplate.PageContent;
                                emailContent = emailContent.Replace("@username", order.CheckInPerson);
                                emailContent = emailContent.Replace("@userPhone", order.DeliveryPhone);
                                emailContent = emailContent.Replace("@redInvoice", order.Att1 == "1" ? "Có" : "Không");
                                emailContent = emailContent.Replace("@invoiceId", order.InvoiceID);
                                emailContent = emailContent.Replace("@paymentType", ((PaymentTypeEnum)order.DeliveryPayment).DisplayName());
                                emailContent = emailContent.Replace("@deliveryAddress", order.DeliveryAddress);
                                emailContent = emailContent.Replace("@totalAmount", order.TotalAmount == 0 ? "0" : double.Parse(order.TotalAmount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@discountAmount", order.Discount == 0 ? "0" : double.Parse(order.Discount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@finalAmount", order.FinalAmount == 0 ? "0" : double.Parse(order.FinalAmount.ToString()).ToString("#,###", cul.NumberFormat) + " VND");
                                emailContent = emailContent.Replace("@deliveryTime", ConfigurationManager.AppSettings["DeliveryTime"] + " ngày");
                                //emailContent = emailContent.Replace("@hotline", hotline);
                                var tbodyIndex = emailContent.IndexOf("<tbody>") + "<tbody>".Length;
                                var bodyContent = "";
                                foreach (var productItem in order.OrderDetails)
                                {
                                    var rowData = "<tr>";
                                    rowData = rowData + "<td style='vertical-align:top;width:200pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Product.ProductName + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Product.Code + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt2 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt1 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.OrderDetailAtt3 + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + productItem.Quantity + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + double.Parse(productItem.UnitPrice.ToString()).ToString("#,###", cul.NumberFormat) + "</p></td>";
                                    rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + double.Parse(productItem.TotalAmount.ToString()).ToString("#,###", cul.NumberFormat) + "</p></td>";
                                    //rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + double.Parse(productItem.Discount.ToString()).ToString("#,###", cul.NumberFormat) + "</p></td>";
                                    //rowData = rowData + "<td style='vertical-align:top;width:77.85pt'><p style='margin-left:0in; margin-right:0in; text-align:center'>" + (productItem.FinalAmount == 0 ? "0" : double.Parse(productItem.FinalAmount.ToString()).ToString("#,###", cul.NumberFormat)) + "</p></td>";
                                    rowData = rowData + "</tr>";
                                    bodyContent = bodyContent + rowData;
                                }
                                emailContent = emailContent.Insert(tbodyIndex, bodyContent);
                            }
                            SendEmailAsync(ConfigurationManager.AppSettings["AdminEmail"], emailTitle, emailContent);
                        }
                        catch (Exception e)
                        {

                        }

                        #endregion

                        return Json(new
                        {
                            success = true,
                            message = "Tạo order thành công"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            message = "Tạo order thất bại"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = ex.Message
                });
            }

        }



        public JsonResult CheckDiscount(string str)
        {
            try
            {
                var datenow = HmsService.Models.Utils.GetCurrentDateTime().ToString("dd_MM_yy");
                //Tạo api
                var promotionApi = new PromotionApi();
                var promotionDetailApi = new PromotionDetailApi();
                var voucherApi = new VoucherApi();
                var orderDetailPromotionMappingApi = new OrderDetailPromotionMappingApi();
                var orderPromotionMappingApi = new OrderPromotionMappingApi();
                var orderApi = new OrderApi();
                var orderDetailApi = new OrderDetailApi();
                var productApi = new ProductApi();
                var paymentApi = new PaymentApi();

                //Lấy thông tin người dùng đang login
                var currentUser = HttpContext.User.Identity.Name;
                var aspNetUserApi = new AspNetUserApi();
                var userLoggin = aspNetUserApi.GetUserByUsername(currentUser);

                var storeId = this.CurrentStore.ID;
                DateTime now = DateTime.Now;
                var isDiscountDelivery = false;

                //Biến lưu product extra
                List<OrderDetailViewModel> listExtra = new List<OrderDetailViewModel>();

                //Convert order từ json sang model
                OrderOnlineViewModel orderOnline = JsonConvert.DeserializeObject<OrderOnlineViewModel>(str);

                //Nếu convert thất bại
                if (orderOnline == null)
                {
                    return Json(new
                    {
                        success = false,
                    });
                }
                //Convert thành công
                else
                {

                    #region Lưu thông tin order (chưa giảm giá)

                    //Tạo biến ordertail
                    ICollection<OrderDetailViewModel> orderDetailList = new List<OrderDetailViewModel>();

                    //Lưu thông tin order

                    //Duyệt từng order detail luu vào order (chưa áp dụng giảm giá)
                    foreach (var itemDetail in orderOnline.ProductList)
                    {
                        //Lấy product trong database
                        var productDetail = productApi.GetProductById(itemDetail.ProductId);
                        //Thêm product vào orderdetail với thông tin lấy từ server
                        var orderDetail = new OrderDetailViewModel()
                        {
                            TotalAmount = productDetail.Price * itemDetail.Quantity,
                            FinalAmount = (productDetail.Price * (100 - productDetail.DiscountPercent) / 100) * itemDetail.Quantity,
                            Discount = (productDetail.Price * (productDetail.DiscountPercent) / 100) * itemDetail.Quantity,
                            ProductID = productDetail.ProductID,
                            ProductCode = productDetail.Code,
                            Quantity = itemDetail.Quantity,
                            ItemQuantity = 0,
                            Status = (int)OrderDetailStatusEnum.New,
                            UnitPrice = (productDetail.Price * (100 - productDetail.DiscountPercent) / 100),
                            OrderDate = DateTime.Now,
                            ProductType = productDetail.ProductType,
                            StoreId = storeId,
                            OrderDetailPromotionMappingId = itemDetail.OrderDetailPromotionMappingId,
                            OrderPromotionMappingId = itemDetail.OrderPromotionMappingId,
                            TmpDetailId = itemDetail.TempId,
                            OrderDetailAtt1 = itemDetail.Color,
                            OrderDetailAtt2 = itemDetail.Size,
                        };
                        orderDetailList.Add(orderDetail);
                        if (itemDetail.ProductWithExtras != null)
                        {
                            foreach (var productExtra in itemDetail.ProductWithExtras)
                            {
                                var productEx = productApi.GetProductById(productExtra.ProductExtraId);
                                var orderDetailEx = new OrderDetailViewModel()
                                {
                                    Discount = 0,
                                    TotalAmount = (productEx.Price * (100 - productEx.DiscountPercent) / 100) * (productExtra.Quantity * itemDetail.Quantity),
                                    FinalAmount = productExtra.FinalAmount != 0 ? productExtra.FinalAmount : (productEx.Price * (100 - productEx.DiscountPercent) / 100) * (productExtra.Quantity * itemDetail.Quantity),
                                    ProductID = productEx.ProductID,
                                    ProductCode = productEx.Code,
                                    Quantity = (productExtra.Quantity * itemDetail.Quantity),
                                    ItemQuantity = 0,
                                    Status = (int)OrderDetailStatusEnum.New,
                                    UnitPrice = (productEx.Price * (100 - productEx.DiscountPercent) / 100),
                                    OrderDate = DateTime.Now,
                                    TmpDetailId = itemDetail.TempId,
                                    StoreId = storeId,
                                    OrderDetailPromotionMappingId = null,
                                    OrderPromotionMappingId = null
                                };
                                orderDetailList.Add(orderDetailEx);
                                listExtra.Add(orderDetailEx);
                            }
                        }
                    }
                    //Them san pham delivery
                    var deliveryProduct = productApi.GetProductByProductType((int)ProductTypeEnum.Delivery).FirstOrDefault();
                    if (deliveryProduct != null)
                    {
                        if (orderOnline.DeliveryCost != 0)
                        {
                            var orderDetailDelivery = new OrderDetailViewModel()
                            {
                                Discount = 0,
                                TotalAmount = orderOnline.DeliveryCost,
                                FinalAmount = orderOnline.DeliveryCost,
                                ProductID = deliveryProduct.ProductID,
                                ProductCode = deliveryProduct.Code,
                                Quantity = 1,
                                ItemQuantity = 0,
                                Status = (int)OrderDetailStatusEnum.New,
                                UnitPrice = orderOnline.DeliveryCost,
                                OrderDate = DateTime.Now,
                                ProductType = deliveryProduct.ProductType,
                                StoreId = storeId,
                            };
                            orderDetailList.Add(orderDetailDelivery);
                        }
                    }
                    //Tạo thông tin order (đã có giảm giá tại sản phẩm
                    var order = new OrderViewModel()
                    {
                        CheckInDate = DateTime.Now,
                        CustomerID = userLoggin.CustomerId,
                        TotalAmount = orderDetailList.Sum(q => q.TotalAmount),
                        FinalAmount = orderDetailList.Sum(q => q.FinalAmount),
                        Discount = 0, //TODO
                        DiscountOrderDetail = 0, //TODO
                        OrderStatus = (int)OrderStatusEnum.New,
                        OrderType = (int)OrderTypeEnum.Delivery,
                        StoreID = storeId,
                        SourceID = storeId,
                        Notes = orderOnline.Note, //TODO
                        IsFixedPrice = true, //TODO
                        InvoiceID = datenow + "-" + HmsService.Models.Utils.GenerateInvoiceCode(), //TODO
                        DeliveryStatus = (int)DeliveryStatus.New,
                        CheckInPerson = orderOnline.CheckInPerson,
                        VAT = 0,
                        VATAmount = 0,
                        RentType = (int)RentTypeEnum.Default,
                        DeliveryAddress = orderOnline.DeliveryAddress,
                        DeliveryPhone = orderOnline.Phone,
                        DeliveryReceiver = orderOnline.CustomerName,
                        DeliveryType = orderOnline.DeliveryType,
                        DeliveryPayment = orderOnline.DeliveryPayment,
                        SourceType = (int)OrderSourceEnum.Website,
                        Att1 = orderOnline.RequiredCustomer,
                        //PaymentStatus = (int)PaymentStatusEnum.New,
                    };

                    #endregion

                    var finalAmountOrder = orderDetailList.Where(k => k.ProductType != (int)ProductTypeEnum.Delivery).Sum(q => q.FinalAmount);

                    #region Tìm promotion của voucher code

                    //Tạo biến lưu promotion của voucher code
                    VoucherViewModel voucher = null;
                    Promotion promotionVoucher = null;
                    PromotionDetail promotionVoucherDetail = null;
                    bool checkValidVoucher = false;

                    //Nếu order có voucher code thì lấy promotion và promotion detail
                    if (!string.IsNullOrWhiteSpace(orderOnline.Voucher))
                    {
                        voucher = voucherApi.GetVoucherByCode(orderOnline.Voucher);
                        if (voucher != null)
                        {
                            promotionVoucher = promotionApi.GetPromotionByIdReturnEntity(voucher.PromotionID);
                        }
                        if (promotionVoucher != null)
                        {
                            promotionVoucherDetail = promotionDetailApi.GetDetailByCode(promotionVoucher.PromotionCode).FirstOrDefault();
                        }
                        checkValidVoucher = promotionVoucherDetail != null && promotionVoucher != null
                                    && voucher.Quantity > voucher.UsedQuantity
                                    && (!promotionVoucher.IsVoucher.HasValue || promotionVoucher.IsVoucher.Value)
                                    && promotionVoucher.Active
                                    && now >= promotionVoucher.FromDate.GetStartOfDate() && now <= promotionVoucher.ToDate.GetEndOfDate()
                                    && now.Hour >= promotionVoucher.ApplyFromTime && now.Hour <= promotionVoucher.ApplyToTime
                                    && promotionVoucher.VoucherQuantity > promotionVoucher.VoucherUsedQuantity;
                    }

                    #endregion

                    #region Lấy các promotion đang active
                    //Get all active promotion
                    var listPromotion = promotionApi.GetPromotionNotVoucherByStoreId(this.CurrentStore.ID).OrderBy(q => q.ApplyLevel).ToList();

                    //Add promotion của voucher vào list nếu có
                    if (promotionVoucher != null)
                    {
                        listPromotion.Add(promotionVoucher);
                    }
                    #endregion


                    #region Duyệt từng promotion để áp dụng

                    //Tạo list check promotion đã được áp dụng chưa và list promotion detail
                    List<bool> listIsApplyPromotion = new List<bool>();
                    List<PromotionDetail> listPromotionDetail = new List<PromotionDetail>();

                    foreach (var promotion in listPromotion)
                    {
                        bool isUsed = false;
                        if (promotion != null)
                        {
                            var promotionDetailList = promotionDetailApi.GetDetailByCode(promotion.PromotionCode).ToList();
                            foreach (var promotionDetail in promotionDetailList)
                            {
                                listPromotionDetail.Add(promotionDetail);
                                if (promotionDetail != null)
                                {
                                    //Kiểm tra voucher có hợp lệ không
                                    bool isValidVoucher = false;
                                    if (promotion.IsVoucher == true)
                                    {
                                        isValidVoucher = promotionDetail != null && promotion != null
                                        && voucher.Quantity > voucher.UsedQuantity
                                        && (!promotion.IsVoucher.HasValue || promotion.IsVoucher.Value)
                                        && promotion.Active
                                        && now >= promotion.FromDate.GetStartOfDate() && now <= promotion.ToDate.GetEndOfDate()
                                        && now.Hour >= promotion.ApplyFromTime && now.Hour <= promotion.ApplyToTime
                                        && promotion.VoucherQuantity > promotion.VoucherUsedQuantity;
                                    }
                                    else
                                    {
                                        isValidVoucher = promotionDetail != null && promotion != null
                                        && promotion.Active
                                        && now >= promotion.FromDate.GetStartOfDate() && now <= promotion.ToDate.GetEndOfDate()
                                        && now.Hour >= promotion.ApplyFromTime && now.Hour <= promotion.ApplyToTime;
                                    }

                                    double discountOrder = 0, discountOrderDetail = 0;
                                    //List lưu các sản phẩm được tặng
                                    List<OrderDetailViewModel> listGiftOrderDetail = new List<OrderDetailViewModel>();
                                    var promotionGiftType = -1;

                                    //Lấy ra loại quà tặng promotionGiftType của promotion hiện tại
                                    if (promotion != null && promotionDetail != null)
                                    {
                                        promotionGiftType = (int)(PromotionGiftTypeEnum)promotion.GiftType;
                                    }

                                    #region Lấy sẩn phẩm quà tặng và tiền giảm giá
                                    //Tạo biến lưu sản phẩm quà tặng
                                    Product giftProduct = null;
                                    //Số tiền discount
                                    double discount = 0;
                                    //Nếu loại quà tặng là giảm giá theo số tiền hoặc theo %
                                    if (promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate)
                                    {
                                        //Nếu giảm giá ở order detail (chưa test)
                                        if (promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetail)
                                        {
                                            //Lấy thông tin sản phẩm quà tặng và tiền giảm giá
                                            var discountProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                            giftProduct = productApi.GetProductByCode_All(discountProductCode);
                                            //Lấy ra số tiền đưuọc giảm
                                            discount = promotionDetail.DiscountAmount == null ? (promotionDetail.DiscountRate.Value / 100) : (double)promotionDetail.DiscountAmount.Value;
                                        }
                                        //Nếu không thì tính số tiên giảm giá
                                        else
                                        {
                                            giftProduct = null;
                                            discount = promotionDetail.DiscountAmount == null ? (promotionDetail.DiscountRate.Value / 100) : (double)promotionDetail.DiscountAmount.Value;
                                        }
                                    }
                                    //Nếu loại quà tặng là sản phẩm quà tặng (chưa test)
                                    else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                    {
                                        //Lấy sản phẩm quà tặng
                                        var discountProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                        giftProduct = productApi.GetProductByCode_All(discountProductCode);
                                        discount = 0;
                                    }
                                    #endregion

                                    //Duyệt từng orderdetail để áp dụng promotion (chưa sử dụng tới

                                    foreach (var orderDetailItem in orderDetailList)
                                    {
                                        //Áp dụng promotion mức sản phẩm
                                        if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetail && orderDetailItem.ProductCode == promotionDetail.BuyProductCode && isValidVoucher)
                                        {
                                            //Kiểm tra số lượng sản phẩm cần mua để áp dụng promotion
                                            bool isInRange = true;

                                            //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                            isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= orderDetailItem.Quantity) && isInRange;
                                            //So sánh nếu có max quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                            isInRange = (!promotionDetail.MaxBuyQuantity.HasValue || promotionDetail.MaxBuyQuantity.Value >= orderDetailItem.Quantity) && isInRange;

                                            //Promotion giảm giá cho sản phẩm theo số tiền hoặc %
                                            if (promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate)
                                            {
                                                //Lấy sản phẩm được giảm giá
                                                var giftProductCode = promotionDetail.GiftProductCode != null ? promotionDetail.GiftProductCode : promotionDetail.BuyProductCode;
                                                //Lấy product trong database
                                                //var productDetail = productApi.GetProductById(orderDetailItem.ProductID);
                                                var discountDetail = orderDetailList.Where(q => q.ProductCode == giftProductCode).FirstOrDefault();
                                                var discountProduct = productApi.GetProductById(discountDetail.ProductID);
                                                //Chỉ áp dụng giảm giá nếu có mua sản phẩm và số lượng mua đủ
                                                if (discountDetail != null && isInRange)
                                                {
                                                    //Chỉ giảm giá đủ số lượng tối đa của promotion nếu mua vượt quá số lượng đó
                                                    if (promotionDetail.GiftQuantity.HasValue && discountDetail.Quantity > promotionDetail.GiftQuantity.Value)
                                                    {
                                                        //Lấy số lượng sản phẩm sẽ ko dc giảm
                                                        discountDetail.Quantity = discountDetail.Quantity - promotionDetail.GiftQuantity.Value;
                                                        //Lấy product trong database
                                                        var discountDetailFromDatabase = productApi.GetProductById(discountDetail.ProductID);
                                                        //Update lại quantity và giá của sản phẩm ko hưởng từ discount
                                                        discountDetail.TotalAmount = discountDetailFromDatabase.Price * discountDetail.Quantity;
                                                        discountDetail.FinalAmount = (discountDetailFromDatabase.Price * (100 - discountDetailFromDatabase.DiscountPercent) / 100) * discountDetail.Quantity;
                                                        discountDetail.Discount = (discountDetailFromDatabase.Price * (discountDetailFromDatabase.DiscountPercent) / 100) * discountDetail.Quantity;

                                                        discountOrderDetail += discountDetail.Discount;

                                                        //add thông tin sản phẩm đã đưuọc giảm giá với số lượng bằng số lượng đc cho phép
                                                        orderDetailList.Add(new OrderDetailViewModel()
                                                        {
                                                            TotalAmount = (discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * promotionDetail.GiftQuantity.Value,
                                                            FinalAmount = promotionDetail.DiscountAmount == null ? ((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * (1 - discount) * promotionDetail.GiftQuantity.Value) : (((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) - discount) * promotionDetail.GiftQuantity.Value),
                                                            Discount = 0,
                                                            ProductID = giftProduct.ProductID,
                                                            ProductCode = giftProduct.Code,
                                                            Quantity = promotionDetail.GiftQuantity.Value,
                                                            ItemQuantity = 0,
                                                            Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                            UnitPrice = (discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100),
                                                            OrderDate = DateTime.Now,
                                                            StoreId = storeId,
                                                            OrderDetailPromotionMappingId = null,
                                                            OrderPromotionMappingId = null
                                                        });


                                                        isUsed = true;

                                                        //Cộng số tiền đã discount của sản phẩm để lưu thông tin vào order
                                                        var curDetail = orderDetailList.Last();
                                                        curDetail.Discount = curDetail.TotalAmount - curDetail.FinalAmount;
                                                        discountOrderDetail += curDetail.Discount;
                                                    }
                                                    //Giảm giá toàn bộ khi không có quy định số lượng hoặc có nhưng trong giới hạn
                                                    else
                                                    {
                                                        //Giảm giá %
                                                        if (promotionDetail.DiscountAmount == null)
                                                        {
                                                            discountDetail.FinalAmount = ((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) * (1 - discount) * discountDetail.Quantity);
                                                        }
                                                        //Giảm giá trực tiếp
                                                        else
                                                        {
                                                            discountDetail.FinalAmount = (((discountProduct.Price * (100 - discountProduct.DiscountPercent) / 100) - discount) * discountDetail.Quantity);
                                                        }
                                                    }

                                                    isUsed = true;
                                                }
                                            }
                                            //Promotion tặng thêm product
                                            else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                            {
                                                //Nếu fail số lượng min max quantity thì không áp dụng promotion
                                                if (isInRange)
                                                {
                                                    orderDetailList.Add(new OrderDetailViewModel()
                                                    {
                                                        Discount = 0,
                                                        TotalAmount = (giftProduct.Price * (100 - giftProduct.DiscountPercent) / 100) * promotionDetail.GiftQuantity.Value,
                                                        FinalAmount = 0, //Free gift
                                                        ProductID = giftProduct.ProductID,
                                                        ProductCode = giftProduct.Code,
                                                        Quantity = promotionDetail.GiftQuantity.Value,
                                                        ItemQuantity = 0,
                                                        Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                        UnitPrice = (giftProduct.Price * (100 - giftProduct.DiscountPercent) / 100),
                                                        OrderDate = DateTime.Now,
                                                        StoreId = storeId,
                                                        OrderDetailPromotionMappingId = null,
                                                        OrderPromotionMappingId = null
                                                    });

                                                    var curDetail = orderDetailList.Last();
                                                    curDetail.Discount = curDetail.TotalAmount - curDetail.FinalAmount;
                                                    discountOrderDetail += curDetail.Discount;

                                                    isUsed = true;
                                                }
                                            }
                                        }

                                    }


                                    //Promotion mức order
                                    if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.Order && isValidVoucher)
                                    {
                                        //Kiểm tra số tiền hóa đơn cần đạt để áp dụng promotion
                                        bool isInRange = true;

                                        //Kiểm 
                                        isInRange = (!promotionDetail.MinOrderAmount.HasValue || promotionDetail.MinOrderAmount.Value <= finalAmountOrder) && isInRange;
                                        isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= finalAmountOrder) && isInRange;

                                        //Promotion giảm giá
                                        if ((promotionGiftType == (int)PromotionGiftTypeEnum.DiscountAmount || promotionGiftType == (int)PromotionGiftTypeEnum.DiscountRate) && isInRange)
                                        {
                                            finalAmountOrder = promotionDetail.DiscountAmount == null ? finalAmountOrder * (1 - discount) : finalAmountOrder - discount;
                                            isUsed = true;
                                        }
                                        //Promotion tặng quà (chưa test)
                                        else if (promotionGiftType == (int)PromotionGiftTypeEnum.Gift)
                                        {
                                            var boughtOrderDetail = promotionDetail.BuyProductCode == null ? null : orderOnline.ProductList.Where(q => q.ProductCode == promotionDetail.BuyProductCode).FirstOrDefault();
                                            if (promotionDetail.BuyProductCode == null || boughtOrderDetail != null) { }
                                            {
                                                //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                                isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= boughtOrderDetail.Quantity) && isInRange;
                                                isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= boughtOrderDetail.Quantity) && isInRange;
                                            }

                                            //2 trường hợp so sánh nếu fail bất kỳ thì không áp dụng promotion
                                            if (isInRange)
                                            {
                                                orderDetailList.Add(new OrderDetailViewModel()
                                                {
                                                    Discount = 0,
                                                    TotalAmount = giftProduct.Price * promotionDetail.GiftQuantity.Value,
                                                    FinalAmount = 0, //Free gift
                                                    ProductID = giftProduct.ProductID,
                                                    ProductCode = giftProduct.Code,
                                                    Quantity = promotionDetail.GiftQuantity.Value,
                                                    ItemQuantity = 0,
                                                    Status = (int)OrderDetailStatusEnum.New, //TODO:
                                                    UnitPrice = giftProduct.Price,
                                                    OrderDate = DateTime.Now,
                                                    StoreId = storeId,
                                                    OrderDetailPromotionMappingId = null,
                                                    OrderPromotionMappingId = null
                                                });

                                                isUsed = true;
                                            }
                                        }
                                    }
                                    //Promotion giảm giá cho delivery (type orderdetailpercent)
                                    else if (promotion != null && promotion.ApplyLevel == (int)PromotionApplyLevelEnum.OrderDetailPercent && isValidVoucher && orderOnline.AreaId == promotion.AreaId)
                                    {
                                        //Kiểm tra số tiền hóa đơn cần đạt để áp dụng promotion
                                        bool isInRange = true;

                                        //So sánh nếu có max quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                        isInRange = (!promotionDetail.MinOrderAmount.HasValue || promotionDetail.MinOrderAmount.Value <= finalAmountOrder) && isInRange;
                                        isInRange = (!promotionDetail.MaxOrderAmount.HasValue || promotionDetail.MaxOrderAmount.Value >= finalAmountOrder) && isInRange;
                                        //Kiếm sản phẩn delivery
                                        if (promotionDetail.BuyProductCode != null)
                                        {
                                            var BuyProduct = productApi.GetProductByCode_All(promotionDetail.BuyProductCode);
                                            var boughtOrderDetail = orderDetailList.Where(q => q.ProductID == BuyProduct.ProductID).FirstOrDefault();
                                            if (boughtOrderDetail != null)
                                            {
                                                //So sánh nếu có min quantity, nếu ko có max quantity thì cho qua, tiếp tục áp dụng promotion
                                                isInRange = (!promotionDetail.MinBuyQuantity.HasValue || promotionDetail.MinBuyQuantity.Value <= boughtOrderDetail.Quantity) && isInRange;
                                                //2 trường hợp so sánh nếu fail bất kỳ thì không áp dụng promotion
                                                if (isInRange)
                                                {
                                                    //finalAmountOrder = finalAmountOrder - boughtOrderDetail.FinalAmount;
                                                    if (promotionDetail.DiscountRate != null && promotionDetail.DiscountRate.HasValue)
                                                    {
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount * (100 - promotionDetail.DiscountRate.Value) / 100;
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount < 0 ? 0 : boughtOrderDetail.FinalAmount;
                                                    }
                                                    else if (promotionDetail.DiscountAmount != null && promotionDetail.DiscountAmount.HasValue)
                                                    {
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount - (double)promotionDetail.DiscountAmount;
                                                        boughtOrderDetail.FinalAmount = boughtOrderDetail.FinalAmount < 0 ? 0 : boughtOrderDetail.FinalAmount;
                                                    }
                                                    finalAmountOrder = finalAmountOrder + boughtOrderDetail.FinalAmount;
                                                    isDiscountDelivery = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        //Nếu promotion là null thì add detail là null để đủ số lượng trong 2 list
                        else
                        {
                            listPromotionDetail.Add(null);
                        }
                        //Add trạng thái đã áp dụng chưa vào list
                        listIsApplyPromotion.Add(isUsed);
                    }
                    #endregion
                    order.OrderDetails = orderDetailList;
                    if (!isDiscountDelivery)
                    {
                        if (order.OrderDetails.Where(q => q.ProductType == (int)ProductTypeEnum.Delivery).FirstOrDefault() != null)
                        {
                            order.FinalAmount = finalAmountOrder + order.OrderDetails.Where(q => q.ProductType == (int)ProductTypeEnum.Delivery).FirstOrDefault().FinalAmount;
                        }
                    }
                    else
                    {
                        order.FinalAmount = finalAmountOrder;
                    }
                    order.Discount = order.TotalAmount - order.FinalAmount;

                    return Json(new
                    {
                        success = true,
                        checkVoucher = checkValidVoucher,
                        totalDiscount = order.Discount
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = ex.Message
                });
            }
        }

        /*
         * Author: DucBM
         * Method: Save order to DB, used for MAC Chef
         * Params: str - JSON created by AngularJS module, brandId
         * Return: a JSON, success or not
         */
        [HttpPost]
        public async Task<ActionResult> SaveOrderToDatabase(string str, int brandId)//param: brandId
        {
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                var aspNetUserApi = new AspNetUserApi();
                var userLoggin = aspNetUserApi.GetUserByUsername(currentUser);
                var productApi = new ProductApi();
                var storeId = getLastStore(brandId);
                OrderOnlineViewModel order = JsonConvert.DeserializeObject<OrderOnlineViewModel>(str);
                var orderApi = new OrderApi();
                if (order == null)
                {
                    return Json(new
                    {
                        success = false,
                    });
                }
                else
                {
                    OrderCustomEntityViewModel rent = new OrderCustomEntityViewModel();

                    rent.Order = new OrderViewModel();
                    rent.Order.CustomerID = userLoggin.CustomerId;
                    rent.Order.CheckInDate = DateTime.Now;
                    rent.Order.TotalAmount = 0; //Giá trị khởi tạo: 0
                    rent.Order.Discount = 0; //Giá trị khởi tạo: 0
                    rent.Order.DiscountOrderDetail = 0; //Giá trị khởi tạo: 0
                    var orderDetails = new List<OrderDetailViewModel>();
                    List<ProductWithExtras> listExtra = new List<ProductWithExtras>();
                    foreach (var item in order.ProductList)
                    {
                        ProductViewModel product = new ProductApi().Get(item.ProductId);
                        OrderDetailViewModel orderDetail = new OrderDetailViewModel();
                        orderDetail.ProductID = item.ProductId;
                        orderDetail.Quantity = item.Quantity;
                        orderDetail.TotalAmount = (product.Price - (product.Price * product.DiscountPercent / 100)) * item.Quantity;
                        orderDetail.OrderDate = DateTime.Now;
                        orderDetail.Status = 0; // Tình trạng normal của món hàng từ website xuống POS, (thay đổi giá trị 'Hủy' tại POS)
                        orderDetail.IsAddition = false;
                        orderDetail.UnitPrice = product.Price;
                        orderDetail.Discount = (product.Price - product.PrimaryPrice) * item.Quantity;
                        orderDetail.StoreId = CurrentStore.ID;
                        orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
                        orderDetail.TmpDetailId = item.TempId;
                        rent.Order.TotalAmount += orderDetail.TotalAmount;
                        rent.Order.DiscountOrderDetail += orderDetail.Discount;
                        orderDetail.ProductType = productApi.Get(item.ProductId).ProductType;
                        orderDetail.OrderDetailAtt1 = item.Color;
                        orderDetail.OrderDetailAtt2 = item.Size;
                        orderDetails.Add(orderDetail);
                        if (item.ProductWithExtras != null)
                        {
                            foreach (var productExtra in item.ProductWithExtras)
                            {
                                listExtra.Add(productExtra);
                            }
                        }
                    }
                    rent.Order.InvoiceID = "UniChef-1-" + HmsService.Models.Utils.GenerateInvoiceCode();// Thêm quy tắc chung : HiệpPB
                    rent.OrderDetails = orderDetails;
                    rent.Order.FinalAmount = rent.Order.TotalAmount - rent.Order.Discount - rent.Order.DiscountOrderDetail + order.Fee;
                    rent.Order.DeliveryStatus = (int)Enums.StatusOrder.New;
                    rent.Order.CheckInPerson = order.CheckInPerson;
                    rent.Order.Notes = order.Note;
                    rent.Order.OrderStatus = (int)OrderStatusEnum.New;
                    rent.Order.OrderType = (int)OrderTypeEnum.Delivery;
                    rent.Order.StoreID = storeId;//storeId của cửa hàng khách hàng chọn đơn hàng đến. (đơn hàng xuống máy POS)
                    rent.Order.RentType = (int)RentTypeEnum.Default;

                    rent.Order.DeliveryPhone = order.Phone;
                    rent.Order.DeliveryType = order.DeliveryType;
                    rent.Order.DeliveryPayment = order.DeliveryPayment;
                    rent.Order.DeliveryAddress = order.DeliveryAddress;
                    rent.Order.DeliveryPhone = order.Phone;
                    rent.Order.DeliveryReceiver = order.CustomerName;
                    rent.Order.InvoiceStatus = order.VATInvoice;
                    rent.Order.Att1 = string.Format("{0}", order.Fee);
                    rent.Order.IsFixedPrice = false;
                    rent.Order.SourceType = 2;
                    orderApi.CreateOrderDelivery(rent);
                }
                return Json(new
                {
                    success = true
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false
                });
            }
        }

        public ActionResult ShowCart()
        {
            var modal = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
            return PartialView("_ShowCart", modal);
        }


        #region SendEmail
        public void SendEmailAsync(string email, string title, string body)
        {
            MailMessage m = new MailMessage(
                new MailAddress(ConfigurationManager.AppSettings["Email"], "UniChef"),
                new MailAddress(email));
            m.Subject = HtmlToPlainText(title);
            m.Body = body;
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpClient"], Int32.Parse(ConfigurationManager.AppSettings["SmtpClientPort"]));
            smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["PassEmail"]);
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.Send(m);
            smtp.Dispose();
        }
        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html.Trim();
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }

        #endregion SendEmail

        //public ActionResult CreateOrderInfomation(string fullname, string phone, string email, string paymentType, string address, string receiver,
        //    string receiverPhone, string receiverAddress, string note, string returnUrl)
        //{
        //    CartItemsViewModel cart = Session["cart$" + CurrentStore.ID] as CartItemsViewModel;
        //    if (cart != null)
        //    {
        //        CustomerViewModel customer;
        //        customer = new CustomerApi().GetCustomerByEmail(email);
        //        if (customer != null)
        //        {
        //            customer.Name = fullname;
        //            customer.Address = address;
        //            customer.Phone = phone;
        //            new CustomerApi().Edit(customer.CustomerID, customer);
        //        }
        //        else
        //        {
        //            customer = new CustomerViewModel();
        //            customer.Name = fullname;
        //            customer.Email = email;
        //            customer.Address = address;
        //            customer.Phone = phone;
        //            customer.Type = 1;
        //            new CustomerApi().Create(customer);
        //        }
        //        RentDetailsViewModel rent = new RentDetailsViewModel();
        //        rent.Order = new RentViewModel();
        //        rent.Order.CheckInDate = DateTime.Now;
        //        rent.Order.TotalAmount = 0;
        //        rent.Order.Discount = 0; // Chưa xử lý
        //        rent.Order.DiscountOrderDetail = 0;
        //        var orderDetails = new List<OrderDetailViewModel>();
        //        foreach (var item in cart.CartItems)
        //        {
        //            ProductViewModel product = new ProductApi().Get(item.Product.ProductID);
        //            OrderDetailViewModel orderDetail = new OrderDetailViewModel();
        //            orderDetail.ProductID = item.Product.ProductID;
        //            orderDetail.Quantity = item.Quantity;
        //            orderDetail.TotalAmount = product.Price * item.Quantity;
        //            orderDetail.OrderDate = DateTime.Now;
        //            orderDetail.Status = 0;
        //            orderDetail.IsAddition = false;
        //            orderDetail.UnitPrice = product.Price;
        //            orderDetail.Discount = (product.Price - product.PrimaryPrice) * item.Quantity;
        //            orderDetail.StoreId = CurrentStore.ID;
        //            orderDetail.FinalAmount = orderDetail.TotalAmount - orderDetail.Discount;
        //            rent.Order.TotalAmount += orderDetail.TotalAmount;
        //            rent.Order.DiscountOrderDetail += orderDetail.Discount;
        //            orderDetails.Add(orderDetail);

        //        }
        //        rent.OrderDetails = orderDetails;
        //        rent.Order.FinalAmount = rent.Order.TotalAmount - rent.Order.Discount - rent.Order.DiscountOrderDetail;
        //        rent.Order.OrderStatus = 14;
        //        rent.Order.OrderType = 1;
        //        rent.Order.StoreID = CurrentStore.ID;
        //        rent.Order.RentType = 0;
        //        rent.Order.DeliveryAddress = receiverAddress;
        //        rent.Order.DeliveryPhone = receiverPhone;
        //        rent.Order.DeliveryReceiver = receiver;
        //        rent.Order.Att1 = paymentType;
        //        rent.Order.IsFixedPrice = false; // Chưa xử lý
        //        rent.Order.CustomerID = customer.CustomerID;
        //        rent.Order.SourceType = 2;
        //        new RentApi().Create(rent);
        //        //Noty(rent, cart);
        //        Session["cart$" + CurrentStore.ID] = null;
        //    }
        //    return Redirect(returnUrl);
        //}
        #endregion

        #region Payment With Angular
        public JsonResult LoadAllLoadAllDeliveryAddress()
        {
            //return Json();
            return null;
        }
        #endregion


        public JsonResult LoadAddress()
        {
            ProvinceApi provinceApi = new ProvinceApi();
            DistrictApi districtApi = new DistrictApi();
            WardApi wardApi = new WardApi();
            try
            {
                var listProvince = provinceApi.GetAllProvince().OrderBy(q => q.ProvinceName);
                var listDistrict = districtApi.GetAllDistrict().OrderBy(q => q.DistrictName);
                var listWard = wardApi.GetAllWard().OrderBy(q => q.WardName);
                return Json(new { success = true, province = listProvince, district = listDistrict, ward = listWard }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

    }

    public class PromotionWithDetail
    {
        public Promotion Promotion { get; set; }
        public List<PromotionDetail> ListPromotionDetail { get; set; }
    }

}