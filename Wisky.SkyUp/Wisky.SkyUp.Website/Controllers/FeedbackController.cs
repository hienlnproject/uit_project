using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;
using Wisky.SkyUp.Website.Models;
using WiSky.SkyUp.Website.Models;
using Wisky.SkyUp.Website.Models.ViewModels;
using System.IO;
using System.Configuration;
using HmsService.Models.Entities;

namespace Wisky.SkyUp.Website.Controllers
{
    public class FeedbackController : DomainBasedController
    {
        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async System.Threading.Tasks.Task<ActionResult> Submit(WebCustomerFeedbackViewModel model, string returnUrl, string parameter)
        {
            if (ModelState.IsValid)
            {
                model.StoreId = CurrentStore.ID;
                var feedbackApi = new WebCustomerFeedbackApi();

                //GMP 73 74
                if (model.StoreId == 73 || model.StoreId == 74)
                {
                    //PHUONGTA - GMP hiện ngày giờ gửi tin nhắn, tin nhắn hiển thị ở 2 trang admin
                    model.Title = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    model.StoreId = 73;
                    await feedbackApi.CreateAsync(model);
                    model.StoreId = 74;
                    await feedbackApi.CreateAsync(model);
                    return Redirect(returnUrl);
                }

                model.StoreId = this.CurrentStore.ID;
                model.Active = true;

                await feedbackApi.CreateAsync(model);
                returnUrl += "?success=true";
            }
            if (!string.IsNullOrWhiteSpace(parameter))
            {
                returnUrl += parameter;
            }
            return Redirect(returnUrl);
        }
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> SubmitEmail(WebCustomerFeedbackViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.StoreId = CurrentStore.ID;
                var feedbackApi = new WebCustomerFeedbackApi();

                model.StoreId = this.CurrentStore.ID;
                model.Active = true;

                await feedbackApi.CreateAsync(model);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> SubmitNoReload(WebCustomerFeedbackViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.StoreId = CurrentStore.ID;
                var feedbackApi = new WebCustomerFeedbackApi();
                await feedbackApi.CreateAsync(model);
            }
            //if (!string.IsNullOrWhiteSpace(parameter))
            //{
            //    returnUrl += parameter;
            //}
            return Json(new { success = true });
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SendEmail(WebCustomerFeedbackViewModel model, string returnUrl)
        {

            ViewBag.Url = HttpContext.Request.Url.Authority;

            var storeSettingApi = new StoreWebSettingApi();
            var storeSettings = storeSettingApi.GetActiveByStore(CurrentStore.ID);
            var viewModel = new Dictionary<string, string>();
            var sender = new EmailSender(storeSettings.ToArray());
            var mailContent = "";
            switch (CurrentStore.ID)
            {
                case 58://LapTin
                    var path = Utils.ResolveServerUrl(VirtualPathUtility.ToAbsolute("~/ImageUpload/" + CurrentStore.ID + "/" + model.Content), false);
                    sender.Send(model.Email, "Mẫu thiết kế bao bì - Bao bì Lập Tín", "Kính chào quý khách,<br/> Công ty bao bì Lập Tín kính gửi quý khách file thiết kế bao bì <img src=\"" + path + "\">");
                    var laptinContent = string.Format("Kính chào admin,<br/> Có khách hàng đã sử dụng dịch vụ thiết kế bao bì của website: <br/>- Tên: {0}<br/>- Email: {1}<br/>- Số điện thoại: {2}<br/> Mẫu bao bì:<br/><img src=\"{3}\">",
                       model.Fullname, model.Email, model.Phone, path);
                    sender.Send("laptincao@gmail.com", "Mẫu thiết kế bao bì", laptinContent);
                    return Redirect(returnUrl);
                case 62://GAP
                    viewModel.Add("FirstName", Request["FirstName"]);
                    viewModel.Add("LastName", Request["LastName"]);
                    viewModel.Add("School", Request["School"]);
                    viewModel.Add("Graduation", Request["Graduation"]);
                    viewModel.Add("Email", Request["Email"]);
                    viewModel.Add("Company", Request["Company"]);
                    mailContent = RenderRazorViewToString("~/Views/SW.GAP.OnePage/EmailTemplate.cshtml", viewModel);
                    sender.Send("gap.institute@gmail.com", "Thông tin đăng ký học viên", mailContent);
                    return Redirect(returnUrl);
                case 66://TranDat
                    viewModel.Add("Fullname", Request["Fullname"]);
                    viewModel.Add("Email", Request["Email"]);
                    viewModel.Add("Phone", Request["Phone"]);
                    viewModel.Add("Title", Request["Title"]);
                    viewModel.Add("Content", Request["Content"]);
                    viewModel.Add("Website", Request.Url.Host);
                    var allowTitle = "$Tin nhắn khách hàng$Nhận báo giá$Đăng ký tư vấn$";
                    if (!string.IsNullOrEmpty(Request["Title"])
                        && allowTitle.Contains(string.Format(@"${0}$", Request["Title"])))
                    {
                        if (Request["Title"].Equals("Nhận báo giá"))
                        {
                            int categoryId;
                            if (!int.TryParse(Request["CategoryId"], out categoryId))
                            {
                                return HttpNotFound();
                            }
                            var api = new ProductCategoryApi();
                            var category = api.Get(categoryId);
                            if (category == null)
                            {
                                return HttpNotFound();
                            }
                            viewModel.Add("CategoryName", category.CateName);
                        }
                        mailContent = RenderRazorViewToString("~/Views/SW.TranDat/EmailTemplate.cshtml", viewModel);
                        var receiver = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận yêu cầu báo giá")?.Value) ?? "info@trandat.com";
                        sender.Send(receiver, "[" + Request.Url.Host + "] " + Request["Title"], mailContent);
                    }
                    return Redirect(returnUrl);
                case 71://THT
                case 72://THT
                    model.StoreId = CurrentStore.ID;
                    var feedbackApi = new WebCustomerFeedbackApi();

                    // create feedback for vi store
                    model.StoreId = 71;
                    feedbackApi.Create(model);

                    // create feedback for en store
                    model.StoreId = 72;
                    feedbackApi.Create(model);

                    viewModel.Add("Fullname", Request["Fullname"]);
                    viewModel.Add("Email", Request["Email"]);
                    viewModel.Add("Phone", Request["Phone"]);
                    viewModel.Add("Content", Request["Content"]);
                    mailContent = RenderRazorViewToString("~/Views/SW.ThaiHungThinh/EmailTemplate.cshtml", viewModel);
                    sender.Send("info@thaihungthinh.com.vn", "Tin nhắn khách hàng", mailContent);
                    return Redirect(returnUrl);
                case 93://THT
                case 95://THT
                    model.StoreId = CurrentStore.ID;
                    var sendEmail = new WebCustomerFeedbackApi();

                    // create feedback for vi store
                    model.StoreId = 93;
                    sendEmail.Create(model);

                    // create feedback for en store
                    model.StoreId = 95;
                    sendEmail.Create(model);

                    viewModel.Add("Fullname", Request["Fullname"]);
                    viewModel.Add("Email", Request["Email"]);
                    viewModel.Add("Phone", Request["Phone"]);
                    viewModel.Add("Content", Request["Content"]);
                    mailContent = RenderRazorViewToString("~/Views/SW.ThanhNguyen/EmailTemplate.cshtml", viewModel);
                    sender.Send(storeSettings.First(q => q.Name == "EmailUsername").Value, "Tin nhắn khách hàng", mailContent);
                    return Redirect(returnUrl);
                //Wisky
                case 98:
                    model.StoreId = CurrentStore.ID;
                    var sendEmailWisky = new WebCustomerFeedbackApi();

                    // create feedback CustomerFeedack in Admin
                    model.StoreId = 98;
                    sendEmailWisky.Create(model);

                    //Get infor to send email
                    viewModel.Add("name", Request["name"]);
                    viewModel.Add("phone", Request["phone"]);
                    viewModel.Add("email", Request["email"]);
                    viewModel.Add("content", Request["content"]);
                    mailContent = RenderRazorViewToString("~/Views/SW.Wisky/EmailTemplate.cshtml", viewModel);
                    sender.Send(storeSettings.First(q => q.Name == "EmailUsername").Value, "Đăng kí sử dụng dịch vụ Wisky", mailContent);
                    return Json(new { success = true });
                //unispace
                case 9:
                    model.StoreId = CurrentStore.ID;
                    var sendMailUniSpace = new WebCustomerFeedbackApi();

                    model.StoreId = 9;
                    sendMailUniSpace.Create(model);
                    viewModel.Add("Fullname", Request["Fullname"]);
                    viewModel.Add("Email", Request["Email"]);
                    viewModel.Add("UserSubject", Request["UserSubject"]);
                    viewModel.Add("Content", Request["Content"]);
                    mailContent = RenderRazorViewToString("~/Views/SW.UniSpace/EmailTemplate.cshtml", viewModel);
                    sender.Send(storeSettings.First(q => q.Name == "EmailUsername").Value, "Liên hệ", mailContent);
                    return Redirect(returnUrl);
                case 73://GMP
                case 126://Yoga
                    viewModel.Add("name", Request["Fullname"]);
                    viewModel.Add("subject", Request["Title"]);
                    viewModel.Add("email", Request["email"]);
                    viewModel.Add("message", Request["content"]);
                    return Redirect(returnUrl);
                case 74://GMP
                    var cart = Session["cart$" + CurrentStore.ID];
                    if (cart != null)
                    {
                        ViewBag.Cart = cart;
                        viewModel.Add("Fullname", Request["Fullname"]);
                        viewModel.Add("Phone", Request["Phone"]);
                        viewModel.Add("Email", Request["Email"]);
                        viewModel.Add("PaymentType", Request["PaymentType"]);
                        viewModel.Add("Address", Request["Address"]);
                        viewModel.Add("Receiver", Request["Receiver"]);
                        viewModel.Add("ReceiverPhone", Request["ReceiverPhone"]);
                        viewModel.Add("ReceiverAddress", Request["ReceiverAddress"]);
                        viewModel.Add("Note", Request["Content"]);
                        mailContent = RenderRazorViewToString("~/Views/SW.GMP/EmailTemplate.cshtml", viewModel);
                        var receiver = (storeSettings.FirstOrDefault(a => a.Name == "Email nhận đơn đặt hàng")?.Value) ?? "gmp@mailinator.com";
                        sender.Send(receiver, "Đơn đặt hàng mới", mailContent);
                        Session["cart$" + CurrentStore.ID] = null;
                        return Redirect(returnUrl);
                    }

                    break;
            }
            return HttpNotFound();
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SendBookingMail(UserBookingInfo model, string returnUrl)
        {
            var storeSettingApi = new StoreWebSettingApi();
            var storeSettings = storeSettingApi.GetActiveByStore(CurrentStore.ID);
            var sender = new EmailSender(storeSettings.ToArray());
            var kimlongContent = string.Format("Kính chào admin,<br/> Có khách hàng đã đặt phòng qua website: "
                + "<br/>- Tên: {0}<br/>- Số điện thoại: {1}<br/>- Email: {0}<br/>- Phòng: {0}<br/>- Ngày: {0}<br/>",
                    model.Fullname, model.Phone, model.Email, model.Category, model.Datetime);
            sender.Send(model.Email, "Đặt phòng", kimlongContent);
            return Redirect(returnUrl);
        }

        [HttpPost]
        public JsonResult SendEmailFeedBack(string contentFeedBack, int rate, string userName, string proId)
        {
            try
            {
                var sender = ConfigurationManager.AppSettings["emailSend"];
                //download file
                //studentName = System.Web.HttpContext.Current.User.Identity.Name;
                /* var receivers = new List<String>();
                 receivers.Add(ConfigurationManager.AppSettings["emailDesstination"]);
                 var CCs = new List<String>();
                 var BCCs = new List<String>();
                 var subject = "CSKH_ĐÓNG GÓP Ý KIẾN";
                 //var name = sender.Substring(0, sender.IndexOf('@') - 1);
                 var body = "ĐÓNG GÓP TỪ KH: " + customerName + "<br />"
                     + "Email: " + customerEmail + "<br />"
                     + "Nội dung: " + "<br />"
                     + contentFeedBack
                     + "<br /><br />" + "Kính gửi,<br />CSKH_MacChef";
                 HmsService.Models.Utils.SendEmail(body, sender, receivers.ToArray(), CCs.ToArray(), BCCs.ToArray(), subject, true);*/

                var ratingApi = new RatingProductApi();
                var userApi = new AspNetUserApi();
                var productApi = new ProductApi();
                var currentUser = userApi.GetActive().Where(q => q.UserName == userName).FirstOrDefault();
                //var checkRating = ratingApi.GetActive().Where(q => q.UserId == currentUser.Id && q.ProductId == int.Parse(proId)).FirstOrDefault();
                var checkRating = ratingApi.GetRatingProductByUserAndProductId(currentUser.Id, int.Parse(proId));
                var product = productApi.GetProductEntityById(int.Parse(proId));
                if (currentUser != null)
                {
                    var model = new RatingProduct();
                    try
                    {
                        if (checkRating != null)
                        {                            
                            if (!string.IsNullOrWhiteSpace(contentFeedBack))
                            {
                                //model.ReviewContent = contentFeedBack;
                                checkRating.ReviewContent = contentFeedBack;
                            }
                            checkRating.CreateTime = DateTime.Now;
                            checkRating.Active = false;
                            checkRating.Product.RatingTotal = (checkRating.Product.RatingTotal != null && checkRating.Product.RatingTotal != 0) ? checkRating.Product.RatingTotal - checkRating.Star + rate : rate;
                            if (checkRating.Product.NumOfUserVoted == null && checkRating.Product.NumOfUserVoted == 0)
                            {
                                checkRating.Product.NumOfUserVoted = 1;
                            }
                            checkRating.Star = rate;
                            ratingApi.EditRating(checkRating);
                            //productApi.EditProductEntity(product);
                            return Json(new { success = true, message = "Thành công" }, JsonRequestBehavior.AllowGet);

                        }
                        else
                        {
                            model.UserId = currentUser.Id;
                            model.ProductId = int.Parse(proId);
                            model.Star = rate;
                            model.ReviewContent = contentFeedBack;
                            model.Active = false;
                            model.CreateTime = DateTime.Now;
                            if (product.RatingTotal == null)
                            {
                                product.RatingTotal = 0;
                            }
                            if (product.NumOfUserVoted == null)
                            {
                                product.NumOfUserVoted = 0;
                            }
                            product.RatingTotal = product.RatingTotal + model.Star;
                            product.NumOfUserVoted = product.NumOfUserVoted + 1;
                            productApi.EditProductEntity(product);
                            ratingApi.BaseService.Create(model);

                            model.Active = false;
                            ratingApi.BaseService.Update(model);
                            return Json(new { success = true, logged = true, message = "Thành công" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, logged = true, message = "Hệ thống nhận đóng góp ý kiến đang bảo trì. Xin lỗi quý khách" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, logged = false, message = "Hệ thống nhận đóng góp ý kiến đang bảo trì. Xin lỗi quý khách" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, logged = false, message = "Hệ thống nhận đóng góp ý kiến đang bảo trì. Xin lỗi quý khách" }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}