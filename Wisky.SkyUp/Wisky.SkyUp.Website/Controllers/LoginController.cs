﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models;
using HmsService.Sdk;
using HmsService.Models.Entities;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using HmsService.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text.RegularExpressions;

namespace Wisky.SkyUp.Website.Controllers
{
    [Authorize]
    public class LoginController : DomainBasedController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private UserManager<ApplicationUser> userManager;
        // GET: Login
        public LoginController()
        {
        }
        public LoginController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }



        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //GET: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(string userName, string password, bool rememberme)
        {
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                //TempData["returnUrl"] = returnUrl;
                //return RedirectToAction("LoginToSystem");
                var user = UserManager.Find(userName, password);
                var signManager = this.SignInManager;
                if (user != null)
                {
                    if (user.EmailConfirmed == false)
                    {
                        return PartialView("NotConfirmEmail");
                    }
                    SignInStatus result = new SignInStatus();
                    if (rememberme)
                    {
                        result = signManager.PasswordSignIn(userName, password, false, shouldLockout: true);
                    }
                    else
                    {
                        result = signManager.PasswordSignIn(userName, password, false, shouldLockout: false);
                    }
                    //var storeUser = storeUserApi.GetAllStoreUser();

                    if (result == SignInStatus.Success)
                    {
                        if (user.Roles.Any(q => q.RoleId == "13"))//Customer
                        {
                            Session["Username"] = user.UserName;
                            Session["UserId"] = user.Id;
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var user = System.Web.HttpContext.Current.User;
                if (user.IsInRole("Customer"))
                {
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterAccount(string customername, string username, string email, string password, int brandId)
        {
            try
            {
                if (password.Trim().Equals(password.Trim()))
                {
                    if (this.UserManager.FindByName(username.ToLower().Trim()) != null)
                    {
                        return Json(new
                        {
                            message = "Người dùng này đã tồn tại",
                            success = false
                        });
                    }

                    if (this.UserManager.FindByEmail(email.ToLower().Trim()) != null)
                    {
                        return Json(new
                        {
                            message = "Địa chỉ email đã tồn tại",
                            success = false
                        });
                    }


                    var user = new ApplicationUser() { UserName = username.ToLower().Trim(), Email = email.ToLower().Trim(), FullName = customername, BrandId = brandId };
                    var customer = new Customer();
                    customer.BrandId = user.BrandId;
                    customer.Name = user.FullName;
                    customer.Email = user.Email;
                    customer.Gender = true; //dafault is male

                    var customerApi = new CustomerApi();
                    customerApi.BaseService.Create(customer);

                    var result = this.UserManager.Create(user, password.Trim());
                    if (!result.Succeeded)
                    {

                        return Json(new
                        {
                            message = "Tạo người dùng thất bại, vui lòng liên hệ admin!",
                            success = false
                        });
                    }

                    var rs1 = UserManager.AddToRole(user.Id, "Customer");
                    var rs2 = UserManager.AddToRole(user.Id, "ActiveUser");
                    if (!rs1.Succeeded || !rs2.Succeeded)
                    {
                        return Json(new
                        {
                            message = rs1.Errors.ToString() + rs2.Errors.ToString(),
                            success = false
                        });
                    }

                    var aspNetUserApi = new AspNetUserApi();
                    var userAccount = aspNetUserApi.GetUserByUsername(user.UserName);
                    userAccount.CustomerId = customer.CustomerID;
                    aspNetUserApi.BaseService.Update(userAccount);
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Login", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    var callbackUrlContent = "<a href='" + callbackUrl + "'>BẤM VÀO ĐÂY</a>";
                    var emailTitle = "Xác nhận tài khoản UniChef";
                    var emailContent = "Vui lòng bấm vào <a href=\"" + callbackUrl + "\">đây</a> để xác nhận email.";
                    try
                    {
                        var webpageApi = new WebPageApi();
                        var emailTemplate = webpageApi.GetByStoreAndTitle(this.CurrentStore.ID, "Email confirm");
                        var hotline = new StoreWebSettingApi().BaseService.GetActiveByStoreAndName(this.CurrentStore.ID, "Phone").Value;
                        if (emailTemplate != null)
                        {
                            emailTitle = emailTemplate.PageTitle;
                            emailContent = emailTemplate.PageContent;
                            emailContent = emailContent.Replace("@username", user.UserName);
                            emailContent = emailContent.Replace("@hotline", hotline);
                            emailContent = emailContent.Replace("@callbackUrl", callbackUrlContent);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                    
                    await SendEmailAsync(user.Email, emailTitle, emailContent);
                    return Json(new
                    {
                        message = "Tạo người dùng thành công",
                        success = true
                    });

                }
                else
                {
                    return Json(new
                    {
                        message = "Mật khẩu không trùng khớp.",
                        success = false
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    message = "Tạo người dùng thất bại, vui lòng liên hệ admin!",
                    success = false
                });
            }
        }

        public ActionResult CheckCookie()
        {
            string username = "";
            var signManager = this.SignInManager;
            var userCookieName = CookieHelper.CookieName;
            var checkCookie = CookieHelper.CookieExists(userCookieName);
            if (checkCookie)
            {
                username = CookieHelper.GetCookieValue(userCookieName);
                var user = UserManager.FindByName(username);
                // has cookie => login and redirect To returnUrl
                // else create account and set cookie => redirect To returnUrl
                if (user != null)
                {
                    signManager.SignIn(user, false, false);
                    Session["Username"] = user.UserName;
                    Session["UserId"] = user.Id;
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: /Account/Login
        [AllowAnonymous]
        private bool LoginToSystem(string username, string password)
        {
            //string returnUrl = (string)TempData["returnUrl"];
            var signManager = this.SignInManager;
            var userCookieName = CookieHelper.CookieName;

            #region Check Cookie
            // check user cookie
            var checkCookie = CookieHelper.CookieExists(userCookieName);
            if (checkCookie)
            {
                username = CookieHelper.GetCookieValue(userCookieName);
                // has cookie => login and redirect To returnUrl
                // else create account and set cookie => redirect To returnUrl
                if (!username.Equals(""))
                {
                    // Login
                    // redirect
                }
                else
                {
                    // gen username
                    username = CookieHelper.GenUserId();
                    // create Account
                    var check = this.CreateAccount(username, "", "");
                    if (check)
                    {
                        // Add cookie
                        CookieHelper.CreateCookie(userCookieName, username, 365);
                    }
                    // Login
                    // redirect
                }
            }
            else
            {
                // gen username
                username = CookieHelper.GenUserId();
                // create Account
                var check = this.CreateAccount(username, "", "");
                if (check)
                {
                    // Add cookie
                    CookieHelper.CreateCookie(userCookieName, username, 365);
                }
                // Login
                // redirect
            }
            #endregion
            // Login
            // redirect
            var user = UserManager.FindByName(username);
            if (user != null)
            {

                signManager.SignIn(user, false, false);
                Session["Username"] = user.UserName;
                Session["UserId"] = user.Id;

                if (user.Roles.Any(q => q.RoleId == "1"))//Student
                {
                    //if (returnUrl == null)
                    //{
                    //    return true;
                    //}
                    return true;
                }

                // Lỗi
                if (!user.Roles.Any(q => q.RoleId == "1"))
                {
                    return false;
                }
                // Lỗi
                return false;
            }
            else
            {
                // Lỗi
                return false;
            }
        }


        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(string username, string password, string confirmPass)
        {
            try
            {
                var user = new ApplicationUser() { UserName = username.ToLower().Trim(), Email = username.ToLower().Trim() };
                if (password.Trim().Equals(confirmPass.Trim()))
                {


                    var result = await this.UserManager.CreateAsync(user, password.Trim());
                    if (!result.Succeeded)
                    {
                        if (await this.UserManager.FindByNameAsync(username.ToLower().Trim()) != null)
                        {
                            return Json(new
                            {
                                message = "Người dùng này đã tồn tại",
                                success = false
                            });
                        }
                        else
                        {
                            return Json(new
                            {
                                message = "Tạo người dùng thất bại, vui lòng liên hệ admin!",
                                success = false
                            });
                        }
                    }

                    var rs1 = await UserManager.AddToRoleAsync(user.Id, "Customer");
                    var rs2 = await UserManager.AddToRoleAsync(user.Id, "ActiveUser");
                    if (!rs1.Succeeded || !rs2.Succeeded)
                    {
                        return Json(new
                        {
                            message = rs1.Errors.ToString() + rs2.Errors.ToString(),
                            success = false
                        });
                    }
                    return Json(new
                    {
                        message = "Tạo người dùng thành công",
                        success = true
                    });
                }
                else
                {
                    return Json(new
                    {
                        message = "Mật khẩu không trùng khớp.",
                        success = false
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    message = "Tạo người dùng thất bại, vui lòng liên hệ admin!",
                    success = false
                });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ExistedEmailCheck(string username)
        {
            try
            {
                var aspNetUserApi = new AspNetUserApi();
                var existedUser = aspNetUserApi.ExistedEmailCheck(username);
                // return result
                if (existedUser)
                {
                    var user = aspNetUserApi.BaseService.Get(q => q.UserName.Equals(username)).FirstOrDefault();
                    if (user.PasswordHash == "" || user.PasswordHash == null)
                    {
                        return Json(new
                        {
                            existed = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            existed = true
                        }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    var userManager = this.UserManager;
                    var result = UserManager.Create(new ApplicationUser()
                    {
                        UserName = username,
                        Email = username,
                        PasswordHash = ""
                    });

                    var user = userManager.FindByName(username);
                    if (user.Roles.Count() <= 0)
                    {
                        UserManager.AddToRole(user.Id, "Customer");
                        UserManager.AddToRole(user.Id, "ActiveUser");
                    }
                    {
                        return Json(new
                        {
                            existed = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                // lỗi
                return Json(new
                {
                    existed = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }



        public ActionResult LogOff()
        {
            Session.RemoveAll();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            var userCookieName = CookieHelper.CookieName;
            CookieHelper.DeleteCookie(userCookieName);
            HttpCookie languageCookie = Request.Cookies["language"];
            if (languageCookie != null)
            {
                languageCookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(languageCookie);
                CookieHelper.DeleteCookie("language");
            }
            //return this.RedirectToAction("Index", "Home", new { parameters = this.CurrentPageDomain.Directory });
            return Redirect("~/");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        [AllowAnonymous]
        private bool CreateAccount(string username, string password, string confirmPass)
        {
            try
            {
                var user = new ApplicationUser() { UserName = username.ToLower().Trim(), Email = username + "wisky.vn@wisky.vn", PasswordHash = "" };
                if (password.Trim().Equals(confirmPass.Trim()))
                {
                    var result = this.UserManager.Create(user);
                    if (!result.Succeeded)
                    {
                        var a = result.Errors;
                        return false;
                    }

                    var rs = UserManager.AddToRole(user.Id, "Student");
                    if (!rs.Succeeded)
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public ActionResult UpdateInformation(string username, string email, string newPassword, string oldPassword)
        {
            if (oldPassword.Trim().Equals(""))
            {

            }
            return Json(new
            {
                existed = false
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowInformation(string username)
        {

            var aspNetUserApi = new AspNetUserApi();
            var user = aspNetUserApi.BaseService.Get(q => q.UserName.Equals(username)).FirstOrDefault();
            var gender = user.Customer.Gender.GetValueOrDefault();
            var birthDay = user.Customer.BirthDay != null ? user.Customer.BirthDay.Value.ToString("dd/MM/yyyy") : null;
            return Json(new
            {
                fullname = user.FullName,
                email = user.Email,
                gender = gender,
                birthDay = birthDay,
                success = true,
            }, JsonRequestBehavior.AllowGet);
        }


        #region Login External

        // POST: /Login/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Login", new { ReturnUrl = returnUrl }));
        }
        // GET: /Login/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return Redirect("/dang-nhap");
            }
            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return PartialView("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }
        //
        // GET: /Login/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Login/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Login/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Mã không hợp lệ.");
                    return View(model);
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                var brandId = this.CurrentStore.BrandId;
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Username, Email = model.Email, BrandId = brandId, FullName = info.ExternalIdentity.Name };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        var customer = new Customer();
                        customer.BrandId = user.BrandId;
                        customer.Name = user.FullName;
                        customer.Email = user.Email;
                        customer.Gender = true; // default is male

                        var customerApi = new CustomerApi();
                        customerApi.BaseService.Create(customer);
                        if (customer.CustomerID != 0)
                        {
                            var rs1 = UserManager.AddToRole(user.Id, "Customer");
                            var rs2 = UserManager.AddToRole(user.Id, "ActiveUser");
                            if (rs1.Succeeded && rs2.Succeeded)
                            {
                                var aspNetUserApi = new AspNetUserApi();
                                var userAccount = aspNetUserApi.GetUserByUsername(user.UserName);
                                userAccount.CustomerId = customer.CustomerID;
                                aspNetUserApi.BaseService.Update(userAccount);
                                if (userAccount.CustomerId != 0)
                                {
                                    return RedirectToLocal(returnUrl);
                                }
                                else
                                {
                                    customerApi.Delete(customer.CustomerID);
                                    UserManager.Delete(user);
                                }
                            }
                            else
                            {
                                customerApi.Delete(customer.CustomerID);
                                UserManager.Delete(user);
                                AddErrors(rs1);
                                AddErrors(rs2);
                            }
                        }
                        else
                        {
                            UserManager.Delete(user);
                        }
                    }
                    else
                    {
                        AddErrors(result);
                        UserManager.Delete(user);
                    }
                }
                else
                {
                    AddErrors(result);
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResendConfirmEmail(string email)
        {
            var result = await UserManager.FindByEmailAsync(email);
            if (result != null && result.EmailConfirmed == false)
            {
                //Send an email with this link
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(result.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Login", new { userId = result.Id, code = code }, protocol: Request.Url.Scheme);
                var callbackUrlContent = "<a href='" + callbackUrl + "'>BẤM VÀO ĐÂY</a>";
                var emailTitle = "Xác nhận tài khoản UniChef";
                var emailContent = "Vui lòng bấm vào <a href=\"" + callbackUrl + "\">đây</a> để xác nhận email.";
                try
                {
                    var webpageApi = new WebPageApi();
                    var emailTemplate = webpageApi.GetByStoreAndTitle(this.CurrentStore.ID, "Email confirm");
                    var hotline = new StoreWebSettingApi().BaseService.GetActiveByStoreAndName(this.CurrentStore.ID, "Phone").Value;
                    if (emailTemplate != null)
                    {
                        emailTitle = emailTemplate.PageTitle;
                        emailContent = emailTemplate.PageContent;
                        emailContent = emailContent.Replace("@username", result.UserName);
                        emailContent = emailContent.Replace("@hotline", hotline);
                        emailContent = emailContent.Replace("@callbackUrl", callbackUrlContent);
                    }
                }
                catch (Exception e)
                {

                }
                
                await SendEmailAsync(result.Email, emailTitle, emailContent);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            // If we got this far, something failed, redisplay form
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user != null && user.EmailConfirmed == true)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Login", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await SendEmailAsync(user.Email, "Đặt lại mật khẩu", "Vui lòng đặt lại mật khẩu của bạn bằng cách nhấp vào <a href=\"" + callbackUrl + "\">đây</a>");
                    //return RedirectToAction("ForgotPasswordConfirmation", "Login");
                    return View("ForgotPasswordConfirmation");
                }
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                Session.RemoveAll();
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                var userCookieName = CookieHelper.CookieName;
                CookieHelper.DeleteCookie(userCookieName);
                return RedirectToAction("ResetPasswordConfirmation", "Login");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                Session.RemoveAll();
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                var userCookieName = CookieHelper.CookieName;
                CookieHelper.DeleteCookie(userCookieName);
                return RedirectToAction("ResetPasswordConfirmation", "Login");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }
        //
        // GET: /Login/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }
        public async Task SendEmailAsync(string email, string title, string body)
        {
            MailMessage m = new MailMessage(
                new MailAddress(ConfigurationManager.AppSettings["Email"], "UniChef"),
                new MailAddress(email));
            m.Subject = HtmlToPlainText(title);
            m.Body = body;
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpClient"], Int32.Parse(ConfigurationManager.AppSettings["SmtpClientPort"]));
            smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["PassEmail"]);
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            await smtp.SendMailAsync(m);
        }
        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html.Trim();
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
        #endregion

        public ActionResult LoadOrder(string username)
        {
            var orderApi = new OrderApi();
            var order = orderApi.BaseService.GetOrderByUserName(username).OrderByDescending(q => q.CheckInDate).ToList();
            var orderList = order.Select(a => new
            {
                orderCode = a.InvoiceID,
                totalAmount = HmsService.Models.Utils.ToMoney(a.FinalAmount),
                totalDiscount = HmsService.Models.Utils.ToMoney(a.Discount + a.DiscountOrderDetail),
                orderStatus = ((OrderStatusEnum)a.OrderStatus).DisplayName(),
                deliveryPayment = ((PaymentTypeEnum)a.DeliveryPayment).DisplayName(),
                checkInDate = a.CheckInDate.Value.ToString("dd/MM/yyyy"),
                orderDetails = a.OrderDetails.Select(q => new
                {
                    product = q.Product.ProductName,
                    orderDate = q.OrderDate,
                    price = HmsService.Models.Utils.ToMoney(q.UnitPrice),
                    quantity = q.Quantity,
                    amount = HmsService.Models.Utils.ToMoney(q.FinalAmount),
                })
            });
            return Json(new { orderList = orderList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProfile(string name, string email, string gender, string birthDay)
        {
            var aspNetUserApi = new AspNetUserApi();
            var currentUserId = this.User.Identity.GetUserId();
            var currentUser = aspNetUserApi.GetUserById(currentUserId);

            try
            {
                if (name.Length != 0)
                {
                    currentUser.FullName = name;
                }
                if (email.Length != 0)
                {
                    currentUser.Email = email;
                }
                if (gender.Length != 0)
                {
                    if (gender == "1")
                    {
                        currentUser.Customer.Gender = true;
                    }
                    else
                    {
                        currentUser.Customer.Gender = false;
                    }
                }
                if (birthDay.Length != 0)
                {
                    currentUser.Customer.BirthDay = HmsService.Models.Utils.ToDateTime(birthDay);
                }
                aspNetUserApi.BaseService.Update(currentUser);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "Đã có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
                throw;
            }

            return Json(new { success = true, message = "Cập nhật thành công" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordModel();
            ViewBag.storeId = "0";
            return View(model);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> ChangePassword(ChangePasswordModel model)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            //UserManager.PasswordHasher = new MP5Hasher(FormsAuthPasswordFormat.MD5);
            var currAccount = AuthenUtils.GetUser(this.TempData, this.User.Identity);
            if (ModelState.IsValid)
            {
                var result = await UserManager.ChangePasswordAsync(currAccount.Id, model.OldPassword, model.NewPassword);

                if (result.Succeeded)
                {
                    //return this.Redirect("/");
                    LogOff();
                    return Json(new { success = true, message = "Thay đổi Password thành công." });
                }
                else
                {
                    //return this.RedirectToAction(this.Url.Action("ChangePassword"));
                    return Json(new { success = false, message = "Password cũ không chính xác. Vui lòng thử lại" });
                }
            }
            //return this.RedirectToAction(this.Url.Action("ChangePassword"));
            return Json(new { success = false, message = "Thay đổi thất bại, xin thử lại." });
        }
    }
}