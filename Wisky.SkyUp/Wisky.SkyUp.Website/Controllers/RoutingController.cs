﻿using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models.Routing;
using Wisky.SkyUp.Website.Models.ViewModels;

namespace Wisky.SkyUp.Website.Controllers
{
    public class RoutingController : DomainBasedController
    {

        public async Task<ActionResult> Index(string parameters, bool? partial)
        {
            HttpCookie languageCookie = Request.Cookies["language"];
            if (languageCookie != null)
            {
                ViewBag.languge = languageCookie.Value;
                languageCookie.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Add(languageCookie);
            }
            else
            {
                ViewBag.languge = "vi";
            }
            var storeId = this.CurrentStore.ID;
            //Create session for countView
            Session["StoreID"] = storeId;
            // Get the matching theme - DucBM
            var storeTheme = new StoreThemeApi().GetActiveStoreThemeByStoreId(storeId)
                .Where(t => t.IsUsing == true).FirstOrDefault();
            var themeFolder = "";
            if (storeTheme != null)
            {
                var themeId = storeTheme.ThemeId;
                var theme = new ThemeApi().GetActiveThemes()
                    .Where(t => t.ThemeId == themeId).FirstOrDefault();
                if (theme != null)
                {
                    themeFolder = theme.ThemeFolderUrl;
                }
            }
            // Get the matching route
            var routes = await new StoreWebRouteApi().GetStoreRoutesAsync(storeId);

            var routeWorker = new RouteWorker(routes, parameters ?? "/", this.Request.QueryString);
            if (routeWorker.ResultRoute == null)
            {
                return this.HttpNotFound();
            }

            // Append the View Models required
            var model = new RoutedPageViewModel()
            {
                RouteInfo = routeWorker.ResultRoute,
                StoreInfo = this.CurrentStore,
            };

            var viewModelResult = await routeWorker.BuildViewModels(model);
            if (!viewModelResult)
            {
                return this.HttpNotFound();
            }

            this.ViewBag.Model = model;
            this.ViewBag.CurrentStore = this.CurrentStore;
            this.ViewBag.CurrentPageDomain = this.CurrentPageDomain;

            //author: LongPH - View count section
            if (Session.IsNewSession)
            {
                // Increase View Count
                var counter = await this.Service<IStoreWebViewCounterService>().GetAndIncreaseAsync(this.CurrentStore.ID);
                model.ViewCounter = new StoreWebViewCounterViewModel(counter);
            }
            else
            {
                var counter = await this.Service<IStoreWebViewCounterService>().GetCounter(this.CurrentStore.ID);
                model.ViewCounter = new StoreWebViewCounterViewModel(counter);
            }

            // Return the appropriate View of the route
            var viewPath = model.ResolveViewPath(themeFolder, routeWorker.ResultRoute.StoreWebRoute.StoreWebRoute.ViewName);
            ViewResultBase result;
            if (partial.GetValueOrDefault())
            {
                result = this.PartialView(viewName: viewPath, model: model);
            }
            else
            {
                var viewResult = this.View(viewName: viewPath, model: model);

                // Set the Layout Path
                var layoutName = routeWorker.ResultRoute.StoreWebRoute.StoreWebRoute.LayoutName;
                if (layoutName == null)
                {
                    layoutName = "_Layout.cshtml";
                }

                if (layoutName == "_")
                {
                    viewResult.MasterName = null;
                }
                else
                {
                    var layoutPath = model.ResolveViewPath(themeFolder, layoutName);
                    viewResult.MasterName = layoutPath;
                }

                result = viewResult;
            }

            return result;
        }
    }
}