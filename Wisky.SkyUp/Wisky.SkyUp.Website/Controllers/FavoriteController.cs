﻿using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Controllers
{
    public class FavoriteController : DomainBasedController
    {
        // GET: Favorite
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAllProductAndIsActive(string userName)
        {
            var api = new FavoritedApi();
            var userApi = new AspNetUserApi();
            var check = userApi.GetUserByUsername(userName);
            var model = new FavoritedViewModel();
            var fav = api.GetActiveFavoriteProduct(check.Id).Select(q => new ProductFavoriteViewModel
            {
                ProductID = q.ProductID,
                ProductImg = q.PicURL,
                DiscountPercent = q.DiscountPercent,
                ProductName = q.ProductName,
                DisplayPrice = HmsService.Models.Utils.ToMoney(q.Price),
                ProductPrice = q.Price,
                ProductCode = q.Code,
                SeoName = q.SeoName,
            }).ToList();


            return Json(new
            {
                success = true,
                result = fav.ToList()
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateSttFavorite(string favoriteStt, string userName, string proId)
        {
            bool rs = false;
            bool loged = false;
            bool stt = false;
            if (favoriteStt.Equals("1"))
            {
                stt = true;
            }
            else
            {
                stt = false;
            }
            var favoritedApi = new FavoritedApi();
            var userApi = new AspNetUserApi();
            var currentUser = userApi.GetActive().Where(q => q.UserName == userName).FirstOrDefault();
            if (currentUser != null)
            {
                loged = true;
                var checkFavoriteStt = favoritedApi.GetActive().Where(q => q.UserID == currentUser.Id && q.ProductID == int.Parse(proId)).FirstOrDefault();
                var model = new FavoritedViewModel();
                try
                {
                    if (checkFavoriteStt != null)
                    {
                        checkFavoriteStt.FavoriteStt = stt;
                        favoritedApi.Edit(checkFavoriteStt.Id, checkFavoriteStt);
                        rs = true;
                    }
                    else
                    {
                        model.UserID = currentUser.Id;
                        model.ProductID = int.Parse(proId);
                        model.FavoriteStt = stt;
                        model.Active = true;
                        favoritedApi.Create(model);
                        rs = true;
                    }
                }
                catch (Exception)
                {
                    rs = false;
                }
            }

            return Json(new { result = rs, loged = loged }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult LoadSttFavorite(string userName, string proId)
        {
            bool rs = false;

            var favoritedApi = new FavoritedApi();
            var userApi = new AspNetUserApi();
            var currentUser = userApi.GetActive().Where(q => q.UserName == userName).FirstOrDefault();
            if (currentUser != null)
            {
                var favorite = favoritedApi.GetActive().Where(q => q.UserID == currentUser.Id && q.ProductID == int.Parse(proId)).FirstOrDefault();
                if (favorite != null)
                {
                    if (favorite.FavoriteStt == true)
                    {
                        rs = true;
                    }
                    else
                    {
                        rs = false;

                    }
                }
                else
                {
                    rs = false;
                }
            }
            else
            {
                rs = false;
            }

            return Json(new { result = rs }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult UpdateRating(int rate, string userName, string proId)
        {
            var ratingApi = new RatingStarApi();
            var userApi = new AspNetUserApi();
            bool rs = false;
            var currentUser = userApi.GetActive().Where(q => q.UserName == userName).FirstOrDefault();
            if (currentUser != null)
            {
                var checkRating = ratingApi.GetActive().Where(q => q.UserID == currentUser.Id && q.ProductID == int.Parse(proId)).FirstOrDefault();
                var model = new RatingStarViewModel();
                try
                {
                    if (checkRating != null)
                    {
                        checkRating.Rate = rate;
                        ratingApi.Edit(checkRating.ID, checkRating);
                        rs = true;
                    }
                    else
                    {
                        model.UserID = currentUser.Id;
                        model.ProductID = int.Parse(proId);
                        model.Rate = rate;
                        ratingApi.Create(model);
                        rs = true;
                    }
                }
                catch (Exception)
                {
                    rs = false;
                }
            }

            return Json(new { result = rs }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult LoadCmt(string proId)
        {
            bool rs = false;
            var ratingProduct = new RatingProductApi();
            var favoritedApi = new FavoritedApi();
            var userApi = new AspNetUserApi();
            /*var currentUser = userApi.GetActive().Where(q => q.UserName == userName).FirstOrDefault();
            var favorite = favoritedApi.GetActive().Where(q=> q.ProductID == int.Parse(proId)).FirstOrDefault();*/
            var listCmt = from ratingproductTable in ratingProduct.Get()
                          join userTable in userApi.GetActive()
                          on ratingproductTable.UserId equals userTable.Id
                          where (ratingproductTable.ProductId == int.Parse(proId) && ratingproductTable.Active==true)
                          select new { UserName = userTable.UserName, CreateTime = ratingproductTable.CreateTime.ToString("dd/MM/yyyy hh:mm"), Star = ratingproductTable.Star, ReviewContent = ratingproductTable.ReviewContent };
            return Json(new { result = listCmt }, JsonRequestBehavior.AllowGet);

        }
    }
}