﻿using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Controllers
{
    public class BlogsController : DomainBasedController
    {
        public ActionResult ShowSingleBlog(string blogURL)
        {
            if (blogURL == null)
            {
                return this.HttpNotFound();
            }
            else
            {
                int storeId = this.CurrentStore.ID;
                BlogCategoryViewModel model = new BlogCategoryApi().GetBlogCategory(storeId, blogURL, true);
                if (model != null)
                {
                    return View(model);
                }
                else
                {
                    return this.HttpNotFound();
                }
            }
        }
    }
}