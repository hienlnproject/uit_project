﻿using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Controllers
{
    public class CountController : DomainBasedController
    {
        // GET: Count
        public ActionResult Index()
        {
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> GetNumberOfViewBySeoName(string seoName)
        {
            var blogPostApi = new BlogPostApi();
            var blogPost = await blogPostApi.BaseService.GetBlogPostBySeoNameAsync(seoName, CurrentStore.ID);
            var numOfView = 0;
            if (blogPost != null)
            {
                numOfView = blogPost.TotalVisit.GetValueOrDefault() + 1;
                blogPost.TotalVisit = numOfView;
                blogPostApi.BaseService.Update(blogPost);
                blogPostApi.BaseService.Save();
            }
            return Json(new { number = numOfView }, JsonRequestBehavior.AllowGet);
        }

        public async System.Threading.Tasks.Task<ActionResult> GetNumberOfViewById(int id)
        {
            var blogPostApi = new BlogPostApi();
            var blogPost = await blogPostApi.BaseService.GetActiveByStoreAsync(id, CurrentStore.ID);
            var numOfView = 0;
            if (blogPost!=null)
            {
                numOfView = blogPost.TotalVisit.GetValueOrDefault() + 1;
                blogPost.TotalVisit = numOfView;
                blogPostApi.BaseService.Update(blogPost);
                blogPostApi.BaseService.Save();
            }
            return Json(new { number = numOfView }, JsonRequestBehavior.AllowGet);
        }

        public async System.Threading.Tasks.Task<ActionResult> GetNumberOfViewByIdWithStoreId(int id, int storeId)
        {
            var blogPostApi = new BlogPostApi();
            var blogPost = await blogPostApi.BaseService.GetActiveByStoreAsync(id, storeId);
            var numOfView = 0;
            if (blogPost != null)
            {
                numOfView = blogPost.TotalVisit.GetValueOrDefault() + 1;
                blogPost.TotalVisit = numOfView;
                blogPostApi.BaseService.Update(blogPost);
                blogPostApi.BaseService.Save();
            }
            return Json(new { number = numOfView }, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //public JsonResult BaoTest()
        //{
        //    BrandApi bApi = new BrandApi();
        //    bApi.CreateAsync("123456");
        //    return Json(new { ok = "1234" }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public JsonResult PhuTest()
        //{
        //    StoreApi sApi = new StoreApi();
        //    var a = String.Empty;
        //    //sApi.ed
        //    return Json(new { ok = "1234" }, JsonRequestBehavior.AllowGet);
        //}

    }
}