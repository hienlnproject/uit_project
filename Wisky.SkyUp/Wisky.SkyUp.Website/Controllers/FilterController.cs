﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;

namespace Wisky.SkyUp.Website.Controllers
{
    public class FilterController : DomainBasedController
    {
        // GET: Sort
        public ActionResult Index()
        {
            return View();
        }

        public List<String> GeneralFilter()
        {
            var productCategoryApi = new ProductCategoryApi();
            //var listProducts = productCategoryApi.Get
            var result = new List<String>();
            var keys = HttpUtility.ParseQueryString(Request.Url.Query).Keys;
            foreach(var key in keys)
            {
                System.Diagnostics.Debug.WriteLine(key.ToString());
                var value = Request.QueryString[key.ToString()];
                result.Add(value);
            }
            return result;
        }

        private String getProductSpecValue(ProductDetailsViewModel p, String specName)
        {
            var spec = p.ProductSpecifications.FirstOrDefault(a => a.Name.Equals(specName));
            var value = (spec == null) ? "" : spec.Value;
            return value;
        }

        [HttpGet]
        public IEnumerable<ProductDetailsViewModel> GetAllProductsSortedBySpec(string specName, bool desc)
        {
            IEnumerable<ProductDetailsViewModel> result = new List<ProductDetailsViewModel>();
            var listProducts = new ProductApi().GetActiveWithSpecsByStoreId(CurrentStore.ID);
            if (specName != null)
            {
                if (desc == true)
                {
                    result = listProducts.OrderByDescending(a => getProductSpecValue(a, specName));
                } else
                {
                    result = listProducts.OrderBy(a => getProductSpecValue(a, specName));
                }
            }

            return result;
        }

        [HttpGet]
        public List<String> DetachToString(string sortPurpose)
        {
            if (sortPurpose == null)
            {
                return null;
            }
            //var sortPurpose = "Thương hiệu;Tính năng;Chất liệu";
            string[] purposes = sortPurpose.Split(';');
            List<String> result = new List<String>();//Checking
            var listProducts = new ProductApi().GetActiveWithSpecsByStoreId(CurrentStore.ID);
            foreach (var purpose in purposes)
            {
                foreach (var product in listProducts)
                {
                    if (product.ProductSpecifications.Count() != 0)
                    {
                        String option = product.ProductSpecifications
                                    .Where(q => q.Name.ToLower().Equals(purpose.ToLower()))
                                    .FirstOrDefault().Value;
                        if (result.Where(q => q.Equals(option)).FirstOrDefault() == null)
                        {
                            result.Add(option);
                        }
                    }
                }
            }
            return result;
        }

        [HttpGet]
        public List<ProductSpecificationViewModel> Detach(string sortPurpose)//Thương hiệu;Tính năng;Chật liệu
        {
            if (sortPurpose== null)
            {
                return null;
            }
            //var sortPurpose = "Thương hiệu;Tính năng;Chất liệu";
            string[] purposes = sortPurpose.Split(';');
            List<ProductSpecificationViewModel> result = new List<ProductSpecificationViewModel>();//Checking
            var listProducts = new ProductApi().GetActiveWithSpecsByStoreId(CurrentStore.ID);
            foreach (var purpose in purposes)
            {
                foreach (var product in listProducts)
                {
                    if (product.ProductSpecifications.Count() != 0)
                    {
                        var option = product.ProductSpecifications
                                    .Where(q => q.Name.ToLower().Equals(purpose.ToLower()))
                                    .FirstOrDefault();
                        if (result.Where(q => q.Equals(option)).FirstOrDefault() == null)
                        {
                            result.Add(option);
                        }
                    }
                }
            }
            return result;
        }
    }
}