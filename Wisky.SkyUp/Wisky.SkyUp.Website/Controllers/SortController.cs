﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;

namespace Wisky.SkyUp.Website.Controllers
{
    public class SortController : DomainBasedController
    {
        // GET: Sort
        public ActionResult Index()
        {
            return View();
        }
        private List<ProductSpecificationViewModel> Detach(string sortPurpose)//Thương hiệu;Tính năng;Chật liệu
        {
            //var sortPurpose = "Thương hiệu;Tính năng;Chất liệu";
            string[] purposes = sortPurpose.Split(';');
            List<ProductSpecificationViewModel> result = new List<ProductSpecificationViewModel>();//Checking
            var listProducts = new ProductApi().GetActiveWithSpecsByStoreId(CurrentStore.ID);
            foreach (var purpose in purposes)
            {
                foreach (var product in listProducts)
                {
                    if (product.ProductSpecifications.Count() != 0)
                    {
                        var option = product.ProductSpecifications
                                    .Where(q => q.Name.ToLower().Equals(purpose.ToLower()))
                                    .FirstOrDefault();
                        if (result.Where(q => q.Equals(option)).FirstOrDefault() == null)
                        {
                            result.Add(option);
                        }
                    }
                }
            }
            return result;
        }
    }
}