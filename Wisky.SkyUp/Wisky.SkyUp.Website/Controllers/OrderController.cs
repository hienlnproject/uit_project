﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SkyWeb.DatVM.Mvc;
using HmsService.Sdk;
using HmsService.Models;
using WiSky.SkyUp.Website.Models;
using Wisky.SkyUp.Website.Models.ViewModels;

namespace Wisky.SkyUp.Website.Controllers
{
    public class OrderController : BaseController
    {
        //public ActionResult Index()
        //{
        //    return View();
        //}

            //Comment các View Cart, Infomation bên dưới
        public async System.Threading.Tasks.Task<ActionResult> LoadItemByCategory(int cateId, string pattern)
        {
            var productApi = new ProductApi();
            pattern = (pattern ?? "").ToLower();
            var products = await productApi.GetActiveByProductCategoryAndPatternAsync(cateId, pattern);
            return Json(new
            {
                products = products.Select(a => new
                {
                    image = a.PicURL != null ? a.PicURL : "a",
                    name = a.ProductName != null ? a.ProductName : "a",
                    id = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).ProductID : a.ProductID,
                    discount = a.DiscountPercent,
                    price = a.ProductType == (int)ProductTypeEnum.General ? productApi.GetProductGeneral(a.ProductID).Price : a.Price,
                    category = a.CatID,
                    hasGeneral = a.ProductType == (int)ProductTypeEnum.General ? true : false,
                    quantity = 1,
                    note = ""
                }
                )
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExtra(int cateId)
        {
            var productCate = new ProductCategoryApi();
            //Get product Extra
            var productExtra = productCate.GetProductExtra(cateId);

            //Get product Parent of extra
            return Json(new
            {
                productExtra = productExtra.Select(a => new
                {
                    id = a.ProductID,
                    name = a.ProductName,
                    price = a.Price
                })
                //productParent = productParent
            });
        }

        [HttpGet]
        public JsonResult GetGeneral(int productId)
        {
            var productApi = new ProductApi();
            //Get product child of product general
            var productGeneral = productApi.GetAllProductGeneral(productId);

            //Get product general
            return Json(new
            {
                productGeneral = productGeneral.Select(a => new
                {
                    id = a.ProductID,
                    att = a.Att1,
                    price = a.Price,
                    code = a.Code,
                    discount = a.DiscountPrice
                })
            }, JsonRequestBehavior.AllowGet);
        }
        //Get Extra of product child general
        public JsonResult GetExtraOfGeneral(string productId)
        {
            var productApi = new ProductApi();
            var productExtraApi = new ProductExtraApi();
            int proId = Convert.ToInt32(productId);
            //Get product extra of product child general
            var productExtra = productExtraApi.GetProductExtraByProductAsyn(proId);

            //Get product child general
            var productGeneral = productApi.Get(proId);
            return Json(new
            {
                productExtra = productExtra,
                productGeneral = productGeneral
            });
        }
        //Get list order has extra to edit
        public JsonResult UpdateOrderExtra(int orderId)
        {
            var orderDetailApi = new OrderDetailApi();
            var orderDetail = orderDetailApi.OrderMaster(orderId);
            //Get order detail has type is Master
            var orderMaster =
                 orderDetail.OrderDetail1.FirstOrDefault(a => a.ProductOrderType == (int)ProductOrderType.Master);
            //Get order detail has type is Child
            var orderChild = orderDetail.OrderDetail1.Where(a => a.ProductOrderType == (int)ProductOrderType.Child);
            return Json(new
            {
                orderMaster = orderMaster,
                orderChild = orderChild
            });
        }
        //Get list order no extra to edit
        public JsonResult UpdateOrderSingle(int orderId)
        {
            var orderDetailApi = new OrderDetailApi();
            var orderSingle = orderDetailApi.Get(orderId);
            return Json(new
            {
                orderSingle = orderSingle
            }, JsonRequestBehavior.AllowGet);
        }
        //List Category
        public JsonResult LoadCategories()
        {
            var cateApi = new ProductCategoryApi();
            var categories = cateApi.Get().Where(a => a.IsDisplayed && a.IsExtra == false).ToList();
            return Json(new
            {
                categories = categories.Select(a => new { id = a.CateID, name = a.CateName })
            }, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Cart()
        //{
        //    return View();
        //}

        //public ActionResult Information()
        //{
        //    return View();
        //}

    }
}
