using System;
using UrlHelper;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Wisky.SkyUp.Website.Routing.UrlHelperStartup), "Start")]
namespace Wisky.SkyUp.Website.Routing
{
	public static class UrlHelperStartup
	{
		public static void Start()
		{
			UrlManager<AppUrls>.Initialize();
		}
	}
}