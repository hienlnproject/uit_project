﻿using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wisky.SkyUp.Website.Libs
{
    public class myLib
    {
        public static string getSpec(IEnumerable<ProductSpecificationViewModel> spec)
        {
            string result = string.Empty;
            foreach (var item in spec)
            {
                if (!item.Name.ToLower().Equals("nhà sản xuất") &&
                    !item.Name.ToLower().Equals("tình trạng") &&
                    !item.Name.ToLower().Equals("manufacturer") &&
                    !item.Name.ToLower().Equals("status") &&
                    item.Name.ToLower().IndexOf("document") < 0 &&
                    item.Name.ToLower().IndexOf("tài liệu") < 0)
                {
                    result += item.Name+ ":" + item.Value + ";";
                }
            }
            return result;
        }
        public static string getDownload(IEnumerable<ProductSpecificationViewModel> documents)
        {
            string result = string.Empty;
            var name = string.Empty;
            foreach (var item in documents)
            {
                if (item.Name.ToLower().IndexOf("tài liệu") >= 0 || item.Name.ToLower().IndexOf("document") >= 0)
                {
                    //Cắt chuỗi tên tài liệu: Trong admin: Tài liệu: Tài liệu download => Tài liệu download đưa vào result
                    if (item.Name.ToLower().IndexOf("tài liệu") == 0)
                    {
                        name = item.Name.Substring(item.Name.IndexOf(":") + 1, item.Name.Length - item.Name.IndexOf(":") - 1);
                    }
                    if (item.Name.ToLower().IndexOf("document") == 0)
                    {
                        name = item.Name.Substring(item.Name.IndexOf(":") + 1, item.Name.Length - item.Name.IndexOf(":") - 1);
                    }
                    result += name + ":" + item.Value + ";";
                }
            }
            return result;
        }
        public static void genStoreWebRoute(int storeId)
        {
            var entity = new StoreWebRoute();
            var storeWebRouteApi = new StoreWebRouteApi();
            
        }
    }
}
