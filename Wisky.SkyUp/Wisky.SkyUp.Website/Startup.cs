﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wisky.SkyUp.Website.Startup))]
namespace Wisky.SkyUp.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
