﻿using AutoMapper;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels
{
    public class StoreCreateViewModel
    {
        [Required]
        public string StoreName { get; set; }

        [Required]
        public string StoreAddress { get; set; }

        [Required]
        private int StoreId { get; set; }

        [Required]
        public string BrandName { get; set; }

        public StoreCreateViewModel() : base() { }
    }
}
