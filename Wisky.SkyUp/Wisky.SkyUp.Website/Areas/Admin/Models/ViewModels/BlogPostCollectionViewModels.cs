﻿using AutoMapper;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels
{
    //public partial class BlogPostCollectionEditViewModel : BlogPostCollectionDetailsViewModel
    //{
    //    public IEnumerable<SelectListItem> AvailableCollections { get; set; }
    //    public BlogPostCollectionEditViewModel() : base() { }
    //    public BlogPostCollectionEditViewModel(BlogPostCollectionViewModel original, IMapper mapper) : this()
    //    {
    //        mapper.Map(original, this);
    //    }
    //    public BlogPostCollectionEditViewModel(BlogPostCollectionDetailsViewModel original, IMapper mapper) : this()
    //    {
    //        mapper.Map(original, this);
    //        //this.SelectedBlogPostCollections = original.BlogPostCollections.Select(q => q.Id).ToArray();
    //    }
    //}
    public partial class BlogPostCollectionEditViewModel : BlogPostCollectionViewModel
    {
        public IEnumerable<SelectListItem> AvailableBlogCollections { get; set; }
        [Required]
        public override string Name
        {
            get
            {
                return base.Name;
            }

            set
            {
                base.Name = value;
            }
        }

        [Required]
        public override string SeoName
        {
            get
            {
                return base.SeoName;
            }

            set
            {
                base.SeoName = value;
            }
        }

        public BlogPostCollectionEditViewModel() : base() { }

        public BlogPostCollectionEditViewModel(BlogPostCollectionViewModel original, IMapper mapper) : this()
        {
            mapper.Map(original, this);
        }
    }
}
