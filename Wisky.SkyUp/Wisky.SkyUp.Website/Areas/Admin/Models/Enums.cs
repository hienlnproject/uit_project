﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models
{
    public class Enums
    {
        public enum WebSetting
        {
            Title ,
            Slogan ,
            Description ,
            Facebook ,
            Twitter ,
            Youtube  ,
            Zalo ,

        }

        public enum StatusOrder
        {
            New = 1,
            Processing = 2,
            Finish = 3,
            Cancel = 4
        }
    }

    
}