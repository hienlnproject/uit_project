﻿using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels;
using Wisky.SkyUp.Website.Controllers;
using Wisky.SkyUp.Website.Models;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{

    [Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class WebSettingsController : DomainBasedController
    {

        public ActionResult Index()
        {
            var model =  new StoreWebSettingApi()
                .GetActiveByStore(this.CurrentStore.ID);

            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken(), ValidateInput(false)]
        public async Task<ActionResult> Index(StoreWebSettingsEditViewModel model)
        {
            if (model != null)
            {
                var settings = model.Pairs.Select(q => new KeyValuePair<int, string>(q.Id, q.Value));

                try
                {
                    await new StoreWebSettingApi().MassUpdate(settings, this.CurrentStore.ID);
                }
                catch (UnauthorizedAccessException)
                {
                    // This exception is thrown when something is not right:
                    // this store is trying to modify value of other store.
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

            }

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

    }
}