﻿using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Areas.Admin.Models;
using Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels;
using Wisky.SkyUp.Website.Controllers;
using Wisky.SkyUp.Website.Models;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{

    [Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class ProductController : DomainBasedController
    {

        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new ProductApi().GetAdminWithFilterAsync(
                this.CurrentStore.ID, request.searchPhrase,
                request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<ProductViewModel>(result);
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Create()
        {
            var model = new ProductEditViewModel()
            {
                IsAvailable = true,
            };

            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Create(ProductEditViewModel model)
        {
            // Validate category
            var categoryApi = new ProductCategoryApi();

            if (!await categoryApi.ValidateStoreCategory(model.CatID, this.CurrentStore.ID))
            {
                this.ModelState.AddModelError(nameof(model.CatID), "Invalid Category ID");
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }

            var productApi = new ProductApi();
            model.Product.CreateTime = DateTime.Now;
            if(model.Product.Position == null)
            {
                model.Product.Position = 999;
            }
            //Set cứng 1 số field
            model.Product.IsMostOrdered = false;
            model.Product.IsDefaultChildProduct = 0;

            await productApi.CreateAsync(model.Product,
                model.SelectedImages?.ToArray() ?? new string[0],
                model.SelectedProductCollections?.ToArray() ?? new int[0],
                model?.SpecValues ?? new KeyValuePair<string, string>[0]);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(ProductEditViewModel model)
        {
            model.AvailableCategories =
                (await new ProductCategoryApi().GetByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => model.CatID == q.CateID);
            model.AvailableCollections =
                (await new ProductCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = await new ProductApi()
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new ProductEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Edit(ProductEditViewModel model, string[] ImageSpec)
        {
            var api = new ProductApi();
            // Validate
            var info = await api
                .GetByStoreIdAsync(model.ProductID, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }

            model.Active = info.Product.Active;
            model.Product.CreateTime = DateTime.Now;
            if (model.Product.Position == null)
            {
                model.Product.Position = 999;
            }

            //Set cứng 1 số field
            //model.Product.IsDefaultChildProduct = 1;
            //model.Product.IsMostOrdered = true;

            api.EditAsync(model.Product,
                model?.SelectedImages?.ToArray() ?? new string[0],
                model?.SelectedProductCollections?.ToArray() ?? new int[0],
                model?.SpecValues ?? new KeyValuePair<string, string>[0], ImageSpec);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(ProductEditViewModel model)
        {
            model.AvailableCategories =
                (await new ProductCategoryApi().GetByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => model.CatID == q.CateID);
            model.AvailableCollections = (await new ProductCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedProductCollections.Contains(q.Id));
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var productApi = new ProductApi();
            var info = await productApi
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            await productApi.DeactivateAsync(id.Value);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public ActionResult EditSeoName()
        {
            var product = new ProductApi();
            //product.EditSeoName();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        

    }
}