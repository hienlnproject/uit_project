﻿using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels;
using Wisky.SkyUp.Website.Controllers;
using Wisky.SkyUp.Website.Models;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{

    [Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class BlogPostController : DomainBasedController
    {

        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new BlogPostApi().GetAdminWithFilterAsync(
                this.CurrentStore.ID, request.searchPhrase,
                request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<BlogPostDetailsViewModel>(result);
            var jsonResult = this.Json(model, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public async Task<ActionResult> Create()
        {
            var model = new BlogPostEditViewModel()
            {
                BlogPost = new BlogPostViewModel(),
            };

            
            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Create(BlogPostEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }

            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.Active = true;

            var api = new BlogPostApi();
            await api.CreateAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0]);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(BlogPostEditViewModel model)
        {
            model.AvailableCollections = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = await new BlogPostApi()
                .GetDetailsByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new BlogPostEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogPostEditViewModel model)
        {
            var api = new BlogPostApi();
            // Validate
            var info = await api
                .GetByStoreIdAsync(model.BlogPost.Id, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }

            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.CreatedTime = info.CreatedTime;
            model.BlogPost.UpdatedTime = DateTime.Now;
            model.BlogPost.Active = true;
            await api.EditAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0]);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(BlogPostEditViewModel model)
        {
            model.AvailableCollections = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.ID))
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedBlogPostCollections.Contains(q.Id));
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var productApi = new BlogPostApi();
            var info = await productApi
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            await productApi.DeactivateAsync(id.Value);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

    }
}