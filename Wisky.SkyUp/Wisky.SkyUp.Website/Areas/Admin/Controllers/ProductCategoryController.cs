﻿using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HmsService.Models;
using Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels;
using Wisky.SkyUp.Website.Controllers;
using Utils = Wisky.SkyUp.Website.Models.Utils;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{

    [Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class ProductCategoryController : DomainBasedController
    {

        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new ProductCategoryApi().GetAdminWithFilterAsync(
                this.CurrentStore.ID, request.searchPhrase,
                request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<ProductCategoryViewModel>(result);
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Create()
        {
            var model = new ProductCategoryEditViewModel();

            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProductCategoryEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }

            model.StoreId = this.CurrentStore.ID;
            model.ImageFontAwsomeCss = model.IconEnum.ToString();
            model.Type = (int)model.CategoryTypes;
            var productCategoryApi = new ProductCategoryApi();
            await productCategoryApi.CreateAsync(model);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(ProductCategoryEditViewModel model)
        {
            model.AvailableCategories = (await new ProductCategoryApi()
                .GetByStoreIdAsync(this.CurrentStore.BrandId.Value))
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => model.ParentCateId == q.CateID, true, "--Không có--");

            model.CategoryTypes = ProductCategoryType.Economy;
            model.IconEnum = IconCategoryEnum.glass;
            model.Active = true;
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = await new ProductCategoryApi()
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.BrandId.Value);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new ProductCategoryEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ProductCategoryEditViewModel model)
        {
            var api = new ProductCategoryApi();

            // Validate
            var info = await api
                .GetByStoreIdAsync(model.CateID, this.CurrentStore.BrandId.Value);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }

            model.ImageFontAwsomeCss = model.IconEnum.ToString();
            model.Type = (int)model.CategoryTypes;
            model.StoreId = this.CurrentStore.ID;
            model.Active = true;
            await api.EditAsync(model.CateID, model);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(ProductCategoryEditViewModel model)
        {
            model.AvailableCategories = (await new ProductCategoryApi()
                .GetByStoreIdAsync(this.CurrentStore.BrandId.Value))
                .ToSelectList(q => q.CateName, q => q.CateID.ToString(), q => model.ParentCateId == q.CateID, true, "--Không có--");

            model.CategoryTypes = (ProductCategoryType) model.Type;

            var imageFontIcon = model.ImageFontAwsomeCss.Replace(".", "");
            if (!string.IsNullOrWhiteSpace(imageFontIcon))
            {
                model.IconEnum = (IconCategoryEnum) Enum.Parse(typeof (IconCategoryEnum), imageFontIcon);
            }
            else
            {
                model.IconEnum = IconCategoryEnum.glass;
            }
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var api = new ProductCategoryApi();
            var info = await api
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.BrandId.Value);

            if (info == null)
            {
                return this.IdNotFound();
            }

            await api.DeactivateAsync(id.Value);

            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

    }
}