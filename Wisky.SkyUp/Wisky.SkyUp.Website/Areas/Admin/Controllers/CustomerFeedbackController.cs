﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using Wisky.SkyUp.Website.Controllers;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{
    [Authorize(Roles = Website.Models.Utils.AdminAuthorizeRoles)]
    public class CustomerFeedbackController : DomainBasedController
    {
        // GET: Admin/CustomerFeedback
        public ActionResult Index()
        {
            return View();
        }

        //public JsonResult IndexList(BootgridRequestViewModel request)
        //{
        //    var result = new CustomerFeedbackApi().GetAdminWithFilterAsync(
        //        this.CurrentStore.ID, request.searchPhrase,
        //        request.current, request.rowCount, request.FirstSortTerm);

        //    var model = new BootgridResponseViewModel<CustomerFeedbackViewModel>(result);
        //    return this.Json(model, JsonRequestBehavior.AllowGet);
        //}

        //public async Task<ActionResult> Detail(int? id)
        //{
        //    var customerFeedbackApi = new CustomerFeedbackApi();
        //    var info = await customerFeedbackApi
        //        .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

        //    if (info == null)
        //    {
        //        return this.IdNotFound();
        //    }
            
        //    return this.View("Detail", info);
        //}

        //public async Task<ActionResult> Delete(int? id)
        //{

        //    var customerFeedbackApi = new CustomerFeedbackApi();
        //    var info = await customerFeedbackApi
        //        .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

        //    if (info == null)
        //    {
        //        return this.IdNotFound();
        //    }

        //    await customerFeedbackApi.DeactivateAsync(id.Value);

        //    return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        //}

    }
}