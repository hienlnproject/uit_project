﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ElFinder;
using Wisky.SkyUp.Website.Controllers;
using Wisky.SkyUp.Website.Models;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{

    //[Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class FileController : DomainBasedController
    {
        private Connector _connector;
        // GET: Admin/File
        public ActionResult Index()
        {
            return View();
        }
        public Connector Connector
        {
            get
            {
                if (_connector == null)
                {
                    var storeId = this.CurrentStore.ID;
                    var storeName = this.CurrentStore.Name;
                    FileSystemDriver driver = new FileSystemDriver();
                    var directory = new DirectoryInfo(Server.MapPath("~/Images/uploaded/" + storeId));
                    if (!directory.Exists)
                    {
                        directory.Create();
                        directory.Refresh();
                    }

                    //var directory02 = new DirectoryInfo(Server.MapPath("../"));
                    DirectoryInfo thumbsStorage = directory;
                    driver.AddRoot(new Root(directory, "/Images/uploaded/" + storeId + "/")
                    {
                        Alias = "/" + storeName,
                        StartPath = directory,
                        ThumbnailsStorage = thumbsStorage,
                        MaxUploadSizeInMb = 2.2,
                        ThumbnailsUrl = "/",
                        Directory = directory,
                    });
                    _connector = new Connector(driver);
                }
                return _connector;
            }
        }

        public ActionResult LoadFile()
        {
            return Connector.Process(this.HttpContext.Request);
        }

        [HttpPost]
        public ActionResult SelectFile(List<String> values)
        {
            //var directory = new DirectoryInfo(Path.Combine(Directory.GetParent(Server.MapPath("")).Parent.FullName, "images/uploaded"));
            string rootUrl = "\\Images\\uploaded\\";
            //new Uri("/Images/uploaded").ToString();
            var json = new List<String>();
            foreach (var file in values)
            {
                //@"\Images\" 
                var sliceString = Connector.GetFileByHash(file).FullName.Split(new string[] { rootUrl }, StringSplitOptions.None);
                //string[] sliceString = Regex.Split(Connector.GetFileByHash(file).FullName, @"\VC\");
                var url = rootUrl + sliceString[1];
                url = url.Replace(@"\", "/").Replace(@"\\", "/");
                json.Add(url);
            }
            return Json(json);
        }

        public ActionResult Thumbs(string tmb)
        {
            return Connector.GetThumbnail(Request, Response, tmb);
        }

        public ActionResult GetImageFromElfinder(string elementId)
        {
            return View("_ElfinderTextBox", model: elementId);
        }
        public ActionResult BrowseFile()
        {
            return View("_Elfinder");
        }
        // GET: Admin/File
        public JsonResult Upload()
        {
            //write your handler implementation here.
            this.HttpContext.Response.ContentType = "text/plain";

            var file = Request.Files["file"];
            var newFileName = "";
            if (file != null && file.ContentLength > 0)
            {
                var fileExtension = Path.GetExtension(file.FileName);
                newFileName = Guid.NewGuid().ToString() + fileExtension;
                var path = string.Format("{0}\\{1}", Server.MapPath("~/ImageUpload"), newFileName);
                file.SaveAs(path);
            }
            this.HttpContext.Response.Write(newFileName);
            return Json(new
            {
                success = true
            });
        }

        public FileResult UploadCanvas(string folder, string imageData, string fileName)
        {
            byte[] data;
            var fileExtension = ".png";
            var newFileName = Guid.NewGuid().ToString() + fileExtension;
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                newFileName = fileName + fileExtension;
            }
            var path = string.Format("{0}\\{1}\\{2}", Server.MapPath("~/ImageUpload"), CurrentStore.ID, folder);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var filePath = string.Format("{0}\\{1}", path, newFileName);
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    data = Convert.FromBase64String(imageData);
                    bw.Write(data);
                    bw.Close();
                }
                fs.Close();
            }
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + folder + ".png\"");
            return File(data, "image/png");
        }
    }
}