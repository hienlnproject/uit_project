﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using Wisky.SkyUp.Website.Controllers;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{
    public class ImageCollectionController : DomainBasedController
    {
        // GET: Admin/ImageCollection
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new ProductImageCollectionApi().GetAdminWithFilter(
                this.CurrentStore.ID, request.searchPhrase,
                request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<ProductImageCollectionDetailsViewModel>(result);
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = await new ImageCollectionApi()
                .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            //var model = new ImageCollectionDetailsViewModel(info, this.Mapper);

            return this.View(info);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ImageCollectionDetailsViewModel model)
        {
            var api = new ImageCollectionApi();
            // Validate
            var info = await api
                .GetByStoreIdAsync(model.ImageCollection.Id, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            model.ImageCollection.StoreId = this.CurrentStore.ID;
            model.ImageCollection.Active = true;
            await api.EditAsync(model.ImageCollection, model.Items);
            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });

        }

    }
}