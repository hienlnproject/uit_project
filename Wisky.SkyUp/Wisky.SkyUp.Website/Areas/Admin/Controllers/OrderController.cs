﻿using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Controllers;

namespace Wisky.SkyUp.Website.Areas.Admin.Controllers
{
    [Authorize(Roles = Website.Models.Utils.AdminAuthorizeRoles)]
    public class OrderController : DomainBasedController
    {
        // GET: Admin/Order
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult IndexList(BootgridRequestViewModel request)
        //{
        //    var result = new RentApi().GetAdminWithFilterAsync(
        //        this.CurrentStore.ID, request.searchPhrase,
        //        request.current, request.rowCount, request.FirstSortTerm);

        //    var model = new BootgridResponseViewModel<RentViewModel>(result);
        //    return this.Json(model, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult OrderDetail(int id)
        //{
        //    var result = new RentApi().GetOrderById(this.CurrentStore.ID, id);
        //    if (result == null)
        //    {
        //        return Json(new
        //        {
        //            success = false
        //        });
        //    }
        //    return Json(new
        //    {
        //        success = true,
        //        data = result
        //    });
        //}
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    var orderApi = new RentApi();

        //    var info = orderApi.GetOrderById(this.CurrentStore.ID, id.GetValueOrDefault());

        //    if (info == null)
        //    {
        //        return this.IdNotFound();
        //    }

        //    await orderApi.DeleteAsync(info.Order.RentID);
        //    //await orderApi.DeactivateAsync(info.Order.RentID);

        //    return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        //}

        //public ActionResult ChangeStatusOrder(int rentId, int orderStatus)
        //{
        //    var orderApi = new RentApi();
        //    var info = orderApi.Get(rentId);
        //    info.DeliveryStatus = orderStatus;
        //    orderApi.Edit(info.RentID, info);
        //    return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        //}
    }
}