﻿using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin With Directory",
                "{parameters}/Admin/{controller}/{action}/{id}",
                new { controller = "Default", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Wisky.SkyUp.Website.Areas.Admin.Controllers", }
            );

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Default", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Wisky.SkyUp.Website.Areas.Admin.Controllers", }
            );
        }
    }
}