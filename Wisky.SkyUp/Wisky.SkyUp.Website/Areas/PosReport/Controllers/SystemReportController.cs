﻿using HmsService.Models;
using HmsService.Sdk;
using HmsService.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using System.Windows.Forms;
using Wisky.SkyUp.Website.Areas.PosReport.Models;

namespace Wisky.SkyUp.Website.Areas.PosReport.Controllers
{
    public class SystemReportController : BaseController
    {
        // GET: PosReport/SystemReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RevenueReport()
        {
            return View();
        }

        public JsonResult LoadRevenueReport(JQueryDataTableParamModel param, string startTime, string endTime, int brandId)
        {
            var listDateReport = new List<TempSystemRevenueReportItem>();

            var storeApi = new StoreApi();
            var dateReportApi = new DateReportApi();

            var today = DateTime.Now.ToString("dd/MM/yyyy");
            try
            {
                if (startTime == "" || endTime == "")
                {
                    // 1. get ngay (ngày đầu tháng -> ngày hiện tại)
                    var dateNow = DateTime.Now;
                    var startDate = new DateTime(dateNow.Year, dateNow.Month, 1);
                    var tempStartDate = startDate;
                    var endDate = dateNow.GetEndOfDate();
                    // 2. lấy list store
                    var storeList = storeApi.GetActiveStoreByBrandId(brandId)
                        .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch))
                        .OrderBy(a => a.Name).ToList();
                    // 3. duyệt ngày theo store -> lấy dc doanh thu của ngày theo tất cả cửa hàng
                    for (var d = startDate; startDate <= endDate; d.AddDays(1))
                    {
                        double totalDateAmount = 0;
                        double finalDateAmount = 0;
                        double discountDateFee = 0;
                        foreach (var store in storeList)
                        {
                            var dateReport = dateReportApi.GetDateReportTimeRangeAndStore(startDate, startDate.GetEndOfDate(), store.ID).ToList();

                            var totalAmount = (double)dateReport.Sum(a => a.TotalAmount);
                            var finalAmount = (double)dateReport.Sum(a => a.FinalAmount);
                            var discountFee = (double)dateReport.Sum(a => a.Discount) + (double)dateReport.Sum(a => a.DiscountOrderDetail);

                            totalDateAmount += totalAmount;
                            finalDateAmount += finalAmount;
                            discountDateFee += discountFee;
                        }
                        listDateReport.Add(new TempSystemRevenueReportItem()
                        {
                            StartTime = startDate.ToString("dd/MM/yyyy"),
                            TotalAmount = totalDateAmount,
                            FinalAmount = finalDateAmount,
                            TotalDiscountFee = discountDateFee
                        });
                        startDate = startDate.AddDays(1);
                    }
                }
                else
                {
                    var startDate = DateTime.Parse(startTime).GetStartOfDate();
                    var endDate = DateTime.Parse(endTime).GetEndOfDate();
                    // 1. get ngay (ngày đầu tháng -> ngày hiện tại)
                    var dateNow = DateTime.Now;
                    var tempStartDate = startDate;
                    // 2. lấy list store
                    var storeList = storeApi.GetActiveStoreByBrandId(brandId)
                        .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch))
                        .OrderBy(a => a.Name);
                    // 3. duyệt ngày theo store -> lấy dc doanh thu của ngày theo tất cả cửa hàng
                    for (var d = startDate; startDate <= endDate; d.AddDays(1))
                    {
                        double totalDateAmount = 0;
                        double finalDateAmount = 0;
                        double discountDateFee = 0;
                        foreach (var store in storeList)
                        {
                            var dateReport = dateReportApi.GetDateReportTimeRangeAndStore(startDate, startDate.GetEndOfDate(), store.ID).ToList();

                            var totalAmount = (double)dateReport.Sum(a => a.TotalAmount);
                            var finalAmount = (double)dateReport.Sum(a => a.FinalAmount);
                            var discountFee = (double)dateReport.Sum(a => a.Discount) + (double)dateReport.Sum(a => a.DiscountOrderDetail);

                            totalDateAmount += totalAmount;
                            finalDateAmount += finalAmount;
                            discountDateFee += discountFee;

                        }
                        listDateReport.Add(new TempSystemRevenueReportItem()
                        {
                            StartTime = startDate.ToString("dd/MM/yyyy"),
                            TotalAmount = totalDateAmount,
                            FinalAmount = finalDateAmount,
                            TotalDiscountFee = discountDateFee
                        });
                        startDate = startDate.AddDays(1);
                    }
                }
                var list = listDateReport.Select(a => new IConvertible[]
                        {
                        a.StartTime,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalAmount),
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalDiscountFee),
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.FinalAmount)
                        }).ToArray();

                //var totalRecords = await customerStores.CountAsync();
                var totalRecords = 10;

                //return Json(new
                //{
                //    sEcho = param.sEcho,
                //    iTotalRecords = totalRecords,
                //    iTotalDisplayRecords = list.Count(),
                //    aaData = list
                //}, JsonRequestBehavior.AllowGet);

                return Json(new
                {
                    dataList = list
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        public ActionResult ExportRevenueTableToExcel(JQueryDataTableParamModel param, string startTime, string endTime, int brandId)
        {
            var listDateReport = new List<TempSystemRevenueReportItem>();
            var storeApi = new StoreApi();
            var dateReportApi = new DateReportApi();

            var today = DateTime.Now.ToString("dd/MM/yyyy");
            if (startTime == "" || endTime == "")
            {
                // 1. get ngay (ngày đầu tháng -> ngày hiện tại)
                var dateNow = DateTime.Now;
                var startDate = new DateTime(dateNow.Year, dateNow.Month, 1);
                var tempStartDate = startDate;
                var endDate = dateNow.GetEndOfDate();
                // 2. lấy list store
                var storeList = storeApi.GetActiveStoreByBrandId(brandId)
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch))
                    .OrderBy(a => a.Name).ToList();
                // 3. duyệt ngày theo store -> lấy dc doanh thu của ngày theo tất cả cửa hàng
                for (var d = startDate; startDate <= endDate; d.AddDays(1))
                {
                    double totalDateAmount = 0;
                    double finalDateAmount = 0;
                    double discountDateFee = 0;
                    foreach (var store in storeList)
                    {
                        var dateReport = dateReportApi.GetDateReportTimeRangeAndStore(startDate, startDate.GetEndOfDate(), store.ID).ToList();

                        var totalAmount = (double)dateReport.Sum(a => a.TotalAmount);
                        var finalAmount = (double)dateReport.Sum(a => a.FinalAmount);
                        var discountFee = (double)dateReport.Sum(a => a.Discount) + (double)dateReport.Sum(a => a.DiscountOrderDetail);

                        totalDateAmount += totalAmount;
                        finalDateAmount += finalAmount;
                        discountDateFee += discountFee;
                    }
                    listDateReport.Add(new TempSystemRevenueReportItem()
                    {
                        StartTime = startDate.ToString("dd/MM/yyyy"),
                        TotalAmount = totalDateAmount,
                        FinalAmount = finalDateAmount,
                        TotalDiscountFee = discountDateFee
                    });
                    startDate = startDate.AddDays(1);
                }
            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();
                // 1. get ngay (ngày đầu tháng -> ngày hiện tại)
                var dateNow = DateTime.Now;
                var tempStartDate = startDate;
                // 2. lấy list store
                var storeList = storeApi.GetActiveStoreByBrandId(brandId)
                    .Where(a => string.IsNullOrEmpty(param.sSearch) || a.Name.ToLower().Contains(param.sSearch))
                    .OrderBy(a => a.Name);
                // 3. duyệt ngày theo store -> lấy dc doanh thu của ngày theo tất cả cửa hàng
                for (var d = startDate; startDate <= endDate; d.AddDays(1))
                {
                    double totalDateAmount = 0;
                    double finalDateAmount = 0;
                    double discountDateFee = 0;
                    foreach (var store in storeList)
                    {
                        var dateReport = dateReportApi.GetDateReportTimeRangeAndStore(startDate, startDate.GetEndOfDate(), store.ID).ToList();

                        var totalAmount = (double)dateReport.Sum(a => a.TotalAmount);
                        var finalAmount = (double)dateReport.Sum(a => a.FinalAmount);
                        var discountFee = (double)dateReport.Sum(a => a.Discount) + (double)dateReport.Sum(a => a.DiscountOrderDetail);

                        totalDateAmount += totalAmount;
                        finalDateAmount += finalAmount;
                        discountDateFee += discountFee;

                    }
                    listDateReport.Add(new TempSystemRevenueReportItem()
                    {
                        StartTime = startDate.ToString("dd/MM/yyyy"),
                        TotalAmount = totalDateAmount,
                        FinalAmount = finalDateAmount,
                        TotalDiscountFee = discountDateFee
                    });
                    startDate = startDate.AddDays(1);
                }
            }
            var list = listDateReport.Select(a => new
            {
                a = a.StartTime,
                b = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalAmount),
                c = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalDiscountFee),
                d = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.FinalAmount)
            }).ToArray();
            List<string> header = new List<string>();
            header.Add("#;1;1");
            header.Add("Ngày;1;1");
            header.Add("Tổng doanh thu;1;1");
            header.Add("Tổng giảm giá;1;1");
            header.Add("Doanh thu sau giảm giá;1;1");

            string fileName = "Báo cáo theo doanh thu tháng";

            bool success = false;
            Thread thdSyncRead = new Thread(new ThreadStart(() => exportExcel(header, list, ref fileName, ref success)));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.IsBackground = false;
            thdSyncRead.Priority = ThreadPriority.Highest;
            thdSyncRead.Start();
            thdSyncRead.Join();
            //if (!success)
            //{
            //    thdSyncRead.Abort();
            //}
            thdSyncRead.Abort();
            return Json(new
            {
                success = success,
                fileName = fileName,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportHourAllStoreToExcel(JQueryDataTableParamModel param, string startTime, string endTime, int brandId)
        {
            var orderApi = new OrderApi();
            var hourReport = new List<HourReportModel>();
            for (int i = 6; i < 24; i++)
            {
                hourReport.Add(new HourReportModel()
                {
                    StartTime = i,
                    EndTime = (i + 1)

                });
            }

            if (startTime == "" && endTime == "")
            {
                var dateNow = DateTime.Now;
                var startDate = dateNow.GetStartOfDate();
                var endDate = dateNow.Date.GetEndOfDate();

                var orders = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                    .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish)
                        .Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });

                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });


                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");

            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();
                var rents = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });
                var result = rents.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });

                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");
            }
            var list = hourReport.Select(a => new
            {
                a = a.StartTime + ":00 - " + a.EndTime + ":00",
                b = a.TakeAway,
                c = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceTakeAway),
                d = a.AtStore,
                e = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceAtStore),
                f = a.Delivery,
                g = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceDelivery),
                h = a.TotalQuantity,
                i = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalPrice),

            }).ToArray();


            List<string> header = new List<string>();
            header.Add("STT;1;1");
            header.Add("Khoảng thời gian;1;1");
            header.Add("Số lượng(Mang đi);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Số lượng(Tại store);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Số lượng(Giao hàng);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Tổng cộng;1;1");
            header.Add("Thành tiền;1;1");

            string fileName = "báo cáo theo giờ";

            bool success = false;
            Thread thdSyncRead = new Thread(new ThreadStart(() => exportExcel(header, list, ref fileName, ref success)));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            thdSyncRead.Join(120000);
            if (!success)
            {
                thdSyncRead.Abort();
            }

            return Json(new
            {
                success = success,
                fileName = fileName,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportHourOneStoreToExcel(JQueryDataTableParamModel param, string startTime, string endTime, int storeIdReport, int brandId)
        {
            var orderApi = new OrderApi();
            var hourReport = new List<HourReportModel>();
            for (int i = 6; i < 24; i++)
            {
                hourReport.Add(new HourReportModel()
                {
                    StartTime = i,
                    EndTime = (i + 1)

                });
            }

            if (startTime == "" && endTime == "")
            {
                var dateNow = DateTime.Now;
                var startDate = dateNow.GetStartOfDate();
                var endDate = dateNow.Date.GetEndOfDate();
                var orders = orderApi.GetRentsByTimeRange(storeIdReport, startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });


                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });


                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");

            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();
                var orders = orderApi.GetRentsByTimeRange(storeIdReport, startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });
                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });

                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");
            }
            var list = hourReport.Select(a => new
            {
                a = a.StartTime + ":00 - " + a.EndTime + ":00",
                b = a.TakeAway,
                c = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceTakeAway),
                d = a.AtStore,
                e = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceAtStore),
                f = a.Delivery,
                g = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.Delivery),
                h = a.TotalQuantity,
                i = string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalPrice)

            }).ToArray();
            List<string> header = new List<string>();
            header.Add("STT;1;1");
            header.Add("Khoảng thời gian;1;1");
            header.Add("Số lượng(Mang đi);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Số lượng(Tại store);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Số lượng(Giao hàng);1;1");
            header.Add("Thành tiền;1;1");
            header.Add("Tổng cộng;1;1");
            header.Add("Thành tiền;1;1");

            string fileName = "báo cáo theo giờ";

            bool success = false;
            Thread thdSyncRead = new Thread(new ThreadStart(() => exportExcel(header, list, ref fileName, ref success)));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            thdSyncRead.Join(120000);
            if (!success)
            {
                thdSyncRead.Abort();
            }

            return Json(new
            {
                success = success,
                fileName = fileName,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CategoryReport()
        {
            return View();
        }

        public async Task<JsonResult> LoadCategories(int brandId)
        {
            var productCategoryApi = new ProductCategoryApi();
            var categories = await productCategoryApi.GetByBrandIdAsync(brandId);
            return Json(categories);
        }

        public async Task<JsonResult> LoadStores(int brandId)
        {
            var storeApi = new StoreApi();
            var stores = await storeApi.GetActiveStoreByBrandId(brandId).ToListAsync();
            return Json(stores);
        }

        public JsonResult LoadHourReportComparison(string startTime, string endTime, int? Time, int brandId)
        {
            var storeApi = new StoreApi();
            var orderApi = new OrderApi();
            var listStores = storeApi.GetStoresByBrandIdAndType(brandId, 5);

            List<DateProductViewModel> listDateProductByCateId = new List<DateProductViewModel>();
            List<string> listStoreName = new List<string>();
            List<double> listFinalAmount = new List<double>();
            if (startTime == "" && endTime == "")
            {
                var dateNow = DateTime.Now;
                var startDate = dateNow.GetStartOfDate();
                var endDate = dateNow.Date.GetEndOfDate();

                var orders = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                    .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish && a.CheckinHour == Time);

                var result = orders.GroupBy(a => a.Store).Select(b => new
                {
                    StoreId = b.Key.ID,
                    FinalAmount = b.Sum(c => c.FinalAmount)
                });

                foreach (var store in listStores)
                {
                    var storeName = store.Name;
                    listStoreName.Add(storeName);
                    var finalAmount = result.Where(a => a.StoreId == store.ID);
                    listFinalAmount.Add(finalAmount.Sum(a => a.FinalAmount));
                }

            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();

                var rents = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                     .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish && a.CheckinHour == Time);


                var result = rents.GroupBy(a => a.Store).Select(b => new
                {
                    StoreId = b.Key.ID,
                    FinalAmount = b.Sum(c => c.FinalAmount)
                });

                foreach (var store in listStores)
                {
                    var storeName = store.Name;
                    listStoreName.Add(storeName);
                    var finalAmount = result.Where(a => a.StoreId == store.ID).ToList();
                    listFinalAmount.Add(finalAmount.Sum(a => a.FinalAmount));
                }
            }

            return Json(new
            {
                dataChart = new
                {
                    StoreName = listStoreName,
                    FinalAmount = listFinalAmount
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexHourReport()
        {
            return View();
        }

        public ActionResult HourReportAllStore(JQueryDataTableParamModel param, string startTime, string endTime, int brandId)
        {

            var hourReport = new List<HourReportModel>();
            var orderApi = new OrderApi();
            for (int i = 6; i < 24; i++)
            {
                hourReport.Add(new HourReportModel()
                {
                    StartTime = i,
                    EndTime = (i + 1)
                });
            }

            if (startTime == "" && endTime == "")
            {
                var dateNow = DateTime.Now;

                //var startDate = dateNow.AddDays(-1).GetStartOfDate();
                //var endDate = dateNow.AddDays(-1).Date.GetEndOfDate();

                var startDate = dateNow.GetStartOfDate();
                var endDate = dateNow.Date.GetEndOfDate();
                //Chuyển từ rent sang order
                var orders = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                    .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish)
                        .Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });

                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });


                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");

            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();
                var orders = orderApi.GetAllOrderByDate(startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });
                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });

                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");
            }
            int count = 6;
            var list = hourReport.Select(a => new IConvertible[]
                    {
                        count++,
                        a.StartTime+":00 - "+a.EndTime+":00",
                        a.TakeAway,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceTakeAway),
                        a.AtStore,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceAtStore),
                        a.Delivery,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceDelivery),
                        a.TotalQuantity,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalPrice),
                        a.StartTime

                    }).ToArray();
            var _time = hourReport.Select(a => a.StartTime + ":00 - " + a.EndTime + ":00").ToArray();
            var _takeAway = hourReport.Select(a => a.TakeAway).ToArray();
            var _atStore = hourReport.Select(a => a.AtStore).ToArray();
            var _delivery = hourReport.Select(a => a.Delivery).ToArray();


            return Json(new
            {
                datatable = list,
                chartdata = new
                {
                    Time = _time,
                    TakeAway = _takeAway,
                    AtStore = _atStore,
                    Delivery = _delivery
                }
            }, JsonRequestBehavior.AllowGet);
            //return PartialView("_HourReport", hourReport);
        }

        public ActionResult HourReportOneStore(JQueryDataTableParamModel param, string startTime, string endTime, int storeIdReport, int brandId)
        {
            var hourReport = new List<HourReportModel>();
            var orderApi = new OrderApi();
            for (int i = 6; i < 24; i++)
            {
                hourReport.Add(new HourReportModel()
                {
                    StartTime = i,
                    EndTime = (i + 1)

                });
            }

            if (startTime == "" && endTime == "")
            {
                var dateNow = DateTime.Now;

                //var startDate = dateNow.AddDays(-1).GetStartOfDate();
                //var endDate = dateNow.AddDays(-1).GetEndOfDate();

                var startDate = dateNow.GetStartOfDate();
                var endDate = dateNow.GetEndOfDate();
                var orders = orderApi.GetRentsByTimeRange(storeIdReport, startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });


                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });


                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");

            }
            else
            {
                var startDate = DateTime.Parse(startTime).GetStartOfDate();
                var endDate = DateTime.Parse(endTime).GetEndOfDate();
                var orders = orderApi.GetRentsByTimeRange(storeIdReport, startDate, endDate, brandId)
                        .Where(a => a.OrderType != (int)OrderTypeEnum.DropProduct && a.OrderStatus == (int)OrderStatusEnum.Finish).Select(q => new
                        {
                            q.RentID,
                            q.OrderType,
                            q.CheckinHour,
                            q.TotalAmount,
                        });
                var result = orders.GroupBy(r => new { r.OrderType, Time = r.CheckinHour }).Select(r => new
                {
                    OrderType = r.Key.OrderType,
                    OrderTime = r.Key.Time,
                    TotalOrder = r.Count(),
                    Money = r.Sum(a => a.TotalAmount)
                });

                foreach (var item in hourReport)
                {

                    var takeAway = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.TakeAway && r.OrderTime == item.StartTime);
                    item.TakeAway = (takeAway == null) ? 0 : takeAway.TotalOrder;
                    item.PriceTakeAway = (takeAway == null) ? 0 : takeAway.Money;

                    var atStore = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.AtStore && r.OrderTime == item.StartTime);
                    item.AtStore = (atStore == null) ? 0 : atStore.TotalOrder;
                    item.PriceAtStore = (atStore == null) ? 0 : atStore.Money;

                    var delivery = result.FirstOrDefault(r => r.OrderType == (int)OrderTypeEnum.Delivery && r.OrderTime == item.StartTime);
                    item.Delivery = (delivery == null) ? 0 : delivery.TotalOrder;
                    item.PriceDelivery = (delivery == null) ? 0 : delivery.Money;

                    item.TotalQuantity = item.TakeAway + item.AtStore + item.Delivery;
                    item.TotalPrice = item.PriceTakeAway + item.PriceAtStore + item.PriceDelivery;
                }
                ViewBag.Start = startDate.ToString("dd-MM-yyyy");
                ViewBag.End = endDate.ToString("dd-MM-yyyy");
            }
            int count = 6;
            var list = hourReport.Select(a => new IConvertible[]
                    {
                        count++,
                        a.StartTime+":00 - "+a.EndTime+":00",
                        a.TakeAway,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceTakeAway),
                        a.AtStore,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.PriceAtStore),
                        a.Delivery,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.Delivery),
                        a.TotalQuantity,
                        string.Format(CultureInfo.InvariantCulture,
                           "{0:0,0}", a.TotalPrice)

                    }).ToArray();
            var _time = hourReport.Select(a => a.StartTime + ":00 - " + a.EndTime + ":00").ToArray();
            var _takeAway = hourReport.Select(a => a.TakeAway).ToArray();
            var _atStore = hourReport.Select(a => a.AtStore).ToArray();
            var _delivery = hourReport.Select(a => a.Delivery).ToArray();


            return Json(new
            {
                datatable = list,
                chartdata = new
                {
                    Time = _time,
                    TakeAway = _takeAway,
                    AtStore = _atStore,
                    Delivery = _delivery
                }
            }, JsonRequestBehavior.AllowGet);
            //return PartialView("_HourReport", hourReport);
        }

        private void exportExcel(List<string> headers, IEnumerable<object> _list, ref string fileName, ref bool success)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            string selectedPath = "";
            DialogResult confirm = folderDlg.ShowDialog();
            if (confirm == DialogResult.OK)
            {

                Environment.SpecialFolder root = folderDlg.RootFolder;
                selectedPath = folderDlg.SelectedPath;
                if (!string.IsNullOrEmpty(selectedPath))
                {
                    int length = selectedPath.Length;
                    int temp = selectedPath.LastIndexOf("\\");
                    if (selectedPath.LastIndexOf("\\") == length - 1)
                    {

                        fileName = selectedPath + fileName + ".xls";
                    }
                    else
                    {
                        fileName = selectedPath + "\\" + fileName + ".xls";
                    }
                    var result = Utils.ExportToExcel(headers, _list, fileName);
                    if (result)
                    {
                        success = true;
                    }
                }
            }
        }

        public class TempSystemRevenueReportItem
        {
            public string StartTime { get; set; }
            public double TotalAmount { get; set; }
            public double FinalAmount { get; set; }
            public double TotalDiscountFee { get; set; }
        }
    }
}