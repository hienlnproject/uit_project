﻿using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models;

namespace Wisky.SkyUp.Website.Areas.SysAdmin.Controllers
{

    [Authorize(Roles = Utils.SysAdminAuthorizeRoles)]
    public class StoreController : BaseController
    {


        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult IndexList(BootgridRequestViewModel request)
        {
            var result = new StoreApi().GetAdminWithFilterAsync(
                request.searchPhrase, request.current, request.rowCount, request.FirstSortTerm);

            var model = new BootgridResponseViewModel<StoreViewModel>(result);
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel();

            this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Create(Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                this.PrepareCreate(model);
                return this.View(model);
            }

            await new StoreApi().CreateAsync(model, nameof(model.ID));

            this.SetMessage("Store Created!");
            return this.RedirectToAction("Edit", new { id = model.ID, });
        }

        private void PrepareCreate(Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel model)
        {
        }

        public async Task<ActionResult> Edit(int? id)
        {
            // Validate
            var info = await new StoreApi().GetAsync(id);

            if (info == null || !info.isAvailable.GetValueOrDefault())
            {
                return this.IdNotFound();
            }

            var model = new Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel(info, this.Mapper);

            this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Edit(Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel model)
        {
            var api = new StoreApi();
            // Validate
            var info = await api.GetAsync(model.ID);

            if (info == null || !info.isAvailable.GetValueOrDefault())
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                this.PrepareEdit(model);
                return this.View(model);
            }

            model.isAvailable = true;

            await api.EditAsync(model.ID, model);

            return this.RedirectToAction("Index");
        }

        public void PrepareEdit(Wisky.SkyUp.Website.Areas.SysAdmin.Models.StoreEditViewModel model)
        {
        }

        public async Task<ActionResult> Delete(int? id)
        {
            var api = new StoreApi();
            var info = await api.GetAsync(id);

            if (info == null)
            {
                return this.IdNotFound();
            }

            await api.DeactivateAsync(id.Value);

            return this.RedirectToAction("Index");
        }

    }
}