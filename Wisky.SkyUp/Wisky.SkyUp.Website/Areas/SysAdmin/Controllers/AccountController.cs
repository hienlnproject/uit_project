﻿using HmsService.Sdk;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Wisky.SkyUp.Website.Models.Identity;
using HmsService.ViewModels;

namespace Wisky.SkyUp.Website.Areas.SysAdmin.Controllers
{

    [Authorize(Roles = Utils.SysAdminAuthorizeRoles)]
    public class AccountController : BaseController
    {
        private const string SysAdminRoleName = "SysAdmin";

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Install(string token, bool? forceResetPassword)
        {
            if (token != ConfigurationManager.AppSettings["SysAdminInstallToken"])
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unauthorized request source.");
            }

            var context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var accountNames = ConfigurationManager.AppSettings["SysAdminAccounts"].Split(';');
            var defaultPassword = ConfigurationManager.AppSettings["SysAdminDefaultPassword"];

            this.CreateRoleIfNotExist(SysAdminRoleName, roleManager);

            foreach (var accountName in accountNames)
            {
                this.CreateOrResetPassword(userManager, accountName, defaultPassword, forceResetPassword.GetValueOrDefault());
            }

            return this.Content("OK!");
        }

        public async Task<ActionResult> Index()
        {
            var model = await new AspNetUserApi()
                .GetDetails();

            return this.View(model);
        }

        public ActionResult Create()
        {
            var model = new AspNetUserEditViewModel()
            {
                AspNetUser = new HmsService.ViewModels.AspNetUserViewModel(),
            };

            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AspNetUserEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var userManager = this.UserManager;
            var result = await UserManager.CreateAsync(new ApplicationUser()
            {
                UserName = model.AspNetUser.UserName,
                Email = "default@wisky.vn",
            });
            this.ThrowIfNotSucceed(result);

            var user = await userManager.FindByNameAsync(model.AspNetUser.UserName);

            this.SetMessage("Created!");
            return this.RedirectToAction("Edit", new { user.Id, });
        }

        public async Task<ActionResult> Edit(string id)
        {
            var details = await new AspNetUserApi().GetDetails(id);

            if (details == null)
            {
                return this.IdNotFound();
            }

            // Exchange to higher model
            var model = new AspNetUserEditViewModel(details, this.Mapper);
            await this.PrepareEdit(model);

            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AspNetUserEditViewModel model)
        {
            var api = new AspNetUserApi();
            var entity = await api.GetAsync(model?.AspNetUser?.Id);

            if (entity == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }

            await api.EditAsync(model.AspNetUser, model.SelectedRoles ?? new string[0], model.AdminStoreId);

            this.SetMessage("Edited");
            return this.RedirectToAction(nameof(this.Index));
        }

        private async Task PrepareEdit(AspNetUserEditViewModel model)
        {
            model.AvailableStores = (await new StoreApi().GetActiveAsync())
                .ToSelectList(q => q.Name, q => q.ID.ToString(), q => q.ID == model.AdminStoreId, true, "- No Store -");

            var roleIds = model.Roles.Select(q => q.Id);
            model.AvailableRoles = (await new AspNetRoleApi().GetActiveAsync())
                .ToSelectList(q => q.Name, q => q.Id.ToString(), q => roleIds.Contains(q.Id));
        }

        public async Task<ActionResult> SetPassword(string id, string password)
        {
            var userManager = this.UserManager;

            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return this.IdNotFound("User Id not found");
            }

            var token = await userManager.GeneratePasswordResetTokenAsync(user.Id);
            var result = await userManager.ResetPasswordAsync(user.Id, token, password);
            this.ThrowIfNotSucceed(result);

            this.SetMessage("Password set.");
            return this.RedirectToAction("Index");
        }

        private void CreateOrResetPassword(UserManager<ApplicationUser> userManager, string username, string password, bool forceResetPassword)
        {
            var user = userManager.FindByName(username);

            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = username,
                    Email = "sysadmin@wisky.vn",
                };

                var result = userManager.Create(user, password);
                this.ThrowIfNotSucceed(result);
            }
            else
            {
                if (forceResetPassword)
                {
                    var token = userManager.GeneratePasswordResetToken(user.Id);
                    var result = userManager.ResetPassword(user.Id, token, password);
                    this.ThrowIfNotSucceed(result);
                }
            }

            if (!userManager.IsInRole(user.Id, SysAdminRoleName))
            {
                var result = userManager.AddToRole(user.Id, SysAdminRoleName);
                this.ThrowIfNotSucceed(result);
            }
        }

        private void CreateRoleIfNotExist(string role, RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExists(role))
            {
                roleManager.Create(new IdentityRole()
                {
                    Name = role,
                });
            }
        }

        private void ThrowIfNotSucceed(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new OperationCanceledException(string.Join(Environment.NewLine, result.Errors));
            }
        }

    }
}