﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Wisky.SkyUp.Website.Areas.Admin.Models.ViewModels;
using HmsService.ViewModels;
using Wisky.SkyUp.Website.Models.ViewModels;
using Wisky.SkyUp.Website.Util;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using Wisky.SkyUp.Website.Controllers;

namespace Wisky.SkyUp.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HmsService.ApiEndpoint.Entry(this.AdditionalMapperConfig);

            Application["OnlineUsers"] = 0;

        }

        void Session_Start(object sender, EventArgs e)
        {
           
                Application.Lock();
                Application["OnlineUsers"] = (int)Application["OnlineUsers"] + 1;
                Application.UnLock();
            
            


        }

        void Session_End(object sender, EventArgs e)
        {
            if (Session["StoreID"].Equals("49"))
            {
                Application.Lock();
                Application["OnlineUsers"] = (int)Application["OnlineUsers"] - 1;
                Application.UnLock();
            }


            //var counter = await this.Service<IStoreWebViewCounterService>().GetAndIncreaseAsync(this.CurrentStore.ID);
            //model.ViewCounter = new StoreWebViewCounterViewModel(counter);


        }


        public void AdditionalMapperConfig(IMapperConfiguration config)
        {
            config.CreateMap<ProductViewModel, ProductViewModel>();
            config.CreateMap<ProductDetailsViewModel, ProductDetailsViewModel>();
            config.CreateMap<ProductViewModel, ProductDetailsViewModel>();
            config.CreateMap<ProductEditViewModel, ProductViewModel>();
            config.CreateMap<ImageCollectionDetailsViewModel, ImageCollectionDetailsViewModel>();
            config.CreateMap<ImageCollectionViewModel, ImageCollectionViewModel>();
            //config.CreateMap<RentViewModel, RentViewModel>();

            config.CreateMap<BlogPost, BlogPostViewModel>();
            config.CreateMap<BlogPostViewModel, BlogPostViewModel>();
            config.CreateMap<BlogPostEditViewModel, BlogPostViewModel>();
            //config.CreateMap<BlogPostCollection, BlogPostCollectionWithPostsViewModel>();

            config.CreateMap<AspNetUserDetailsViewModel, AspNetUserEditViewModel>();
            config.CreateMap<ProductCategory, HmsService.ViewModels.ProductCategoryEditViewModel>()
                .ForMember(q => q.Products, opt => opt.MapFrom(q => q.Products));
            //config.CreateMap<ProductViewModel, ProductRandomViewModel>()
            //    .ForMember(vm=>vm,opt=>opt.MapFrom(m=>m.ProductID.RandomValue()));
        }

    }
}