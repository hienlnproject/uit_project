﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.Identity
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập email")]
        [EmailAddress(ErrorMessage = "Vui lòng nhập đúng kiểu email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên tài khoản")]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [StringLength(100, ErrorMessage = "Mật khẩu ít nhất {2} ký tự.", MinimumLength = 6)]
        [RegularExpression(@"((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$", ErrorMessage = "Mật khẩu phải có ký tự đặc biệt, chữ in hoa, số")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Mật khẩu xác nhận không giống mật khẩu.")]
        public string ConfirmPassword { get; set; }

        public string CurrentEmail { get; set; }

        public string CurrentPassword { get; set; }
    }
}