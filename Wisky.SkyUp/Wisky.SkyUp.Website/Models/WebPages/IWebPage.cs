﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.WebPages
{

    public interface IWebPage
    {

    }

    public enum WebPageType
    {
        Standalone,
        NewsList,
        NewsDetails,
        ProductList,
        ProductDetails,
        Cart,
    }

}