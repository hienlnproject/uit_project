﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models
{
    // DucBM
    public class FilterObject
    {
        public List<KeyValuePair<string, List<string>>> purposes { get; set; }
        public FilterObject()
        {
            purposes = new List<KeyValuePair<string, List<string>>>();
        }

        // Method: Check if Filter Object contains pair
        public bool Contains(string name, string value)
        {
            foreach (var purpose in this.purposes)
            {
                if (purpose.Key.Equals(name) && purpose.Value.Contains(value)) return true;
            }
            return false;
        }

        // Method: Add a pair Name, Value
        public void Add(string name, string value)
        {
            if (Contains(name, value)) return;
            bool nameExist = false;
            foreach (var purpose in purposes)
            {
                if (purpose.Key.ToLower().Trim()
                    .Equals(name.ToLower().Trim()))
                {
                    nameExist = true;
                    purpose.Value.Add(value);
                    break;
                }
            }
            if (nameExist == false)
            {
                List<string> values = new List<string>();
                values.Add(value);
                KeyValuePair<string, List<string>> purpose =
                    new KeyValuePair<string, List<string>>(name, values);
                purposes.Add(purpose);
            }
        }

        // Method: Remove a pair Name, Value
        public void Remove(string name, string value)
        {
            foreach (var purpose in purposes)
            {
                if (purpose.Key.ToLower().Trim()
                    .Equals(name.ToLower().Trim()))
                {
                    purpose.Value.Remove(value);
                    if (purpose.Value.Count() == 0) purposes.Remove(purpose);
                    break;
                }
            }
        }

        // Method: Convert FilterObject to string
        public string ToString()
        {
            try
            {
                List<string> purposeStrings = new List<string>();
                foreach (var purpose in purposes)
                {
                    string s = purpose.Key + ":" + String.Join(",", purpose.Value);
                    purposeStrings.Add(s);
                }
                string result = String.Join(";", purposeStrings);
                return result;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // Convert string to FilterObject
        public static FilterObject ParseFilterObject(string filterString)
        {
            try
            {
                FilterObject obj = new FilterObject();
                if (filterString == null || filterString.Equals("")) return obj;
                List<string> purposeStrings = filterString.Split(';').ToList();
                foreach (var s in purposeStrings)
                {
                    string[] arr = s.Split(':');
                    string key = arr[0];
                    List<string> values = arr[1].Split(',').ToList();
                    KeyValuePair<string, List<string>> purpose =
                        new KeyValuePair<string, List<string>>(key, values);
                    obj.purposes.Add(purpose);
                }
                return obj;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    // DucBM
    public class ProductFilter
    {
        /*
            Filter Products By Specs
            Input: Products List
            Output: Filtered Products List
        */
        // filterString: ?filterString=name1:val1,val2;name2:val3,val4
        public static IEnumerable<ProductDetailsViewModel> FilterBySpecs(IEnumerable<ProductDetailsViewModel> products, string filterString)
        {
            if (filterString == null || filterString.Equals("")) return products;
            FilterObject filterOject = FilterObject.ParseFilterObject(filterString);

            List<ProductDetailsViewModel> result = new List<ProductDetailsViewModel>();
            // Loop 1: Traverse products
            foreach (var product in products) {
                // Loop 2: Traverse filter purposes
                bool matched = true;
                
                foreach (var purpose in filterOject.purposes)
                {
                    var name = purpose.Key;
                    var values = purpose.Value;
                    // Get Product Spec that matches name
                    var spec = product.ProductSpecifications.FirstOrDefault
                        (p => p.Name.ToLower().Trim()
                        .Equals(name.ToLower().Trim()));

                    // Loop 3: Traverse spec values in filter
                    if (spec == null || values.FirstOrDefault(a => spec.Value.Split(',').Contains(a)) == null)
                    {
                        //result.Add(product);
                        //break;
                        matched = false;
                        break;
                    }
                }
                if (matched ) result.Add(product);
            }
            return (IEnumerable<ProductDetailsViewModel>)result;
        }

        /*
            Get Specs from Products List
        */
        public static FilterObject GetSpecsFromProducts(IEnumerable<ProductDetailsViewModel> listProduct)
        {
            FilterObject result = new FilterObject();
            if (listProduct!=null)
            {
                foreach (var product in listProduct)
                {
                    foreach (var spec in product.ProductSpecifications)
                        foreach (var value in spec.Value.Split(','))
                        result.Add(spec.Name, value);
                }
            }
            return result;
        }

        /*
            Make filter string
            Add a pair to current filter
            return Filter string
        */
        public static string MakeAddFilterString(string current, string name, string value)
        {
            FilterObject obj = FilterObject.ParseFilterObject(current);
            if (obj == null) return "";
            obj.Add(name, value);
            string result = obj.ToString();
            return result;
        }

        /*
            Make filter string
            Remove a pair to current filter
            return Filter string
        */
        public static string MakeRemoveFilterString(string current, string name, string value)
        {
            FilterObject obj = FilterObject.ParseFilterObject(current);
            if (obj == null) return "";
            obj.Remove(name, value);
            string result = obj.ToString();
            return result;
        }

    }
}