﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class ProductPageViewModel
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public IEnumerable<ProductPageItemViewModel> Items { get; set; }
        public string Contact { get; set; }
        public int? ParentCateId { get; set; }
        public string Image { get; set; }
    }

    public class ProductPageItemViewModel
    {
        public int Id { get; set; }
        public int Group { get; set; }
        public int CategoryId { get; set; }
        public string SeoName { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }

        //PHUONGTA - add more properties
        public string Code { get; set; }
        public string Price { get; set; }
        public string Producer { get; set; }
        public string Status { get; set; }
        public string Spec { get; set; }
        public string Documents { get; set; }
        public string DiscountPrice { get; set; }
    }   

}