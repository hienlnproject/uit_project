﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class CartItemsViewModel
    {
        public CartItemsViewModel()
        {
            CartItems = new List<CartItem>();
            Total = 0;
        }

        public List<CartItem> CartItems { get; set; }

        public double Total { get; set; }
        
        public string FormatedTotal
        {
            get
            {
                return string.Format("{0:N0}", Total);
            }
        }

        public void AddItem(ProductViewModel product, int quantity)
        {
            var existedProduct = CartItems.FirstOrDefault(a => a.Product.ProductID == product.ProductID);
            if (existedProduct == null)
            {
                existedProduct = new CartItem
                {
                    Product = product,
                    //Quantity = quantity,
                    Quantity = 0,
                    Total = quantity*product.PrimaryPrice
                };
                CartItems.Add(existedProduct);
            }
            //existedProduct.Quantity++;
            existedProduct.Quantity += quantity;
            existedProduct.Total = existedProduct.Quantity * product.PrimaryPrice;
            Total = CartItems.Sum(a => a.Quantity * a.Product.PrimaryPrice);
        }

        public void RemoveItem(ProductViewModel product)
        {
            var existedProduct = CartItems.FirstOrDefault(a => a.Product.ProductID == product.ProductID);
            if (existedProduct == null)
            {
                return;
            }
            CartItems.Remove(existedProduct);
            Total = CartItems.Sum(a => a.Quantity * a.Product.PrimaryPrice);
        }

        public void UpdateCart(List<KeyValuePair<int, int>> cartInfos)
        {
            foreach (var info in cartInfos)
            {
                var existedProduct = CartItems.FirstOrDefault(a => a.Product.ProductID == info.Key);
                if (existedProduct == null)
                {
                    continue;
                }
                if (info.Value <= 0)
                {
                    CartItems.Remove(existedProduct);
                }
                else
                {
                    existedProduct.Quantity = info.Value;
                    existedProduct.Total = existedProduct.Quantity * existedProduct.Product.PrimaryPrice;
                }
            }
            Total = CartItems.Sum(a => a.Quantity * a.Product.PrimaryPrice);
        }
    }
    public class CartItem
    {
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }
        public double Total { get; set; }
    }
}