﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class ProductRandomViewModel:ProductViewModel
    {
        public int Random { get; set; }
    }
}