﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class UserBookingInfo
    {
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public string Identity { get; set; }
        public string Email { get; set; }
        public string Category { get; set; }
        public DateTime Datetime { get; set; }
    }
}