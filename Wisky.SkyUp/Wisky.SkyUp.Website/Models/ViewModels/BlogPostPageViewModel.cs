﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class BlogPostPageViewModel
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public IEnumerable<BlogPostPageItemViewModel> Items { get; set; }
        public BlogPostPageItemViewModel Item { get; set; }
        public string Contact { get; set; }

        public int CategoryId { get; set; }
        public int? ParentCateId { get; set; }
    }

    public class BlogPostPageItemViewModel
    {
        public int Id { get; set; }
        //public int Group { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
    }
}
