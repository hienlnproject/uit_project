﻿using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wisky.SkyUp.Website.Models.Routing;

namespace Wisky.SkyUp.Website.Models.ViewModels
{

    public class RoutedPageViewModel
    {
        public Dictionary<string, object> ViewModels { get; set; } = new Dictionary<string, object>();

        public object this[string name]
        {
            get
            {
                return this.ViewModel<object>(name);
            }
        }

        public RouteInfo RouteInfo { get; set; }
        public StoreViewModel StoreInfo { get; set; }
        public StoreWebViewCounterViewModel ViewCounter { get; set; }
        public T ViewModel<T>(string name)
        {
            object result;
            if (this.ViewModels.TryGetValue(name, out result) && result is T)
            {
                return (T)result;
            }
            else
            {
                return default(T);
            }
        }

        /*
            Create General View Model
            Author: DucBM, KhanhNB
        */
        public object CreateGeneralViewModel(GeneralViewModelType type)
        {
            switch (type)
            {
                // 1. Layout
                case GeneralViewModelType.LayoutGeneralViewModel:
                    return (object)new LayoutGeneralViewModel()
                    {
                        Settings = this.ViewModel<IEnumerable<StoreWebSettingViewModel>>("Settings"),
                        CategoryTree = this.ViewModel<IEnumerable<ProductCategoryTreeViewModel>>("CategoryTree"),
                        News = this.ViewModel<BlogPostCollectionWithPostsViewModel>("News"),
                        Testimonials = this.ViewModel<BlogPostCollectionWithPostsViewModel>("Testimonials"),
                        HotProducts = this.ViewModel<ProductCollectionDetailsViewModel>("HotProducts"),
                        PopularProducts = this.ViewModel<ProductCollectionDetailsViewModel>("PopularProducts"),
                        BlogView = this.ViewModel<IEnumerable<BlogPostViewModel>>("BlogView"),

                    };
                // 2. Index
                case GeneralViewModelType.IndexGeneralViewModel:
                    return (object)new IndexGeneralViewModel()
                    {
                        MainSlider = this.ViewModel<ImageCollectionDetailsViewModel>("MainSlider"),
                        Features = this.ViewModel<ImageCollectionDetailsViewModel>("Features"),
                        Gallery = this.ViewModel<ImageCollectionDetailsViewModel>("Gallery"),
                        PartnerLogos = this.ViewModel<ImageCollectionDetailsViewModel>("PartnerLogos"),
                        PageIntroduction = this.ViewModel<WebPageViewModel>("PageIntroduction"),

                    };
                // 3. Products
                case GeneralViewModelType.ProductsGeneralViewModel:
                    return (object)new ProductsGeneralViewModel()
                    {
                        Category = this.ViewModel<ProductCategoryDetailsWithProductDetailsViewModel>("Category"),
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                        Suggestions = this.ViewModel<ProductCollectionDetailsViewModel>("Suggestion"),
                        CategoryDetails = this.ViewModel<ProductCategoryDetailsViewModel>("CategoryDetails"),
                        AllCategories = this.ViewModel<IEnumerable<ProductCategoryViewModel>>("AllCategories"),
                        AllProducts = this.ViewModel<IEnumerable<ProductViewModel>>("AllProducts"),
                        AllCategoryWithoutExtra = this.ViewModel<IEnumerable<ProductCategoryTreeViewModel>>("AllCategoryWithoutExtra"),
                    };
                // 4. Product details
                case GeneralViewModelType.ProductDetailsGeneralViewModel:
                    return (object)new ProductDetailsGeneralViewModel()
                    {
                        ProductDetails = this.ViewModel<ProductDetailsViewModel>("ProductDetails"),
                        RelatedProducts = this.ViewModel<IEnumerable<ProductViewModel>>("RelatedProducts"),
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                    };
                // 5. Blogs
                case GeneralViewModelType.BlogsGeneralViewModel:
                    return (object)new BlogsGeneralViewModel()
                    {
                        Blogs = this.ViewModel<BlogPostCollectionWithPostsViewModel>("Blogs"),
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                        Categories = this.ViewModel<List<BlogPostCollectionWithPostsViewModel>>("Categories"),
                        BlogView = this.ViewModel<IEnumerable<BlogPostViewModel>>("BlogView"),
                        BlogDetail = this.ViewModel<BlogPostViewModel>("BlogDetail"),
                    };
                // 6. Blog Details
                case GeneralViewModelType.BlogDetailsGeneralViewModel:
                    return (object)new BlogDetailsGeneralViewModel()
                    {
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                        BlogDetails = this.ViewModel<BlogPostDetailsViewModel>("BlogDetails"),
                    };
                // 7. About
                case GeneralViewModelType.AboutGeneralViewModel:
                    return (object)new AboutGeneralViewModel()
                    {
                        Page = this.ViewModel<WebPageViewModel>("Page"),
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                        OurTeam = this.ViewModel<BlogPostCollectionWithPostsViewModel>("OurTeam"),
                    };
                // 8. Contact
                case GeneralViewModelType.ContactGeneralViewModel:
                    return (object)new ContactGeneralViewModel()
                    {
                        Page = this.ViewModel<WebPageViewModel>("Page"),

                    };
                // 9. Shopping Cart
                case GeneralViewModelType.ShoppingCartGeneralViewModel:
                    return (object)new ShoppingCartGeneralViewModel()
                    {
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),

                    };
                // 10. Checkout
                case GeneralViewModelType.CheckoutGeneralViewModel:
                    return (object)new CheckoutGeneralViewModel()
                    {
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),

                    };
                // 10. Sidebar
                case GeneralViewModelType.SidebarGeneralViewModel:
                    return (object)new SidebarGeneralViewModel()
                    {
                        Menu = this.ViewModel<WebPageViewModel>("Menu"),
                    };
                // 11. Search 
                case GeneralViewModelType.SearchGeneralViewModel:
                    return (object)new SearchGeneralViewModel
                    {
                        MatchedProducts = this.ViewModel<IEnumerable<ProductViewModel>>("MatchedProducts"),
                        RecommendedProducts = this.ViewModel<ProductCollectionDetailsViewModel>("RecommendedProducts"),
                    };
                // 12. Gallery 
                case GeneralViewModelType.GalleryGeneralViewModel:
                    return (object)new GalleryGeneralViewModel
                    {
                        Banner = this.ViewModel<ImageCollectionDetailsViewModel>("Banner"),
                        Gallery = this.ViewModel<ImageCollectionDetailsViewModel>("Gallery"),
                    };
            }
            return null;
        }

        public string ResolveViewPath(string viewFolder, string viewName)
        {
            //View path default when ThemeId field is NULL
            string viewPath = string.Format("~/Views/{0}/{1}", viewFolder, viewName);
            //if (this.StoreInfo.ThemeId.HasValue)
            //{
            //    //ThemeId has value to find theme Views
            //    var themeApi = new ThemeApi();
            //    string themeShortName = themeApi.GetShortNameById(this.StoreInfo.ThemeId.Value);
            //    viewPath= string.Format("~/Views/{0}/{1}", themeShortName, viewName);
            //}
            return viewPath;
        }

    }

}

public enum GeneralViewModelType : int
{
    LayoutGeneralViewModel = 0,
    IndexGeneralViewModel = 1,
    ProductsGeneralViewModel = 2,
    ProductDetailsGeneralViewModel = 3,
    BlogsGeneralViewModel = 4,
    BlogDetailsGeneralViewModel = 5,
    AboutGeneralViewModel = 6,
    ContactGeneralViewModel = 7,
    ShoppingCartGeneralViewModel = 8,
    CheckoutGeneralViewModel = 9,
    SidebarGeneralViewModel = 10,
    SearchGeneralViewModel = 11,
    GalleryGeneralViewModel = 12,
}