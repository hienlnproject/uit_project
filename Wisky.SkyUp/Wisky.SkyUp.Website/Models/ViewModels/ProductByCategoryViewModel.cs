﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class ProductByCategoryViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string SeoName { get; set; }
        public IEnumerable<ProductByCategoryItemViewModel> Products { get; set; }
    }

    public class ProductByCategoryItemViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string SeoName { get; set; }
        public string PicUrl { get; set; }
        public string Description { get; set; }
        public string FormattedPrice { get; set; }
    }
}