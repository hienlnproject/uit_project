﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class ProductDetailViewModel
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public int DiscountedPrice { get; set; }
        public string PicURL { get; set; }
        public int DiscountPrice { get; set; }
        public string Description { get; set; }
    }
}