﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.ViewModels
{
    public class CartJSON
    {
        public CartItemJSON[] Items { get; set; }
    }

    public class CartItemJSON
    {
        public int id { get; set; }
        public int quantity { get; set; }
    }
}