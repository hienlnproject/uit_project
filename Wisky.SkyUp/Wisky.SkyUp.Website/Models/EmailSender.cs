﻿using System.Linq;
using System.Net;
using System.Net.Mail;
using HmsService.ViewModels;
using Limilabs.Client.POP3;
using Limilabs.Mail;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WiSky.SkyUp.Website.Models
{
    public class EmailSender
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpEmailAddress { get; set; }
        public string SmtpEmailName { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool UseSsl { get; set; }

        public EmailSender() { }

        public EmailSender(StoreWebSettingViewModel[] settings)
        {
            this.SmtpHost = settings.First(q => q.Name == "EmailHostName").Value;
            this.SmtpPort = int.Parse(settings.First(q => q.Name == "EmailPort").Value);
            this.SmtpEmailAddress = settings.First(q => q.Name == "EmailUsername").Value;
            this.SmtpEmailName = settings.First(q => q.Name == "EmailDisplayName").Value;
            this.SmtpUsername = settings.First(q => q.Name == "EmailUsername").Value;
            this.SmtpPassword = settings.First(q => q.Name == "EmailPassword").Value;
            this.UseSsl = settings.First(q => q.Name == "EmailRequiredSSL").Value == "true";
        }

        public void Send(string targetAddress, string subject, string content)
        {
            // Command line argument must the the SMTP host.
            SmtpClient client = new SmtpClient(this.SmtpHost, this.SmtpPort);
            client.Credentials = new NetworkCredential(this.SmtpUsername, this.SmtpPassword);
            client.EnableSsl = this.UseSsl;
            // Specify the e-mail sender.
            // Create a mailing address that includes a UTF8 character
            // in the display name.
            MailAddress from = new MailAddress(this.SmtpEmailAddress, this.SmtpEmailName, System.Text.Encoding.UTF8);
            // Set destinations for the e-mail message.
            MailAddress to = new MailAddress(targetAddress);
            // Specify the message content.
            MailMessage message = new MailMessage(from, to);
            message.Body = content;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = subject;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            client.Send(message);
        }

        public void SendPop3()
        {
            Outlook.Application app = new Outlook.Application();
            
            Outlook.MailItem mailItem = app.CreateItem(Outlook.OlItemType.olMailItem);
        }

    }
}