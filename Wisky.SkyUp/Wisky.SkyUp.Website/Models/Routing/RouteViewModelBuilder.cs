﻿using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Resources;
using System.Reflection;
using Wisky.SkyUp.Website.Models.ViewModels;
using SkyWeb.DatVM.Mvc.Autofac;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Models.Routing
{

    public class RouteViewModelBuilder
    {

        public async static Task<object> CreateViewModel(RouteViewModelType type, string inputValue, StoreViewModel store)
        {
            switch (type)
            {
                case RouteViewModelType.ProductCollections:
                    return await GetProductCollections(store);

                case RouteViewModelType.ProductCollection:
                    return await GetProductCollection(TryParseInt(inputValue), store);

                case RouteViewModelType.ProductCategories:
                    return await GetStoreCategories(store);

                case RouteViewModelType.ProductCategoriesByBrand:
                    return await GetBrandCategories(store);

                case RouteViewModelType.CategoryDetails:
                    return await GetCategoryDetails(inputValue, store);

                case RouteViewModelType.ProductDetails:
                    return await GetProductDetails(inputValue, store);

                case RouteViewModelType.ProductDetailByBrandId:
                    return await GetProductDetailsByBrandId(inputValue, store);

                case RouteViewModelType.StoreProducts:
                    return await GetStoreProducts(store);

                case RouteViewModelType.ProductCategoriesTree:
                    return GetProductCategoriesTree(store);
                case RouteViewModelType.ProductCategoriesExtraTree:
                    return GetProductCategoriesExtraTree(store);

                case RouteViewModelType.ProductCategoriesTreeWithoutExtra:
                    return GetProductCategoriesTreeWithoutExtra(store);

                case RouteViewModelType.ProductCategoriesSubTreeByGroup:
                    return GetProductCategoriesSubTreeByGroup(store, TryParseInt(inputValue));

                case RouteViewModelType.ProductSearchCollection:
                    return GetProductByKeyword(inputValue, store);

                case RouteViewModelType.ProductSearchCollectionByBrandId:
                    return GetProductByKeywordUseBrandId(inputValue, store);

                case RouteViewModelType.SearchProductByName:
                    return SearchProductByName(inputValue, store);

                case RouteViewModelType.ProductCategoriesWithProduct:
                    return GetStoreCategoriesWithProduct(store);

                case RouteViewModelType.ProductSubCategories:
                    return GetSubCategories(inputValue, store);

                //case RouteViewModelType.ProductCollectionJS:
                //    return GetProductCollectionRJ(TryParseInt(inputValue), store);

                case RouteViewModelType.LikelyProducts:
                    return GetLikelyProducts(inputValue, store);

                case RouteViewModelType.StoreProductsWithSpecs:
                    return await GetStoreProductsWithSpecs(store);

                case RouteViewModelType.AllProductsByStore:
                    return await GetAllProductsByStore(store);

                case RouteViewModelType.LikelyRandomProducts:
                    return ProductRandomInCategory(store, inputValue);

                case RouteViewModelType.BlogCategory:
                    throw new NotImplementedException();
                case RouteViewModelType.BlogDetails:
                    return await GetBlogPostAsync(inputValue, store);
                case RouteViewModelType.BlogCollections:
                    throw new NotImplementedException();
                case RouteViewModelType.BlogCollection:
                    return await GetBlogCollectionAsync(TryParseInt(inputValue), store);
                case RouteViewModelType.BlogSearchCollection:
                    return GetBlogPostByKeyword(inputValue, store);
                case RouteViewModelType.StoreBlogs:
                    return await GetStoreBlogs(store);
                case RouteViewModelType.BlogDetailsWithImages:
                    return await GetBlogPostDetailAsync(inputValue, store);

                //Get BlogCollection with all All Collection and Blog Details (Like ProductCategory 108)
                case RouteViewModelType.BlogCollectionWithSubAndBlogDetails:
                    return GetBlogCollectionWithSubAndBlogDetails(TryParseInt(inputValue), store);

                case RouteViewModelType.BlogCollectionWithBlogPostDetails:
                    return GetBlogCollectionWithBlogPostDetails(TryParseInt(inputValue), store);

                case RouteViewModelType.ImageCollections:
                    return await GetImageCollections(store);
                case RouteViewModelType.ImageCollection:
                    return await GetImageCollection(TryParseInt(inputValue), store);

                case RouteViewModelType.WebPage:
                    return await GetWebPage(TryParseInt(inputValue), store);

                case RouteViewModelType.WebSettings:
                    return GetStoreWebSettings(store);

                case RouteViewModelType.LanguagePackage:
                    return GetResourceManager(inputValue, store);
                case RouteViewModelType.ListCategories:
                    return ListCategories(store);

                // DucBM
                case RouteViewModelType.CategoryDetailsWithSpecs:
                    return await GetCategoryDetailsWithProductDetails(inputValue, store);

                //Yan
                case RouteViewModelType.ProductCategoryViewModel:
                    return GetProductCategoriesDetailTree(inputValue);

                case RouteViewModelType.ProductCollectionUsingSeo:
                    return GetProductCollectionBySeoName(inputValue, store);
                default:
                    return null;
            }
        }

        private static IEnumerable<ProductViewModel> ProductRandomInCategory(StoreViewModel store, string seoName)
        {
            //int count = category.Count();
            //int index = new Random().Next(count);
            //var cate = category.OrderBy(a => a.Id).Skip(index).FirstOrDefault();
            //var product = cate.Products.Take(7).ToList();
            var result = new ProductApi().GetProductOfCategory(seoName, store.ID);
            int count = result.Count();
            int index = new Random().Next(count);
            return result.Skip(index).Take(8);
        }

        private static ResourceManager GetResourceManager(string inputValue, StoreViewModel store)
        {
            return new ResourceManager(inputValue, Assembly.GetExecutingAssembly());
        }

        private static async Task<BlogPostDetailsViewModel> GetBlogPostDetailAsync(string seoname, StoreViewModel store)
        {
            var result = await new BlogPostApi().GetDetailsBySeoNameAsync(seoname, store.ID);

            if (result == null || !result.BlogPost.Active || result.BlogPost.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        private static List<BlogPostCollectionWithPostsViewModel> GetBlogCollectionWithSubAndBlogDetails(int? id, StoreViewModel store)
        {
            if (id == null) return null;
            var api = new BlogPostCollectionApi();
            var collectionIds = api.GetActiveByStoreId(store.ID).Where(a => a.ParentId == id && a.Active).Select(q => q.Id);
            List<BlogPostCollectionWithPostsViewModel> collection = new List<BlogPostCollectionWithPostsViewModel>();
            foreach (var item in collectionIds)
            {
                var blogPost = api.GetDetails(item);
                collection.Add(blogPost);
            }
            return collection;
        }

        private static List<BlogPostDetailsViewModel> GetBlogCollectionWithBlogPostDetails(int? id, StoreViewModel store)
        {
            var blogPostApi = new BlogPostApi();

            var listBlogPost = blogPostApi.GetBlogPostByCollectionId(id);

            List<BlogPostDetailsViewModel> result = new List<BlogPostDetailsViewModel>();
            foreach (var item in listBlogPost)
            {
                var blogPostDetail = (blogPostApi.GetDetailsByStoreId(item.Id, store.ID));
                if (blogPostDetail != null)
                {
                    result.Add(blogPostDetail);
                }
            }
            return result;
        }

        private static async Task<IEnumerable<BlogPostViewModel>> GetStoreBlogs(StoreViewModel store)
        {
            var result = await new BlogPostApi().GetByStoreIdAsync(store.ID, false);
            return result;
        }

        private static IEnumerable<ProductCategoryDetailsViewModel> GetStoreCategoriesWithProduct(StoreViewModel store)
        {
            return new ProductCategoryApi().GetStoreCategoriesWithProduct(store.ID);
        }
        private static IEnumerable<ProductViewModel> GetLikelyProducts(string seoname, StoreViewModel store)
        {
            var result = new ProductApi().GetLikelyProducts(seoname, store.ID);
            return result;
        }

        private static IEnumerable<ProductCategoryDetailsViewModel> GetSubCategories(string seoName, StoreViewModel store)
        {
            var result = new ProductCategoryApi().GetSubCategories(seoName, store.ID);
            return result;
        }
        private static IEnumerable<BlogPostViewModel> GetBlogPostByKeyword(string keyword, StoreViewModel store)
        {
            var result = new BlogPostApi().GetMatchingCollection(store.ID, keyword);
            return result;
        }

        private static IEnumerable<ProductViewModel> GetProductByKeyword(string keyword, StoreViewModel store)
        {
            var result = new ProductApi().GetMatchingCollection(store.ID, keyword);
            return result;
        }

        private static IEnumerable<ProductViewModel> SearchProductByName(string keyword, StoreViewModel store)
        {
            var result = new ProductApi().SearchProductByName(store.ID, keyword);
            return result;
        }

        /*
         * Author: Phudcq
         * Method: Get product by keyword and brandId
         * Return: ProductViewModel
         */

        private static IEnumerable<ProductViewModel> GetProductByKeywordUseBrandId(string keyword, StoreViewModel store)
        {
            var result = new ProductApi().GetMatchingCollection(store.BrandId.Value, keyword);
            return result;
        }

        public static async Task<ProductCollectionDetailsViewModel> GetProductCollection(int? id, StoreViewModel store)
        {
            if (id == null) { return null; }

            var result = await new ProductCollectionApi()
                .GetDetails(id.Value);

            if (result == null || !result.ProductCollection.Active || result.ProductCollection.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        //public static async Task<IEnumerable<object>> GetProductCollectionRJ(int? id, StoreViewModel store)
        //{
        //    if (id == null) { return null; }
        //    var result = await new ProductCollectionApi()
        //        .GetDetails(id.Value);
        //    if (result==null || !result.ProductCollection.Active || result.ProductCollection.StoreId!=store.ID)
        //    {
        //        return null;
        //    }
        //    var data = result.Items.Select(a => new
        //    {
        //        catID = a.Product.CatID,
        //        productID = a.Product.ProductID,
        //        productName = a.Product.ProductName,
        //        prodSeoName = a.Product.SeoName,
        //        price = a.Product.Price,
        //        discountPrice = a.Product.DiscountPrice,
        //        image = a.Product.PicURL != null ? a.Product.PicURL : "a",
        //    });

        //    return data;
        //} 


        #region Product & Product Categories & Product Collections

        private static async Task<IEnumerable<ProductCollectionViewModel>> GetProductCollections(StoreViewModel store)
        {
            return await new ProductCollectionApi()
                .GetByStoreIdAsync(store.ID);
        }

        private static async Task<IEnumerable<ProductCategoryViewModel>> GetStoreCategories(StoreViewModel store)
        {
            return await new ProductCategoryApi()
                .GetByStoreIdAsync(store.ID);
        }

        private static async Task<IEnumerable<ProductCategoryViewModel>> GetBrandCategories(StoreViewModel store)
        {
            return await new ProductCategoryApi()
                .GetByBrandIdAsync((int)store.BrandId);
        }

        private static async Task<ProductCategoryDetailsViewModel> GetCategoryDetails(string seoName, StoreViewModel store)
        {
            var result = await new ProductCategoryApi().GetCategoryDetails(seoName, store.ID);
            if (result == null || result.Category.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        // author: DucBM
        private static async Task<ProductCategoryDetailsWithProductDetailsViewModel> GetCategoryDetailsWithProductDetails(string seoName, StoreViewModel store)
        {
            var result = await new ProductCategoryApi().GetCategoryDetailsWithProductDetails(seoName, store.ID);

            if (result == null || result.Category.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        private static async Task<ProductDetailsViewModel> GetProductDetails(string seoName, StoreViewModel store)
        {
            var result = await new ProductApi().GetProductDetailsAsync(seoName, (int)store.BrandId);//BaoTD: store.ID => (int)store.BrandId

            if (result == null || !result.Product.IsAvailable)
            {
                return null;
            }

            //if (result == null || !result.Product.IsAvailable.GetValueOrDefault() || result.ProductCategory.StoreId != store.ID)
            //{
            //    return null;
            //}

            return result;
        }

        /*
         * Author: BaoTD
         * Method: Get product details by seoName and brandId
         * Return: ProductDetailsViewModel
         */
        private static async Task<ProductDetailsViewModel> GetProductDetailsByBrandId(string seoName, StoreViewModel store)
        {
            var result = await new ProductApi().GetProductDetailsAsync(seoName, store.BrandId.Value);

            if (result == null || !result.Product.IsAvailable)
            {
                return null;
            }

            return result;
        }

        private static async Task<IEnumerable<ProductViewModel>> GetStoreProducts(StoreViewModel store)
        {
            return await new ProductApi().GetActiveByStoreIdAsync(store.ID);
        }

        private static async Task<IEnumerable<ProductViewModel>> GetAllProductsByStore(StoreViewModel store)
        {
            return await new ProductApi().GetActiveByBrandId(store.BrandId.Value);
        }

        private static async Task<IEnumerable<ProductDetailsViewModel>> GetStoreProductsWithSpecs(StoreViewModel store)
        {
            return await new ProductApi().GetActiveWithSpecsByStoreIdAsync(store.ID);
        }

        private static IEnumerable<ProductCategoryTreeViewModel> GetProductCategoriesTree(StoreViewModel store)
        {
            return new ProductCategoryApi().GetFullTreeByStore(store.ID);
        }
        private static IEnumerable<ProductCategoryViewModel> GetProductCategoriesExtraTree(StoreViewModel store)
        {
            return new ProductCategoryApi().GetFullExtraTreeByStore(store.ID);
        }
        private static IEnumerable<ProductCategoryTreeViewModel> GetProductCategoriesTreeWithoutExtra(StoreViewModel store)
        {
            return new ProductCategoryApi().GetFullTreeByBrandWithoutExtra(store.BrandId.Value);
        }
        private static IEnumerable<ProductCategoryTreeViewModel> GetProductCategoriesSubTreeByGroup(StoreViewModel store, int? groupCate)
        {
            //return new ProductCategoryApi().GetSubTreeByGroup(store.BrandId.Value, groupCate);
            return null;
        }

        public static ProductCategoryViewModel GetProductCategoriesDetailTree(string seoName)
        {
            //return new ProductCategoryApi().GetProductCategoryDetailTree(seoName);
            return null;
        }

        public static ProductCollectionDetailsViewModel GetProductCollectionBySeoName(string seoName, StoreViewModel store)
        {
            if (seoName == null) { return null; }

            var result = new ProductCollectionApi().GetDetailsBySeoName(seoName, store);

            if (result == null || !result.ProductCollection.Active || result.ProductCollection.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        #endregion

        #region Blog Post & Blog post Collections

        private static async Task<BlogPostCollectionWithPostsViewModel> GetBlogCollectionAsync(int? id, StoreViewModel store)
        {
            if (id == null) { return null; }

            var result = await new BlogPostCollectionApi().GetDetailsAsync(id.Value);

            if (result == null || !result.Active || result.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        private static async Task<BlogPostViewModel> GetBlogPostAsync(string seoName, StoreViewModel store)
        {
            var result = await new BlogPostApi().GetBlogPostBySeoNameAsync(seoName, store.ID);

            if (result == null || !result.Active || result.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        #endregion

        #region Image Collections

        private static async Task<IEnumerable<ImageCollectionViewModel>> GetImageCollections(StoreViewModel store)
        {
            return await new ImageCollectionApi()
                .GetByStoreIdAsync(store.ID);
        }

        private static async Task<ImageCollectionDetailsViewModel> GetImageCollection(int? collectionId, StoreViewModel store)
        {
            if (collectionId == null) { return null; }

            var result = await new ImageCollectionApi().GetByStoreIdAsync(collectionId ?? 0, store.ID);

            if (result == null || !result.ImageCollection.Active || result.ImageCollection.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        #endregion

        #region Web Page

        private static async Task<WebPageViewModel> GetWebPage(int? webPageId, StoreViewModel store)
        {
            if (webPageId == null) { return null; }

            var result = await new WebPageApi().GetAsync(webPageId);

            if (result == null || !result.IsActive || result.StoreId != store.ID)
            {
                return null;
            }

            return result;
        }

        #endregion

        #region Web Settings

        private static IEnumerable<StoreWebSettingViewModel> GetStoreWebSettings(StoreViewModel store)
        {
            return new StoreWebSettingApi().GetActiveByStore(store.ID);
        }


        #endregion

        #region Helper Methods

        private static int? TryParseInt(string inputValue)
        {
            int result;
            if (int.TryParse(inputValue, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        #endregion

        //CategoryId return List<int> PHUONGTA
        public static async Task<List<int>> ListCategories(StoreViewModel store)
        {
            var productCategoryApi = new ProductCategoryApi();
            var categories = await productCategoryApi.GetByStoreIdAsync(store.ID);
            List<int> cateIds = null;
            foreach (var item in categories)
            {
                cateIds.Add(item.CateID);
            }
            return cateIds;
        }
    }

    public enum RouteViewModelType : int
    {
        ProductCollections = 100,
        ProductCollection = 101,
        ProductCategories = 102,
        ProductCategoriesByBrand = 802,
        CategoryDetails = 103,
        ProductDetails = 104,
        ProductDetailByBrandId = 804, //Author: Bao
        StoreProducts = 105,
        AllProductsByStore = 805,
        ProductCategoriesTree = 106,
        ProductCategoriesExtraTree = 117,
        ProductCategoriesTreeWithoutExtra = 806,//Author: Phu
        ProductCategoriesSubTreeByGroup = 906,//Author: Phu
        ProductSearchCollection = 107,
        ProductSearchCollectionByBrandId = 907, //Author: Phu
        ProductCategoriesWithProduct = 108,
        ProductSubCategories = 109,
        LikelyProducts = 110,
        StoreProductsWithSpecs = 111,
        LikelyRandomProducts = 112,
        //ProductCollectionJS = 113,
        CategoryDetailsWithSpecs = 114,
        ProductCategoryViewModel = 115, //Author: Yan
        SearchProductByName = 116,

        ProductCollectionUsingSeo = 169,

        BlogCategory = 200,
        BlogDetails = 201,
        BlogCollections = 202,
        BlogCollection = 203,
        BlogSearchCollection = 204,
        StoreBlogs = 205,
        BlogDetailsWithImages = 206,
        BlogCollectionWithSubAndBlogDetails = 207,
        BlogCollectionWithBlogPostDetails = 208,

        ImageCollections = 300,
        ImageCollection = 301,

        WebPage = 400,

        WebSettings = 500,

        LanguagePackage = 600,

        ListCategories = 700
    }

}
