﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wisky.SkyUp.Website.Models.Routing
{

    public class RouteInfo
    {
        public StoreWebRouteWithModelsViewModel StoreWebRoute { get; set; }
        public Dictionary<string, string> RouteValues { get; set; } = new Dictionary<string, string>();
        
    }
    
}