﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Models.ViewModels;

namespace Wisky.SkyUp.Website.Models.Routing
{

    public class RouteWorker
    {

        public RouteInfo GlobalRoute { get; private set; }
        public RouteInfo ResultRoute { get; private set; }

        public RouteWorker(StoreWebRoutesViewModel storeRoutes, string parameters, NameValueCollection query)
        {
            this.GlobalRoute = new RouteInfo()
            {
                StoreWebRoute = storeRoutes.GlobalRoute,
            };
            this.ResultRoute = this.ProcessRoutes(storeRoutes, parameters, query);
        }

        private RouteInfo ProcessRoutes(StoreWebRoutesViewModel storeRoutes, string parameters, NameValueCollection query)
        {
            if (parameters.StartsWith("/"))
            {
                parameters = parameters.Substring(1);
            }

            var parameterParts = parameters.Split('/').Where(q => !string.IsNullOrEmpty(q)).ToArray();
            if (parameterParts.Length == 0)
            {
                parameterParts = new string[] { "", };
            }

            foreach (var route in storeRoutes.StoreWebRoutes)
            {
                // Skip system routes
                if (route.StoreWebRoute.Path.StartsWith("["))
                {
                    continue;
                }

                var result = new RouteInfo()
                {
                    StoreWebRoute = route,
                };

                // Split the path into parts
                var routeParts = route.StoreWebRoute.Path.Split('/');
                bool acceptable = true;

                for (int i = 0; i < routeParts.Length && acceptable; i++)
                {
                    var parameterName = routeParts[i];

                    var isVariable = parameterName.StartsWith("{") || parameterName.EndsWith("}");
                    if (isVariable)
                    {
                        parameterName = parameterName.Substring(1, parameterName.Length - 2);

                        var variableInfo = route.Models.FirstOrDefault(q => q.IdRouteValueName == parameterName);
                        var isOptional = variableInfo != null && variableInfo.IdRouteDefaultValue != null;

                        if (!isOptional && i >= parameterParts.Length)
                        {
                            acceptable = false;
                            break;
                        }

                        // Get the parameter value, or get default value if there is none
                        string parameterValue;
                        if (i >= parameterName.Length)
                        {
                            parameterValue = variableInfo.IdRouteDefaultValue;
                        }
                        else
                        {
                            parameterValue = parameterParts[i];
                        }
                        result.RouteValues.Add(parameterName, parameterValue);
                    }
                    else
                    {
                        if (i >= parameterParts.Length || parameterParts[i] != parameterName)
                        {
                            acceptable = false;
                            break;
                        }
                    }
                }

                if (!acceptable)
                {
                    continue;
                }

                foreach (string queryKey in query)
                {
                    if (!result.RouteValues.ContainsKey(queryKey))
                    {
                        result.RouteValues.Add(queryKey, query[queryKey]);
                    }
                }

                return result;
            }

            return null;
        }

        public async Task<bool> BuildViewModels(RoutedPageViewModel model)
        {
            // Assign ViewModel of the View and the Global
            var modelList = this.ResultRoute.StoreWebRoute.Models;

            if (this.GlobalRoute?.StoreWebRoute?.Models != null)
            {
                modelList = modelList.Concat(this.GlobalRoute.StoreWebRoute.Models);
            }

            foreach (var routeModel in modelList)
            {
                string inputValue = null;

                // Try getting input value either from request parameter, or default value if any
                if (string.IsNullOrEmpty(routeModel.IdRouteValueName) ||
                    !this.ResultRoute.RouteValues.TryGetValue(routeModel.IdRouteValueName, out inputValue))
                {
                    inputValue = routeModel.IdRouteDefaultValue;
                }

                var viewModel = await RouteViewModelBuilder.CreateViewModel((RouteViewModelType)routeModel.ViewModelType, inputValue, model.StoreInfo);

                if (viewModel == null)
                {
                    return false;
                }

                model.ViewModels.Add(routeModel.Name, viewModel);
            }

            return true;
        }

    }

}