﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyUp.Website.Controllers;

namespace Wisky.SkyUp.Website.Filters
{

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class StoreAdminAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller as DomainBasedController;

            if (!controller.IsPageAdmin)
            {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }


        }
    }

}