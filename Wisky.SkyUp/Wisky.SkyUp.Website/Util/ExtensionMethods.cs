﻿using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyUp.Website.Util
{
    public static class ExtensionMethods
    {

        public static int RandomValue(this int value)
        {
            var random = new Random();
            return random.Next();
        }
        public static IHtmlString GetWebSetting(this HtmlHelper helper, IEnumerable<HmsService.ViewModels.StoreWebSettingViewModel> settings, string name)
        {
            return helper.Raw(settings?.FirstOrDefault(a => a.Name.Equals(name))?.Value ?? "");
        }

        public static int GetWebSettingInt(this IEnumerable<StoreWebSettingViewModel> settings, string name)
        {
            var valueStr = settings?.FirstOrDefault(a => a.Name.Equals(name))?.Value ?? "0";
            int value = 0;
            int.TryParse(valueStr, out value);
            return value;
        }

        public static string LanguageVersionUrl(this UrlHelper helper, string directory)
        {
            if (string.IsNullOrWhiteSpace(directory))
            {
                return "/" + helper.RequestContext.RouteData.Values["parameters"];
            }
            return "/" + directory + "/" + helper.RequestContext.RouteData.Values["parameters"];
        }

        public static string GetSpecificationValue(this IEnumerable<ProductSpecificationViewModel> specs, string name)
        {
            var spec = specs.FirstOrDefault(a => a.Name.ToLower().Equals(name.ToLower()));
            return spec?.Value;
        }

        public static string GetSpecificationDateValue(this IEnumerable<ProductSpecificationViewModel> specs,
            string name, string formatSpec, string formatValue, string defaultString = "")
        {
            var spec = specs.FirstOrDefault(a => a.Name.ToLower().Equals(name.ToLower()));
            DateTime date;
            if (DateTime.TryParseExact(spec.Value, formatSpec, null, System.Globalization.DateTimeStyles.None, out date))
            {
                return date.ToString(formatValue);
            }
            return defaultString;
        }

        public static string UrlWithDirectory(this HtmlHelper helper, params string[] url)
        {

            var finalUrl = helper.ViewContext.ViewBag.Directory;
            var count = 0;
            foreach (var item in url)
            {
                count++;
                if (!string.IsNullOrWhiteSpace(item))
                {
                    if (count == url.Length && item.Contains("?"))
                    {
                        finalUrl += item;
                    }
                    else
                    {
                        finalUrl += Uri.EscapeDataString(item.Trim());
                    }
                    if (count < url.Length)
                    {
                        finalUrl += "/";
                    }
                }
            }
            return finalUrl;
        }
    }
}