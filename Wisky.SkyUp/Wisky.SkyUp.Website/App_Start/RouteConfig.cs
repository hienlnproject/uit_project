﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Wisky.SkyUp.Website
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Feedback",
                url: "SWFeedback/{action}",
                defaults: new { controller = "Feedback", action = "Submit" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );

            routes.MapRoute(
                name: "Cart With Directory",
                url: "{parameters}/SWCart/{action}",
                defaults: new { controller = "Cart" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );

            //routes.MapRoute(
                
            //    name: "Cate",
            //    url: "danh-muc/{*params}",
            //    defaults: new { controller = "Category"})

            routes.MapRoute(
                name: "Cart",
                url: "SWCart/{action}",
                defaults: new { controller = "Cart" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );

            routes.MapRoute(
               name: "Login",
               url: "Login/{action}",
               defaults: new { controller = "Login" },
               namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
           );

            routes.MapRoute(
                name: "Count",
                url: "SWCount/{action}",
                defaults: new { controller = "Count" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );
            routes.MapRoute(
                name: "CountEN",
                url: "en/SWCount/{action}",
                defaults: new { controller = "Count" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );
            routes.MapRoute(
                name: "Manage",
                url: "SWManage/{action}",
                defaults: new { controller = "Manage", action = "Index" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );

            routes.MapRoute(
                name: "Sort",
                url: "SWSort/{action}",
                defaults: new { controller = "Sort" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );
            routes.MapRoute(
              name: "Favorite",
              url: "Favorite/{action}",
              defaults: new { controller = "Favorite" },
              namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
          );
            routes.MapRoute(
                name: "Default",
                url: "{*parameters}",
                defaults: new { controller = "Routing", action = "Index" },
                namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            );
           
            //routes.MapRoute(
            //    name: "Default",
            //    url: "danh-muc/{parentCategory}/{category}",
            //    defaults: new { controller = "Routing", action = "Index" },
            //    namespaces: new string[] { "Wisky.SkyUp.Website.Controllers", }
            //);
        }
    }
}
