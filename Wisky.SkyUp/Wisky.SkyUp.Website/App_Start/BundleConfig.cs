﻿using System.Web;
using System.Web.Optimization;

namespace Wisky.SkyUp.Website
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/plugins/bootstrap/bootstrap.min.css",
                      "~/Content/plugins/font-awesome.min.css",
                      "~/Content/plugins/swiper.min.css",
                      "~/Content/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css",
                      "~/Content/plugins/slick/slick.css",
                      "~/Content/plugins/swal/sweetalert.css",
                      "~/Content/plugins/alertifyjs-v1.10.0/css/alertify.min.css",
                      "~/Content/plugins/alertifyjs-v1.10.0/css/themes/bootstrap.min.css",
                      "~/Content/plugins/rating-start/star-rating.min.css",                      
                      "~/Content/css/theme.css",                      
                      "~/Content/css/style.css",
                      "~/Content/css/about.css",
                      "~/Content/css/product.css",
                      "~/Content/css/shopping-cart.css",
                      "~/Content/css/checkout.css",
                      "~/Content/css/sign-in.css",
                      "~/Content/css/size-chart.css",
                      "~/Content/css/return-exchange.css",
                      "~/Content/css/forgot-password.css",
                      "~/Content/css/product-details.css",
                      "~/Content/css/register.css",
                      "~/Content/css/account.css",
                      "~/Content/css/change-password.css",
                      "~/Content/plugins/jssocials-1.4.0/dist/jssocials.css",
                      "~/Content/plugins/jssocials-1.4.0/dist/jssocials-theme-flat.css")); // thu tu load quan trong day, load thu vien truoc khi load dam' con lai ^^

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                      "~/Content/plugins/jquery-1.11.3.min.js",
                      "~/Content/plugins/bootstrap/bootstrap.min.js",
                      "~/Content/plugins/swiper.min.js",
                      "~/Content/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js",
                      "~/Content/plugins/slick/slick.min.js",
                      "~/Content/plugins/swal/sweetalert.min.js",
                      "~/Content/plugins/alertifyjs-v1.10.0/alertify.min.js",
                      "~/Content/plugins/rating-start/star-rating.min.js",

                        // AngularJS libraries
                        "~/Content/js/angularjs/angular.min.js",
                        "~/Content/js/angularjs/angular-route.min.js",
                        "~/Content/js/angularjs/ngStorage.min.js",
                        "~/Content/js/angularjs/ui-bootstrap-tpls-0.13.1.min.js",

                        "~/Content/js/angularjs/pagination/dirPagination.js",
                        //"~/Content/js/angularjs/pagination/dirPagination.spec.js",
                        //"~/Content/js/angularjs/pagination/dirPagination.tpl.html",
                        // config file
                        "~/Content/js/orderApplication/app.js",
                        // AngularJS services
                        "~/Content/js/orderApplication/services/productService.js",
                        "~/Content/js/orderApplication/services/extraService.js",
                        // AngularJS controllers
                        "~/Content/js/orderApplication/controllers/modalController.js",
                        "~/Content/js/orderApplication/controllers/productController.js",
                        "~/Content/js/orderApplication/controllers/productDetailController.js",
                        "~/Content/js/orderApplication/controllers/orderController.js",
                        "~/Content/js/orderApplication/controllers/deliveryController.js",
                        "~/Content/js/orderApplication/directives/errorDirective.js",

                      "~/Content/js/lang.vi.js",
                      "~/Content/js/lang.en.js",
                      "~/Content/js/moment.min.js",                      
                      "~/Content/js/combodate.js", 
                      "~/Content/plugins/jssocials-1.4.0/dist/jssocials.js",
                      "~/Content/js/mustache.min.js",
                      "~/Content/js/jquery.nicescroll.min.js",
                      "~/Content/js/script.js"));

        }
    }
}