﻿function GetProductSold(isViewAll, viewDate) {
    
    var checkView = {
        "viewAll": isViewAll,
        "dateView": viewDate
        };

    $.ajax({
        url: "/Dashboard/GetProductSold",
        type: "POST",
        data: JSON.stringify(checkView),
        contentType: 'application/json',
        success: function (result) {
            $("#tableProductSold").fadeOut(100, function () {
                $('#tableProductSold').html(result);
                $("#tableProductSold").fadeIn(100);

            });
            
        }
    });
}

//Month
function GetProductSoldMonth(isViewAll, viewDate) {
    
    var checkView = {
        "viewAll": isViewAll,
        "dateView": viewDate
    };

    $.ajax({
        url: "/Report/GetProductSold",
        type: "POST",
        data: JSON.stringify(checkView),
        contentType: 'application/json',
        success: function (result) {
            $("#tableProductSold").fadeOut(100, function () {
                $('#tableProductSold').html(result);
                $("#tableProductSold").fadeIn(100);

            });
            
        }
    });
}


function ViewAll() {
    GetProductSold(true, $("#datepicker").val());
    $("#ViewAll").addClass("hidden");
    $("#ViewShort").removeClass('hidden');
}

function ViewAllMonth() {
    GetProductSoldMonth(true, $("#datepicker").val());
    $("#ViewAll").addClass("hidden");
    $("#ViewShort").removeClass('hidden');
}

function ViewShort() {
    GetProductSold(false, $("#datepicker").val());
    $("#ViewShort").addClass("hidden");
    $("#ViewAll").removeClass('hidden');
}

function ViewShortMonth() {
    GetProductSoldMonth(false, $("#datepicker").val());
    $("#ViewShort").addClass("hidden");
    $("#ViewAll").removeClass('hidden');
}


function GetGeneralReport(dateView) {
    
    var checkView = {
        "dateView": dateView
    };

    $.ajax({
        url: "/Dashboard/GeneralReport",
        type: "POST",
        data: JSON.stringify(checkView),
        contentType: 'application/json',
        success: function (result) {
            $("#blockLeft").fadeOut(100, function () {
                $('#blockLeft').html(result);
                $("#blockLeft").fadeIn(100);

            });
            
        }
    });
}

//Month
function GetGeneralReportMonth(dateView) {
    
    var checkView = {
        "dateView": dateView
    };

    $.ajax({
        url: "/Report/GeneralReport",
        type: "POST",
        data: JSON.stringify(checkView),
        contentType: 'application/json',
        success: function (result) {
            $("#blockLeft").fadeOut(100, function () {
                $('#blockLeft').html(result);
                $("#blockLeft").fadeIn(100);

            });
            
        }
    });
}