﻿function changeBookerName(bookingId, newValue) {
    if (bookingId != null && newValue != null) {
        $.ajax({
            url: "/Booking/UpdateBookerName",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: newValue
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function changeBookerPhone(bookingId, newValue) {
    if (bookingId != null && newValue != null) {
        $.ajax({
            url: "/Booking/UpdateBookerPhone",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: newValue
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function changeBookerIDC(bookingId, newValue) {
    if (bookingId != null && newValue != null) {
        $.ajax({
            url: "/Booking/UpdateBookerIDC",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: newValue
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function updateDeposit(bookingId, newValue) {
    if (bookingId != null && newValue != null && parseInt(newValue)) {
        $.ajax({
            url: "/Booking/UpdateDeposit",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: parseInt(newValue)
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function updateTotal(bookingId, newValue) {
    if (bookingId != null && newValue != null && parseInt(newValue)) {
        $.ajax({
            url: "/Booking/UpdateTotal",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: parseInt(newValue)
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function updateNote(bookingId, newValue) {
    if (bookingId != null && newValue != null) {
        $.ajax({
            url: "/Booking/UpdateNote",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: newValue
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function updateBookingStatus(bookingId, newValue) {
    if (bookingId != null && newValue != null) {
        $.ajax({
            url: "/Booking/UpdateBookingStatus",
            type: "POST",
            data: {
                bookingId: bookingId,
                newValue: parseInt(newValue)
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                    var place = $("#tblBookingDetail");
                    $(place).html('<img src="../../Content/ace/assets/img/"/>');
                    GetBookingItems(bookingId, place);
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Cập nhật thất bại.",1);
            }
        });
    }
    else {
        ShowMessage("Thao tác không hợp lệ", "1");
    }
}

function checkBookingItem() {
    var id = $("#bookingId").val();
    if (id) {
        $.ajax({
            url: "/Booking/CheckBookingItemsAvailable",
            type: "POST",
            data: { id: id },
            success: function (data) {
                $("#bookingItemsTbl tbody").html(data);
                $("#bookingItemsTbl").find(".error").tooltip();             
            },
            error: function (xhr, err) {
                ShowMessage("readyState: " + xhr.readyState,1);
            }
        });
    }
}

function deleteStayer() {
    var id = $(event.toElement).attr("data-id");
    var element = $(event.toElement);
    if (id != null) {
        $.ajax({
            url: "/Booking/DeleteStayer",
            type: "POST",
            data: {
                id: id
            },
            success: function (data) {
                if (data.Result) {
                    ShowMessage("Cập nhật thành công.", "2");
                    $(element).closest("tr").remove();
                } else {
                    ShowMessage("Cập nhật thất bại.", "1");
                }
            },
            error: function (xhr, err) {
                ShowMessage("Thao tác thất bại",1);
            }
        });
    }
}

function addStayer() {
    var name = $("#stayerName").val();
    var phone = $("#txtStayerPhone").val();
    var idc = $("#txtIDC").val();
    var bookingId = $("#bookingId").val();
    $.ajax({
        url: "/Booking/AddStayer",
        type: "POST",
        data: {
            bookingId: bookingId,
            name: name,
            phone: phone,
            idc: idc
        },
        success: function (data) {
            if (data.Result) {
                $("#addStayerModal").modal('hide');
                $("#tblStayerMessage").addClass('hidden');
                $("#tblStayer").removeClass('hidden');
                var row = '<tr><td>' + name + '</td><td>' + idc + '</td><td>' + phone + '</td>'
                    + '<td><button class="btn btn-minier btn-warning" onclick="deleteStayer()" '
                    + 'data-id="' + data.StayerId + '"><i class="icon-remove"></i></button></td></tr>';
                $("#tblStayer tbody").append(row);
            } else {
                ShowMessage("Cập nhật thất bại.", "1");
            }
        },
        error: function (xhr, err) {
            ShowMessage("Thao tác thất bại",1);
        }
    });
}

$(document).ready(function () {
    $("#bookingItemsTbl").on('click', '.delete-bookingItem', function () {
        var self = $(this).closest("tr");
        var bookingItemId = $(this).attr("data-id");

        if (bookingItemId) {
            var info = $(this).closest("tr");
            var roomType = $($(info).children()[1]).text();
            var from = $($(info).children()[2]).text();
            var to = $($(info).children()[3]).text();
            var quantity = $($(info).children()[4]).text();
            var total = $($(info).children()[5]).text();
            var body = "<span>Bạn thực sự muốn xóa booking item sau:</span><table>"
                    + "<tr><td><strong>Loại phòng : <strong></td><td>" + roomType + "</td></tr>"
                    + "<tr><td><strong>Từ ngày: <strong></td><td>" + from + "</td></tr>"
                    + "<tr><td><strong>Đến ngày: <strong></td><td>" + to + "</td></tr>"
                    + "<tr><td><strong>Số lượng : <strong></td><td>" + quantity + "</td></tr>"
                    + "<tr><td><strong>Tổng cộng : <strong></td><td>" + total + "</td></tr>"
                    + "</table>";

            var element = $(this);
            bootbox.dialog({
                title: 'Xác nhận',
                message: body,
                buttons:
                {
                    "ok":
                     {
                         "label": "<i class='icon-remove'></i> Đồng ý!",
                         "className": "btn-sm btn-success",
                         "callback": function () {
                             $.ajax({
                                 url: "/Booking/DeleteBookingItemByID",
                                 type: "POST",
                                 data: { id: bookingItemId },
                                 success: function (data) {
                                     if (data.result) {
                                         self.remove();
                                         ShowMessage("Xóa thông tin thành công.", "2");
                                     } else {
                                         ShowMessage("Không thể xóa booking item này. Vui lòng kiểm tra lại thông tin.", "1");
                                     }

                                 },
                                 error: function (xhr, err) {
                                     ShowMessage("responseText: " + xhr.responseText,1);
                                 }
                             });
                         }
                     },
                    "close":
                     {
                         "label": "<i class='icon-ok'></i> Đóng",
                         "className": "btn-sm btn-danger",
                         "callback": function () {
                             bootbox.hideAll();
                         }
                     }
                }
            });
            //$("#confirmDiv").confirmModal({
            //    heading: 'Xác định xóa booking item',
            //    body: body,
            //    callback: function () {
            //        //goi ajax xóa booking item
            //        $.ajax({
            //            url: "/Booking/DeleteBookingItemByID",
            //            type: "POST",
            //            data: { id: bookingItemId },
            //            success: function (data) {
            //                if (data.result) {
            //                    $("#bookingItemsTbl tbody").html(data.view);
            //                    ShowMessage("Xóa thông tin thành công.", "2");
            //                } else {
            //                    ShowMessage("Không thể xóa booking item này. Vui lòng kiểm tra lại thông tin.", "1");
            //                }

            //            },
            //            error: function (xhr, err) {
            //                alert("responseText: " + xhr.responseText);
            //            }
            //        });
            //    }
            //});
        } else {
            ShowMessage("Xin lỗi, chúng tôi không thể thực hiện yêu cầu này.",1);
        }
    });

    $("#bookingItemsTbl").on('click', '.view-bookingItem', function () {
        var id = $(this).attr("data-id");
        if (id) {
            $("#bookingItemModal").modal('show');
            
            $.ajax({
                url: "/Booking/ViewBookingItemByID",
                type: "POST",
                data: { id: id },
                success: function (data) {
                    $("#bookingItemModal").find(".modal-body").html(data.view);
                    $('#numberOfFragment').ace_spinner({ value: 0, min: 0, max: 10, step: 1, btn_up_class: 'btn-info', btn_down_class: 'btn-info' })
				    .on('change', function () {
				        //alert(this.value)
				    });

                    //set datepicker
                    $('#txtFromDate-detail').datepicker({
                        dateFormat: 'dd-mm-yy', onSelect: function () {
                            var date1 = $.datepicker.parseDate('dd-mm-yy', $("#txtFromDate-detail").val());
                            date1.setDate(date1.getDate() + 1);
                            $("#txtToDate-detail").datepicker("option", "minDate", date1);
                        }
                    });
                    $('#txtToDate-detail').datepicker({
                        dateFormat: 'dd-mm-yy', onSelect: function () {
                            var date2 = $.datepicker.parseDate('dd-mm-yy', $("#txtToDate-detail").val());
                            date2.setDate(date2.getDate() - 1);
                            $("#txtFromDate-detail").datepicker("option", "maxDate", date2);
                        }
                    });
                    //set min,max,milestone,numberOfHandler
                    min = 0;
                    max = parseInt(data.max);
                    milestoneDate = $.datepicker.parseDate("dd-mm-yy", data.milestone);
                    numberOfHandler = parseInt(data.numberOfHandler);

                    //create slider
                    var handlers = data.handlers;
                    createNewSlider(min, max, handlers);
                    createHandlerValue();
                    updateColors(handlers, max);
                    
                },
                error: function (xhr, err) {
                    ShowMessage("readyState: " + xhr.readyState + "\nstatus: " + xhr.status, 1);
                    
                }
            });

        } else {
            ShowMessage("Xin lỗi chúng tôi không thể thực hiện thao tác này.",1);
        }

    });

});

function GetBookingItems(id,place) {
    $.ajax({
        url: "/Booking/GetAllBookingItemsByBookingId",
        type: "POST",
        data: {
           id : id
        },
        success: function (data) {
            $(place).html(data);
        },
        error: function (xhr, err) {
            //
        }
    });
}
