﻿
function SearchRoomBooking() {
    var from = ConvertDateVNToUS($('#fromDate').val());
    var to = ConvertDateVNToUS($('#toDate').val());
    $.ajax({
        url: "/Booking/SearchBooking",
        type: "GET",
        data: { from: from, to: to },
        success: function (result) {
            $("#search-body").html(result);
        }
    });

}
//add Booking Item
function addBooking() {
    var newBooking = new Object();
    newBooking.quantity = $("#txtQuantity").val();
    newBooking.from = $.datepicker.parseDate('dd-mm-yy', $("#txtFromDate").val());
    newBooking.to = $.datepicker.parseDate('dd-mm-yy', $("#txtToDate").val());
    newBooking.feeChange = $("#txtFeeChange").val();
    if (!checkNewBookingValid(newBooking)) return;

    var json = new Object();
    json.RoomType = $("#ddlRoomType").find('option:selected').val();
    var source = $("#ddlSourceID").find('option:selected').val();
    json.SourceID = (source != undefined) ? source : $("#ddlSourceID").val();
    
    json.BookingArray = newBooking;

    //tao json
    var myJsonString = JSON.stringify(json);
    $.ajax({
        url: "/Booking/AddNewBookingItem",
        type: "POST",
        data: { json: myJsonString },
        success: function (data) {
            if (data.result) {
               // ShowMessage("Thao tác thực hiện thành công.", "2");
                var table = $("#tblBookingDetail tbody");
                table.html(data.view);
                $("#txtFromDate").val("");
                $("#txtToDate").val("");
                $("#txtQuantity").val("1");
                $("#txtFromDate").datepicker("option", "maxDate", null);
                $("#txtFromDate").datepicker("option", "minDate", null);
                $("#txtToDate").datepicker("option", "maxDate", null);
                $("#txtToDate").datepicker("option", "minDate", null);
                $("#ddlSourceID").attr("disabled", "disabled");
                $("#addBookingItemModal").modal('hide');
                $("#txtFeeChange").val("0");
                checkBookingItem();
            } else {
                ShowMessage(data.message, "1");
            }

        },
        error: function (xhr, err) {
            ShowMessage("Thêm phòng thất bại", 1);
        }
    });
}
function checkNewBookingValid(booking) {
    var errorString = "";
    //kiem tra gia tri null
    if (booking.quantity == null || booking.from == null || booking.to == null) {
         errorString = "Vui lòng điền đây đủ thông tin vào loại phòng, số phòng và thời gian ở";
    }

    //kiem tra so luong
    if (isNaN(parseFloat(booking.quantity)) || !isFinite(booking.quantity) || parseFloat(booking.quantity) < 1) {
         errorString = "Số lượng phòng không đúng định dạng";
    }

    //kiem tra ngay den va ngay di
    if (booking.from > booking.to) { errorString = "Ngày đến phải lớn hơn ngày từ"; }

    if (errorString != "") {
        ShowMessage(errorString, 3);
        return false;
    } else return true;
}
//check Booking Item Valid Or Not
function checkBookingItem(id) {
    $.ajax({
        url: "/Booking/CheckBookingItemsAvailable",
        type: "POST",
        data: { id: id },
        success: function (data) {                     
            $("#tblBookingDetail tbody").html(data);
            $("#tblBookingDetail").find(".error").tooltip();
            if ($("#tblBookingDetail").find(".error").length == 0) {
                //ShowMessage("Đủ số lượng phòng", 2);
            }
        },
        error: function (xhr, err) {
            ShowMessage("Truyền dữ liệu thất bại", 1);
            return false;
        }
    });
}
//delete Booking
function destroyBooking() {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có muốn hủy đơn đặt phòng này không?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Booking/CancelBooking",
                         type: "POST",
                         success: function (data) {
                             if (!data.Result && data.HasSession) {
                                 ShowMessage("Thao tác không thành công.", 1);
                             } else {
                                 $("#itemDatatable").dataTable()._fnAjaxUpdate();
                                 $("#modal").modal('hide');
                                 ShowMessage("Hủy đơn thành công", 2);                              
                                 clearAllData();
                             }
                         },
                         error: function (xhr, err) {
                             ShowMessage("Có lỗi trong truyền dữ liệu", 1);

                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
    //var body = "Bạn có muốn hủy booking không?";
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác định xóa Booking',
    //    body: body,
    //    callback: function () {
    //        $.ajax({
    //            url: "/Booking/CancelBooking",
    //            type: "POST",
    //            success: function (data) {
    //                if (!data.Result && data.HasSession) {
    //                    alert("Thao tác không thành công.");
    //                } else {
    //                    $("#modal").modal('hide');
    //                    clearAllData();
    //                }
    //            },
    //            error: function (xhr, err) {
    //                alert("Delete failed");
    //            }
    //        });
    //    }
    //});
}
function clearAllData() {
    $("#modal").find("input").val("");
    $("#tblBookingDetail tbody").find("tr").remove();
    $("#StayerTbl tbody").find("tr").remove();
    $("#tab4").html("<h5 class='text-center'>(Không có dữ liệu)</h5>");
    $("#txtDeposit").val("0");
    $("#txtFeeChange").val("0");
}
//finish booking
function finishBooking() {
    var check1 = checkBookerInfoValid();
    var check2 = checkStayerInfoValid();
    var check3 = checkDepositValid();

    if (check1 && check2 && check3) {
        //get booker info
        var booker = new Object();
        booker.name = $("#txtBookerName").val();
        booker.phone = $("#txtBookerPhone").val();
        booker.idc = $("#txtBookerIDC").val();

        //get stayer info list
        var stayerList = new Array();
        var tableRows = $("#StayerTbl tbody").find("tr");
        if (tableRows) {
            for (var i = 0; i < tableRows.length; i++) {
                var row = tableRows[i];
                var stayer = new Object();
                stayer.name = $($(row).find("td")[1]).text();
                stayer.phone = $($(row).find("td")[2]).text();
                stayer.idc = $($(row).find("td")[3]).text();
                stayerList.push(stayer);
            }
        }

        //get comment
        var comment = $("#commentText").val();

        //get deposit
        var deposit = $("#txtDeposit").val();

        ////get hiddenID
        //var hiddenID = $("#txtHiddenBookingId").val();

        var json = new Object();
        json.booker = booker;
        json.stayerList = stayerList;
        json.comment = comment;
        json.deposit = deposit;
        json.sourceId = $("#ddlSourceID").val();
        //json.hiddenID = hiddenID;

        $.ajax({
            url: "/Booking/AddBooking",
            type: "POST",
            data: { jsonString: JSON.stringify(json) },
            success: function (data) {
                if (data == 'True') {
                    $("#modal").modal('hide');
                    clearAllData();
                    $("#itemDatatable").dataTable()._fnAjaxUpdate();
                    ShowMessage("Lưu thông tin thành công", 2);
                } else {
                    ShowMessage("Cần thêm phòng trước khi hoàn tất đặt phòng hoặc sai thông tin thuê phòng", 1);
                }
            },
            error: function (xhr, err) {
                ShowMessage("Truyền dữ liệu không thành công", 1);
            }
        });


    }
}
function checkBookerInfoValid() {
    if ($("#txtBookerName").val() == "") {
        ShowMessage("Vui lòng nhập thông tin người đặt phòng", 3);
        $("#txtBookerName").focus();
        return false;
    }
    return true;
}
function checkStayerInfoValid() {

    return true;
}
function checkDepositValid() {
    var deposit = $("#txtDeposit").val();
    //kiem tra so luong
    if (!$.isNumeric(deposit)) {
        ShowMessage("Vui lòng nhập chữ số cho tiền đặt cọc", 3);
        return false;
    }
    if (parseFloat(deposit) < 0) {
        ShowMessage("Vui lòng nhập dữ liệu phù hợp", 3);
        return false;
    }

    return true;
}
//add stayer
function addStayer() {
    var name = $("#txtStayerName").val();
    var phone = $("#txtStayerPhone").val();
    var idc = $("#txtStayerIDC").val();

    if (!name) {
        ShowMessage("Vui lòng nhập thêm tên", 3);
        return;
    }

    var table = $("#StayerTbl");
    var number = $(table).find("tr").length + 1;
    var data = "<tr><td class='span1'>" + number + "</td>"
        + "<td class='col-lg-3'>" + name + "</td>"
        + "<td class='span2'>" + phone + "</td>"
        + "<td class='span2'>" + idc + "</td>"
        + "<td class='span2' style='text-align:center'><button class='delete-stayer btn btn-mini btn-alert'><i class='icon-remove'></i></button></td></tr>";
    table.append(data);

    $("#txtStayerName").val("");
    $("#txtStayerPhone").val("");
    $("#txtStayerIDC").val("");
}
function addStayer2() {
    var bookingId = $("#bookingId").val();
    var name = $("#stayerName").val();
    var phone = $("#txtStayerPhone").val();
    var idc = $("#txtIDC").val();

    if (name == "") {
        ShowMessage("Vui lòng nhập tên người ở", 3);
        return;
    }

    $.ajax({
        url: "/Booking/AddGuest",
        type: "POST",
        data: {
            bookingId: bookingId,
            name: name,
            phone: phone,
            idc: idc
        },
        success: function (data) {
            if (data.result) {
                $("#addStayerModal").modal('hide');
                GetBookingGuest(bookingId);
            } else {
                ShowMessage("Thao tác thất bại", 1);
            }
            
        }
    });
};

//split booking detail
function splitBookingDetail(id) {
    var date = $("#betweenDate").val();
    var element = $("#finishSplitBtn").closest("tr").prev();
    if (date && id) {
        $.ajax({
            url: "/Booking/SplitBookingDetailByID",
            type: "POST",
            data: {
                id: id,
                date: date
            },
            success: function (html) {
                if (html) {
                    $("#tab4").html(html);
                }
            },
            error: function (xhr, err) {
                ShowMessage("Truyền dữ liệu thất bại", 1);
            }
        });
    }
    $("#splitBar").closest("tr").remove();
}
function LoadBooking(id) {
    $.ajax({
        url: "/Booking/LoadBooking",
        type: "POST",
        data: {
            Id: id
        },
        success: function (data) {
            ClearData();
            $("#booking-tab1").addClass("active");
            $("#booking-tab2").removeClass("active");
            $("#booking-tab3").removeClass("active");
            $("#booking-tab4").removeClass("active");

            $("#tab1").addClass("active");
            $("#tab2").removeClass("active");
            $("#tab3").removeClass("active");
            $("#tab4").removeClass("active");

            $("#ddlSourceID").attr("disabled", "disabled");
            $("#modal").modal('show');
            $("#txtHiddenBookingId").val(data.Booking.BookingID);
            $("#txtBookerName").val(data.Booking.PersonName);
            $("#txtBookerPhone").val(data.Booking.Phone);
            $("#txtBookerIDC").val(data.Booking.PersonId);
            $("#ddlSourceID").val(data.SourceId);
            $("#txtDeposit").val(data.Booking.Deposit);
            $("#commentText").val(data.Booking.Comment);
            var table = $("#tblBookingDetail tbody");

            //Khuc load bookingItem khuc dau
            /*
            var feechange = 0;
            var total = 0;
            for (var i = 0; i < data.BookingItemList.length; i++) {
                feechange += data.BookingItemList[i].FeeChange;
                total += (data.BookingItemList[i].Total + data.BookingItemList[i].FeeChange);
                var text = "<tr><td>" + (i + 1) + "</td><td>"
                    + data.RoomTypeList[i] + "</td><td>"
                    + ConvertDate(data.BookingItemList[i].FromDate) + "</td><td>"
                    + ConvertDate(data.BookingItemList[i].ToDate) + "</td><td>"
                    + data.BookingItemList[i].Quantity + "</td><td>"
                    + addCommas(data.BookingItemList[i].Total) + "</td><td>"
                    + addCommas(data.BookingItemList[i].FeeChange) + "</td><td>"
                    + addCommas((data.BookingItemList[i].Total + data.BookingItemList[i].FeeChange)) + "</td><td>"
                    + "<button class='view-bookingItem btn btn-minier btn-warning' data-id='" + data.BookingItemList[i].BookingItemID + "'><i class='icon-pencil'></i></button>"
                    + "<button class='delete-bookingItem btn btn-minier btn-danger' data-id='" + data.BookingItemList[i].BookingItemID + "' style='margin-left: 5px;'><i class='icon-remove'></i></button></td></tr>";
                table.append(text);
            }
            text = "<tr class='info'><td colspan='5'>Tổng cộng</td><td  colspan='1'>" + addCommas(data.Booking.Total) + "</td>" +
                    "<td  colspan='1'>" + addCommas(feechange) + "</td>" + "<td  colspan='2'>" + addCommas(total) + "</td>" + "</tr>";
            table.append(text);
            */
            //


            text = "";
            table = $("#StayerTbl");

            $("#btnBooking").text("Cập nhật đơn đặt phòng");
            //for (var j = 0; j < data.StayerInfoList.length; j++) {

            //    text = "<tr><td class='span1'>" + (j + 1) + "</td>"
            //   + "<td class='col-lg-3'>" + data.StayerInfoList[j].StayerName + "</td>"
            //   + "<td class='span2'>" + data.StayerInfoList[j].StayerPhone + "</td>"
            //   + "<td class='span2'>" + data.StayerInfoList[j].StayerIDC + "</td>"
            //   + "<td class='span2' style='text-align:center'><button class='delete-stayer btn btn-mini btn-alert' data-id='" + data.StayerInfoList[i].StayerID + "'><i class='icon-remove'></i></button></td></tr>";
            //    table.append(text);
            //}

            for (var j = 0; j < data.Guests.length; j++) {
                var guest = data.Guests[j];
                text = "<tr><td class='span1'>" + (j + 1) + "</td>"
               + "<td class='col-lg-3'>" + guest.Name + "</td>"
               + "<td class='span2'>" + guest.Phone + "</td>"
               + "<td class='span2'>" + guest.PersonId + "</td>"
               + "<td class='span2' style='text-align:center'><button class='delete-stayer btn btn-mini btn-alert' data-id='" + guest.Id + "'><i class='icon-remove'></i></button></td></tr>";
                table.append(text);
            }

            if (data.BookingItemList.length > 0) {
                checkBookingItem(id);
            }
                     
            $("#modal").modal('show');
            

        },
        error: function (xhr, err) {
            ShowMessage("Lỗi trong truyền dữ liệu", 1);
        }
    });

}
function DeleteBooking(id) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có chắc muốn xóa đơn đặt hàng này?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Booking/DeleteBooking",
                         type: "POST",
                         data: {
                             Id: id
                         },
                         success: function (data) {
                             if (data.Result) {
                                 $('#itemDatatable').dataTable()._fnAjaxUpdate();
                             } else {
                                 ShowMessage("Không thể xóa được booking này", 1);
                             }
                         },
                         error: function (xhr, err) {
                             ShowMessage("Có lỗi xảy ra!", 1);

                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

    //if (confirm(strMessage)) {

    //    $.ajax({
    //        url: "/Booking/DeleteBooking",
    //        type: "POST",
    //        data: {
    //            Id: id
    //        },
    //        success: function (data) {
    //            if (data.Result) {
    //                $('#itemDatatable').dataTable()._fnAjaxUpdate();
    //            } else {
    //                alert("Không thể xóa được booking này");
    //            }
    //        },
    //        error: function (xhr, err) {
    //            alert("Có lỗi xảy ra!");
    //        }
    //    });
    //}

}

//document ready
$(document).ready(function () {

    

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimScroll({
            height: $this.data('height') || 300,
            railVisible: true
        });
    });

    //checkbox
    $("#cbBookForOther").change(function () {
        if (this.checked) {
            //focus menu icons
            $("#booking-tab1").removeClass("active");
            $("#booking-tab2").addClass("active");
            $("#booking-tab3").removeClass("active");
            //focus tab
            $("#tab1").removeClass("active");
            $("#tab2").addClass("active");
            $("#tab3").removeClass("active");
        }
    });
    $("#cbAddComment").change(function () {
        if (this.checked) {
            //focus menu icons
            $("#booking-tab1").removeClass("active");
            $("#booking-tab2").removeClass("active");
            $("#booking-tab3").addClass("active");
            //focus tab
            $("#tab1").removeClass("active");
            $("#tab2").removeClass("active");
            $("#tab3").addClass("active");
        }
    });

    //bookingItem
    $("#tblBookingDetail").on('click', '.delete-bookingItem', function () {
        var bookingItemID = $(this).attr("data-id");
        var info = $(this).closest("tr");
        var roomType = $($(info).children()[1]).text();
        var from = $($(info).children()[2]).text();
        var to = $($(info).children()[3]).text();
        var quantity = $($(info).children()[4]).text();
        var total = $($(info).children()[5]).text();
        var feeChange = $($(info).children()[6]).text();
        var result = $($(info).children()[7]).text();
        var body = "<span>Bạn thực sự muốn xóa yêu cầu đặt phòng sau:</span><table>"
                + "<tr><td><strong>Loại phòng : <strong></td><td>" + roomType + "</td></tr>"
                + "<tr><td><strong>Từ ngày: <strong></td><td>" + from + "</td></tr>"
                + "<tr><td><strong>Đến ngày: <strong></td><td>" + to + "</td></tr>"
                + "<tr><td><strong>Số lượng : <strong></td><td>" + quantity + "</td></tr>"
                + "<tr><td><strong>Tổng cộng : <strong></td><td>" + total + "</td></tr>"
                + "<tr><td><strong>Biến động : <strong></td><td>" + feeChange + "</td></tr>"
                + "<tr><td><strong>Thành tiền : <strong></td><td>" + result + "</td></tr>"
                + "</table>";
        bootbox.dialog({
            title: 'Xác định',
            message: body,
            buttons:
            {
                "ok":
                 {
                     "label": "<i class='icon-remove'></i> Đồng ý!",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                         $.ajax({
                             url: "/Booking/DeleteBookingItemByID",
                             type: "POST",
                             data: { id: bookingItemID },
                             success: function (data) {
                                 if (data.result) {
                                     //$("#tblBookingDetail tbody").html(data.view);
                                     //if (data.view != "") {
                                     //    checkBookingItem();
                                     //} else {
                                     //    $("#tblBookingDetail tbody").html("");
                                     //}
                                     
                                     ShowMessage("Xóa thông tin thành công.", "2");                                  
                                     checkBookingItem($("#bookingId").val());
                                 } else {
                                     ShowMessage("Không thể xóa thông tin này", "1");
                                 }

                             },
                             error: function (xhr, err) {
                                 ShowMessage("Có vấn đề trong truyền dữ liệu", 1);
                             }
                         });
                     }
                 },
                "close":
                 {
                     "label": "<i class='icon-ok'></i> Đóng",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                         bootbox.hideAll();
                     }
                 }
            }
        });
        //var bookingItemID = $(this).attr("data-id");

        //if (bookingItemID) {
        //    var info = $(this).closest("tr");
        //    var roomType = $($(info).children()[1]).text();
        //    var from = $($(info).children()[2]).text();
        //    var to = $($(info).children()[3]).text();
        //    var quantity = $($(info).children()[4]).text();
        //    var total = $($(info).children()[5]).text();
        //    var body = "<span>Bạn thực sự muốn xóa booking item sau:</span><table>"
        //            + "<tr><td><strong>Loại phòng : <strong></td><td>" + roomType + "</td></tr>"
        //            + "<tr><td><strong>Từ ngày: <strong></td><td>" + from + "</td></tr>"
        //            + "<tr><td><strong>Đến ngày: <strong></td><td>" + to + "</td></tr>"
        //            + "<tr><td><strong>Số lượng : <strong></td><td>" + quantity + "</td></tr>"
        //            + "<tr><td><strong>Tổng cộng : <strong></td><td>" + total + "</td></tr>"
        //            + "</table>";

        //    var element = $(this);
        //    $("#confirmDiv").confirmModal({
        //        heading: 'Xác định xóa booking item',
        //        body: body,
        //        callback: function () {
        //            //goi ajax xóa booking item
        //            $.ajax({
        //                url: "/Booking/DeleteBookingItemByID",
        //                type: "POST",
        //                data: { id: bookingItemID },
        //                success: function (data) {
        //                    if (data.result) {
        //                        $("#tblBookingDetail tbody").html(data.view);
        //                        ShowMessage("Xóa thông tin thành công.", "2");
        //                    } else {
        //                        ShowMessage("Không thể xóa booking item này. Vui lòng kiểm tra lại thông tin.", "1");
        //                    }

        //                },
        //                error: function (xhr, err) {
        //                    alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
        //                    alert("responseText: " + xhr.responseText);
        //                }
        //            });
        //        }
        //    });
        //} else {
        //    alert("Xin lỗi, chúng tôi không thể thực hiện yêu cầu này.");
        //}
    });
    $("#tblBookingDetail").on('click', '.view-bookingItem', function () {
        ////focus another tab
        $("#booking-tab1").removeClass("active");
        $("#booking-tab2").removeClass("active");
        $("#booking-tab3").removeClass("active");
        $("#booking-tab4").addClass("active");
        
        $("#tab1").removeClass("active");
        $("#tab2").removeClass("active");
        $("#tab3").removeClass("active");
        $("#tab4").addClass("active");

        var id = $(this).attr("data-id");
     
        if (id) {
            
            $.ajax({
                url: "/Booking/ViewBookingItemByID",
                type: "POST",
                data: { id: id },
                success: function (data) {
                    $("#tab4").html(data.view);
                    
                    $('#numberOfFragment').ace_spinner({ value: 0, min: 0, max: 10, step: 1, btn_up_class: 'btn-info', btn_down_class: 'btn-info' })
				    .on('change', function () {
				        //alert(this.value)
				    });
                    
                    //set datepicker
                    $('#txtFromDate-detail').datepicker({
                        dateFormat: 'dd-mm-yy', onSelect: function () {
                            var date1 = $.datepicker.parseDate('dd-mm-yy', $("#txtFromDate-detail").val());
                            date1.setDate(date1.getDate() + 1);
                            $("#txtToDate-detail").datepicker("option", "minDate", date1);
                        }
                    });
                    $('#txtToDate-detail').datepicker({
                        dateFormat: 'dd-mm-yy', onSelect: function () {
                            var date2 = $.datepicker.parseDate('dd-mm-yy', $("#txtToDate-detail").val());
                            date2.setDate(date2.getDate() - 1);
                            $("#txtFromDate-detail").datepicker("option", "maxDate", date2);
                        }
                    });

                    ////set min,max,milestone,numberOfHandler
                    //min = 0;
                    //max = parseInt(data.max);
                    //milestoneDate = $.datepicker.parseDate("dd-mm-yy", data.milestone);
                    //numberOfHandler = parseInt(data.numberOfHandler);
                    ////create slider
                    //var handlers = data.handlers;
                    //createNewSlider(min, max, handlers);
                    //createHandlerValue();
                    //updateColors(handlers, max);
                    
                },
                error: function (xhr, err) {
                    ShowMessage("Có vấn đề trong truyền dữ liệu", 1);
                }
            });

        } else {
            ShowMessage("Xin lỗi chúng tôi không thể thực hiện thao tác này.", 1);
        }
    });

    //bookingDetail
    $(document).on("click", ".split-bookingDetail", function () {
        var element = $('#splitBar');
        if (element) $(element).closest("tr").remove();

        var id = $(this).attr("data-id");
        if (id == null) { ShowMessage("Không thực hiện được thao tác này.", 1);
            return; }

        var item = $(this).closest("tr");
        var fromDateString = $($(item).children("td")[2]).text();
        var toDateString = $($(item).children("td")[3]).text();
        var date1 = $.datepicker.parseDate('dd-mm-yy', fromDateString);
        date1.setDate(date1.getDate() + 1);
        var date2 = $.datepicker.parseDate('dd-mm-yy', toDateString);
        date2.setDate(date2.getDate() - 1);
        item.after("<tr><td colspan='8'><div id='splitBar' style='float:right'><input type='text' id='betweenDate' class='input-medium'>"
                + "<button id='finishSplitBtn' class='btn btn-mini btn-warning' onclick='splitBookingDetail(" + id + ")'><i class='icon-pencil'></i></button></div></td></tr>");

        $('#betweenDate').datepicker({
            dateFormat: 'dd-mm-yy',
            onSelect: function () {
                $("#finishSplitBtn").focus();
            },
            onClose: function () {
                var element = $('#splitBar');
                if (element && $("#betweenDate").val() == "") $(element).closest("tr").remove();
            }
        });
        $("#betweenDate").datepicker("option", "minDate", date1);
        $("#betweenDate").datepicker("option", "maxDate", date2);
        $("#betweenDate").focus();
    });
    $(document).on("blur", "#finishSplitBtn", function () {
        var element = $('#splitBar');
        if (element) $(element).closest("tr").remove();
    });
    $(document).on("click", ".edit-bookingDetail", function () {

    });
    $(document).on("click", ".delete-bookingDetail", function () {
        var element = $(this);
        var bookingDetailID = $(element).attr("data-id");

        if (bookingDetailID == null) {
            ShowMessage("Không thể thực hiện được thao tác này.", 1);
            return;
        }

        var info = $(this).closest("tr");
        var roomType = $($(info).children()[1]).text();
        var from = $($(info).children()[2]).text();
        var to = $($(info).children()[3]).text();
        var quantity = $($(info).children()[5]).text();
        var price = $($(info).children()[4]).text();
        var total = $($(info).children()[6]).text();
        var body = "<span>Bạn thực sự muốn xóa booking detail sau:</span><table>"
                    + "<tr><td><strong>Loại phòng : <strong></td><td>" + roomType + "</td></tr>"
                    + "<tr><td><strong>Từ ngày: <strong></td><td>" + from + "</td></tr>"
                    + "<tr><td><strong>Đến ngày: <strong></td><td>" + to + "</td></tr>"
                    + "<tr><td><strong>Số lượng : <strong></td><td>" + quantity + "</td></tr>"
                    + "<tr><td><strong>Đơn giá: <strong></td><td>" + price + "</td></tr>"
                    + "<tr><td><strong>Tổng cộng : <strong></td><td>" + total + "</td></tr>"
                    + "</table>";
        bootbox.dialog({
            title: 'Xác nhận',
            message: body,
            buttons:
            {
                "ok":
                 {
                     "label": "<i class='icon-remove'></i> Đồng ý!",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                         $.ajax({
                             url: "/Booking/DeleteBookingDetailByID",
                             type: "POST",
                             data: { id: bookingDetailID },
                             success: function (data) {
                                 if (data.Result) {
                                     $(element).closest("tr").remove();
                                 } else {
                                     ShowMessage("Thao tác không thành công.", 1);
                                 }

                             },
                             error: function (xhr, err) {
                                 ShowMessage("Có lỗi trong truyền dữ liệu", 1);
                             }
                         });
                     }
                 },
                "close":
                 {
                     "label": "<i class='icon-ok'></i> Đóng",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                         bootbox.hideAll();
                     }
                 }
            }
        });
        //$("#confirmDiv").confirmModal({
        //    heading: 'Xác định xóa booking detail',
        //    body: body,
        //    callback: function (id) {
        //        $.ajax({
        //            url: "/Booking/DeleteBookingDetailByID",
        //            type: "POST",
        //            data: { id: bookingDetailID },
        //            success: function (data) {
        //                if (data.Result) {
        //                    $(element).closest("tr").remove();
        //                } else {
        //                    alert("Thao tác không thành công.");
        //                }

        //            },
        //            error: function (xhr, err) {
        //                alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
        //                alert("responseText: " + xhr.responseText);
        //            }
        //        });
        //    }
        //});
    });

    $("#bookingDetailsTbl").find(".error").mouseover(function () {
        $(this).removeClass(".error");
    });

    $("#StayerTbl").on("click", ".delete-stayer", function () {
        $(this).closest("tr").remove();
        var elements = $("#StayerTbl").find("tr");
        for (var i = 0; i < elements.length; i++) {
            $($(elements[i]).children("td")[0]).text(i + 1);
        }
    });

    //$("#tab4").on("keypress", ".td-price", function (event) {
    //    if (event.which == 13) {
    //        //sumit change
    //        var newValue = $(event.target).val();
    //        $(event.target).closest("td").html(newValue);
    //        updateTotalValue($(this).closest("tr"));
    //    }
    //    else if (event.which < 48 || event.which > 57) {
    //        return false;
    //    }
    //});
    //$("#tab4").on("blur", ".td-price", function () {
    //    var value = $(this).children("input").val();
    //    $(this).closest("td").html(value);
    //    updateTotalValue($(this).closest("tr"));
    //});
    //$("#tab4").on("keypress", ".td-quantity", function (event) {
    //    if (event.which == 13) {
    //        //sumit change
    //        var newValue = $(event.target).val();
    //        $(event.target).closest("td").html(newValue);
    //        updateTotalValue($(this).closest("tr"));
    //    }
    //    else if (event.which < 48 || event.which > 57) {
    //        return false;
    //    }
    //});
    //$("#tab4").on("blur", ".td-quantity", function () {
    //    var value = $(this).children("input").val();
    //    $(this).closest("td").html(value);
    //    updateTotalValue($(this).closest("tr"));
    //});
    });
//change to tab
function changeToTab(id) {
    //get the active li
    var activeID = -1;
    var elements = $("#booking-nav").children("li");
    for (var i = 0; i < elements.length; i++) {
        if ($(elements[i]).hasClass("active")) {
            activeID = i;
        }
    }
    //tab4
    if (activeID == 3) {
        if (isUpdated) {
            var body = "Bảng thông tin booking detail có thay đổi. Bạn có muốn lưu lại thay đổi này không?";
            bootbox.dialog({
                title: 'Xác nhận',
                message: body,
                buttons:
                {
                    "ok":
                     {
                         "label": "<i class='icon-remove'></i> Đồng ý!",
                         "className": "btn-sm btn-success",
                         "callback": function () {
                             var callback = function () { loadUpdateBookingItemsView(); };
                             updateBookingDetail(callback);
                         }
                     },
                    "close":
                     {
                         "label": "<i class='icon-ok'></i> Đóng",
                         "className": "btn-sm btn-danger",
                         "callback": function () {
                             bootbox.hideAll();
                         }
                     }
                }
            });
            //$("#confirmDiv").confirmModal({
            //    heading: 'Xác nhập lưu',
            //    body: body,
            //    callback: function () {
            //        var callback = function () { loadUpdateBookingItemsView(); };
            //        updateBookingDetail(callback);
            //    }
            //});
        } else {
            $("#tblBookingDetail tbody").html("");
            checkBookingItem();
        }
    }


}
//update booking item
function updateBookingItem() {
    var element = event.target;
    var id = $(element).attr("data-id");
    var from = $("#txtFromDate-detail").val();
    var to = $("#txtToDate-detail").val();
    
    var fromDate = $.datepicker.parseDate('dd-mm-yy',from).toJSON();
    var toDate = $.datepicker.parseDate('dd-mm-yy', to).toJSON();

    var quantity = +$("#txtQuantity-detail").val();

    $.ajax({
        url: "/Booking/UpdateBookingItem",
        data: {
            id: id,
            from: fromDate,
            to: toDate,
            quantity: quantity
        },
        type: "POST",
        success: function (data) {
            if (data.Result) {
                ShowMessage("Cập nhật thông tin thành công.", "2");
                $("#bookingDetailsTbl").html(data.View);             
            } else {
                ShowMessage("Cập nhật thông tinh thất bại", "1");
            }
        },
        error: function (xhr, err) {
            ShowMessage("Cập nhật thông tin không thành công.", "1");
        }
    });
}
//loadUpdateBookingItemsView
function loadUpdateBookingItemsView() {
    $.ajax({
        url: "/Booking/LoadUpdateBookingItemsView",
        type: "POST",
        success: function (data) {
            if (data.result) {
                $("#tblBookingDetail tbody").html(data.view);
            }
        }
    });
}

//view booking detail
function viewBookingDetailById(id) {
    $("#bookingDetailModal").modal("show");
    
    $.ajax({
        url: "/Booking/ViewBookingItemByID",
        type: "POST",
        data: { id: id },
        success: function (data) {
            //$("#bookingDetailModal >.modal-content >.modal-body").html(data.view);
            //$('#numberOfFragment').ace_spinner({ value: 0, min: 0, max: 10, step: 1, btn_up_class: 'btn-info', btn_down_class: 'btn-info' })
            //.on('change', function () {
            
            //});

            ////set datepicker
            //$('#txtFromDate-detail').datepicker({
            //    dateFormat: 'dd-mm-yy', onSelect: function () {
            //        var date1 = $.datepicker.parseDate('dd-mm-yy', $("#txtFromDate-detail").val());
            //        date1.setDate(date1.getDate() + 1);
            //        $("#txtToDate-detail").datepicker("option", "minDate", date1);
            //    }
            //});
            //$('#txtToDate-detail').datepicker({
            //    dateFormat: 'dd-mm-yy', onSelect: function () {
            //        var date2 = $.datepicker.parseDate('dd-mm-yy', $("#txtToDate-detail").val());
            //        date2.setDate(date2.getDate() - 1);
            //        $("#txtFromDate-detail").datepicker("option", "maxDate", date2);
            //    }
            //});
            
            //set min,max,milestone,numberOfHandler
            //min = 0;
            //max = parseInt(data.max);
            //milestoneDate = $.datepicker.parseDate("dd-mm-yy", data.milestone);
            //numberOfHandler = parseInt(data.numberOfHandler);
            
            //create slider
            //var handlers = data.handlers;
            //createNewSlider(min, max, handlers);
            //createHandlerValue();
            //updateColors(handlers, max);
            
            $("#bookingDetailModal >.widget-box >.widget-body >.widget-main").html(data.view);
            $("#txtFromDate-detail").datepicker({ dateFormat: 'dd-mm-yy' });
            $("#txtToDate-detail").datepicker({ dateFormat: 'dd-mm-yy' });
        },
    });
}

//$("#bookingDetailModal").on("keypress", ".td-price", function (event) {
//    if (event.which == 13) {
//        //sumit change
//        var newValue = $(event.target).val();
//        $(event.target).closest("td").html(newValue);
//        updateTotalValue($(this).closest("tr"));
//    }
//    else if (event.which < 48 || event.which > 57) {
//        return false;
//    }
//});
//$("#bookingDetailModal").on("blur", ".td-price", function () {
//    var value = $(this).children("input").val();
//    $(this).closest("td").html(value);
//    updateTotalValue($(this).closest("tr"));
//});
//$("#bookingDetailModal").on("keypress", ".td-quantity", function (event) {
//    if (event.which == 13) {
//        //sumit change
//        var newValue = $(event.target).val();
//        $(event.target).closest("td").html(newValue);
//        updateTotalValue($(this).closest("tr"));
//    }
//    else if (event.which < 48 || event.which > 57) {
//        return false;
//    }
//});
//$("#bookingDetailModal").on("blur", ".td-quantity", function () {
//    var value = $(this).children("input").val();
//    $(this).closest("td").html(value);
//    updateTotalValue($(this).closest("tr"));
//});

function GetBookingGuest(id) {
    
    $.ajax({
        url: "/Booking/GetGuestsByBookingId",
        type: "POST",
        data: {id : id},
        success: function (data) {
            $("#StayerTbl tbody").html(data);
            
        },
        error: function (xhr, err) {
            ShowMessage("Có lỗi trong truyền dữ liệu", 1);
            
        }
    });
}

function GetBookingPayments(bookingId) {
    
    $.ajax({
        url: "/Booking/GetPaymentsByBookingId",
        type: "POST",
        data: { bookingId: bookingId },
        success: function (data) {
            $("#paymentsTbl").html(data);
            
        },
        error: function (xhr, err) {
            ShowMessage("Có lỗi trong truyền dữ liệu", 1);
            
        }
    });
}

function addPayment() {
    var bookingId = $("#bookingId").val();
    var amount = $("#paymentAmount").val();
    var notes = $("#paymentNote").val();
    
    if (isNaN(bookingId)) {
        ShowMessage("Lỗi không thực hiện được yêu cầu.", 3);
        return;
    }

    if (isNaN(amount)) {
        ShowMessage("Số tiền không đúng định dạng. Vui lòng nhập lại.", 3);
        return;
    }

    $.ajax({
        url: "/Booking/AddPayment",
        type: "POST",
        data: {
            bookingId: bookingId,
            amount: amount,
            notes: notes
        },
        success: function (data) {
            if (data) {
                $("#addPaymentModal").modal('hide');
                GetBookingPayments(bookingId);
            } else {
                ShowMessage("Lỗi không thực hiện được yêu cầu.", 1);
            }
            
        },
        error: function (xhr, err) {
            ShowMessage("Có lỗi trong truyền dữ liệu", 1);
        }
    });

}