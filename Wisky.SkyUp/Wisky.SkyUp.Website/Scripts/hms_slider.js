﻿var milestoneDate = $.datepicker.parseDate("dd-mm-yy", "02-03-2014");
var numberOfHandler = 0;
var colors = ["#D36E6E", "#f0ad4e", "#428bca", "#5cb85c"];
var min = 0;
var max = 16;
var defaultPrice = 100;
var isUpdated = false;

//exchange slider value
function exchangeToDate(number) {
    var date = milestoneDate.getDate();
    var month = milestoneDate.getMonth();
    var year = milestoneDate.getFullYear();
    //deep copy
    var returnDate = new Date(year, month, date);
    returnDate = returnDate.setDate(returnDate.getDate() + number);
    returnDate = new Date(returnDate);
    return formatDate(returnDate);
};

function exchangeToFullDate(number) {
    var date = milestoneDate.getDate();
    var month = milestoneDate.getMonth();
    var year = milestoneDate.getFullYear();
    //deep copy
    var returnDate = new Date(year, month, date);
    returnDate = returnDate.setDate(returnDate.getDate() + number);
    returnDate = new Date(returnDate);
    return formatFullDate(returnDate);
};

function formatDate(d) {
    var dd = d.getDate();
    if (dd < 10) dd = '0' + dd;
    var mm = d.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
    var yy = d.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;
    //return dd + '-' + mm + '-' + yy;
    return dd + '-' + mm;
};

function formatFullDate(d) {
    var dd = d.getDate();
    if (dd < 10) dd = '0' + dd;
    var mm = d.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
    var yy = d.getFullYear();
    return dd + '-' + mm + '-' + yy;
}

function updateColors(values, max) {
    var colorstops = colors[0] + ", "; // start left with the first color
    var colorCount = 0;
    for (var i = 0; i < values.length; i++) {
        if (colorCount != colors.length - 1) {
            colorstops += colors[colorCount] + " " + 100 * values[i] / max + "%,";
            colorstops += colors[colorCount + 1] + " " + 100 * values[i] / max + "%,";
            colorCount++;
        } else {
            colorstops += colors[colorCount] + " " + 100 * values[i] / max + "%,";
            colorstops += colors[0] + " " + 100 * values[i] / max + "%,";
            colorCount = 0;
        }

    }
    // end with the last color to the right
    colorstops += colors[colorCount];

    /* Safari 5.1, Chrome 10+ */
    var css = '-webkit-linear-gradient(left,' + colorstops + ')';
    $('#slider').css('background-image', css);
}

//update fromdate and todate
function updateFragmentDate(index, value) {
    var element = $("#bookingDetailsTbl tbody").find("tr :eq(" + index + ")");
    var nextElement = $("#bookingDetailsTbl tbody").find("tr :eq(" + (index + 1) + ")")
    $(element).find("td :eq(3)").html(value);
    $(nextElement).find("td :eq(2)").html(value);
    updateTotalValue(element);
    updateTotalValue(nextElement);
}

function splitSlider() {
    var number = $("#numberOfFragment").val() - 1;
    if (numberOfHandler == number) {
        return;
    }
    if (max <= number) {
        return;
    }

    numberOfHandler = number;
    var handlers = new Array();
    var range = Math.floor((max - min) / number);
    for (var i = 0; i < number; i++) {
        handlers.push(1 + i * range);
    }
    $("#slider").slider("destroy");
    $("#progress-bar").find(".tooltip-handler-value").remove();
    createNewSlider(min, max, handlers);
    createHandlerValue();
    updateColors(handlers, max);
    updateFullTableInfo(handlers);
    isUpdated = true;
}

function createNewSlider(min, max, handlers, callback) {
    $("#slider").slider({
        animation: true,
        min: min,
        max: max,
        step: 1,
        values: handlers,
        slide: function (event, ui) {
            var index = $(ui.handle).siblings('a').andSelf().index(ui.handle);
            var values = $(this).slider('values');
            if ((index == 0 || ui.value > values[index - 1]) &&
                (index == values.length - 1 || ui.value < values[index + 1]) &&
                (ui.value < max) &&
                (ui.value > min)) {
                updateColors(ui.values, max);
                //print label
                var delay = function () {
                    var place = $("#slider a").index($(ui.handle));
                    var label = $("#handlerValue-" + place);
                    label.html(exchangeToDate(ui.value)).position({
                        my: 'center top',
                        at: 'center bottom',
                        of: ui.handle,
                        offset: "0, 0"
                    });
                }
                //update booking detail table
                updateFragmentDate(index, exchangeToFullDate(ui.value));
                setTimeout(delay, 5);
                isUpdated = true;
                //end print label
                return true;
            } else {
                return false;
            }
            //return (index == 0 || ui.value > values[index - 1]) &&
            //    (index == values.length - 1 || ui.value < values[index + 1]);

        }
    });

}

function createHandlerValue() {
    //create div contains value for handlers
    for (var i = numberOfHandler - 1 ; i >= 0; i--) {
        $("#slider").after("<div id='handlerValue-" + i + "' class='tooltip-handler-value'></div>");
    }
    
    setTimeout(function () {
        for (var j = 0; j < numberOfHandler; j++) {
            $("#handlerValue-" + j).html(exchangeToDate(+$('#slider').slider('values', j))).position({
                my: 'center top',
                at: 'center bottom',
                of: $("#slider a:eq(" + j + ")"),
                offset: "0, 0"
            });
        }
        $('#min').html(exchangeToDate(0)).position({
            my: 'center bottom',
            at: 'left top',
            of: $('#slider'),
            offset: "0, 0"
        });

        $('#max').html(exchangeToDate(max)).position({
            my: 'center bottom',
            at: 'right top',
            of: $('#slider'),
            offset: "0, 0"
        }); 
    }, 300);
}

function updateFullTableInfo(handlers) {
    var element = $("#bookingDetailsTbl tbody");
    var roomType = $("#room-type").html();
    var fromDateNumber = min;
    var toDateNumber = max;
    var number = +$("#txtQuantity-detail").val();
    var total = 0;
    var deleteButton = "<button class='delete-bookingDetail btn btn-mini btn-alert' data-id='722'>"
                         + "<i class='icon-remove'></i></button>";

    var content = "<tr><td>1</td><td>" + roomType + "</td><td>"
                  + exchangeToFullDate(min) + "</td>"
                  + "<td>" + exchangeToFullDate(handlers[0]) + "</td>"
                  + "<td class='td-price' onclick='editPriceValue()'>" + defaultPrice + "</td>"
                  + "<td class='td-quantity' onclick='editQuantity()'>" + number + "</td>"
                  + "<td>" + (number * defaultPrice * handlers[0]) + "</td><td>" + deleteButton + "</td></tr>";
    for (var i = 0; i < handlers.length - 1 ; i++) {
        content += "<tr><td>" + (i + 2) + "</td><td>" + roomType + "</td><td>"
                  + exchangeToFullDate(handlers[i]) + "</td>"
                  + "<td>" + exchangeToFullDate(handlers[i + 1]) + "</td>"
                  + "<td class='td-price' onclick='editPriceValue()'>" + defaultPrice + "</td>"
                  + "<td class='td-quantity' onclick='editQuantity()'>" + number + "</td>"
                  + "<td>" + (number * defaultPrice * (handlers[i + 1] - handlers[i])) + "</td><td>" + deleteButton + "</td></tr>";
    }
    content += "<tr><td>" + (handlers.length + 1) + "</td><td>" + roomType + "</td><td>"
                  + exchangeToFullDate(handlers[handlers.length - 1]) + "</td>"
                  + "<td>" + exchangeToFullDate(max) + "</td>"
                  + "<td class='td-price' onclick='editPriceValue()'>" + defaultPrice + "</td>"
                  + "<td class='td-quantity' onclick='editQuantity()'>" + number + "</td>"
                  + "<td>" + (number * defaultPrice * (max - handlers[handlers.length - 1])) + "</td><td>" + deleteButton + "</td></tr>";

    element.html(content);
}