﻿function ShowCreateUserForm() {
    

    $.ajax({
        url: "/User/CreateUser",
        type: "GET",
        success: function (result) {
            $("#modal").html(result).modal('show');
            
        }
    });
}

function checkEmail() {

    var email = document.getElementById("Email");
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
        ShowMessage("Email không hợp lệ!", 3);
        //document.getElementById("lblError").innerHTML = "Email không hợp lệ!";
        email.focus;
        return false;
    }
    return true;
}

function checkConfirmPass() {
    var pass = $("#Password").val();
    var confirmPass = $("#ConfirmPassword").val();
    if (pass != confirmPass) {
        ShowMessage("Hai mật khẩu chưa trùng khớp!", 3);
        //document.getElementById("lblError").innerHTML = "Password chưa trùng khớp!";
        return false;
    }
    return true;
}

function CreateUser() {

    //    var form = $('#createUserForm');
    var isValid = false;
    var username = $("#Username").val();
    $.ajax({
        url: "/User/SearchByUsername",
        type: "GET",
        data: { username: username },
        success: function (resultData) {
            if (resultData == '0') {
                if (checkEmail() && checkConfirmPass()) {
                    isValid = true;
                }
                //Create Json object
                var user = {
                    Username: $("#Username").val(),
                    Password: $("#Password").val(),
                    ConfirmPassword: $("#ConfirmPassword").val(),
                    Email: $("#Email").val()
                };

                if ($("#Password").val() == "") {
                    ShowMessage("Mật khẩu không được để trống", 3);
                    return false;
                }
                if ($("#Username").val() == "") {
                    ShowMessage("Tên người dùng không được để trống", 3);
                    return false;
                }

                if (isValid) {
                    $.ajax({
                        url: "/User/CreateUser",
                        data: JSON.stringify(user),
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        success: function (result) {
                            if (result != '0') {
                                $("#modal").modal("hide");
                                //                                $("#userTable").fadeOut(200, function () {
                                //                                    $("#userList").prepend(result);
                                //                                    $("#userTable").fadeIn(200);
                                //                                });
                                $('table > tbody > tr:first').before(result);
                                ShowMessage('Tạo người dùng mới thành công', 2);
                            }
                            else {
                                ShowMessage('Có lỗi xảy ra',1);
                                //document.getElementById("lblError").innerHTML = "Có lỗi xảy ra";
                            }
                        },
                        error: function () {
                            ShowMessage('Có lỗi xảy ra', 1);
                            //document.getElementById("lblError").innerHTML = "Có lỗi xảy ra";
                        }
                    });
                }
            } else {
                ShowMessage('Người dùng đã tồn tại', 3);
                //document.getElementById("lblError").innerHTML = "Người dùng đã tồn tại";
            }
        },
        error: function () {
            ShowMessage('Có lỗi xảy ra', 1);
            //document.getElementById("lblError").innerHTML = "Có lỗi xảy ra";
        }
    });
}

function deleteUser(x) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn có muốn xóa tài khoản này?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/UserAdministration/DeleteUser",
                         type: "POST",
                         data: { id: x.id },
                         error: function () {
                             showMessage("Xóa người dùng thất bại",1);
                         },
                         success: function (result) {
                             $("#userTable").fadeOut(200, function () {
                                 $("#trUser-" + x.id).remove();
                                 $("#userTable").fadeIn();
                             });
                             showMessage('Xóa người dùng thành công!',2);
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn xóa tài khoản này?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/UserAdministration/DeleteUser",
    //            type: "POST",
    //            data: { id: x.id},
    //            error: function () {
    //                alert("Delete fail");
    //            },
    //            success: function (result) {
    //                $("#userTable").fadeOut(200, function () {
    //                    $("#trUser-" + x.id).remove();
    //                    $("#userTable").fadeIn();
    //                });
    //            }
    //        });
    //    }
    //});
}

function setUserToAdmin(x, role) {
    $.ajax({
        url: "/UserAdministration/AddToRole",
        type: "POST",
        data: { id: x.id, role: role },
        error: function () {
            ShowMessage("Thêm quyền quản lý thất bại",1);
        },
        success: function (result) {
          $(".search-field > input").css("height", "27px");
          ShowMessage("Thêm quyền quản lý thành công", 2);
        }
    });


    //bootbox.dialog({
    //    title: 'Xác nhận',
    //    message: "<h6>Bạn có muốn cấp quyền quản lý cho tài khoản này?</h6>",
    //    buttons:
    //    {
    //        "ok":
    //         {
    //             "label": "<i class='icon-remove'></i> Đồng ý!",
    //             "className": "btn-sm btn-success",
    //             "callback": function () {
    //                 $.ajax({
    //                     url: "/UserAdministration/AddToRole",
    //                     type: "POST",
    //                     data: { id: x.id, role: role },
    //                     error: function () {
    //                         alert("Set fail");
    //                     },
    //                     success: function (result) {

    //                         $("#userTable").fadeOut(200, function () {
    //                             $.ajax({
    //                                 url: "/User/SearchByUserId",
    //                                 type: "GET",
    //                                 data: { userId: x.id },
    //                                 success: function (resultUser) {

    //                                     $("#trUser-" + x.id).replaceWith(resultUser);

    //                                     $(".chosen-select").chosen();
    //                                     $(".default").css("height", "20px");
    //                                     $("#userTable").fadeIn();


    //                                 }
    //                             });
    //                         });
    //                     }
    //                 });
    //             }
    //         },
    //        "close":
    //         {
    //             "label": "<i class='icon-ok'></i> Đóng",
    //             "className": "btn-sm btn-danger",
    //             "callback": function () {
    //                 bootbox.hideAll();

    //             }
    //         }
    //    }
    //});

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn cấp quyền quản lý cho tài khoản này?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/UserAdministration/AddToRole",
    //            type: "POST",
    //            data: { id: x.id, role: "admin" },
    //            error: function () {
    //                alert("Set fail");
    //            },
    //            success: function (result) {
    //                $("#userTable").fadeOut(200, function () {
    //                    $.ajax({
    //                        url: "/User/SearchByUserId",
    //                        type: "GET",
    //                        data: { userId: x.id },
    //                        success: function (resultUser) {
    //                            $("#trUser-" + x.id).replaceWith(resultUser);
    //                            $("#userTable").fadeIn();
    //                        }
    //                    });
    //                });
    //            }
    //        });
    //    }
    //});
}

function removeRoleAdmin(x, role) {
    $.ajax({
        url: "/UserAdministration/RemoveFromRole",
        type: "POST",
        data: { id: x.id, role: role },
        error: function () {
            ShowMessage("Xóa quyền quản lý thất bại", 1);
        },
        success: function (result) {
            $(".search-field > input").css("height", "27px");
            ShowMessage("Xóa quyền quản lý thành công", 2);
        }
    });

    //bootbox.dialog({
    //    title: 'Xác nhận',
    //    message: "<h6>Bạn có muốn bỏ quyền quản lý của tài khoản này?</h6>",
    //    buttons:
    //    {
    //        "ok":
    //         {
    //             "label": "<i class='icon-remove'></i> Đồng ý!",
    //             "className": "btn-sm btn-success",
    //             "callback": function () {
    //                 $.ajax({
    //                     url: "/UserAdministration/RemoveFromRole",
    //                     type: "POST",
    //                     data: { id: x.id, role: role },
    //                     error: function () {
    //                         alert("Set fail");
    //                     },
    //                     success: function (result) {
    //                         $(".default").css("height", "20px");
    //                         $("#userTable").fadeOut(200, function () {
    //                             $.ajax({
    //                                 url: "/User/SearchByUserId",
    //                                 type: "GET",
    //                                 data: { userId: x.id },
    //                                 success: function (resultUser) {
    //                                     $("#trUser-" + x.id).replaceWith(resultUser);

    //                                     $(".chosen-select").chosen();
    //                                     $(".default").css("height", "20px");

    //                                     $("#userTable").fadeIn();
    //                                 }
    //                             });
    //                         });
    //                     }
    //                 });
    //             }
    //         },
    //        "close":
    //         {
    //             "label": "<i class='icon-ok'></i> Đóng",
    //             "className": "btn-sm btn-danger",
    //             "callback": function () {
    //                 bootbox.hideAll();
    //             }
    //         }
    //    }
    //});
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn bỏ quyền quản lý của tài khoản này?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/UserAdministration/RemoveFromRole",
    //            type: "POST",
    //            data: { id: x.id, role: "admin" },
    //            error: function () {
    //                alert("Set fail");
    //            },
    //            success: function (result) {
    //                $("#userTable").fadeOut(200, function () {
    //                    $.ajax({
    //                        url: "/User/SearchByUserId",
    //                        type: "GET",
    //                        data: { userId: x.id },
    //                        success: function (resultUser) {
    //                            $("#trUser-" + x.id).replaceWith(resultUser);
    //                            $("#userTable").fadeIn();
    //                        }
    //                    });
    //                });
    //            }
    //        });
    //    }
    //});
}

function ChangeApproveUser(isApprove, userId) {
    var Approve;
    if (isApprove == 0) {
        Approve = false;
    } else if (isApprove == 1) {
        Approve = true;
    }
    $.ajax({
        url: "/UserAdministration/ChangeApproval",
        type: "POST",
        data: { id: userId, isApproved: Approve },
        success: function (resultUser) {
            $("#userTable").fadeOut(200, function () {
                $("#trUser-" + userId).replaceWith(resultUser);
                $("#userTable").fadeIn();
            });
        }
    });
}