﻿
var hms = {};
hms.webdb = {};
hms.webdb.db = null;

function getOrderModel(row) {
    return model = {
        OrderId: row.OrderId,
        ProductId: row.ProductId,
        RoomId: row.RoomId,
        Quantity: row.Quantity,
        Price: row.Price,
        OrderDate: row.OrderDate,
        IsUploaded: row.IsUploaded,
        IsNew: row.IsNew
    };
}

hms.webdb.open = function () {
    var dbSize = 5 * 1024 * 1024;
    hms.webdb.db = openDatabase("OfflineDatabase", "1", "HMS Database", dbSize);
};
hms.webdb.onError = function (tx, e) {
    console.log(e.message);
};
hms.webdb.onSuccess = function (tx, e) {
    //alert("Success");
};

//start Room section
hms.webdb.createRoomTable = function (callback) {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
            "Room(Id INTEGER PRIMARY KEY, Name TEXT, RoomStatus INTEGER, BuildingId INTEGER, BuildingName TEXT,FeeType INTEGER)", [], callback, hms.webdb.onError);
    });
};
hms.webdb.dropRoomTable = function () {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE room", []);
    });
};
hms.webdb.addRoom = function (id, name, status, buildingId, buildingName, feeType) {
    var db = hms.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO Room(Id,Name,RoomStatus,BuildingId,BuildingName,FeeType) VALUES (?,?,?,?,?,?)",
            [id, name, status, buildingId, buildingName, feeType],
            hms.webdb.onSuccess,
            hms.webdb.onError);
    });
}
//end Room section

//start Rent section
hms.webdb.createRentTable = function (callback) {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
            "Rent(Id INTEGER PRIMARY KEY, InvoiceId TEXT,RoomId INTEGER,"
            + "CheckInDate TEXT, CheckOutDate TEXT,RentFee INTEGER,OrderFee INTEGER,"
            + "DiscountFee INTEGER,AdditionFee INTEGER,TotalPayment INTEGER,RentType INTEGER,"
            + "Notes TEXT,FeeDescription TEXT,IsFixedPrice BOOLEAN,Status BOOLEAN)", [], callback, hms.webdb.onError);
    });
};
hms.webdb.dropRentTable = function () {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE Rent", []);
    });
};
hms.webdb.addRent = function (id, invoiceId, roomId, checkInDate, checkOutDate, rentFee, orderFee, discountFee, additionFee, totalPayment, rentType, notes, feeDescription, isFixedPrice, status) {
    var db = hms.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT OR IGNORE INTO Rent(Id,InvoiceId,RoomId,CheckInDate,CheckOutDate,"
            + "RentFee,OrderFee,DiscountFee,AdditionFee,TotalPayment,RentType,Notes,"
            + "FeeDescription,IsFixedPrice,Status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            [id, invoiceId, roomId, checkInDate, checkOutDate, rentFee, orderFee, discountFee, additionFee, totalPayment, rentType, notes, feeDescription, isFixedPrice, status],
            hms.webdb.onSuccess,
            hms.webdb.onError);
    });
};
//end Rent section

//start Order Detail section
hms.webdb.createOrderDetailTable = function (callback) {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
            "OrderDetail(Id INTEGER PRIMARY KEY,RentId INTEGER,ProductId INTEGER,"
            + "Unit TEXT, ProductName TEXT,ProductPrice INTEGER,Quantity INTEGER,"
            + "TotalPrice INTEGER, OrderDate TEXT, Status INTEGER)", [], callback, hms.webdb.onError);
    });
};
hms.webdb.dropOrderDetailTable = function () {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE OrderDetail", []);
    });
};
hms.webdb.addOrderDetail = function (id, rentId, productId, unit, productName, productPrice, quantity, totalPrice, orderDate, status) {
    var db = hms.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT OR IGNORE INTO OrderDetail(Id,RentId,ProductId,Unit,ProductName,"
            + "ProductPrice,Quantity,TotalPrice,OrderDate,Status) VALUES (?,?,?,?,?,?,?,?,?,?)",
            [id, rentId, productId, unit, productName, productPrice, quantity, totalPrice, orderDate, status],
            hms.webdb.onSuccess,
            hms.webdb.onError);
    });
};
//end Order Detail section

//start Fee section
hms.webdb.createFeeTable = function (callback) {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
            "Fee(Id INTEGER PRIMARY KEY,RentId INTEGER,Username TEXT,"
            + "Amount INTEGER, Notes TEXT,AddTime TEXT,"
            + "IsAddition BOOLEAN,Status INTEGER)", [], callback, hms.webdb.onError);
    });
};
hms.webdb.dropFeeTable = function () {
    hms.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE Fee", []);
    });
};
hms.webdb.addFee = function (id, rentId, username, amount, notes, addTime, isAddition, status) {
    var db = hms.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT OR IGNORE INTO Fee(Id,RentId,Username,Amount,Notes,"
            + "AddTime,IsAddition,Status) VALUES (?,?,?,?,?,?,?,?)",
            [id, rentId, username, amount, notes, addTime, isAddition, status],
            hms.webdb.onSuccess,
            hms.webdb.onError);
    });
};
//end Fee section

hms.webdb.getRooms = function() {
    $.ajax({
                url: "/Offline/GetAllRooms",
                type: "GET",
                success: function (data) {
                    var result = data.rooms;
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        hms.webdb.addRoom(item.Id, item.Name, item.RoomStatus, item.BuildingId, item.BuildingName, item.FeeType);
                    }              
                }
            });
};
hms.webdb.getRents = function() {
    $.ajax({
                url: "/Offline/GetAllRents",
                type: "GET",
                success: function (data) {
                    var result = data.rents;
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        hms.webdb.addRent(item.Id, item.InvoiceId, item.RoomId, new Date(item.CheckInDate).toJSON(), new Date(item.CheckOutDate).toJSON(), item.RentFee, item.OrderFee, item.DiscountFee, item.AdditionFee, item.TotalPayment, item.RentType, item.Notes, item.FeeDescription, item.IsFixedPrice);
                    }
                }
            });
};
hms.webdb.getOrderDetails = function() {
    $.ajax({
                url: "/Offline/GetAllServices",
                type: "GET",
                success: function (data) {
                    var result = data.services;
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        hms.webdb.addOrderDetail(item.Id, item.RentId, item.ProductId, item.Unit, item.ProductName, item.ProductPrice, item.Quantity, item.TotalPrice, new Date(item.OrderDate).toJSON(), item.Status);
                    }
                }
            });
};
hms.webdb.getFees = function() {
    $.ajax({
                    url: "/Offline/GetAllFeeChanges",
                    type: "GET",
                    success: function (data) {
                        var result = data.feeChanges;
                        for (var counter = 0; counter < result.length; counter++) {
                            var item = result[counter];
                            hms.webdb.addFee(item.Id, item.RentId, item.Username, item.Amount, item.Notes, new Date(item.AddTime).toJSON(), item.IsAddition,0);
                        }
                    }
                });
};
function init() {
    hms.webdb.open();
}