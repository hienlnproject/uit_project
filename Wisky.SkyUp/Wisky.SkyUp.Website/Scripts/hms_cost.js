﻿//function SearchRent() {
//    var beginDate = $('#StartDate').val();
//    var endDate = $('#EndDate').val();
//    beginDate = ConvertDateVNToUS(beginDate);
//    endDate = ConvertDateVNToUS(endDate);
//    $.ajax({
//        url: "/Admin/SearchRent",
//        type: "GET",
//        data: { StartTime: beginDate, EndTime: endDate },
//        success: function (result) {
//            $("#RentTable").fadeOut(300, function () {
//                $('#RentTable').html(result);
//                $("#RentTable").fadeIn(300);
//            });
//        }
//    });
//}

function GetCostByStatus(status, pageIndex, pageSize) {
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
    

    $("#costStatus").val(status);

    //$.ajax({
    //    url: "/Cost/SearchCostByStatus",
    //    type: "GET",
    //    data: { status: status, pageIndex: pageIndex, pageSize: pageSize },
    //    success: function (result) {
    //        $("#CostTable").fadeOut(100, function () {
    //            $('#CostTable').html(result);
    //            $("#CostTable").fadeIn(100);
    //        });

    //    }
    //});

    RefreshTable();
}


function ChangeCostStatus(costId, fromStatus, toStatus, index, isConfirm, confirmText) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: confirmText,
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Cost/ChangeCostStatus",
                         type: "GET",
                         data: { costId: costId, fromStatus: fromStatus, toStatus: toStatus, index: index },
                         success: function (result) {
                             if (result == "1") {
                                 if (toStatus == 2) {
                                     ShowMessage("Duyệt thành công", 2);
                                 }
                                 else if (toStatus == 3 || toStatus == 4 || toStatus == 5) {
                                     ShowMessage("Hủy thành công", 2);
                                 }

                             } else {
                                 if (toStatus == 2) {
                                     ShowMessage("Duyệt thất bại", 1);
                                 }
                                 else if (toStatus == 3 || toStatus == 4 || toStatus == 5) {
                                     ShowMessage("Hủy thất bại", 1);
                                 }

                             }
                             LoadNewCost();

                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
    //var rs = true;
    //if (isConfirm) {
    //    rs = confirm(confirmText);
    //}

    //if (rs) {
    //    $.ajax({
    //        url: "/Cost/ChangeCostStatus",
    //        type: "GET",
    //        data: { costId: costId, fromStatus: fromStatus, toStatus: toStatus, index: index },
    //        success: function (result) {
    //            LoadNewCost();
    //        }
    //    });

    //}

}


function ShowAddCostModal() {
    

    $.ajax({
        url: "/Cost/ShowAddCostModal",
        type: "GET",
        success: function (result) {
            $("#modal").html(result);
            $("#modal").modal('show');
            //  $("#Cost_CostDate").datetimepicker();
            $("#Cost_CostDate").datetimepicker({ dateFormat: 'dd-mm-yy' });
        }
    });
}

function ShowUpdateCostModal(id) {
    

    $.ajax({
        url: "/Cost/ShowUpdateCostModal",
        type: "GET",
        data: { costId: id },
        success: function (result) {
            $("#modal").html(result).modal("show");
            
        }
    });

}


function SubmitAddCost() {
    var paidPerson = $("#Cost_PaidPerson").val();

    if (paidPerson.trim() == "") {
        ShowMessage("Vui lòng nhập người nhận tiền", 3);
        return;
    }

    var sAmount = $("#Cost_Amount").val();

    var amount = 0;


    if (sAmount.trim() == "") {
        ShowMessage("Vui lòng nhập số tiền", 3);
        return;
    }

    if (!$.isNumeric(sAmount)) {
        ShowMessage("Vui lòng nhập chữ số cho số tiền", 3);
        return;
    }
    amount = parseInt(sAmount);

    if (amount <= 0) {
        ShowMessage("Vui lòng nhập số tiền lớn hơn 0", 3);
        return;
    }

    var description = $("#Cost_CostDescription").val();

    if (description.trim() == "") {
        ShowMessage("Vui lòng nhập nội dung", 3);
        return;
    }

    var date = $('#Cost_CostDate').val();

    if (date.trim() == "") {
        ShowMessage("Vui lòng nhập ngày chi", 3);
        return;
    }

    date = ConvertDateVNToUS(date);
    $("#Cost_CostDate").val(date);
    $.ajax({
        url: "/Cost/AddCost",
        data: $('#formCost').serialize(),
        type: "GET",
        success: function (result) {

            if (result == "1") {
                LoadNewCost();
                $("#modal").modal("hide");
                ShowMessage("Thêm chi phí thành công", 2);
            } else {
                ShowMessage("Thêm chi phí thất bại", 1);
            }

        }
    });


}

function SubmitUpdateCost() {
    var paidPerson = $("#Cost_PaidPerson").val();

    if (paidPerson.trim() == "") {
        ShowMessage("Vui lòng nhập người nhận tiền", 3);
        return;
    }

    var sAmount = $("#Cost_Amount").val();

    var amount = 0;

    try {
        if (sAmount.trim() == "") {
            ShowMessage("Vui lòng nhập số tiền", 3);
            return;
        }
        amount = parseInt(sAmount);
    } catch (e) {
        ShowMessage("Vui lòng nhập số cho số tiền", 3);
        return;
    }

    if (amount <= 0) {
        ShowMessage("Vui lòng nhập số tiền lớn hơn 0", 3);
        return;
    }

    var description = $("#Cost_CostDescription").val();

    if (description.trim() == "") {
        ShowMessage("Vui lòng nhập nội dung", 3);
        return;
    }

    var date = $('#Cost_CostDate').val();

    if (date.trim() == "") {
        ShowMessage("Vui lòng nhập ngày chi", 3);
        return;
    }

    // date = ConvertDateVNToUS(date);
    //  $("#Cost_CostDate").val(date);
    $.ajax({
        url: "/Cost/UpdateCost",
        data: $('#updateCostForm').serialize(),
        type: "POST",
        success: function (result) {

            if (result == "1") {
                LoadNewCost();
                $("#modal").modal("hide");
                ShowMessage("Chỉnh sửa chi phí thành công", 2);
            } else {
                ShowMessage("Chỉnh sửa chi phí thất bại", 1);
            }

        }

    });
}

function LoadNewCost() {
    //$(".btn-group").find(".btn").removeClass("active");
    //$('#btnNewCost').addClass('active');
    //1: New Status, page 1, 20 item in pages    
    //GetCostByStatus(1, 1, 20);
    RefreshTable();
}




function ExportToExcel() {

    var beginDate = $('#StartDate').val();
    var endDate = $('#EndDate').val();
    beginDate = ConvertDateVNToUS(beginDate);
    endDate = ConvertDateVNToUS(endDate);
    $.ajax({
        url: "/Admin/ExportToExcel",
        type: "GET",
        data: { StartTime: beginDate, EndTime: endDate }
    });
}
