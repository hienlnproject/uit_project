﻿//////////////////////////////////////// HOTEL DATA ///////////////////////////////

var hotelName = 'KIM NGÂN';
var address = "32-34 KDC Trung Son";
var phone = "08-54333339";
//////////////////////////////////////////////////////////////////////////////////

function CloseRentDetailForm() {
    var roomId = $("#roomId").val();
    var rentType = $("#hdfRentType").val();

    //Kiem tra rentType o mainform neu khac thi load
    $(".aRoom").removeClass('active');
    var roomRentType = $("#r-" + roomId).attr("data-rentType");
    if (roomRentType != rentType) {
        UpdateRoomStatus(roomId);
    }
}

function UpdateRoomStatus(roomId) {

    $.ajax({
        url: "/Cafe/GetRoom",
        type: "GET",
        data: { id: roomId },
        success: function (result) {
            $("#contain-" + roomId).fadeOut(200, function () {
                $("#contain-" + roomId).html(result);
                $("#contain-" + roomId).fadeIn();

            });
        }
    });

}


////////////////////////// SHOW RENT DETAIL //////////////////////////////////



function ShowRent(rentId) {
    
    $("#modal").on('hide', function () {
        CloseRentDetailForm();
    });

    $.ajax({
        url: "/Cafe/ServiceFeeDetail",
        type: "GET",
        data: { rentId: rentId },
        success: function (result) {
            $("#modal").html(result).modal('show');
            FocusRentType();
            SetDateTimePicker();
            changeStatusUpdateButton(true);
            applyScrollbar();
            $container = $('#iso-container');
            $container.isotope({
                // options
                itemSelector: '.serviceItem',
                layoutMode: 'fitRows'
            });
        }
    });


}

function ShowRentDetailReadOnly(rentid) {
    
    $.ajax({
        url: "/Cafe/ViewServiceFeeReadonly",
        type: "GET",
        data: { rentId: rentid },
        success: function (result) {
            $("#modal").html(result).modal('show');
            applyScrollbar();
            
        }
    });
}

function FocusRentType() {
    //Focus rent type button
    var rentType = $("#hdfRentType").val();
    $('[data-rentType]').removeClass("active");
    var btnCurrentFee = $('[data-rentType =' + rentType + ']');
    btnCurrentFee.addClass("active");
}


function applyScrollbar() {
    $('#rentFeeDetail').popover();
    $("#wrapMenu").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapOrderTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapPaymentTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapCustomerTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
}


////////////////////// RENT UPDATING - CHECK OUT /////////////////////////

function UpdateRentInfo() {
    event.preventDefault();
    var rentId = $("#rentId").val();
    var checkInDate = ConvertDateVNToUS($('#CheckInDate').val());
    var invoiceId = $("#InvoiceID").val();
    var rentType = $("#hdfRentType").val();
    var bikeId = $("#BikeID").val();
    var notes = $("#Notes").val();
    var priceGroup = $("#PriceGroupID").val();
    $.ajax({
        url: "/Cafe/UpdateRentInfo",
        type: "GET",
        data: { rentId: rentId, invoiceId: invoiceId, checkInDate: checkInDate, rentType: rentType, bikeId: bikeId, notes: notes, priceGroup: priceGroup },
        success: function (result) {
            if (result == "0") {
                ShowMessage("Cập nhật không thành công!", 1);
            }
            else {
                $("#rentInfo").fadeOut(200, function () {
                    $("#rentInfo").fadeIn();
                    changeStatusUpdateButton(true);
                    //Update number of room.
                    var RentTypeBeforeUpdate = $("#RentTypeBeforeUpdate").val();
                    //Neu thay doi rent type thi moi cap nhat so luong.
                    if (RentTypeBeforeUpdate != rentType) {
                        //Update number of room of rent type before update.
                        //Thue gio.
                        if (RentTypeBeforeUpdate == 1) {
                            var NumberOfRentHour = $("#NumberOfRentHour").val();
                            NumberOfRentHour--;
                            $("#NumberOfRentHour").val(NumberOfRentHour);
                            $("#spanNumberOfRentHour").html("<span class='badge badge-important' id='spanNumberOfRentHour'>" + NumberOfRentHour + "</span>");
                        } else if (RentTypeBeforeUpdate == 2) {
                            //Qua dem.
                            var NumberOfRentNight = $("#NumberOfRentNight").val();
                            NumberOfRentNight--;
                            $("#NumberOfRentNight").val(NumberOfRentNight);
                            $("#spanNumberOfRentNight").html("<span class='badge badge-inverse' id='spanNumberOfRentNight'>" + NumberOfRentNight + "</span>");
                        } else if (RentTypeBeforeUpdate == 3) {
                            //Thue ngay
                            var NumberOfRentDay = $("#NumberOfRentDay").val();
                            NumberOfRentDay--;
                            $("#NumberOfRentDay").val(NumberOfRentDay);
                            $("#spanNumberOfRentDay").html("<span class='badge badge-info' id='spanNumberOfRentDay'>" + NumberOfRentDay + "</span>");
                        }
                        //Update number of room of rent type after update.
                        //Thue gio.
                        if (rentType == 1) {
                            var NumberOfRentHour = $("#NumberOfRentHour").val();
                            NumberOfRentHour++;
                            $("#NumberOfRentHour").val(NumberOfRentHour);
                            $("#spanNumberOfRentHour").html("<span class='badge badge-important' id='spanNumberOfRentHour'>" + NumberOfRentHour + "</span>");
                        } else if (rentType == 2) {
                            //Qua dem.
                            var NumberOfRentNight = $("#NumberOfRentNight").val();
                            NumberOfRentNight++;
                            $("#NumberOfRentNight").val(NumberOfRentNight);
                            $("#spanNumberOfRentNight").html("<span class='badge badge-inverse' id='spanNumberOfRentNight'>" + NumberOfRentNight + "</span>");
                        } else if (rentType == 3) {
                            //Thue ngay
                            var NumberOfRentDay = $("#NumberOfRentDay").val();
                            NumberOfRentDay++;
                            $("#NumberOfRentDay").val(NumberOfRentDay);
                            $("#spanNumberOfRentDay").html("<span class='badge badge-info' id='spanNumberOfRentDay'>" + NumberOfRentDay + "</span>");
                        }
                    }
                });
                ShowMessage('Cập nhật thành công', 2);
                try {
                    $("#orderDatatable").dataTable()._fnAjaxUpdate();
                } catch (e) {

                }
            }
        }
        ,
        error: function () {
            ShowMessage("Cập nhật không thành công!", 1);
        }
    });

}

function changeStatusUpdateButton(disable) {
    if (disable) {
        $("#btnUpdateRentInfo").attr("disabled", "disabled");
        $("#btnUpdateRentInfo").removeClass("btn-info");
        $("#btnCheckOut").removeAttr("disabled", "disabled");
    }
    else {
        $("#btnUpdateRentInfo").removeAttr("disabled");
        $("#btnUpdateRentInfo").addClass("btn-info");
        $("#btnCheckOut").attr("disabled", "disabled");

    }
}


function CheckOut() {

    var rentId = $("#rentId").val();
    //var checkOutDate = $('#CheckOutDate').val(); //Do co che tu sinh ID cua MVC 
    var date = new Date();
    var checkOutDate = date.toJSON();
    var checkInDate = $('#CheckInDate').val();
    var rentType = $("#hdfRentType").val();
    var roomid = $("#roomId").val();
    var editable = $("#editable").val();

    var isPayAll = !($('#isNotPayAll').is(':checked'));

    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Thanh toán?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Cafe/CheckOut",
                         type: "GET",
                         data: {
                             rentId: rentId, checkInDate: ConvertDateVNToUS(checkInDate), checkOutDate: ConvertDateVNToUS(checkOutDate),
                             isPayAll: isPayAll, editable: editable
                         },
                         success: function (result) {
                             if (result.rs || result.isCheckout) {
                                 $('#modal').modal('hide');

                                 if (result.isCheckout) {
                                     ShowMessage("Bàn này đã được thanh toán", 3);

                                     try {
                                         UpdateLabelPanel();
                                     } catch (e) {
                                     }

                                 }

                                 UpdateRoomStatus(roomid);

                                 try {
                                     //   RefreshTable();
                                     $("#orderDatatable").dataTable()._fnAjaxUpdate();
                                 } catch (e) {
                                 }

                                 if (result.isPrepared) {
                                     var NumberOfPrepare = $("#NumberOfPrepare").val();
                                     NumberOfPrepare++;
                                     $("#NumberOfPrepare").val(NumberOfPrepare);
                                     $("#spanNumberOfPrepare").html(NumberOfPrepare);
                                 } else {
                                     var NumberOfReady = $("#NumberOfReady").val();
                                     NumberOfReady++;
                                     $("#NumberOfReady").val(NumberOfReady);
                                     $("#spanNumberOfReady").html(NumberOfReady);
                                 }

                                 if (rentType == 1) {
                                     var NumberOfRentHour = $("#NumberOfRentHour").val();
                                     NumberOfRentHour--;
                                     $("#NumberOfRentHour").val(NumberOfRentHour);
                                     $("#spanNumberOfRentHour").html(NumberOfRentHour);
                                 } else if (rentType == 2) {
                                     //Qua dem.
                                     var NumberOfRentNight = $("#NumberOfRentNight").val();
                                     NumberOfRentNight--;
                                     $("#NumberOfRentNight").val(NumberOfRentNight);
                                     $("#spanNumberOfRentNight").html(NumberOfRentNight);
                                 } else if (rentType == 3) {
                                     //Thue ngay
                                     var NumberOfRentDay = $("#NumberOfRentDay").val();
                                     NumberOfRentDay--;
                                     $("#NumberOfRentDay").val(NumberOfRentDay);
                                     $("#spanNumberOfRentDay").html(NumberOfRentDay);
                                 }

                             } else {
                                 ShowMessage("Thanh toán không thành công", 1);
                             }


                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

   

}

function printReport() {
    var docprint = window.open("", "", 'left=100,top=100,width=700,height=400');
    docprint.document.open();
    docprint.document.write('<html><body>');
    docprint.document.write('HÓA ĐƠN TÍNH TIỀN');
    docprint.document.write('</br>');
    docprint.document.write('cửa hàng ' + hotelName + '<br/>ĐC: ' + address + '<br/>ĐT:' + phone);
    docprint.document.write('</br>*********************');
    docprint.document.write('<br/>Giờ vào: ' + $("#Rent_CheckInDate").val());
    docprint.document.write('<br/>Giờ ra: ' + $("#Rent_CheckOutDate").val());
    docprint.document.write('<br/>Tiền phòng:<strong>' + $("#rentFee").val() + '<strong/>');
    docprint.document.write('<br/>Chi tiết tiền phòng:<br/>' + $("#rentFeeDetail").attr('data-content'));
    docprint.document.write('<br/>Dịch vụ <br/>');
    //Print service
    $('#tblServiceDetail tr').each(function () {
        if (!$(this).hasClass('trHead')) {
            var productName = $(this).find('td').eq(1).html();
            var quantity = $(this).find('td').eq(2).html();
            var totalPrice = $(this).find('td').eq(3).html();
            docprint.document.write(productName + '  ' + quantity + '  ' + totalPrice + '</br>');
        }

    });

    docprint.document.write('Tiền dịch vụ:<strong>' + $("#orderFee").val() + '<strong/>');
    docprint.document.write('<br/>Tổng cộng:<strong>' + $("#totalFee").val() + '<strong/>');


    docprint.document.write('</body></html>');
    docprint.document.close();
    docprint.print();
    docprint.close();
}



///////////////////// RENT FEE CALCULATOR //////////////////////////////

function SetDateTimePicker() {

    //Chinh datetime picker
    var checkindate = $('#CheckInDate').val();
    $('#CheckInDate').val(ConvertDateVNToUS(checkindate));
    $('#CheckInDate').datetimepicker({
        onClose: function (dateText, inst) {
            var rentType = $("#hdfRentType").val();
            // UpdateRentFee(rentType);
        },
        dateFormat: 'dd-mm-yy'
    });
    $('#CheckInDate').datetimepicker('setDate', (new Date($('#CheckInDate').val())));
    //Chinh Datetime picker

    var checkoutdate = $('#CheckOutDate').val();
    $('#CheckOutDate').val(ConvertDateVNToUS(checkoutdate));
    $('#CheckOutDate').datetimepicker({
        onClose: function (dateText, inst) {
            var rentType = $("#hdfRentType").val();
            // UpdateRentFee(rentType);
        },
        dateFormat: 'dd-mm-yy'
    });

    $('#CheckOutDate').datetimepicker('setDate', (new Date($('#CheckOutDate').val())));
}

function ChangePriceGroup() {
    var rentType = $("#hdfRentType").val();
    // UpdateRentFee(rentType);
}

//Cap nhat lai gia tri cua Fee thue phong 
function UpdateRentFee(rentType) {
    return;
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');

    var rentId = $("#rentId").val();
    var checkOutDate = ConvertDateVNToUS($('#CheckOutDate').val()); //Do co che tu sinh ID cua MVC 
    var checkInDate = ConvertDateVNToUS($('#CheckInDate').val());
    var priceGroup = $('#PriceGroupID').val();

    //Thay doi phi khi: gio vao thay doi, gio ra thay doi , hinh thuc thue thay doi
    //InvoiceID, checkInDate, Bike, Note duoc bat bang Onchange, chi con hdfRentType
    var oldRentType = $("#hdfRentType").val();


    $.ajax({
        url: "/Cafe/CalculateRentFee",
        type: "GET",
        data: { rentID: rentId, rentType: rentType, checkInDate: checkInDate, checkOutDate: checkOutDate, priceGroup: priceGroup },
        success: function (result) {

            $('#rentFeeDetail').attr("data-content", result.description);
            $("#rentFee").val(addCommas(result.rentFee));
            UpdateEntireFee(result.rentFee, rentType);

            if (rentType != oldRentType) {
                changeStatusUpdateButton(false);
                $("#hdfRentType").val(rentType);

            }

        }
    });

}

function UpdateEntireFee() {
    if ($("#rentFee").val() == undefined) {
        return;
    }
    
    var rentFee = parseInt($("#rentFee").val().replace(/,/g, ""));
    var orderFee = ($("#hdfOrderFee").val() != null) ? parseInt($("#hdfOrderFee").val()) : parseInt($("#orderFee").val().replace(/,/g, "")); //Lay tu hidden service form

    var addionFee = ($("#hdfTotalAddition").val() != null) ? parseInt($("#hdfTotalAddition").val()) : parseInt($("#addionFee").val().replace(/,/g, ""));
    var discountFee = ($("#hdfTotalDiscount").val() != null) ? parseInt($("#hdfTotalDiscount").val()) : parseInt($("#discountFee").val().replace(/,/g, ""));
    var totalFee = rentFee + orderFee + addionFee - discountFee;

    var totalPayment = ($("#hdfTotalPayment").val() != null) ? parseInt($("#hdfTotalPayment").val()) : parseInt($("#totalPayment").val().replace(/,/g, ""));

    var remainFee = totalFee - totalPayment;

    $("#orderFee").val(addCommas(orderFee));
    $("#rentFee").val(addCommas(rentFee));
    $("#addionFee").val(addCommas(addionFee));
    $("#discountFee").val(addCommas(discountFee));
    $("#totalFee").val(addCommas(totalFee));
    $("#totalPayment").val(addCommas(totalPayment));
    $("#remainFee").val(addCommas(remainFee));
}

/////////////// RENT CUSTOMERS /////////////////////////////////
function addRentCustomer() {
    event.preventDefault();
    var rentId = $("#rentId").val();
    var personId = $("#personId").val();
    var customerName = $("#customerName").val();
    var address = $("#address").val();
    if (personId != "" && customerName != "") {
        $.ajax({
            url: '/Cafe/AddRentCustomers',
            type: 'GET',
            data: { rentId: rentId, personId: personId, customerName: customerName, address: address },
            success: function (result) {
                if (result != null) {
                    $("#personId").val("");
                    $("#customerName").val("");
                    $("#address").val("");
                    $("#customerList").prepend(result);

                    //$("#wrapCustomerTable").fadeOut(200, function () {
                    //    $("#customerList").prepend(result);
                    //    $("#wrapCustomerTable").fadeIn(200, function () {
                    //        $("#wrapCustomerTable").mCustomScrollbar("update");
                    //    });
                    //});

                };

            }
        });
    }

};


function removeCustomer(customerId) {

    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Xóa khách hàng?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {

                     $.ajax({
                         url: "/Cafe/RemoveRentCustomers",
                         type: "GET",
                         data: { customerId: customerId },
                         success: function (result) {

                             if (result == "1") {

                                 $("#wrapCustomerTable").fadeOut(200, function () {
                                     $("#customerid-" + customerId).remove();
                                     $("#wrapCustomerTable").fadeIn(200, function () {
                                         $("#wrapCustomerTable").mCustomScrollbar("update");
                                     });
                                 });

                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

}


////////////////////////// RENT PREPAID /////////////////////////////////////////
function addPayment() {
    event.preventDefault();
    var amount = $("#paymentAmount").val();
    var rentId = $("#rentId").val();
    var notes = $("#paymentNotes").val();

    $.ajax({
        url: "/Cafe/AddPayment",
        type: "GET",
        data: { rentId: rentId, paymentAmount: amount, notes: notes },
        success: function (result) {
            if (result != null) {
                $("#paymentAmount").val("");
                $("#paymentNotes").val("");
                $("#wrapPaymentTable").fadeOut(200, function () {
                    $("#paymentList").prepend(result);
                    $("#wrapPaymentTable").fadeIn(200, function () {
                        $("#wrapPaymentTable").mCustomScrollbar("update");
                        UpdateTotalPayment(amount);

                        //Update Entire Fee
                        UpdateEntireFee();
                    });
                });



            }

        }
    });
};


function RemovePayment(paymentId, amount) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Xóa trả trước?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Cafe/RemovePayment",
                         type: "GET",
                         data: { paymentId: paymentId },
                         success: function (result) {
                             if (result == "1") {
                                 $("#wrapPaymentTable").fadeOut(200, function () {
                                     $("#paymentid-" + paymentId).remove();
                                     $("#wrapPaymentTable").fadeIn(200, function () {
                                         $("#wrapPaymentTable").mCustomScrollbar("update");
                                         UpdateTotalPayment(-amount);

                                         //Update Entire Fee
                                         UpdateEntireFee();
                                     });
                                 });
                             };
                         }


                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
};


function UpdateTotalPayment(amount) {
    //Update total payment
    var totalPayment = parseInt($("#hdfTotalPayment").val());
    totalPayment = totalPayment + parseInt(amount);
    $("#hdfTotalPayment").val(totalPayment);


}
//check is numberic
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}

////////////////////////// RENT ADDITION /////////////////////////////////////////
function addAddition() {
    event.preventDefault();
    var amount = $("#additionAmount").val();
    var rentId = $("#rentId").val();
    var notes = $("#additionNotes").val();
    var isAddition = $("#isAddition").val();

    if (amount == "") {
        ShowMessage('Số tiền không được để trống', 3);
        return false;
    }

    if (isNaN(amount) == true) {
        ShowMessage('Số tiền phải là chữ số', 3);

        return false;
    }

    $.ajax({
        url: "/Cafe/AddFeeChange",
        type: "GET",
        data: { rentId: rentId, feeChangeAmount: amount, notes: notes, isAddition: isAddition },
        success: function (result) {
            if (result != null) {
                $("#additionAmount").val("");
                $("#additionNotes").val("");
                $("#additionList").prepend(result);
            }
        }
    });
};


function removeFeeChange(feeChangeId, amount, isAddition) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn muốn xóa?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Cafe/RemoveFeeChange",
                         type: "GET",
                         data: { feeChangeId: feeChangeId },
                         success: function (result) {
                             if (result == "1") {
                                 $("#wrapAdditionTable").fadeOut(200, function () {
                                     $("#feeChangeId-" + feeChangeId).remove();
                                     $("#wrapAdditionTable").fadeIn(200, function () {
                                         $("#wrapAdditionTable").mCustomScrollbar("update");
                                         UpdateFeeChange(-amount, isAddition);
                                         UpdateEntireFee();
                                     });
                                 });
                             };
                         }


                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });


   
};


function UpdateFeeChange(amount, isAddition) {
    //Update total payment
    if (isAddition == "True") {
        var totalAddition = parseInt($("#hdfTotalAddition").val());
        totalAddition += parseInt(amount);
        $("#hdfTotalAddition").val(totalAddition);
    }
    else {
        var totalDiscount = parseInt($("#hdfTotalDiscount").val());
        totalDiscount += parseInt(amount);
        $("#hdfTotalDiscount").val(totalDiscount);
    }
}

////////////////////////// RENT DISCOUNT /////////////////////////////////////////
//function addDiscount() {
//    var amount = $("#discountAmount").val();
//    var rentId = $("#rentId").val();
//    var notes = $("#discountNotes").val();

//    $.ajax({
//        url: "/Cafe/AddFeeChange",
//        type: "GET",
//        data: { rentId: rentId, feeChangeAmount: amount, notes: notes, isAddition: false },
//        success: function (result) {
//            if (result != null) {
//                $("#discountAmount").val("");
//                $("#discountNotes").val("");
//                $("#wrapDiscountTable").fadeOut(200, function () {
//                    $("#discountList").prepend(result);
//                    $("#wrapDiscountTable").fadeIn(200, function () {
//                        $("#wrapDiscountTable").mCustomScrollbar("update");
//                        UpdateTotalDiscount(amount);
//                        //Update Entire Fee
//                        UpdateEntireFee();
//                    });
//                });


//            }

//        }
//    });
//};


//function RemoveDiscount(feeChangeId, amount) {
//    $.ajax({
//        url: "/Cafe/RemoveFeeChange",
//        type: "GET",
//        data: { feeChangeId: feeChangeId },
//        success: function (result) {
//            if (result == "1") {
//                $("#wrapPaymentTable").fadeOut(200, function () {
//                    $("#feeChangeId-" + feeChangeId).remove();
//                    $("#wrapPaymentTable").fadeIn(200, function () {
//                        $("#wrapPaymentTable").mCustomScrollbar("update");
//                        UpdateTotalDiscount(-amount);
//                        //Update Entire Fee
//                        UpdateEntireFee();
//                    });
//                });
//            };
//        }


//    });
//};


//function UpdateTotalDiscount(amount) {
//    //Update total payment
//    var totalDiscount = parseInt($("#hdfTotalDiscount").val());
//    totalDiscount += parseInt(amount); ;
//    $("#hdfTotalDiscount").val(totalDiscount);


//}


////////////////////////////////// RENT Service //////////////////////////////////////////////

var $container;

function LoadItemByCategory(cateId) {
    //change focus
    if (cateId == null) {
        $container.isotope({ filter: '*' });
    } else {
        $container.isotope({ filter: '.' + cateId });
    }
}

function addItem(productId, price) {
    var rentId = $('#txtRentId').val();
    var quantity = $('#txtQuantity-' + productId).val();

    var sumPrice = quantity * price;



    $.ajax({
        url: '/Service/AddItem',
        type: 'GET',
        data: { rentId: rentId, productId: productId, quantity: quantity },
        success: function (result) {
            //TO-DO check fade out and fade in again
            if (result != null) {
                var newResult = $(result).addClass("warning").clone().wrap('<p>').parent().html();

                $("#wrapOrderTable").fadeOut(200, function () {

                    if ($("#ordered-item .warning") != null) {
                        $("#ordered-item .warning").removeClass("warning").addClass("success");
                    }
                    $("#ordered-item").prepend(newResult);
                    $("#wrapOrderTable").fadeIn(200, function () {
                        $("#wrapOrderTable").mCustomScrollbar("update");

                        updateOrderFee(sumPrice);

                        //Cap nhat lai toan bo chi phi
                        UpdateEntireFee();
                    });
                });
            }
        }
    });

}

function removeItem(orderId) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn muốn xóa dịch vụ?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Service/RemoveItem",
                         type: "GET",
                         data: { orderID: orderId },
                         success: function (result) {
                             if (result == "1") {
                                 $("#wrapOrderTable").fadeOut(200, function () {
                                     var quantity = parseInt($(".order-item-" + orderId).find(".itemQuantity").html());
                                     var price = parseInt($(".order-item-" + orderId).find(".price").html().replace(/\./g, ""));
                                     $(".order-item-" + orderId).remove();
                                     $("#wrapOrderTable").fadeIn(200, function () {
                                         $("#wrapOrderTable").mCustomScrollbar("update");
                                         updateOrderFee(-quantity * price);
                                         //Cap nhat lai toan bo chi phi
                                         UpdateEntireFee();
                                     });
                                 });
                             };
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });



    
};






function updateOrderFee(amount) {
    var orderFee = parseInt($("#hdfOrderFee").val());
    orderFee += parseInt(amount);;
    $("#hdfOrderFee").val(orderFee);
}

//////////////////////////////// RENT CHANGE ROOM /////////////////////////////////////////////////////////////////

function ChangeRoom() {
    var rentId = $("#rentId").val();
    var roomId = $("#roomId").val();
    var newRoomId = $('#roomList :selected').val();
    $.ajax({
        url: "/Cafe/ChangeRoom",
        type: "GET",
        data: { rentId: rentId, roomId: roomId, newRoomId: newRoomId },
        success: function (result) {
            if (result == "1") {
                //$('#modal').modal('hide');
                //UpdateRoomStatus(roomId);
                //UpdateRoomStatus(newRoomId);
                location = "/Cafe/Index";
                //ShowMessage('Chuyển phòng thành công', 2);
            }
            else {
                ShowMessage("Vui lòng kiểm tra trạng thái phòng được chuyển đã sẵn sàng!", 3);
            }
        }
    });
};

//Chuyển trạng thái phòng từ chờ duyệt thành đang thuê
function RestoreStatus(rentID) {
    $.ajax({
        url: "/Cafe/RestoreStatus",
        type: "GET",
        data: { rentId: rentID },
        success: function (result) {
            if (result == '1') {
                $("#RentID-" + rentID).remove();

               // RefreshTable();
                $("#orderDatatable").dataTable()._fnAjaxUpdate();
                $("#modal").modal('hide');
                ShowMessage("Khôi phục bàn thành công", 2);
            } else {
                ShowMessage("Khôi phục bàn thất bại", 1);
            }

        }
    });
}

//Load tung tab trong hoa don phong


function getAdvanceTab() {
    //TO-D0 
    
    $.ajax({
        url: "/Cafe/GetChangeRoomView",
        type: "GET",
        success: function (result) {
            $("#tab5").html(result);
            
        }
    });
}

function getServiceTab() {
    var rentID = $('#rentId').val();
    var check = $("#serviceTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetRentService",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#tab4").html(result);
                $("#tab4").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getFeeChangeTab() {
    var rentID = $('#rentId').val();
    var check = $("#feeChangeTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetRentFeeChange",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#tab3").html(result);
                $("#tab3").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getCustomerTab() {
    var rentID = $('#rentId').val();

    var check = $("#customerTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetRentCustomers",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#tab2").html(result);
                $("#tab2").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getPaymentTab() {
    var rentID = $('#rentId').val();

    var check = $("#prepaidTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetRentPayments",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#tab7").html(result);
                $("#tab7").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getTaskTab() {
    var rentID = $('#rentId').val();
    var check = $("#taskTab").val();

    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetRentTask",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#tab6").html(result);
                $('#ExecuteDate').datetimepicker({ dateFormat: 'dd-mm-yy' });
                $('#ExecuteDate').datetimepicker('setDate', (new Date()));
                $('#CreateDate').datetimepicker({ dateFormat: 'dd-mm-yy' });
                $('#CreateDate').datetimepicker('setDate', (new Date()));
                
            }
        });
    }
}

function getInvoiceTab() {
    var rentID = $('#rentId').val();
    var check = $("#invoiceTab").val();

    if (check == null) {
        
        $.ajax({
            url: "/Cafe/GetInvoice",
            type: "GET",
            data: { rentId: rentID },
            success: function(result) {
                $("#tab1").html(result);
                $("#widget-footer").removeClass("hidden");
                $(document).unbind('keydown');
                $(document).keydown(function (e) { keyHandler(e); });
                
            }
        });
    } else {
        //refresh invoice area
        
        $.ajax({
            url: "/Cafe/GetDetailTab",
            type: "GET",
            data: { rentId: rentID },
            success: function (result) {
                $("#rentSubDetail").html(result);
                
            }
        });
    }
}

function printDiv() {
    //var content = '<h1>Bàn ' + $("#roomName").val() + '</h1>' + '<h6>Danh sách dịch vụ</h6>'
    //    + '<table border="1">' + $("#tblServiceDetail").html() + '</table><br/>' + $("#printForm").html() + '<br/>'
    //    + '<b>Cám ơn quý khách đã sử dụng dịch vụ của chúng tôi.</b>';
    var rentId = $("#rentId").val();
    var served = $("#ServedPerson").val();
    if (rentId == null) return;

    $.ajax({
        url: "/Cafe/PrintBill",
        type: "GET",
        data: {
            rentId: rentId,
            served: served
        },
        success: function (result) {
            window.frames["print_frame"].document.body.innerHTML = result;
            var printContents = window.frames["print_frame"].document.getElementById("bill-container").innerHTML;
            window.frames["print_frame"].document.body.innerHTML = printContents;
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();
        },
        error: function() {
            ShowMessage("Không thực hiện được", 1);
        }
    });

    //window.frames["print_frame"].document.body.innerHTML = content;
    //window.frames["print_frame"].window.focus();
    //window.frames["print_frame"].window.print();
}

function keyHandler(e) {
    switch (e.keyCode) {
        case 112://f1 - Xin hủy
            event.preventDefault();
            if ($("#invoiceTab").val() != null) {
                var rentId = $('#rentId').val();
                var roomId = $('#roomId').val();
                var status = $('#rentStatus').val();
                destroyBookingHere(rentId, status, roomId);
            }
            break;
        case 113://f2 - In hoa don
            event.preventDefault();
            if ($("#invoiceTab").val() != null) {
                printDiv();
            }
            break;
        case 114://f3- Thanh toan
            event.preventDefault();
            if ($("#invoiceTab").val() != null) {
                CheckOut(true);
            }
            break;
        case 115://f4 - Hoa don
            event.preventDefault();
            ChangeTabFocus("#billTab");
            getInvoiceTab();
            break;
        case 117://f4 - Dich vu
            event.preventDefault();
            ChangeTabFocus("#servicedTab");
            break;
        case 118://f7 - Phu thu giam gia
            event.preventDefault();
            getFeeChangeTab();
            ChangeTabFocus("#discountTab");
            break;
        case 119://f7 - Nang cao
            event.preventDefault();
            getAdvanceTab();
            ChangeTabFocus("#additionTab");
            break;
    }
}
//Quan ly Task

function updateTask(TaskID) {
    $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone'  onclick='updateTask(" + TaskID + ")'/>");
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Sự kiện đã hoàn thành?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Cafe/checkDone",
                         type: "GET",
                         data: { TaskID: TaskID },
                         success: function (result) {
                             if (result) {
                                 $("#checkButton-" + TaskID).html("");
                                 $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone' checked='checked' disabled='disabled' />");
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Sự kiện đã hoàn thành?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Cafe/checkDone",
    //            type: "GET",
    //            data: { TaskID: TaskID },
    //            success: function (result) {
    //                if (result) {
    //                    $("#checkButton-" + TaskID).html("");
    //                    $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone' checked='checked' disabled='disabled' />");
    //                }
    //            }
    //        });
    //    }
    //});
}

function removeTask(TaskID) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Xóa công việc?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {

                     $.ajax({
                         url: "/Cafe/deleteTask",
                         type: "GET",
                         data: { TaskID: TaskID },
                         success: function (result) {
                             if (result) {
                                 $("#row-" + TaskID).html("");
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

   

}

function insertTask() {
    $("#error-lable").fadeOut(300);
    var RentID = $("#RentID").val();
    var TaskDesctiption = $("#TaskDescription").val();
    var ExecuteDate = ConvertDateVNToUS($("#ExecuteDate").val());
    var CreateDate = ConvertDateVNToUS($("#CreateDate").val());
    if (TaskDesctiption == "") {
        $("#error-lable").html("Vui lòng nhập mô tả công việc!");
        $("#error-lable").fadeIn(300);
        return;
    }
    if (CreateDate > ExecuteDate) {
        $("#error-lable").html("Thời điểm thực hiện phải lớn hơn hiện tại!");
        $("#error-lable").fadeIn(300);
        return;
    }

    $.ajax({
        url: "/Cafe/insertTask",
        type: "GET",
        data: { rentID: RentID, description: TaskDesctiption, executeDate: ExecuteDate, createDate: CreateDate },
        success: function (result) {
            if (result != -1) {
                $("#body1").prepend(result);
                $("#TaskDescription").val("");
            } else {
                ShowMessage("Thêm không thành công. Vui lòng thử lại sau!", 1);
            }
        }
    });

}