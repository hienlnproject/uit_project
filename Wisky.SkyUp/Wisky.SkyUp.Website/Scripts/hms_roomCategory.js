﻿function ShowRoomCategoryDetail(RoomCategoryID) {
    
    $("#modal").on('hide', function () {
        UpdateRoomCategoryRow(RoomCategoryID);
    });
    $.ajax({
        url: "/RoomCategory/ViewDetail",
        type: "GET",
        data: { RoomCategoryID: RoomCategoryID },
        success: function (result) {
            $("#modal").html(result).modal('show');
            
        }
    });
}

function UpdateRoomCategoryRow(RoomCategoryID) {
    $.ajax({
        url: "/RoomCategory/GetRoomCategoryByID",
        type: "GET",
        data: { RoomCategoryID: RoomCategoryID },
        success: function (result) {
            $("#tr-" + RoomCategoryID).replaceWith(result);
        }
    });
}

function UpdateDefaultPriceGroupList(RoomCategoryID) {
    $.ajax({
        url: "/RoomCategory/GetCategoryPricegroupByCategoryID",
        type: "GET",
        data: { RoomCategoryID: RoomCategoryID },
        success: function (result) {
            $("#cboPricegroups").html(result);
        }
    });
}

function UpdateRoomCategory() {
    var RoomCategoryID = $("#RoomCategoryID").val();
    var CategoryName = $("#CategoryName").val();
    var ShortName = $("#ShortName").val();
    var DefaultPricegroupID = $("#DefaultPricegroupID").val();
    var ListPricegroup = [];
    $('#ListPriceGroup input:checked').each(function () {
        ListPricegroup.push(this.value);
    });
    var ListReadyRoom = [];
    $('#ListReadyRoom input:checked').each(function () {
        ListReadyRoom.push(this.value);
    });
    $.ajax({
        url: "/RoomCategory/UpdateRoomCategory",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify({
            CategoryID: RoomCategoryID,
            CategoryName: CategoryName,
            ShortName: ShortName,
            DefaultPricegroupID: DefaultPricegroupID,
            ListPricegroup: ListPricegroup,
            ListReadyRoom: ListReadyRoom
        }),
        success: function (result) {
            if (result > 0) {
                UpdateDefaultPriceGroupList(RoomCategoryID);
                ShowRoomCategoryDetail(RoomCategoryID);
            } else {
                ShowMessage("Có lỗi xảy ra, vui lòng tải lại trang!",1);
            }
        }
    });
}

function GetAddRoomCategoryForm() {
    
    $.ajax({
        url: "/RoomCategory/GetAddRoomCategoryForm",
        type: "GET",
        success: function (result) {
            $("#modal").html(result).modal('show');
            
        }
    });
}

function AddRoomCategory() {
    var CategoryName = $("#CategoryName").val();
    if (CategoryName=="") {
        ShowMessage('Vui lòng nhập Loại phòng', 3);
        return false;
    }

    var ShortName = $("#ShortName").val();
    if (ShortName == "") {
        ShowMessage('Vui lòng nhập Tên rút gọn', 3);
        return false;
    }

    var DefaultPricegroupID = $("#DefaultPricegroupID").val();
    var ListPricegroup = [];
    $('#ListPriceGroup input:checked').each(function () {
        ListPricegroup.push(this.value);
    });
    var ListReadyRoom = [];
    $('#ListReadyRoom input:checked').each(function () {
        ListReadyRoom.push(this.value);
    });
    if (ListPricegroup.length == 0) {
        ShowMessage("Vui lòng chọn ít nhất một mức giá", 3);
        return false;
    }
    $.ajax({
        url: "/RoomCategory/AddRoomCategory",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify({ 
            CategoryName: CategoryName,
            ShortName : ShortName,
            DefaultPricegroupID: DefaultPricegroupID,
            ListPricegroup: ListPricegroup,
            ListReadyRoom: ListReadyRoom
        }),
        success: function (result) {
            $(result).prependTo("table > tbody");
            $("#modal").modal("hide");
            ShowMessage('Thêm loại phòng thành công!',2);
        }
    });
}

function LoadRoomByCategory(CategoryID) {
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
    
    //Category = -1 --> Load ready room.
    if (CategoryID != -1) {
        $.ajax({
            url: "/RoomCategory/GetRoomByRoomCategoryID",
            type: "POST",
            data: { RoomCategoryID: CategoryID },
            success: function (result) {
                $("#wrapMenu").html(result);
            }
        });
    } else {
        $.ajax({
            url: "/RoomCategory/GetReadyRoom",
            type: "POST",
            success: function (result) {
                $("#wrapMenu").html(result);
            }
        });
    }
}