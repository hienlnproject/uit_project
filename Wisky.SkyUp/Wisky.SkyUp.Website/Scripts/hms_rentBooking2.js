﻿function openConfirmModal(roomId, roomName) {
    bootbox.dialog({
        message: "<h3 class='bigger-110'>Xác nhận dọn bàn</h3><br/>" +
            "<h6>Thay đổi bàn " + roomName + " sang trạng thái đã dọn?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     ChangeRoomStatusToReady(roomId);
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
};

function ChangeRoomStatusToReady(roomId) {
    $.ajax({
        url: "/Cafe/ChangeRoomStatus",
        type: "GET",
        data: { roomId: roomId, fromStatus: 3, toStatus: 1 }, //3: Prepare --> 1: Ready
        success: function (result) {
            var NumberOfReady = $("#NumberOfReady").val();
            var NumberOfPrepare = $("#NumberOfPrepare").val();
            $("#contain-" + roomId).fadeOut(200, function () {
                NumberOfReady++;
                $("#NumberOfReady").val(NumberOfReady);
                $("#spanNumberOfReady").html("(" + NumberOfReady + ")");
                NumberOfPrepare--;
                $("#spanNumberOfPrepare").html("(" + NumberOfPrepare + ")");
                $("#NumberOfPrepare").val(NumberOfPrepare);
                $("#contain-" + roomId).html(result);
                $("#contain-" + roomId).fadeIn();
            });
        }
    });
}

////////////// RENT BOOKING FORM  ////////////////////////////////
function ShowBookingForm(roomId, roomName) {
    
    $.ajax({
        url: "/Cafe/ShowBookingForm",
        type: "GET",
        data: { roomId: roomId, roomName: roomName },
        success: function (result) {
            $("#modal").modal('hide');
            $("#modal").html(result).modal('show');
            $('#CheckInDate').datetimepicker({ dateFormat: 'dd-mm-yy' });
            $('#CheckInDate').datetimepicker('setDate', (new Date()));
            $('#InvoiceID').val(GetDateStamp());
            
            BookRoom();
            
        }
    });

   
}

function ChangeRentType(rentType) {
    //Change rent type button
    $("#hdfRentType").val(rentType);
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
}

function BookRoom() {
    var roomId = $("#roomId").val();
    var checkInDate = ConvertDateVNToUS($('#CheckInDate').val());
    var invoiceId = $("#InvoiceID").val();
    var rentType = $("#hdfRentType").val();
    var bikeId = $("#BikeID").val();
    var notes = $("#Notes").val();
    var priceGroup = $("#priceGroup").val();
    $.ajax({
        url: "/Cafe/BookRoom",
        type: "GET",
        data: { roomId: roomId, invoiceId: invoiceId, checkInDate: checkInDate, rentType: rentType, bikeId: bikeId, notes: notes, priceGroup: priceGroup },
        success: function (result) {
            if (result != -1) {
                ShowRent(result);
                //Update number of room.
                var NumberOfReady = $("#NumberOfReady").val();
                NumberOfReady--;
                $("#NumberOfReady").val(NumberOfReady);
                $("#spanNumberOfReady").html("(" + NumberOfReady + ")");
                //Thue gio.
                if (rentType == 1) {
                    var NumberOfRentHour = $("#NumberOfRentHour").val();
                    NumberOfRentHour++;
                    $("#NumberOfRentHour").val(NumberOfRentHour);
                    $("#spanNumberOfRentHour").html("(" + NumberOfRentHour + ")");
                } else if (rentType == 2) {
                    //Qua dem.
                    var NumberOfRentNight = $("#NumberOfRentNight").val();
                    NumberOfRentNight++;
                    $("#NumberOfRentNight").val(NumberOfRentNight);
                    $("#spanNumberOfRentNight").html("(" + NumberOfRentNight + ")");
                } else if (rentType == 3) {
                    //Thue ngay
                    var NumberOfRentDay = $("#NumberOfRentDay").val();
                    NumberOfRentDay++;
                    $("#NumberOfRentDay").val(NumberOfRentDay);
                    $("#spanNumberOfRentDay").html("(" + NumberOfRentDay + ")");
                }
                
                UpdateRoomStatus(roomId);
            }
            else {
                
                $("#modal").modal('hide');
                ShowMessage("Bàn đã được đặt", 3);
                UpdateRoomStatus(roomId);
            }
        }
    });

}

function GetDateStamp() {
    var date = new Date();
    var dateStr = padStr(date.getFullYear() - 2000) +
            padStr(1 + date.getMonth()) +
                padStr(date.getDate()) +
                    padStr(date.getHours()) +
                        padStr(date.getMinutes()) +
                            padStr(date.getSeconds());
    return dateStr;
}

function padStr(i) {
    return (i < 10) ? "0" + i : "" + i;
}

function orderItem(productId, price) {
    var quantityPlace = $(event.toElement).closest(".input-group").children(".input-quantity");
    var label = $(event.srcElement).closest(".ace-spinner").closest("li").find(".label");
    var productName = $(event.srcElement).closest(".ace-spinner").closest("li").find(".title").html();
    var quantity = $(event.toElement).closest(".input-group").children(".input-quantity").val();
    //title
    var rentId = $('#txtRentId').val();
    var sumPrice = quantity * price;
    $.ajax({
        url: '/Service/AddItem',
        type: 'GET',
        data: { rentId: rentId, productId: productId, quantity: quantity },
        success: function (result) {
            //TO-DO check fade out and fade in again
            if (result != null) {
                //var newResult = $(result).addClass("warning").clone().wrap('<p>').parent().html();
                $(quantityPlace).val(1);
                $(label).html(1);
                var text = productName + " : " + quantity;
                ShowMessage(text,2);
               

                $("#wrapOrderTable").fadeOut(200, function () {
                    $(".ordered-item").prepend(result);
                    $('.itemQuantity').editable({
                        type: 'spinner',
                        spinner: {
                            min: 1, max: 500, step: 1
                        },
                        success: function (response, newValue) {
                            var id = $(this).closest("tr").attr("data-id");
                            var oldQuantity = parseInt($(this).html());
                            var price = $(this).closest("tr").find("td.price").html();
                            var totalPlace = $(this).closest("tr").find("td.total");
                            updateOrderDetails(id, oldQuantity, parseInt(newValue), price, totalPlace);
                        }
                    });
                    $("#wrapOrderTable").fadeIn(200, function () {
                        //$("#wrapOrderTable").mCustomScrollbar("update");
                        updateOrderFee(sumPrice);
                        //Cap nhat lai toan bo chi phi
                        UpdateEntireFee();             
                    });
                });
            }
        }
    });
}


function updateOrderDetails(id, oldQuantity, newValue, price, totalPlace) {
    $.ajax({
        url: '/Service/UpdateItemQuantity',
        type: 'GET',
        data: { orderId: id, quantity: newValue },
        success: function (result) {
            if (result == "1") {
                price = price.replace(/\./g, "");
                var sumPrice = newValue * parseInt(price);
                //format sumprice to money format
                $(totalPlace).html(ThousandSeperator(sumPrice));
                //$("#wrapOrderTable").mCustomScrollbar("update");
                //updateOrderFee((newValue - oldQuantity) * parseInt(price));
                //Cap nhat lai toan bo chi phi
                //UpdateEntireFee();
                //
                $(".order-item-" + id).find(".itemQuantity").html(newValue);
                $(".order-item-" + id).find(".total").html(ThousandSeperator(sumPrice));

                //refresh invoice area
                
                $.ajax({
                    url: "/Cafe/GetDetailTab",
                    type: "GET",
                    data: {
                        rentId: $("#rentId").val()
                    },
                    success: function (result1) {
                        $("#rentSubDetail").html(result1);
                        
                    }
                });
            } else {
                ShowMessage("Thao tác không thành công",1);
            }
        }
    });
}