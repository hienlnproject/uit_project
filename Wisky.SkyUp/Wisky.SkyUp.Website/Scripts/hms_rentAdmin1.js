﻿
function GetRentByStatus(status, pageIndex, pageSize) {

    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');

    var approve = $("#approveButton").val();
    var disable = $("#disableButton").val();
    var deleted = $("#deleteButton").val();

    if (status == approve) {
        $("#approveButton").show();
        $("#disableButton").hide();
        $("#deleteButton").hide();

        visibleForDisable = false;
        visibleForDelete = false;
    }
    else if (status == disable) {
        $("#approveButton").hide();
        $("#disableButton").show();
        $("#deleteButton").hide();

        visibleForDisable = true;
    }
    else if (status == deleted) {
        $("#approveButton").hide();
        $("#disableButton").hide();
        $("#deleteButton").show();

        visibleForDelete = true;

    } else {
        $("#approveButton").hide();
        $("#disableButton").hide();
        $("#deleteButton").hide();

        visibleForDisable = false;
        visibleForDelete = false;
    }


    $("#rentStatus").val(status);

    RefreshTable();

    //$.ajax({
    //    url: "/Admin/SearchRentByStatus",
    //    type: "GET",
    //    data: { status: status, pageIndex: pageIndex, pageSize: pageSize },
    //    success: function (result) {
    //        //$("#RentTable").fadeOut(100, function () {
    //        //    $('#RentTable').html(result);
    //        //    $("#RentTable").fadeIn(100);
    //        //});

    //    }
    //});

}



function CancelRent(rentId, invoiceId) {

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn chuyển hóa đơn ' + invoiceId + ' sang trạng thái chờ hủy ?</h6><br />Lý do:<textarea type="" id="cancelReason" class="input-medium" style="height: 50px;resize: none;"/>',
    //    callback: function () {
    //        var reason = $('#cancelReason').val();
    //        $.ajax({
    //            url: "/Admin/CancelRent",
    //            type: "GET",
    //            data: { rentId: rentId, reason: reason },
    //            success: function (result) {
    //                if (result == "1") {
    //                    $("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
    //                }
    //            }
    //        });
    //    }
    //});

    bootbox.dialog({
        title: "Xác nhận",
        message: '<h6>Bạn có muốn chuyển hóa đơn <b>' + invoiceId + '</b> sang trạng thái chờ hủy ?</h6><br />Lý do:<textarea type="" id="cancelReason" class="form-control" style="height: 50px;resize: none;"/>',
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     var reason = $('#cancelReason').val();
                     $.ajax({
                         url: "/Admin/CancelRent",
                         type: "GET",
                         data: { rentId: rentId, reason: reason },
                         success: function (result) {
                             if (result == "1") {
                                 //$("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
                                 //           RefreshTable();
                                 $("#orderDatatable").dataTable()._fnAjaxUpdate();
                             } else {
                                 ShowMessage("Xin hủy không thành công", 1);
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
}


function AcceptCancelRentRequest(rentId, invoiceId) {

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có đồng ý chuyển hóa đơn ' + invoiceId + ' sang trạng thái đã hủy ?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Admin/AcceptCancelRentRequest",
    //            type: "GET",
    //            data: { rentId: rentId },
    //            success: function (result) {
    //                if (result == "1") {
    //                    $("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
    //                }
    //            }
    //        });
    //    }
    //});

    bootbox.dialog({
        title: "Xác nhận",
        message: '<h6>Bạn có đồng ý chuyển hóa đơn ' + invoiceId + ' sang trạng thái đã hủy ?</h6>',
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     var reason = $('#cancelReason').val();
                     $.ajax({
                         url: "/Admin/AcceptCancelRentRequest",
                         type: "GET",
                         data: { rentId: rentId },
                         success: function (result) {
                             if (result == "1") {
                                 //$("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
                                 //RefreshTable();
                                 $("#orderDatatable").dataTable()._fnAjaxUpdate();
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

}


function DeleteRent(rentId, invoiceId) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có muốn hủy hóa đơn này vĩnh viễn không?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Admin/DeleteRent",
                         type: "GET",
                         data: { rentId: rentId },
                         success: function (result) {
                             if (result == "1") {
                                 //$("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
                                 // RefreshTable();
                                 $("#orderDatatable").dataTable()._fnAjaxUpdate();
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Hóa đơn ' + invoiceId + ' đã được hủy. Bạn có muốn xóa hẳn hóa đơn này không?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Admin/DeleteRent",
    //            type: "GET",
    //            data: { rentId: rentId },
    //            success: function (result) {
    //                if (result == "1") {
    //                    $("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });
    //                }
    //            }
    //        });
    //    }
    //});
}

function ApproveRent(rentId) {
    $.ajax({
        url: "/Admin/ApproveRent",
        type: "GET",
        data: { rentId: rentId },
        success: function (result) {
            if (result == "1") {
                //$("#RentID-" + rentId).fadeOut(500, function () { $(this).remove(); });

                // RefreshTable();
                $("#orderDatatable").dataTable()._fnAjaxUpdate();
                ShowMessage("Duyệt thành công", 2);
            } else {
                ShowMessage("Duyệt thất bại", 1);
            }
        }
    });

}

function ApproveAllRent(type) {

    var checks = new Array();

    $(".abc:checked").each(function () {
        if ($(this).is(':checked')) {
            checks.push($(this).val());
        }
    });

    if (checks.length > 0) {

        $.ajax({
            url: "/Admin/ApproveAllRents",
            type: "POST",
            traditional: true,
            data: { type: type, ids: checks },
            success: function (result) {

                if (result == '1') {
                    //RefreshTable();
                    $("#orderDatatable").dataTable()._fnAjaxUpdate();
                    ShowMessage("Duyệt thành công", 2);

                    $("#cbAll").attr("checked", false);

                } else {
                    ShowMessage("Duyệt thất bại", 1);
                }


            }
        });

    } else {
        ShowMessage("Vui lòng chọn hóa đơn", 3);
    }

}

function DisabledAllRent(type) {
    var checks = new Array();

    $(".abc:checked").each(function () {
        if ($(this).is(':checked')) {
            checks.push($(this).val());
        }
    });

    if (checks.length > 0) {
        bootbox.dialog({
            title: 'Xác nhận',
            message: '<h6>Bạn có muốn đưa những hóa đơn này vào thùng rác?</h6>',
            buttons:
            {
                "ok":
                 {
                     "label": "<i class='icon-remove'></i> Đồng ý!",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                         $.ajax({
                             url: "/Admin/DisabledAllRents",
                             type: "GET",
                             traditional: true,
                             data: { type: type, ids: checks },
                             success: function (result) {

                                 if (result == '1') {
                                     //  RefreshTable();
                                     $("#orderDatatable").dataTable()._fnAjaxUpdate();
                                     ShowMessage("Hủy thành công", 2);
                                     
                                     $("#cbAll").attr("checked", false);

                                 } else {
                                     ShowMessage("Hủy thất bại", 1);
                                 }

                             }
                         });


                     }
                 },
                "close":
                 {
                     "label": "<i class='icon-ok'></i> Đóng",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                         bootbox.hideAll();
                     }
                 }
            }
        });

    }
    else {
        ShowMessage("Vui lòng chọn hóa đơn", 3);
    }
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn đưa tất cả hóa đơn vào thùng rác?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Admin/DisabledAllRents",
    //            type: "GET",
    //            success: function (result) {
    //                if (result == "1") {
    //                    GetRentByStatus(4, 1, 20);
    //                    $('#btnDisabled').addClass('active');
    //                    $('#WaitDisabled').removeClass('active');
    //                }
    //            }
    //        });
    //    }
    //});
}

function DeleteAllRent(type) {
    var checks = new Array();

    $(".abc:checked").each(function () {
        if ($(this).is(':checked')) {
            checks.push($(this).val());
        }
    });

    if (checks.length > 0) {


        bootbox.dialog({
            title: 'Xác nhận',
            message: "<h6>Bạn có muốn xóa những hóa đơn này?</h6>",
            buttons:
            {
                "ok":
                 {
                     "label": "<i class='icon-remove'></i> Đồng ý!",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                         $.ajax({
                             url: "/Admin/DeleteAllRents",
                             type: "GET",
                             traditional: true,
                             data: { type: type, ids: checks },
                             success: function (result) {
                                
                                 if (result == '1') {
                                     //     RefreshTable();
                                     $("#orderDatatable").dataTable()._fnAjaxUpdate();
                                     ShowMessage("Xóa thành công", 2);

                                     $("#cbAll").attr("checked", false);

                                 } else {
                                     ShowMessage("Xóa thất bại", 1);
                                 }
                                 
                             }
                         });
                     }
                 },
                "close":
                 {
                     "label": "<i class='icon-ok'></i> Đóng",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                         bootbox.hideAll();
                     }
                 }
            }
        });

    }
    else {
        ShowMessage("Vui lòng chọn hóa đơn", 3);
    }
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn xóa tất cả hóa đơn?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Admin/DeleteAllRents",
    //            type: "GET",
    //            success: function (result) {
    //                if (result == 1) {
    //                    GetRentByStatus(1, 1, 20);
    //                    $('#btnInstay').addClass('active');
    //                    $('#btnDisabled').removeClass('active');
    //                }
    //            }
    //        });
    //    }
    //});
}



//function ExportToExcel() {

//    var beginDate = $('#StartDate').val();
//    var endDate = $('#EndDate').val();
//    beginDate = ConvertDateVNToUS(beginDate);
//    endDate = ConvertDateVNToUS(endDate);
//    $.ajax({
//        url: "/Admin/ExportToExcel",
//        type: "GET",
//        data: { StartTime: beginDate, EndTime: endDate }
//    });
//}




//////////////////////////TACH FILE SAU - USER MANAGEMENT //////////////////////////////////////////
