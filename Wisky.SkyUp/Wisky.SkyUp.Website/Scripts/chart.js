﻿function SearchPerformanceReportByDate() {

    var fromDate = ConvertDateVNToUS($('#fromDate').val()); //Do co che tu sinh ID cua MVC 
    var toDate = ConvertDateVNToUS($('#toDate').val());
    var from = new Date(fromDate);
    var to = new Date(toDate);
    if (from >= to) {
        ShowMessage('Ngày sau phải lớn hơn ngày trước!', 3);
    } else if (from == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày bắt đầu', 3);
    } else if (to == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày kết thúc', 3);
    }
    else {
        $(".btn").removeClass('active');
        
        $.ajax({
            url: "/Report/GetDatePerformanceReport",
            type: "GET",
            data: { fromDate: fromDate, toDate: toDate },
            success: function (result) {
                $("#searchContent").fadeOut(100, function () {
                    $('#searchContent').html(result);
                    $("#searchContent").fadeIn(100);
                    ShowChart();
                });
                
            }
        });
    }
}

function SearchPerformanceReportFromNow(totalDate) {
    
    $(".btn").removeClass('active');
    $.ajax({
        url: "/Report/GetDatePerformanceReportFromNow",
        type: "GET",
        data: { totalDate: totalDate },
        success: function (result) {
            $("#searchContent").fadeOut(100, function () {
                $('#searchContent').html(result);
                $("#searchContent").fadeIn(100);
                ShowChart();
            });
            
        }
    });
}


// Week performance

function SearchPerformanceReportByWeek() {
    
    var fromDate = ConvertDateVNToUS($('#fromDate').val()); //Do co che tu sinh ID cua MVC 
    var toDate = ConvertDateVNToUS($('#toDate').val());
    var from = new Date(fromDate);
    var to = new Date(toDate);
    if (from >= to) {
        ShowMessage('Ngày sau phải lớn hơn ngày trước!', 3);
    } else if (from == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày bắt đầu', 3);
    } else if (to == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày kết thúc', 3);
    } else {
        
        $(".btn").removeClass('active');
        $.ajax({
            url: "/Report/GetWeekPerformanceReport",
            type: "GET",
            data: { fromDate: fromDate, toDate: toDate },
            success: function (result) {
                $("#searchContent").fadeOut(100, function () {
                    $('#searchContent').html(result);
                    $("#searchContent").fadeIn(100);
                    ShowChart();
                });
                
            }
        });
    }
}

function SearchPerformanceReportFromNowByWeek(totalMonth) {
    
    $(".btn").removeClass('active');
    $.ajax({
        url: "/Report/GetWeekPerformanceReportFromNow",
        type: "GET",
        data: { totalMonth: totalMonth },
        success: function (result) {
            $("#searchContent").fadeOut(100, function () {
                $('#searchContent').html(result);
                $("#searchContent").fadeIn(100);
                ShowChart();
            });
            
        }
    });
}


// Month performance
function SearchPerformanceReportFromNowByMonth(totalMonth) {
    
    $(".btn").removeClass('active');
    $.ajax({
        url: "/Report/GetMonthPerformanceReportFromNow",
        type: "GET",
        data: { totalMonth: totalMonth },
        success: function (result) {
            $("#searchContent").fadeOut(100, function () {
                $('#searchContent').html(result);
                $("#searchContent").fadeIn(100);
                ShowChart();
            });
            
        }
    });
}

function SearchPerformanceReportByMonth() {
    
    var fromDate = ConvertDateVNToUS($('#fromDate').val()); //Do co che tu sinh ID cua MVC 
    var toDate = ConvertDateVNToUS($('#toDate').val());
    var from = new Date(fromDate);
    var to = new Date(toDate);
    if (from >= to) {
        ShowMessage('Ngày sau phải lớn hơn ngày trước!', 3);
    } else if (from == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày bắt đầu', 3);
    } else if (to == "Invalid Date") {
        ShowMessage('Vui lòng nhập thông tin ngày kết thúc', 3);
    } else {
        
        $(".btn").removeClass('active');
        $.ajax({
            url: "/Report/GetMonthPerformanceReport",
            type: "GET",
            data: { fromDate: fromDate, toDate: toDate },
            success: function (result) {
                $("#searchContent").fadeOut(100, function () {
                    $('#searchContent').html(result);
                    $("#searchContent").fadeIn(100);
                    ShowChart();
                });
                
            }
        });
    }
}



// Show chart
function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
};

function flotHover() {
    $("#placeholder").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $("#tooltip").remove();
                var x = item.datapoint[1],
                      y = addCommas(item.datapoint[1].toFixed(0));
                showTooltip(item.pageX, item.pageY, y + " %");
            }
        }
    });
}



