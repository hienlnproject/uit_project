﻿function removeCharacter(v, ch) {
    var tempValue = v + "";
    var becontinue = true;
    while (becontinue == true) {
        var point = tempValue.indexOf(ch);
        if (point >= 0) {
            var myLen = tempValue.length;
            tempValue = tempValue.substr(0, point) + tempValue.substr(point + 1, myLen);
            becontinue = true;
        } else {
            becontinue = false;
        }
    }
    return tempValue;
}

function characterControl(value) {
    var tempValue = "";
    var len = value.length;
    for (i = 0; i < len; i++) {
        var chr = value.substr(i, 1);
        if ((chr < '0' || chr > '9') && chr != '.' && chr != ',') {
            chr = '';
        }

        tempValue = tempValue + chr;
    }
    return tempValue;
}



function ThousandSeperator(value, digit) {
    var thausandSepCh = ".";
    var decimalSepCh = ",";

    var tempValue = "";
    var realValue = value + "";
    var devValue = "";
    realValue = characterControl(realValue);
    var comma = realValue.indexOf(decimalSepCh);
    if (comma != -1) {
        tempValue = realValue.substr(0, comma);
        devValue = realValue.substr(comma);
        devValue = removeCharacter(devValue, thausandSepCh);
        devValue = removeCharacter(devValue, decimalSepCh);
        devValue = decimalSepCh + devValue;
        if (devValue.length > 3) {
            devValue = devValue.substr(0, 3);
        }
    } else {
        tempValue = realValue;
    }
    tempValue = removeCharacter(tempValue, thausandSepCh);

    var result = "";
    var len = tempValue.length;
    while (len > 3) {
        result = thausandSepCh + tempValue.substr(len - 3, 3) + result;
        len -= 3;
    }
    result = tempValue.substr(0, len) + result;
    return result + devValue;
}

function SlipThoundsandDelimeter(ItemID) {
    var value = document.getElementById(ItemID).value.split(".");
    var result = "";
    var i = 0;
    while (i < value.length) {
        result += value[i];
        i++;
    }
    return result;
}

function GetAllPriceGroup() {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/GetAllPriceGroup",
        type: "GET",
        success: function (result) {
            if (result == 0) {
                ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
            } else {
                $("#PriceConfigHome").html(result);
            }
            
        },
        error: function () {
            ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.", 1);
            
        }
    });
}

function GetAllCategoryPriceGroup() {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/GetAllCategoryPriceGroup",
        type: "GET",
        success: function (result) {
            if (result == 0) {
                ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
            } else {
                $("#PriceConfigHome").html(result);
            }
            
        },
        error: function () {
            ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.", 1);
            
        }
    });
}

function ViewDetailPriceGroup(id) {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/GetByID",
        type: "GET",
        data: { priceGroupID: id },
        success: function (result) {
            $("#PriceConfigHome").html(result);
            $('#StartTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                showMeridian: false,
                defaultValue: "12:00"
            });
            $('#EndTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                showMeridian: false,
                defaultValue: "12:00"
            });
            
        }
    });
}

function ShowAddPriceGroupForm() {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/ShowAddPriceGroupForm",
        type: "GET",
        success: function (result) {
            $("#PriceConfigHome").html(result);
            $('#StartTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                showMeridian: false,
                defaultValue: "12:00"
            });
            $('#EndTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                showMeridian: false,
                defaultValue: "12:00"
            });
            
        }
    });
}

function SaveAndCountinue() {
    var DayPrice = SlipThoundsandDelimeter("PriceDay");
    $("#PriceDay").val(DayPrice);
    var FirstHourPrice = SlipThoundsandDelimeter("FirstTimeValue");
    $("#FirstTimeValue").val(FirstHourPrice);
    var SencondHourPrice = SlipThoundsandDelimeter("SecondTimeValue");
    $("#SecondTimeValue").val(SencondHourPrice);
    var ThirdHourPrice = SlipThoundsandDelimeter("ThirdTimeValue");
    $("#ThirdTimeValue").val(ThirdHourPrice);
    var NextHourPrice = SlipThoundsandDelimeter("NextTimeValue");
    $("#NextTimeValue").val(NextHourPrice);
    var form = document.getElementById("AddPriceGroupForm");
    $.ajax({
        url: "/PriceManager/PriceGroup/SaveAndContinue",
        type: "POST",
        data: $(form).serialize(),
        success: function (result) {
            if (result == 0) {
                ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.", 1);
            } else {
                $("#btnSavePricegroup").html("<button class='btn btn-sm btn-primary' onclick='UpdatePriceGroup()'>Lưu<i class='icon-save icon-on-right'></i></button>");
                $("#PriceNightConfigSection").html(result);
            }
        },
        error: function () {
            ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
        }
    });
}

function ShowPriceNightModal() {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/ShowPriceNightConfigForm",
        type: "GET",
        success: function (result) {
            $("#modal").html(result).modal('show');
            $('#CheckInTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                hourMin: 18,
                minute: 30,
                showMeridian: false,
                defaultValue: "18:00"
            });
            $('#CheckOutTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                hourMax: 12,
                showMeridian: false,
                defaultValue: "12:00"
            });
            $('#MaxDuration').timepicker({
                stepMinute: 1,
                showHour: true,
                showMinute: true,
                showSecond: true,
                hourMax: 23,
                showMeridian: false,
                defaultValue: "23:59:59"
            });
            
        }
    });
}

function InsertPriceNight() {
    var PriceNight = SlipThoundsandDelimeter("PriceNight");
    $("#PriceNight").val(PriceNight);
    var form1 = document.getElementById("PriceNightConfigForm");
    $.ajax({
        url: "/PriceManager/PriceGroup/InsertPriceNight",
        type: "POST",
        data: $(form1).serialize(),
        success: function (result) {
            if (result == 0) {
                ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
            } else {
                $("#modal").modal("hide");
                $(result).prependTo("#ListPriceNight tbody");
            }
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage(err.Message,1);
        }
    });
}

function DeletePriceNight(id) {
    //$("#confirmDiv").confirmModal({
    //        heading: 'Xác nhận',
    //        body: '<h6>Bạn có muốn xóa cấu hình này?</h6>',
    //        callback: function () {
    //            $.ajax({
    //                url: "/PriceGroup/DeletePriceNight",
    //                type: "POST",
    //                data: {PriceNightID : id},
    //                success: function (result) {
    //                    if (result == 0) {
    //                        alert("Có lỗi xảy ra. Vui lòng thử tải lại trang.");
    //                    } else {
    //                        $("#ListPriceNight").fadeOut(200, function () {
    //                            $("#trPriceNight-" + id).remove();
    //                            $("#ListPriceNight").fadeIn();
    //                        });
    //                    }
    //                },
    //                error: function (xhr, status, error) {
    //                    var err = eval("(" + xhr.responseText + ")");
    //                    alert(err.Message);
    //                }
    //            });
    //       }
    //});
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có muốn xóa cấu hình này?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/PriceManager/PriceGroup/DeletePriceNight",
                         type: "POST",
                         data: { PriceNightID: id },
                         success: function (result) {
                             if (result == 0) {
                                 ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
                             } else {
                                 $("#ListPriceNight").fadeOut(200, function () {
                                     $("#trPriceNight-" + id).remove();
                                     $("#ListPriceNight").fadeIn();
                                 });
                             }
                         },
                         error: function (xhr, status, error) {
                             var err = eval("(" + xhr.responseText + ")");
                             ShowMessage(err.Message,1);
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
}

function DeletePriceGroup(id) {
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn xóa cấu hình này?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/PriceGroup/DeletePriceGroup",
    //            type: "POST",
    //            data: { PriceGroupID: id },
    //            success: function (result) {
    //                if (result == 0) {
    //                    alert("Có lỗi xảy ra. Vui lòng thử tải lại trang.");
    //                } else {
    //                    $("#ListPriceGroup").fadeOut(200, function () {
    //                        $("#trPriceGroup-" + id).remove();
    //                        $("#ListPriceGroup").fadeIn();
    //                    });
    //                }
    //            },
    //            error: function (xhr, status, error) {
    //                var err = eval("(" + xhr.responseText + ")");
    //                alert(err.Message);
    //            }
    //        });
    //    }
    //});
    
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có muốn xóa cấu hình này?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/PriceManager/PriceGroup/DeletePriceGroup",
                         type: "POST",
                         data: { PriceGroupID: id },
                         success: function (result) {
                             if (result == 0) {
                                 ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
                             } else {
                                 $("#ListPriceGroup").fadeOut(200, function () {
                                     $("#trPriceGroup-" + id).remove();
                                     $("#ListPriceGroup").fadeIn();
                                 });
                             }
                         },
                         error: function (xhr, status, error) {
                             var err = eval("(" + xhr.responseText + ")");
                             ShowMessage(err.Message,1);
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
}

function ShowAddCategoryPriceGroup(id) {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/ShowAddCategoryPriceGroup",
        type: "GET",
        data: { PriceGroupID: id },
        success: function (result) {
            $("#modal").html(result).modal('show');
            
        }
    });
}

function DeleteCategoryPriceGroup(RoomCategoryID, PriceGroupID) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "Bạn có muốn xóa cấu hình này?",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/PriceManager/PriceGroup/DeleteCategoryPriceGroup",
                         type: "POST",
                         data: { CategoryID: RoomCategoryID, PricegroupID: PriceGroupID },
                         success: function (result) {
                             if (result == 0) {
                                 ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
                             } else {
                                 $("#ListCategoryPriceGroup").fadeOut(200, function () {
                                     $("#" + PriceGroupID + "-" + RoomCategoryID).remove();
                                     $("#ListCategoryPriceGroup").fadeIn();
                                 });
                             }
                         },
                         error: function (xhr, status, error) {
                             var err = eval("(" + xhr.responseText + ")");
                             ShowMessage(err.Message,1);
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
    
    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Bạn có muốn xóa cấu hình này?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/PriceGroup/DeleteCategoryPriceGroup",
    //            type: "POST",
    //            data: { CategoryID: RoomCategoryID, PricegroupID: PriceGroupID },
    //            success: function (result) {
    //                if (result == 0) {
    //                    alert("Có lỗi xảy ra. Vui lòng thử tải lại trang.");
    //                } else {
    //                    $("#ListCategoryPriceGroup").fadeOut(200, function () {
    //                        $("#" + PriceGroupID + "-" + RoomCategoryID).remove();
    //                        $("#ListCategoryPriceGroup").fadeIn();
    //                    });
    //                }
    //            },
    //            error: function (xhr, status, error) {
    //                var err = eval("(" + xhr.responseText + ")");
    //                alert(err.Message);
    //            }
    //        });
    //    }
    //});
    
}

function UpdatePriceGroup() {
    var PriceGroupID = $("#PricegroupID").val();
    var DayPrice = SlipThoundsandDelimeter("PriceDay");
    $("#PriceDay").val(DayPrice);
    var FirstHourPrice = SlipThoundsandDelimeter("FirstTimeValue");
    $("#FirstTimeValue").val(FirstHourPrice);
    var SencondHourPrice = SlipThoundsandDelimeter("SecondTimeValue");
    $("#SecondTimeValue").val(SencondHourPrice);
    var ThirdHourPrice = SlipThoundsandDelimeter("ThirdTimeValue");
    $("#ThirdTimeValue").val(ThirdHourPrice);
    var NextHourPrice = SlipThoundsandDelimeter("NextTimeValue");
    $("#NextTimeValue").val(NextHourPrice);
    var PriceGroupForm = document.getElementById("AddPriceGroupForm");
    $.ajax({
        url: "/PriceManager/PriceGroup/UpdatePriceGroup",
        type: "POST",
        data: $(PriceGroupForm).serialize(),
        success: function (result) {
            if (result == 0) {
                ShowMessage("Có lỗi xảy ra. Vui lòng thử tải lại trang.",1);
            } else {
                ViewDetailPriceGroup(PriceGroupID);
            }
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage(err.Message,1);
        }
    });
}

function ViewPriceNightDetail(RoomPriceID) {
    
    $.ajax({
        url: "/PriceManager/PriceGroup/ShowUpdatePriceNight",
        type: "GET",
        data: { RoomPriceID: RoomPriceID },
        success: function (result) {
            $("#modal").html(result).modal('show');
            $('#CheckInTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                hourMin: 18,
                minute: 30,
                showMeridian: false,
                defaultValue: "18:00"
            });
            $('#CheckOutTime').timepicker({
                stepMinute: 15,
                showHour: true,
                showMinute: true,
                hourMax: 12,
                showMeridian: false,
                defaultValue: "12:00"
            });
            $('#MaxDuration').timepicker({
                stepMinute: 1,
                showHour: true,
                showMinute: true,
                showSecond: true,
                hourMax: 23,
                showMeridian: false,
                defaultValue: "23:59:59"
            });
            
        }
    });
}

function UpdatePriceNight() {
    var PriceNight = SlipThoundsandDelimeter("PriceNight");
    $("#PriceNight").val(PriceNight);
    var PriceNightID = $("#PriceNightID").val();
    var form = document.getElementById("UpdatePriceNightForm");
    $.ajax({
        url: "/PriceManager/PriceGroup/UpdatePriceNight",
        type: "POST",
        data: $(form).serialize(),
        success: function (result) {
            $("#trPriceNight-" + PriceNightID).replaceWith(result);
            $("#modal").modal("hide");
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            ShowMessage(err.Message,1);
        }
    });
}