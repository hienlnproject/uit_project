﻿//////////////////////////////////////// HOTEL DATA ///////////////////////////////

var hotelName = 'KIM NGÂN';
var address = "32-34 KDC Trung Son";
var phone = "08-54333339";
//////////////////////////////////////////////////////////////////////////////////

function CloseRentDetailForm() {
    var roomId = $("#roomId").val();
    var rentType = $("#hdfRentType").val();

    //Kiem tra rentType o mainform neu khac thi load
    $(".aRoom").removeClass('active');
    var roomRentType = $("#r-" + roomId).attr("data-rentType");
    if (roomRentType != rentType) {
        UpdateRoomStatus(roomId);
    }
}


////////////////////////// SHOW RENT DETAIL //////////////////////////////////



function ShowRent(rentId) {
    
    $("#modal").on('hide', function () {
        CloseRentDetailForm();
    });


    $.ajax({
        url: "/Rent/Index",
        type: "GET",
        data: { rentId: rentId },
        cache: true,
        success: function (result) {
            $("#modal").html(result).modal('show');
            FocusRentType();
            SetDateTimePicker();
            changeStatusUpdateButton(true);
            applyScrollbar();
            
        }
    });


}

function ShowRentDetailReadOnly(rentid) {
    
    $.ajax({
        url: "/Rent/ViewRentReadonly",
        type: "GET",
        data: { rentId: rentid },
        cache: true,
        success: function (result) {
            $("#modal").html(result).modal('show');
            applyScrollbar();
            
        }
    });
}

function FocusRentType() {
    //Focus rent type button
    var rentType = $("#hdfRentType").val();
    $('[data-rentType]').removeClass("active");
    var btnCurrentFee = $('[data-rentType =' + rentType + ']');
    btnCurrentFee.addClass("active");
}

function applyScrollbar() {
    $('#rentFeeDetail').popover();
    $("#wrapMenu").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapOrderTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapPaymentTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
    $("#wrapCustomerTable").mCustomScrollbar({

        advanced: {
            updateOnContentResize: true
        }
    });
}


////////////////////// RENT UPDATING - CHECK OUT /////////////////////////

function UpdateRentInfo() {
    event.preventDefault();
    var rentId = $("#rentId").val();
    //var checkInDate = ConvertDateVNToUS($('#CheckInDate').val());

    var checkInDate = $('#CheckInDate').val();

    var invoiceId = $("#InvoiceID").val();
    var rentType = $("#hdfRentType").val();
    var bikeId = $("#BikeID").val();
    var notes = $("#Notes").val();
    var priceGroup = $("#PriceGroupID").val();
    var roomid = $("#roomId").val();

    var isFixedPrice = $('#isFixedPrice').is(':checked');

    var rentFee = $("#rentFee").val();

    rentFee = rentFee.replace(/\,/g, '');
    rentFee = rentFee.replace(/\./g, '');

    var fee = parseInt(rentFee);

    $.ajax({
        url: "/Rent/UpdateRentInfo",
        type: "GET",
        data: {
            rentId: rentId, invoiceId: invoiceId, checkInDate: checkInDate, rentType: rentType, bikeId: bikeId,
            notes: notes, priceGroup: priceGroup, isFixedPrice: isFixedPrice, rentFee: fee
        },
        cache: true,
        success: function (result) {
            if (result == "0") {
                ShowMessage("Cập nhật không thành công!", 1);
            }
            else {
                $("#rentInfo").fadeOut(200, function () {
                    UpdateRoomStatus(roomid);
                    $(".has-success").removeClass('has-success');
                    $("#rentInfo").fadeIn();
                    changeStatusUpdateButton(true);
                    //Update number of room.
                    var RentTypeBeforeUpdate = $("#RentTypeBeforeUpdate").val();
                    //Neu thay doi rent type thi moi cap nhat so luong.
                    if (RentTypeBeforeUpdate != rentType) {
                        //Update number of room of rent type before update.
                        //Thue gio.
                        if (RentTypeBeforeUpdate == 1) {
                            var NumberOfRentHour = $("#NumberOfRentHour").val();
                            NumberOfRentHour--;
                            $("#NumberOfRentHour").val(NumberOfRentHour);
                            $("#spanNumberOfRentHour").html("<span class='badge badge-important' id='spanNumberOfRentHour'>" + NumberOfRentHour + "</span>");
                        } else if (RentTypeBeforeUpdate == 2) {
                            //Qua dem.
                            var NumberOfRentNight = $("#NumberOfRentNight").val();
                            NumberOfRentNight--;
                            $("#NumberOfRentNight").val(NumberOfRentNight);
                            $("#spanNumberOfRentNight").html("<span class='badge badge-inverse' id='spanNumberOfRentNight'>" + NumberOfRentNight + "</span>");
                        } else if (RentTypeBeforeUpdate == 3) {
                            //Thue ngay
                            var NumberOfRentDay = $("#NumberOfRentDay").val();
                            NumberOfRentDay--;
                            $("#NumberOfRentDay").val(NumberOfRentDay);
                            $("#spanNumberOfRentDay").html("<span class='badge badge-info' id='spanNumberOfRentDay'>" + NumberOfRentDay + "</span>");
                        }
                        //Update number of room of rent type after update.
                        //Thue gio.
                        if (rentType == 1) {
                            var NumberOfRentHour = $("#NumberOfRentHour").val();
                            NumberOfRentHour++;
                            $("#NumberOfRentHour").val(NumberOfRentHour);
                            $("#spanNumberOfRentHour").html("<span class='badge badge-important' id='spanNumberOfRentHour'>" + NumberOfRentHour + "</span>");
                        } else if (rentType == 2) {
                            //Qua dem.
                            var NumberOfRentNight = $("#NumberOfRentNight").val();
                            NumberOfRentNight++;
                            $("#NumberOfRentNight").val(NumberOfRentNight);
                            $("#spanNumberOfRentNight").html("<span class='badge badge-inverse' id='spanNumberOfRentNight'>" + NumberOfRentNight + "</span>");
                        } else if (rentType == 3) {
                            //Thue ngay
                            var NumberOfRentDay = $("#NumberOfRentDay").val();
                            NumberOfRentDay++;
                            $("#NumberOfRentDay").val(NumberOfRentDay);
                            $("#spanNumberOfRentDay").html("<span class='badge badge-info' id='spanNumberOfRentDay'>" + NumberOfRentDay + "</span>");
                        }
                    }
                });
                ShowMessage('Cập nhật thành công', 2);
                try {
                    $("#orderDatatable").dataTable()._fnAjaxUpdate();
                } catch (e) {

                }
            }
        },
        error: function () {
            ShowMessage("Cập nhật không thành công!", 1);
        }
    });

}

function changeStatusUpdateButton(disable) {
    if (disable) {
        $("#btnUpdateRentInfo").attr("disabled", "disabled");
        $("#btnUpdateRentInfo").removeClass("btn-info");
        $("#btnCheckOut").removeAttr("disabled", "disabled");
    }
    else {
        $("#btnUpdateRentInfo").removeAttr("disabled");
        $("#btnUpdateRentInfo").addClass("btn-info");
        $("#btnCheckOut").attr("disabled", "disabled");

    }
}


function CheckOut() {

    var rentId = $("#rentId").val();
    var checkOutDate = ($('#CheckOutDate').val()); //Do co che tu sinh ID cua MVC 
    var checkInDate = $('#CheckInDate').val();
    var rentType = $("#hdfRentType").val();
    var roomid = $("#roomId").val();
    var editable = $("#editable").val();



    //validaton date checkin and checkout
    var validateCheckInDate = (checkInDate.replace(/:/g, "-")).replace(" ", "-");
    var validateCheckOutDate = (checkOutDate.replace(/:/g, "-")).replace(" ", "-");

    var outYear, outMonth, outDate, outHour, outMin;
    var inYear, inMonth, inDate, inHour, inMin;

    var strArrIn = validateCheckInDate.split('-');
    inDate = strArrIn[0];
    inMonth = strArrIn[1];
    inYear = strArrIn[2];
    inHour = strArrIn[3];
    inMin = strArrIn[4];


    var strArrOut = validateCheckOutDate.split('-');
    outDate = strArrOut[0];
    outMonth = strArrOut[1];
    outYear = strArrOut[2];
    outHour = strArrOut[3];
    outMin = strArrOut[4];

    var dateIn = new Date(inYear, inMonth - 1, inDate, inHour, inMin);
    var dateOut = new Date(outYear, outMonth - 1, outDate, outHour, outMin);

    if (dateIn > dateOut) {
        event.preventDefault();
        ShowMessage('Thời gian vào không được trước thời gian ra!', 1);
        return false;
    }

    var isPayAll = !($('#isNotPayAll').is(':checked'));

    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn có muốn trả phòng?</h6>",
        buttons:
            {
                "ok":
                    {
                        "label": "<i class='icon-remove'></i> Đồng ý!",
                        "className": "btn-sm btn-success",
                        "callback": function () {

                            $.ajax({
                                url: "/Rent/CheckOut",
                                type: "GET",
                                data: {
                                    rentId: rentId, checkInDate: ConvertDateVNToUS(checkInDate), checkOutDate: ConvertDateVNToUS(checkOutDate),
                                    rentType: rentType, isPayAll: isPayAll, editable: editable
                                },
                                success: function (result) {

                                    if (result == "1" || result == "3") {
                                        $('#modal').modal('hide');

                                        if (result == "3") {
                                            ShowMessage("Phòng này đã được thanh toán", 3);

                                            try {
                                                UpdateLabelPanel();
                                            } catch (e) {
                                            }
                                        }

                                        UpdateRoomStatus(roomid);

                                        try {

                                            //RefreshTable();
                                            $("#orderDatatable").dataTable()._fnAjaxUpdate();
                                        } catch (e) {
                                        }

                                        //Update number of room.
                                        var NumberOfPrepare = $("#NumberOfPrepare").val();
                                        NumberOfPrepare++;
                                        $("#NumberOfPrepare").val(NumberOfPrepare);
                                        $("#spanNumberOfPrepare").html("<span class='' id='spanNumberOfPrepare'>" + NumberOfPrepare + "</span>");
                                        //Thue gio.
                                        if (rentType == 1) {
                                            var NumberOfRentHour = $("#NumberOfRentHour").val();
                                            NumberOfRentHour--;
                                            $("#NumberOfRentHour").val(NumberOfRentHour);
                                            $("#spanNumberOfRentHour").html("<span class='' id='spanNumberOfRentHour'>" + NumberOfRentHour + "</span>");
                                        } else if (rentType == 2) {
                                            //Qua dem.
                                            var NumberOfRentNight = $("#NumberOfRentNight").val();
                                            NumberOfRentNight--;
                                            $("#NumberOfRentNight").val(NumberOfRentNight);
                                            $("#spanNumberOfRentNight").html("<span class='' id='spanNumberOfRentNight'>" + NumberOfRentNight + "</span>");
                                        } else if (rentType == 3) {
                                            //Thue ngay
                                            var NumberOfRentDay = $("#NumberOfRentDay").val();
                                            NumberOfRentDay--;
                                            $("#NumberOfRentDay").val(NumberOfRentDay);
                                            $("#spanNumberOfRentDay").html("<span class='' id='spanNumberOfRentDay'>" + NumberOfRentDay + "</span>");
                                        }
                                    } else {
                                        ShowMessage("Thanh toán không thành công", 1);
                                    }
                                }
                            });
                        }
                    },
                "close":
                    {
                        "label": "<i class='icon-ok'></i> Đóng",
                        "className": "btn-sm btn-danger",
                        "callback": function () {
                            bootbox.hideAll();
                        }
                    }
            }
    });
}

function printReport() {
    var docprint = window.open("", "", 'left=100,top=100,width=700,height=400');
    docprint.document.open();
    docprint.document.write('<html><body>');
    docprint.document.write('HÓA ĐƠN TÍNH TIỀN');
    docprint.document.write('</br>');
    docprint.document.write('cửa hàng ' + hotelName + '<br/>ĐC: ' + address + '<br/>ĐT:' + phone);
    docprint.document.write('</br>*********************');
    docprint.document.write('<br/>Giờ vào: ' + $("#Rent_CheckInDate").val());
    docprint.document.write('<br/>Giờ ra: ' + $("#Rent_CheckOutDate").val());
    docprint.document.write('<br/>Tiền phòng:<strong>' + $("#rentFee").val() + '<strong/>');
    docprint.document.write('<br/>Chi tiết tiền phòng:<br/>' + $("#rentFeeDetail").attr('data-content'));
    docprint.document.write('<br/>Dịch vụ <br/>');
    //Print service
    $('#tblServiceDetail tr').each(function () {
        if (!$(this).hasClass('trHead')) {
            var productName = $(this).find('td').eq(1).html();
            var quantity = $(this).find('td').eq(2).html();
            var totalPrice = $(this).find('td').eq(3).html();
            docprint.document.write(productName + '  ' + quantity + '  ' + totalPrice + '</br>');
        }

    });

    docprint.document.write('Tiền dịch vụ:<strong>' + $("#orderFee").val() + '<strong/>');
    docprint.document.write('<br/>Tổng cộng:<strong>' + $("#totalFee").val() + '<strong/>');


    docprint.document.write('</body></html>');
    docprint.document.close();
    docprint.print();
    docprint.close();
}



///////////////////// RENT FEE CALCULATOR //////////////////////////////

function SetDateTimePicker() {

    //Chinh datetime picker
    var checkindate = $('#CheckInDate').val();

    //$('#CheckInDate').val(ConvertDateVNToUS(checkindate));

    $('#CheckInDate').datetimepicker({
        onClose: function (dateText, inst) {
            var rentType = $("#hdfRentType").val();
            UpdateRentFee(rentType);
        },
        dateFormat: 'dd-mm-yy'
    });

    $('#CheckInDate').datetimepicker('setDate', (new Date($('#CheckInDate').val())));
    //Chinh Datetime picker

    //var checkoutdate = $('#CheckOutDate').val();
    //$('#CheckOutDate').val(ConvertDateVNToUS(checkoutdate));

    $('#CheckOutDate').datetimepicker({
        onClose: function (dateText, inst) {
            var rentType = $("#hdfRentType").val();
            UpdateRentFee(rentType);
        },
        dateFormat: 'dd-mm-yy'
    });
    $('#CheckOutDate').datetimepicker('setDate', (new Date($('#CheckOutDate').val())));

    $("#CheckInDate").change(function () {
        $(this).closest('.form-group').addClass("has-success");
    });

    $("#CheckOutDate").change(function () {
        $(this).closest('.form-group').addClass("has-success");
    });

}

function ChangePriceGroup() {
    var rentType = $("#hdfRentType").val();
    UpdateRentFee(rentType);
}

//Cap nhat lai gia tri cua Fee thue phong 
function UpdateRentFee(rentType) {

    var isFixedPrice = $("#isFixedPrice").is(':checked');
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
    $("#hdfRentType").val(rentType);

    if (!isFixedPrice) {

        var rentId = $("#rentId").val();
        var checkOutDate = ConvertDateVNToUS($('#CheckOutDate').val()); //Do co che tu sinh ID cua MVC 
        var checkInDate = ConvertDateVNToUS($('#CheckInDate').val());
        var priceGroup = $('#PriceGroupID').val();

        //Thay doi phi khi: gio vao thay doi, gio ra thay doi , hinh thuc thue thay doi
        //InvoiceID, checkInDate, Bike, Note duoc bat bang Onchange, chi con hdfRentType
        var oldRentType = $("#hdfRentType").val();


        $.ajax({
            url: "/Rent/CalculateRentFee",
            type: "GET",
            data: { rentID: rentId, rentType: rentType, checkInDate: checkInDate, checkOutDate: checkOutDate, priceGroup: priceGroup },
            cache: true,
            success: function (result) {

                $('#rentFeeDetail').attr("data-content", result.description);
                $("#rentFee").val(addCommas(result.rentFee));
                UpdateEntireFee(result.rentFee, rentType);


                changeStatusUpdateButton(false);

                //if (rentType != oldRentType) {

                //    $("#hdfRentType").val(rentType);

                //}

            }
        });
    }
}

function UpdateEntireFee() {
    //var rentFee = parseInt($("#rentFee").val().replace(/,/g, ""));
    //var orderFee = ($("#hdfOrderFee").val() != null) ? parseInt($("#hdfOrderFee").val()) : parseInt($("#orderFee").val().replace(/,/g, "")); //Lay tu hidden service form

    //var addionFee = ($("#hdfTotalAddition").val() != null) ? parseInt($("#hdfTotalAddition").val()) : parseInt($("#addionFee").val().replace(/,/g, ""));
    //var discountFee = ($("#hdfTotalDiscount").val() != null) ? parseInt($("#hdfTotalDiscount").val()) : parseInt($("#discountFee").val().replace(/,/g, ""));
    //var totalFee = rentFee + orderFee + addionFee - discountFee;

    //var totalPayment = ($("#hdfTotalPayment").val() != null) ? parseInt($("#hdfTotalPayment").val()) : parseInt($("#totalPayment").val().replace(/,/g, ""));

    //var remainFee = totalFee - totalPayment;

    //$("#orderFee").val(addCommas(orderFee));
    //$("#rentFee").val(addCommas(rentFee));
    //$("#addionFee").val(addCommas(addionFee));
    //$("#discountFee").val(addCommas(discountFee));
    //$("#totalFee").val(addCommas(totalFee));
    //$("#totalPayment").val(addCommas(totalPayment));
    //$("#remainFee").val(addCommas(remainFee));
}

/////////////// RENT CUSTOMERS /////////////////////////////////
function addRentCustomer() {
    event.preventDefault();
    var rentId = $("#rentId").val();
    var personId = $("#personId").val();
    var customerName = $("#customerName").val();
    var address = $("#address").val();
    if (personId == "") {
        ShowMessage("Vui lòng nhập CMND", 3);
        return false;
    }
    if (customerName == "") {
        ShowMessage("Vui lòng nhập Tên khách hàng", 3);
        return false;
    }


    if (personId != "" && customerName != "") {
        $.ajax({
            url: '/Rent/AddRentCustomers',
            type: 'GET',
            data: { rentId: rentId, personId: personId, customerName: customerName, address: address },
            success: function (result) {
                if (result != null) {
                    $("#personId").val("");
                    $("#customerName").val("");
                    $("#address").val("");
                    $("#customerList").prepend(result);

                    //$("#wrapCustomerTable").fadeOut(200, function () {
                    //    $("#customerList").prepend(result);
                    //    $("#wrapCustomerTable").fadeIn(200, function () {
                    //        $("#wrapCustomerTable").mCustomScrollbar("update");
                    //    });
                    //});

                };

            }
        });
    }

};

function removeCustomer(customerId) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Xóa khách hàng?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Rent/RemoveRentCustomers",
                         type: "GET",
                         data: { customerId: customerId },
                         success: function (result) {

                             if (result == "1") {

                                 $("#wrapCustomerTable").fadeOut(200, function () {
                                     $("#customerid-" + customerId).remove();
                                     $("#wrapCustomerTable").fadeIn(200, function () {
                                         $("#wrapCustomerTable").mCustomScrollbar("update");
                                     });
                                 });

                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });

}

////////////////////////// RENT PREPAID /////////////////////////////////////////
function addPayment() {

    event.preventDefault();
    var amount = $("#paymentAmount").val();
    var rentId = $("#rentId").val();
    var notes = $("#paymentNotes").val();
    if (notes == "") {
        ShowMessage("Vui lòng nhập nội dung", 3);
        return false;
    }

    if (amount == "") {
        ShowMessage('Vui lòng nhập Số tiền', 3);
        return false;
    }
    if (isNaN(amount) == true) {
        ShowMessage('Vui lòng nhập Số tiền là chữ số', 3);
        return false;
    }

    $.ajax({
        url: "/Rent/AddPayment",
        type: "GET",
        data: { rentId: rentId, paymentAmount: amount, notes: notes },
        success: function (result) {
            if (result != null) {
                $("#paymentAmount").val("");
                $("#paymentNotes").val("");
                //$("#wrapPaymentTable").fadeOut(200, function () {
                //    $("#paymentList").prepend(result);

                //    $("#wrapPaymentTable").fadeIn(200, function () {
                //        $("#wrapPaymentTable").mCustomScrollbar("update");
                //        UpdateTotalPayment(amount)
                //        //Update Entire Fee
                //        UpdateEntireFee();
                //    });
                //});

                $("#paymentList").fadeOut(200, function () {
                    $("#paymentList").html(result);
                    $("#paymentList").fadeIn(200, function () {
                        UpdateTotalPayment(amount)
                        //Update Entire Fee
                        UpdateEntireFee();
                    });
                });

            }

        }
    });
};

function removePayment(paymentId, amount) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn muốn xóa thanh toán này?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Rent/RemovePayment",
                         type: "GET",
                         data: { paymentId: paymentId },
                         success: function (result) {
                             //if (result == "1") {                               
                             //$("#wrapPaymentTable").fadeOut(200, function () {
                             //    $("#paymentid-" + paymentId).remove();
                             //    $("#wrapPaymentTable").fadeIn(200, function () {
                             //        $("#wrapPaymentTable").mCustomScrollbar("update");
                             //        UpdateTotalPayment(-amount);

                             //        //Update Entire Fee
                             //        UpdateEntireFee();
                             //    });
                             //});
                             //};
                             $("#paymentList").fadeOut(200, function () {
                                 $("#paymentList").html(result);
                                 $("#paymentList").fadeIn(200, function () {
                                     UpdateTotalPayment(amount)
                                     //Update Entire Fee
                                     UpdateEntireFee();
                                 });
                             });
                         }


                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
};


function UpdateTotalPayment(amount) {
    //Update total payment
    var totalPayment = parseInt($("#hdfTotalPayment").val());
    totalPayment = totalPayment + parseInt(amount);
    $("#hdfTotalPayment").val(totalPayment);


}

////////////////////////// RENT ADDITION /////////////////////////////////////////
function addAddition() {
    event.preventDefault();
    var amount = $("#additionAmount").val();
    var rentId = $("#rentId").val();
    var notes = $("#additionNotes").val();
    var isAddition = $("#isAddition").val();

    if (notes == "") {
        ShowMessage("Vui lòng nhập nội dung", 3);
        return false;
    }

    if (amount == "") {
        ShowMessage('Số tiền không được để trống', 3);
        return false;
    }

    if (isNaN(amount) == true) {
        ShowMessage('Số tiền phải là chữ số', 3);
        return false;
    }


    $.ajax({
        url: "/Rent/AddFeeChange",
        type: "GET",
        data: { rentId: rentId, feeChangeAmount: amount, notes: notes, isAddition: isAddition },
        success: function (result) {
            if (result != null) {
                $("#additionAmount").val("");
                $("#additionNotes").val("");
                $("#additionList").prepend(result);
            }

        }
    });
};

function removeFeeChange(feeChangeId, amount, isAddition) {
    var element = $(event.toElement).closest("tr");
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn muốn xóa ?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Rent/RemoveFeeChange",
                         type: "GET",
                         data: { feeChangeId: feeChangeId },
                         success: function (result) {
                             if (result == "1") {
                                 $("#wrapAdditionTable").fadeOut(200, function () {
                                     //$("#feeChangeId-" + feeChangeId).remove();
                                     $(element).remove();
                                     $("#wrapAdditionTable").fadeIn(200, function () {
                                         $("#wrapAdditionTable").mCustomScrollbar("update");

                                         UpdateFeeChange(-amount, isAddition);
                                         UpdateEntireFee();
                                     });
                                 });
                             };
                         }


                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });



};


function UpdateFeeChange(amount, isAddition) {
    //Update total payment
    if (isAddition == "True") {
        var totalAddition = parseInt($("#hdfTotalAddition").val());
        totalAddition += parseInt(amount);
        $("#hdfTotalAddition").val(totalAddition);
    }
    else {
        var totalDiscount = parseInt($("#hdfTotalDiscount").val());
        totalDiscount += parseInt(amount);
        $("#hdfTotalDiscount").val(totalDiscount);
    }
}

////////////////////////// RENT DISCOUNT /////////////////////////////////////////
//function addDiscount() {
//    var amount = $("#discountAmount").val();
//    var rentId = $("#rentId").val();
//    var notes = $("#discountNotes").val();

//    $.ajax({
//        url: "Rent/AddFeeChange",
//        type: "GET",
//        data: { rentId: rentId, feeChangeAmount: amount, notes: notes, isAddition: false },
//        success: function (result) {
//            if (result != null) {
//                $("#discountAmount").val("");
//                $("#discountNotes").val("");
//                $("#wrapDiscountTable").fadeOut(200, function () {
//                    $("#discountList").prepend(result);
//                    $("#wrapDiscountTable").fadeIn(200, function () {
//                        $("#wrapDiscountTable").mCustomScrollbar("update");
//                        UpdateTotalDiscount(amount);
//                        //Update Entire Fee
//                        UpdateEntireFee();
//                    });
//                });


//            }

//        }
//    });
//};




//function RemoveDiscount(feeChangeId, amount) {
//    $.ajax({
//        url: "Rent/RemoveFeeChange",
//        type: "GET",
//        data: { feeChangeId: feeChangeId },
//        success: function (result) {
//            if (result == "1") {
//                $("#wrapPaymentTable").fadeOut(200, function () {
//                    $("#feeChangeId-" + feeChangeId).remove();
//                    $("#wrapPaymentTable").fadeIn(200, function () {
//                        $("#wrapPaymentTable").mCustomScrollbar("update");
//                        UpdateTotalDiscount(-amount);
//                        //Update Entire Fee
//                        UpdateEntireFee();
//                    });
//                });
//            };
//        }


//    });
//};


//function UpdateTotalDiscount(amount) {
//    //Update total payment
//    var totalDiscount = parseInt($("#hdfTotalDiscount").val());
//    totalDiscount += parseInt(amount); ;
//    $("#hdfTotalDiscount").val(totalDiscount);


//}


////////////////////////////////// RENT Service //////////////////////////////////////////////



function LoadItemByCategory(rentId, cateId) {
    //change focus
    
    var check = $("#service" + cateId).html();
    if (check == "") {
        $.ajax({
            url: "/Service/LoadItemByCategory",
            type: "GET",
            data: { cateId: cateId, txtRentId: rentId },
            cache: true,
            success: function (result) {
                $("#service" + cateId).html(result);
            }
        });
    }
}



function LoadItemByCategoryServiceNew(rentId, cateId, fromDate, toDate, category) {
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
    
    $.ajax({
        url: "/Service/LoadItemByCategoryServiceNew",
        type: "GET",
        data: { cateId: cateId, txtRentId: rentId, fromDate: fromDate, toDate: toDate, category: category },
        cache: true,
        success: function (result) {
            $("#wrapMenu").fadeOut(200, function () {
                $("#wrapMenu").html(result);
                $("#wrapMenu").fadeIn(200, function () {
                    $("#wrapMenu").mCustomScrollbar({
                        advanced: {
                            updateOnContentResize: true
                        }
                    });
                });

            });
            

        }
    });
}


function LoadItemByCategoryServiceNewMonth(rentId, cateId, fromDate, toDate, category) {
    //change focus
    $(event.toElement).closest(".btn-group").find(".btn").removeClass("active");
    $(event.toElement).addClass('active');
    
    $.ajax({
        url: "/Service/LoadItemByCategoryServiceNewMonth",
        type: "GET",
        data: { cateId: cateId, txtRentId: rentId, fromDate: fromDate, toDate: toDate, category: category },
        cache: true,
        success: function (result) {
            $("#wrapMenu").fadeOut(200, function () {
                $("#wrapMenu").html(result);
                $("#wrapMenu").fadeIn(200, function () {
                    $("#wrapMenu").mCustomScrollbar({
                        advanced: {
                            updateOnContentResize: true
                        }
                    });
                });

            });
            
        }
    });
}


function addItem(productId, price) {
    var rentId = $('#txtRentId').val();
    var quantity = $('#txtQuantity-' + productId).val();

    var sumPrice = quantity * price;



    $.ajax({
        url: '/Service/AddItem',
        type: 'GET',
        data: { rentId: rentId, productId: productId, quantity: quantity },
        success: function (result) {
            //TO-DO check fade out and fade in again
            if (result != null) {
                var newResult = $(result).addClass("warning").clone().wrap('<p>').parent().html();

                $("#wrapOrderTable").fadeOut(200, function () {

                    if ($("#ordered-item .warning") != null) {
                        $("#ordered-item .warning").removeClass("warning").addClass("success");
                    }
                    $("#ordered-item").prepend(newResult);
                    $("#wrapOrderTable").fadeIn(200, function () {
                        $("#wrapOrderTable").mCustomScrollbar("update");

                        updateOrderFee(sumPrice);

                        //Cap nhat lai toan bo chi phi
                        UpdateEntireFee();
                    });
                });
            }
        }
    });

}

function removeItem(orderId) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Bạn muốn xóa dịch vụ?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Service/RemoveItem",
                         type: "GET",
                         data: { orderID: orderId },
                         cache: true,
                         success: function (result) {
                             if (result == "1") {
                                 $("#wrapOrderTable").fadeOut(200, function () {
                                     var quantity = parseInt($(".order-item-" + orderId).find(".itemQuantity").html());
                                     var price = parseInt($(".order-item-" + orderId).find(".price").html().replace(/\./g, ""));
                                     $(".order-item-" + orderId).remove();
                                     $("#wrapOrderTable").fadeIn(200, function () {
                                         $("#wrapOrderTable").mCustomScrollbar("update");
                                         updateOrderFee(-quantity * price);
                                         //Cap nhat lai toan bo chi phi
                                         UpdateEntireFee();
                                     });
                                 });
                             };
                             bootbox.hideAll();
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });
};

function updateOrderFee(amount) {
    var orderFee = parseInt($("#hdfOrderFee").val());
    orderFee += parseInt(amount);;
    $("#hdfOrderFee").val(orderFee);
}

//////////////////////////////// RENT CHANGE ROOM /////////////////////////////////////////////////////////////////

function ChangeRoom() {
    var rentId = $("#rentId").val();
    var roomId = $("#roomId").val();
    var newRoomId = $('#roomList :selected').val();
    $.ajax({
        url: "/Rent/ChangeRoom",
        type: "GET",
        data: { rentId: rentId, roomId: roomId, newRoomId: newRoomId },
        success: function (result) {
            if (result == "1") {
                $('#modal').modal('hide');
                UpdateRoomStatus(roomId);
                UpdateRoomStatus(newRoomId);
                ShowMessage('Chuyển phòng thành công', 2);
            }
            else {
                ShowMessage("Vui lòng kiểm tra trạng thái phòng được chuyển đã sẵn sàng!", 3);
            }
        }
    });
};

//Chuyển trạng thái phòng từ chờ duyệt thành đang thuê
function RestoreStatus(rentID) {
    $.ajax({
        url: "/Rent/RestoreStatus",
        type: "GET",
        data: { rentId: rentID },
        success: function (result) {
            if (result == '1') {
                $("#RentID-" + rentID).remove();

                //  RefreshTable();
                $("#orderDatatable").dataTable()._fnAjaxUpdate();
                $("#modal").modal('hide');
                ShowMessage("Khôi phục phòng thành công", 2);
            } else {
                ShowMessage("Khôi phục phòng thất bại", 1);
            }
        }
    });
}

//Load tung tab trong hoa don phong


function getAdvanceTab() {
    //TO-D0 
    
    $.ajax({
        url: "/Rent/GetChangeRoomView",
        type: "GET",
        cache: true,
        success: function (result) {
            $("#tab5").html(result);
            
        }
    });
}

function getServiceTab() {
    var rentID = $('#rentId').val();
    var check = $("#serviceTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetRentService",
            type: "GET",
            data: { rentId: rentID },
            cache: true,
            async: true,
            success: function (result) {
                $("#tab4").html(result);
                $("#tab4").append("<div class='clearfix'></div>");
                loadServicedProduct(rentID);
                
            }
        });
    }
}

function loadServicedProduct(rentID) {
    $.ajax({
        url: "/Service/LoadItems",
        type: "GET",
        data: {
            rentId: rentID,
            editable: true,
            isShortDisplay: false
        },
        cache: true,
        async: true,
        success: function (result) {
            $("#serviceArea").html(result);
        }
    });
}

function getFeeChangeTab() {
    var rentID = $('#rentId').val();
    var check = $("#feeChangeTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetRentFeeChange",
            type: "GET",
            cache: true,
            data: { rentId: rentID },
            success: function (result) {
                $("#tab3").html(result);
                $("#tab3").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getCustomerTab() {
    var rentID = $('#rentId').val();

    var check = $("#customerTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetRentCustomers",
            type: "GET",
            cache: true,
            data: { rentId: rentID },
            success: function (result) {
                $("#tab2").html(result);
                $("#tab2").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getPaymentTab() {
    var rentID = $('#rentId').val();

    var check = $("#prepaidTab").val();
    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetRentPayments",
            type: "GET",
            cache: true,
            data: { rentId: rentID },
            success: function (result) {
                $("#tab7").html(result);
                $("#tab7").append("<div class='clearfix'></div>");
                
            }
        });
    }
}

function getTaskTab() {
    var rentID = $('#rentId').val();
    var check = $("#taskTab").val();

    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetRentTask",
            type: "GET",
            cache: true,
            data: { rentId: rentID },
            success: function (result) {
                $("#tab6").html(result);
                $('#ExecuteDate').datetimepicker({ dateFormat: 'dd-mm-yy' });
                $('#ExecuteDate').datetimepicker('setDate', (new Date()));
                $('#CreateDate').datetimepicker({ dateFormat: 'dd-mm-yy' });
                $('#CreateDate').datetimepicker('setDate', (new Date()));
                
            }
        });
    }
}

function getChargeTab() {
    var rentID = $('#rentId').val();
    var check = $("#chargeTab").val();

    if (check == null) {
        
        $.ajax({
            url: "/Rent/GetCharge",
            type: "GET",
            cache: true,
            data: { rentId: rentID },
            success: function (result) {
                $("#tab8").html(result);
                
            }
        });
    } else {
        loadServicedOrderDetail(rentID);
    }


}

function loadServicedOrderDetail(rentID) {
    
    $.ajax({
        url: "/Rent/LoadServicedOrderDetail",
        type: "GET",
        cache: true,
        data: { rentId: rentID },
        success: function (result) {
            $("#servicedList").html(result);
            
        }
    });
}

//Quan ly Task
function updateTaskModal(TaskID, element) {
    //  $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone'   checked='checked' disabled='disabled'>");
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Sự kiện đã hoàn thành?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Rent/checkDone",
                         type: "GET",
                         data: { TaskID: TaskID },
                         success: function (result) {

                             if (result) {
                                 element.disabled = true;
                             }
                         }
                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     element.checked = false;
                     bootbox.hideAll();

                 }
             }
        }
    });
}

function updateTask(TaskID, element) {
    //  $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone'  onclick='updateTask(" + TaskID + ")'/>");
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Sự kiện đã hoàn thành?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {
                     $.ajax({
                         url: "/Rent/checkDone",
                         type: "GET",
                         data: { TaskID: TaskID },
                         success: function (result) {

                             if (result) {
                                 element.disabled = true;
                                 $("#taskDatatable").dataTable()._fnAjaxUpdate();
                             }
                         }

                     });
                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     element.checked = false;
                     bootbox.hideAll();
                 }
             }
        }
    });

    //$("#confirmDiv").confirmModal({
    //    heading: 'Xác nhận',
    //    body: '<h6>Sự kiện đã hoàn thành?</h6>',
    //    callback: function () {
    //        $.ajax({
    //            url: "/Rent/checkDone",
    //            type: "GET",
    //            data: { TaskID: TaskID },
    //            success: function (result) {
    //                if (result) {
    //                    $("#checkButton-" + TaskID).html("");
    //                    $("#checkButton-" + TaskID).html("<input type='checkbox' id='IsDone' checked='checked' disabled='disabled' />");
    //                }
    //            }
    //        });
    //    }
    //});
}

function removeTask(TaskID) {
    bootbox.dialog({
        title: 'Xác nhận',
        message: "<h6>Xóa công việc?</h6>",
        buttons:
        {
            "ok":
             {
                 "label": "<i class='icon-remove'></i> Đồng ý!",
                 "className": "btn-sm btn-success",
                 "callback": function () {

                     $.ajax({
                         url: "/Rent/deleteTask",
                         type: "GET",
                         data: { TaskID: TaskID },
                         success: function (result) {
                             if (result) {
                                 $("#row-" + TaskID).html("");
                             }
                         }
                     });

                 }
             },
            "close":
             {
                 "label": "<i class='icon-ok'></i> Đóng",
                 "className": "btn-sm btn-danger",
                 "callback": function () {
                     bootbox.hideAll();
                 }
             }
        }
    });



}

function insertTask() {
    var RentID = $("#RentID").val();
    var TaskDesctiption = $("#TaskDescription").val();
    var ExecuteDate = ConvertDateVNToUS($("#ExecuteDate").val());
    var CreateDate = ConvertDateVNToUS($("#CreateDate").val());
    var sExecuteDate = $("#ExecuteDate").val();

    if (TaskDesctiption == "") {
        ShowMessage("Vui lòng nhập mô tả công việc", 3);
        return;
    }

    if (sExecuteDate == "") {
        ShowMessage("Vui lòng nhập ngày thực hiện", 3);
        return;
    }

    if (CreateDate > ExecuteDate) {
        ShowMessage("Thời điểm thực hiện phải lớn hơn hiện tại", 3);
        return;
    }

    $.ajax({
        url: "/Rent/insertTask",
        type: "GET",
        data: { rentID: RentID, description: TaskDesctiption, executeDate: ExecuteDate, createDate: CreateDate },
        success: function (result) {
            if (result != -1) {
                $("#body1").prepend(result);
                $("#TaskDescription").val("");
            } else {
                ShowMessage("Thêm không thành công. Vui lòng thử lại sau!", 1);
            }
        }
    });

}

function printInvoice() {
    var dispsetting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    dispsetting += "scrollbars=yes,width=780, height=780, left=100, top=25";

    var docprint = window.open("", "", dispsetting);
    docprint.document.open();
    docprint.document.write('<html><head><title>HÓA ĐƠN TÍNH TIỀN</title>');
    docprint.document.write('</head><body>');
    docprint.document.write('cửa hàng ' + hotelName + '<br/>ĐC: ' + address + '<br/>ĐT:' + phone);
    docprint.document.write('</br>*********************');
    docprint.document.write('<br/>Mã số phiếu ' + $("#InvoiceID").val());
    docprint.document.write('<br/>Giờ vào: ' + $("#CheckInDate").val());
    docprint.document.write('<br/>Giờ ra: ' + $("#CheckOutDate").val());
    docprint.document.write('<br/>Số xe: ' + $("#BikeID").val());
    docprint.document.write('<br/>Hình thức thuê: ' + $("#RentType").val());
    docprint.document.write('<br/>Tiền phòng: ' + $("#rentFee").val());
    docprint.document.write('<br/>Tiền dịch vụ: ' + $("#orderFee").val());
    docprint.document.write('<br/>Phụ thu: ' + $("#addionFee").val());
    docprint.document.write('<br/>Giảm giá: ' + $("#discountFee").val());
    docprint.document.write('<br/>Tổng cộng: ' + $("#totalFee").val());
    docprint.document.write('<br/>Đã thanh toán: ' + $("#totalPayment").val());
    docprint.document.write('<br/>Còn lại: ' + $("#remainFee").val());

    //$('#tblServiceDetail tr').each(function () {
    //    if (!$(this).hasClass('trHead')) {
    //        var productName = $(this).find('td').eq(1).html();
    //        var quantity = $(this).find('td').eq(2).html();
    //        var totalPrice = $(this).find('td').eq(3).html();
    //        docprint.document.write(productName + '  ' + quantity + '  ' + totalPrice + '</br>');
    //    }
    //});

    //docprint.document.write('Tiền dịch vụ:<strong>' + $("#orderFee").val() + '<strong/>');
    //docprint.document.write('<br/>Tổng cộng:<strong>' + $("#totalFee").val() + '<strong/>');

    docprint.document.write('</body></html>');
    docprint.document.close();
    docprint.print();
    docprint.close();
}

function getDetailTab() {
    var rentID = $('#rentId').val();
    
    $.ajax({
        url: "/Rent/GetDetailTab",
        type: "GET",
        data: { rentId: rentID },
        cache: true,
        async: true,
        success: function (result) {
            $("#rentSubDetail").html(result);
            
        }
    });
}