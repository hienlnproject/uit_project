﻿
var emenu = {};
emenu.webdb = {};
emenu.info = { };
emenu.webdb.db = null;

emenu.info.CurrentCategoryId = "#cate-1";
emenu.info.OldCategoryId = null;
emenu.info.CurrentRoomId = null;
emenu.info.CurrentRoomName = null;
emenu.info.CurrentOrderDetails = new Array();

function getOrderModel(row) {
    return model = {
        OrderId: row.OrderId,
        ProductId: row.ProductId,
        RoomId: row.RoomId,
        Quantity: row.Quantity,
        Price:row.Price,
        OrderDate: row.OrderDate,
        IsUploaded: row.IsUploaded,
        IsNew: row.IsNew
    };
}

emenu.webdb.open = function () {
    var dbSize = 5 * 1024 * 1024;
    emenu.webdb.db = openDatabase("Emenu", "1", "Emenu Database", dbSize);
}

emenu.webdb.onError = function (tx, e) {
    //alert("There has been an error" + e.message);
    console.log(e.message);
}

emenu.webdb.onSuccess = function (tx,e) {
    //alert("Success");
}

//start Room section
emenu.webdb.createRoomTable = function (callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
                      "Room(RoomId INTEGER PRIMARY KEY, Name TEXT, RoomStatus INTEGER, FloorId INTEGER, FloorName TEXT)", [], callback);
    });
}

emenu.webdb.dropRoomTable = function () {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE room", []);
    });
}

emenu.webdb.addRoom = function (id,name,status,floorId,floorName) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO Room(RoomId,Name,RoomStatus,FloorId,FloorName) VALUES (?,?,?,?,?)",
            [id,name,status,floorId,floorName],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    });
}

emenu.webdb.updateRoom = function (id, name, status, floorId, floorName) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("UPDATE Room SET Name = ?,RoomStatus = ?, FloorId = ?,FloorName = ? WHERE RoomId = ?",
            [name, status, floorId,floorName, id],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    });
}

emenu.webdb.getAllRoomByFloorId = function (floorId,callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Room WHERE FloorId = ?", [floorId], callback, emenu.webdb.onError);
    });
}

emenu.webdb.getAllFloors = function (callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("SELECT DISTINCT FloorId,FloorName FROM Room", [], callback, emenu.webdb.onError);
    });
}
//end Room section

//start Product section
emenu.webdb.createProductTable = function (callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
                      "Product(ProductId INTEGER PRIMARY KEY, ProductName TEXT, Price INTEGER, PicURL TEXT)", [], callback);
    });
}

emenu.webdb.dropProductTable = function () {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE Product", []);
    });
}

emenu.webdb.addProduct = function (id, name, price, picUrl) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO Product(ProductId,ProductName,Price,PicURL) VALUES (?,?,?,?)",
            [id, name, price, picUrl],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    });
}

emenu.webdb.updateProduct = function (id, name, price, picUrl) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("UPDATE Room SET ProductName = ?,Price = ?, PicURL = ? WHERE ProductId = ?",
            [name, price, picUrl, id],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    });
}
//end Product section

//start OrderDetails section
emenu.webdb.createOrderDetailTable = function (callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS " +
                      "OrderDetail(OrderId INTEGER PRIMARY KEY, ProductId INTEGER, RoomId INTEGER, Quantity INTEGER,Price INTEGER, OrderDate TEXT, IsUploaded BOOLEAN, IsNew BOOLEAN)", [], callback,
                      emenu.webdb.onError
        );
    });
}

emenu.webdb.dropOrderDetailTable = function () {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("DROP TABLE OrderDetail", []);
    });
}

emenu.webdb.addOrderDetail = function (orderId, productId, roomId, quantity,price, orderDate,isUploaded,isNew) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO OrderDetail(OrderId,ProductId,RoomId,Quantity,Price,OrderDate,IsUploaded,IsNew) VALUES (?,?,?,?,?,?,?,?)",
            [orderId, productId, roomId, quantity,price, orderDate, isUploaded, isNew],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    }); 
}

emenu.webdb.updateOrder = function (orderId, productId, roomId, quantity,price, orderDate, isUploaded, isNew) {
    var db = emenu.webdb.db;
    db.transaction(function (tx) {
        tx.executeSql("UPDATE OrderDetail SET ProductId = ?,RoomId = ?, Quantity = ?,Price = ?, OrderDate = ?,IsUploaded = ?, IsNew = ? WHERE OrderId = ?",
            [productId, roomId, quantity,price, orderDate,isUploaded,isNew,orderId],
            emenu.webdb.onSuccess,
            emenu.webdb.onError);
    });
}

emenu.webdb.loadOrderDetailsByRoomId = function (roomId,callback) {
    emenu.webdb.db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM OrderDetail,Product WHERE OrderDetail.ProductId = Product.ProductId AND OrderDetail.RoomId = ?", [roomId], callback, emenu.webdb.onError);
    });
}
//end Product section

function renderLocations(tx,rs) {
    var rowOutput = "";
    var locations = $("#selectTable");
    var text = $("#local-text");
    text.html(rs.rows.item(0).FloorName);
    for (var i = 0; i < rs.rows.length; i++) {
        rowOutput += renderLocation(rs.rows.item(i));
    }
    locations.html(rowOutput);
}

function renderLocation(row) {
    return '<div class="location-tile one-third column tableChoose" onclick="chooseRoom(' + row.RoomId + ',' + '\'' + row.Name + '\'' + ')">' + row.Name + '</div>';
}

function renderFloors(tx, rs) {
    var rowOutput = "";
    var locations = $("#selectLocation");
    for (var i = 0; i < rs.rows.length; i++) {
        rowOutput += renderFloor(rs.rows.item(i));
        //alert(rs.rows.item(i).FloorName);
    }
    locations.html(rowOutput);
}

function renderFloor(row) {
    return "<li onclick='DisplayDDL("+ row.FloorId +")'><a>" + row.FloorName + "</a></li>";
    
}

function renderOrderDetails(tx, rs) {
    var location = $("#orderList");
    var rowOutput = "";
    for (var i = 0; i < rs.rows.length; i++) {
        rowOutput += renderOrderDetail(rs.rows.item(i));
        var model = getOrderModel(rs.rows.item(i));
        emenu.info.CurrentOrderDetails.push(model);
    }
    location.html(rowOutput);
}

function renderOrderDetail(row) {
    return '<li class="order-item-li"><div class="order-item">'
                   +'<img class="order-item-img order-item-margin" src="http://daphnedishes.files.wordpress.com/2009/11/spaghetti-meat-sauce.jpg?w=480&h=319" />'
                   +'<div class="order-item-margin">'+row.ProductName+'</div>'
                   +'<div class="order-item-margin">'+row.Price+' VND</div>'
                   +'<div class="order-item-margin">'+row.Quantity+'</div>'
                   +'<div style="margin-top: 3%;">'+row.Price*row.Quantity+'</div>'
                   +'</div></li>';
}

function chooseRoom(roomId,roomName) {
    emenu.info.CurrentRoomId = roomId;
    $(".ddl-btn.room-modal").html(roomName);
    //emenu.webdb.loadOrderDetailsByRoomId(roomId, renderOrderDetails);
}

function init() {
    emenu.webdb.open();
    
    if (isOnLine()) {
        emenu.webdb.dropRoomTable();
        emenu.webdb.createRoomTable(function () {
            $.ajax({
                url: "/Emenu/GetRooms",
                type: "GET",
                success: function (result) {
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        emenu.webdb.addRoom(item.RoomId, item.RoomName, item.RoomStatus, item.FloorId, item.FloorName);
                    }
                    emenu.webdb.getAllFloors(renderFloors);
                }
            });
        });

        emenu.webdb.dropProductTable();
        emenu.webdb.createProductTable(function () {
            $.ajax({
                url: "/Emenu/GetProducts",
                type: "GET",
                success: function (result) {
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        emenu.webdb.addProduct(item.ProductId, item.ProductName, item.Price, item.PicURL);
                    }
                }
            });
        });

        emenu.webdb.dropOrderDetailTable();
        emenu.webdb.createOrderDetailTable(function () {
            $.ajax({
                url: "/Emenu/GetOrderDetails",
                type: "GET",
                success: function (result) {
                    for (var counter = 0; counter < result.length; counter++) {
                        var item = result[counter];
                        emenu.webdb.addOrderDetail(item.OrderId, item.ProductId, item.RoomId, item.Quantity,item.Price,item.OrderDate,true,false);
                    }
                }
            });
        });
      
    } else {
        
    }
    
    
    //init room selection

    
    
}

window.addEventListener("online", function (e) {
    ShowMessage("online",3);
}, true);

window.addEventListener("offline", function (e) {
    ShowMessage("offline",3);
    location = "/Content/Emenu/html/index.html";
}, true);

function isOnLine() {
    return navigator.onLine;
}

function updateSelectedProduct(id,addition) {
    var elements = $(".item.one-third.column");
    for (var i=0;i<elements.length;i++) {
        if ($(elements[i]).data("id") == id) {
            var currentQuantity = parseInt($(elements[i]).find(".order-quan").html());
            currentQuantity = isNaN(currentQuantity) ? 0 : currentQuantity;

            currentQuantity += addition;

            currentQuantity = currentQuantity == -1 ? 0 : currentQuantity;
            
            if (currentQuantity == 0) {
                $(elements[i]).find(".overlay-quan").removeClass("selected");
                $(elements[i]).find(".order-quan").html(0);
                return;
            }
            
            $(elements[i]).find(".overlay-quan").addClass("selected");
            $(elements[i]).find(".order-quan").html(currentQuantity);
            return;
        }
    }
}

function saveSelectedProducts() {
    var elements = $(".item.one-third.column").find(".overlay-quan.selected").closest(".item.one-third.column");
    for (var i = 0; i < elements.length; i++) {
        var id = $(elements[i]).data("id");
        var price = $(elements[i]).data("price");
        var quantity = parseInt($(elements[i]).find(".order-quan").html());
        $(elements[i]).find(".order-quan").html(0);
        quantity = isNaN(quantity) ? 1 : quantity;
        var date = new Date();
        emenu.webdb.addOrderDetail(Date.now(), id, emenu.info.CurrentRoomId, quantity, price, date.toJSON(), false, true);
    }

    elements.find(".overlay-quan").removeClass("selected");
    
}